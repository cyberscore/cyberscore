<?php

set_include_path(__DIR__ . "/..");
require_once('includes/startup.php');
require_once('includes/router.php');
require_once('src/router.php');

$matched_route = Router::instance()->match(
  $_SERVER['REQUEST_METHOD'],
  $_SERVER['REQUEST_URI']
);

if ($matched_route) {
  extract_request_params($matched_route[1]);

  foreach ($matched_route[2] as $mw) {
    ($mw)();
  }

  $matched_route[0]->run($matched_route[1]);

  // TODO: run after hooks here (middlewares)
} else {
  http_response_code(404);
  render_with("home/404", ['page_title' => "Page not found"]);

  $timer->finish();
  log_event('page_request', ['execution_time' => $timer->delta()]);
}
