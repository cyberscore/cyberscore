﻿function popup(windowname) {
  document.getElementById(windowname).classList.toggle('popup--closed');
}

function closePopup(windowname) {
  document.getElementById(windowname).classList.add('popup--closed');
}
