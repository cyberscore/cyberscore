/* tslint:disable */
/* eslint-disable */
/**
* @param {Uint8Array} proof
* @param {string} game
* @returns {ProofResult}
*/
export function ocr(proof: Uint8Array, game: string): ProofResult;
/**
*/
export class CoinsProof {
  free(): void;
/**
*/
  coins: number;
/**
*/
  stage: string;
}
/**
*/
export class CoverProof {
  free(): void;
/**
*/
  crown_tundra_caught: number;
/**
*/
  crown_tundra_seen: number;
/**
*/
  galar_caught: number;
/**
*/
  galar_seen: number;
/**
*/
  isle_of_armor_caught: number;
/**
*/
  isle_of_armor_seen: number;
}
/**
*/
export class IndexProof {
  free(): void;
/**
*/
  pokemon_obtained: number;
/**
*/
  pokemon_seen: number;
/**
*/
  total_pokemon_caught: number;
}
/**
*/
export class InfoProof {
  free(): void;
/**
*/
  dex: number;
/**
*/
  height_max: number;
/**
*/
  height_min: number;
/**
*/
  weight_max: number;
/**
*/
  weight_min: number;
}
/**
*/
export class KFLProof {
  free(): void;
/**
*/
  coins?: CoinsProof;
}
/**
*/
export class LeagueProof {
  free(): void;
/**
*/
  best_rally_score: number;
/**
*/
  curry_dex: number;
/**
*/
  pokemon_caught: number;
/**
*/
  shiny_pokemon_found: number;
}
/**
*/
export class LostAndFoundProof {
  free(): void;
/**
*/
  retrieved_dropped_items: number;
/**
*/
  total_mp: number;
}
/**
*/
export class NPSProof {
  free(): void;
/**
*/
  photo?: PhotoProof;
/**
*/
  photodex?: PhotoDexProof;
}
/**
*/
export class OCR {
  free(): void;
/**
* @param {string} game
*/
  constructor(game: string);
/**
* @param {Uint8Array} proof
* @returns {ProofResult}
*/
  ocr(proof: Uint8Array): ProofResult;
}
/**
*/
export class PLAProof {
  free(): void;
/**
*/
  info?: InfoProof;
/**
*/
  lost_and_found?: LostAndFoundProof;
/**
*/
  pokedex_cover?: PokedexCoverProof;
/**
*/
  research?: ResearchTaskProof;
}
/**
*/
export class PLGProof {
  free(): void;
/**
*/
  index?: IndexProof;
/**
*/
  park?: ParkProof;
/**
*/
  pokedex?: PokedexProof;
}
/**
*/
export class POGOProof {
  free(): void;
/**
*/
  pokedex_entry?: PokedexEntryProof;
}
/**
*/
export class PS4Proof {
  free(): void;
/**
*/
  score?: ScoreProof;
}
/**
*/
export class PSWSHPokedexProof {
  free(): void;
/**
*/
  dex: number;
/**
*/
  number_battled: number;
/**
*/
  region: string;
}
/**
*/
export class PSWSHProof {
  free(): void;
/**
*/
  cover?: CoverProof;
/**
*/
  league?: LeagueProof;
/**
*/
  pokedex?: PSWSHPokedexProof;
}
/**
*/
export class PScaVioProof {
  free(): void;
/**
*/
  profile?: ProfileProof;
/**
*/
  sandwich?: SandwichProof;
}
/**
*/
export class ParkProof {
  free(): void;
/**
*/
  dex: Uint32Array;
/**
*/
  points: Uint32Array;
/**
*/
  time: Float64Array;
}
/**
*/
export class PhotoDexProof {
  free(): void;
/**
*/
  dex: number;
/**
*/
  score: number;
}
/**
*/
export class PhotoProof {
  free(): void;
/**
*/
  dex: number;
/**
*/
  score: number;
/**
*/
  stars: number;
}
/**
*/
export class PokedexCoverProof {
  free(): void;
/**
*/
  coastlands_caught?: number;
/**
*/
  coastlands_seen?: number;
/**
*/
  fieldlands_caught?: number;
/**
*/
  fieldlands_seen?: number;
/**
*/
  highlands_caught?: number;
/**
*/
  highlands_seen?: number;
/**
*/
  icelands_caught?: number;
/**
*/
  icelands_seen?: number;
/**
*/
  mirelands_caught?: number;
/**
*/
  mirelands_seen?: number;
/**
*/
  research_points: number;
/**
*/
  total_caught: number;
/**
*/
  total_seen: number;
}
/**
*/
export class PokedexEntryProof {
  free(): void;
/**
*/
  caught?: number;
/**
*/
  dex: number;
/**
*/
  height_max?: number;
/**
*/
  height_min?: number;
/**
*/
  lucky?: number;
/**
*/
  purified?: number;
/**
*/
  seen?: number;
/**
*/
  variant?: string;
/**
*/
  weight_max?: number;
/**
*/
  weight_min?: number;
}
/**
*/
export class PokedexProof {
  free(): void;
/**
*/
  alola: boolean;
/**
*/
  dex: number;
/**
*/
  height_max: number;
/**
*/
  height_min: number;
/**
*/
  metric: boolean;
/**
*/
  number_caught: number;
/**
*/
  weight_max: number;
/**
*/
  weight_min: number;
}
/**
*/
export class ProfileProof {
  free(): void;
/**
*/
  badges_collected: number;
/**
*/
  pokedex_caught: number;
/**
*/
  pokedex_seen: number;
/**
*/
  recipes_collected: number;
/**
*/
  shiny_pokemon_battled: number;
}
/**
*/
export class Proof {
  free(): void;
/**
*/
  height: number;
/**
*/
  kirby_forgotten_land?: KFLProof;
/**
*/
  new_pokemon_snap?: NPSProof;
/**
*/
  picross_s4?: PS4Proof;
/**
*/
  pokemon_go?: POGOProof;
/**
*/
  pokemon_legends_arceus?: PLAProof;
/**
*/
  pokemon_lets_go?: PLGProof;
/**
*/
  pokemon_scarlet_violet?: PScaVioProof;
/**
*/
  pokemon_sword_shield?: PSWSHProof;
/**
*/
  width: number;
}
/**
*/
export class ProofResult {
  free(): void;
/**
* @returns {Proof | undefined}
*/
  value(): Proof | undefined;
/**
* @returns {number}
*/
  log_count(): number;
/**
* @param {number} index
* @returns {string}
*/
  log_entry_message(index: number): string;
/**
* @param {number} index
* @returns {boolean}
*/
  log_entry_bbox_exists(index: number): boolean;
/**
* @param {number} index
* @returns {number | undefined}
*/
  log_entry_bbox_x(index: number): number | undefined;
/**
* @param {number} index
* @returns {number | undefined}
*/
  log_entry_bbox_y(index: number): number | undefined;
/**
* @param {number} index
* @returns {number | undefined}
*/
  log_entry_bbox_w(index: number): number | undefined;
/**
* @param {number} index
* @returns {number | undefined}
*/
  log_entry_bbox_h(index: number): number | undefined;
}
/**
*/
export class ResearchTaskProof {
  free(): void;
/**
*/
  dex: number;
/**
*/
  research_level: number;
/**
*/
  tasks: Uint32Array;
}
/**
*/
export class SandwichProof {
  free(): void;
/**
*/
  name: string;
/**
*/
  times_made: number;
}
/**
*/
export class ScoreProof {
  free(): void;
/**
*/
  level: string;
/**
*/
  seconds: number;
/**
*/
  section: string;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_plgproof_free: (a: number) => void;
  readonly __wbg_get_plgproof_index: (a: number) => number;
  readonly __wbg_set_plgproof_index: (a: number, b: number) => void;
  readonly __wbg_get_plgproof_pokedex: (a: number) => number;
  readonly __wbg_set_plgproof_pokedex: (a: number, b: number) => void;
  readonly __wbg_get_plgproof_park: (a: number) => number;
  readonly __wbg_set_plgproof_park: (a: number, b: number) => void;
  readonly __wbg_indexproof_free: (a: number) => void;
  readonly __wbg_get_indexproof_pokemon_obtained: (a: number) => number;
  readonly __wbg_set_indexproof_pokemon_obtained: (a: number, b: number) => void;
  readonly __wbg_get_indexproof_pokemon_seen: (a: number) => number;
  readonly __wbg_set_indexproof_pokemon_seen: (a: number, b: number) => void;
  readonly __wbg_get_indexproof_total_pokemon_caught: (a: number) => number;
  readonly __wbg_set_indexproof_total_pokemon_caught: (a: number, b: number) => void;
  readonly __wbg_pokedexproof_free: (a: number) => void;
  readonly __wbg_get_pokedexproof_dex: (a: number) => number;
  readonly __wbg_set_pokedexproof_dex: (a: number, b: number) => void;
  readonly __wbg_get_pokedexproof_alola: (a: number) => number;
  readonly __wbg_set_pokedexproof_alola: (a: number, b: number) => void;
  readonly __wbg_get_pokedexproof_number_caught: (a: number) => number;
  readonly __wbg_set_pokedexproof_number_caught: (a: number, b: number) => void;
  readonly __wbg_get_pokedexproof_metric: (a: number) => number;
  readonly __wbg_set_pokedexproof_metric: (a: number, b: number) => void;
  readonly __wbg_get_pokedexproof_weight_min: (a: number) => number;
  readonly __wbg_set_pokedexproof_weight_min: (a: number, b: number) => void;
  readonly __wbg_get_pokedexproof_weight_max: (a: number) => number;
  readonly __wbg_set_pokedexproof_weight_max: (a: number, b: number) => void;
  readonly __wbg_get_pokedexproof_height_min: (a: number) => number;
  readonly __wbg_set_pokedexproof_height_min: (a: number, b: number) => void;
  readonly __wbg_get_pokedexproof_height_max: (a: number) => number;
  readonly __wbg_set_pokedexproof_height_max: (a: number, b: number) => void;
  readonly __wbg_parkproof_free: (a: number) => void;
  readonly __wbg_get_parkproof_dex: (a: number, b: number) => void;
  readonly __wbg_set_parkproof_dex: (a: number, b: number, c: number) => void;
  readonly __wbg_get_parkproof_points: (a: number, b: number) => void;
  readonly __wbg_set_parkproof_points: (a: number, b: number, c: number) => void;
  readonly __wbg_get_parkproof_time: (a: number, b: number) => void;
  readonly __wbg_set_parkproof_time: (a: number, b: number, c: number) => void;
  readonly __wbg_npsproof_free: (a: number) => void;
  readonly __wbg_get_npsproof_photo: (a: number) => number;
  readonly __wbg_set_npsproof_photo: (a: number, b: number) => void;
  readonly __wbg_get_npsproof_photodex: (a: number) => number;
  readonly __wbg_set_npsproof_photodex: (a: number, b: number) => void;
  readonly __wbg_photoproof_free: (a: number) => void;
  readonly __wbg_get_photoproof_score: (a: number) => number;
  readonly __wbg_set_photoproof_score: (a: number, b: number) => void;
  readonly __wbg_photodexproof_free: (a: number) => void;
  readonly __wbg_get_photodexproof_dex: (a: number) => number;
  readonly __wbg_set_photodexproof_dex: (a: number, b: number) => void;
  readonly __wbg_get_photodexproof_score: (a: number) => number;
  readonly __wbg_set_photodexproof_score: (a: number, b: number) => void;
  readonly __wbg_get_photoproof_dex: (a: number) => number;
  readonly __wbg_get_photoproof_stars: (a: number) => number;
  readonly __wbg_set_photoproof_dex: (a: number, b: number) => void;
  readonly __wbg_set_photoproof_stars: (a: number, b: number) => void;
  readonly __wbg_pswshproof_free: (a: number) => void;
  readonly __wbg_get_pswshproof_league: (a: number) => number;
  readonly __wbg_set_pswshproof_league: (a: number, b: number) => void;
  readonly __wbg_get_pswshproof_pokedex: (a: number) => number;
  readonly __wbg_set_pswshproof_pokedex: (a: number, b: number) => void;
  readonly __wbg_get_pswshproof_cover: (a: number) => number;
  readonly __wbg_set_pswshproof_cover: (a: number, b: number) => void;
  readonly __wbg_leagueproof_free: (a: number) => void;
  readonly __wbg_pswshpokedexproof_free: (a: number) => void;
  readonly __wbg_get_pswshpokedexproof_region: (a: number, b: number) => void;
  readonly __wbg_set_pswshpokedexproof_region: (a: number, b: number, c: number) => void;
  readonly __wbg_coverproof_free: (a: number) => void;
  readonly __wbg_get_coverproof_galar_caught: (a: number) => number;
  readonly __wbg_set_coverproof_galar_caught: (a: number, b: number) => void;
  readonly __wbg_get_coverproof_galar_seen: (a: number) => number;
  readonly __wbg_set_coverproof_galar_seen: (a: number, b: number) => void;
  readonly __wbg_get_coverproof_isle_of_armor_caught: (a: number) => number;
  readonly __wbg_set_coverproof_isle_of_armor_caught: (a: number, b: number) => void;
  readonly __wbg_get_coverproof_isle_of_armor_seen: (a: number) => number;
  readonly __wbg_set_coverproof_isle_of_armor_seen: (a: number, b: number) => void;
  readonly __wbg_get_coverproof_crown_tundra_caught: (a: number) => number;
  readonly __wbg_set_coverproof_crown_tundra_caught: (a: number, b: number) => void;
  readonly __wbg_get_coverproof_crown_tundra_seen: (a: number) => number;
  readonly __wbg_set_coverproof_crown_tundra_seen: (a: number, b: number) => void;
  readonly __wbg_get_pswshpokedexproof_dex: (a: number) => number;
  readonly __wbg_get_leagueproof_curry_dex: (a: number) => number;
  readonly __wbg_get_leagueproof_best_rally_score: (a: number) => number;
  readonly __wbg_get_leagueproof_pokemon_caught: (a: number) => number;
  readonly __wbg_get_leagueproof_shiny_pokemon_found: (a: number) => number;
  readonly __wbg_get_pswshpokedexproof_number_battled: (a: number) => number;
  readonly __wbg_set_pswshpokedexproof_dex: (a: number, b: number) => void;
  readonly __wbg_set_leagueproof_curry_dex: (a: number, b: number) => void;
  readonly __wbg_set_leagueproof_best_rally_score: (a: number, b: number) => void;
  readonly __wbg_set_leagueproof_pokemon_caught: (a: number, b: number) => void;
  readonly __wbg_set_leagueproof_shiny_pokemon_found: (a: number, b: number) => void;
  readonly __wbg_set_pswshpokedexproof_number_battled: (a: number, b: number) => void;
  readonly __wbg_pscavioproof_free: (a: number) => void;
  readonly __wbg_get_pscavioproof_profile: (a: number) => number;
  readonly __wbg_set_pscavioproof_profile: (a: number, b: number) => void;
  readonly __wbg_get_pscavioproof_sandwich: (a: number) => number;
  readonly __wbg_set_pscavioproof_sandwich: (a: number, b: number) => void;
  readonly __wbg_profileproof_free: (a: number) => void;
  readonly __wbg_get_profileproof_badges_collected: (a: number) => number;
  readonly __wbg_set_profileproof_badges_collected: (a: number, b: number) => void;
  readonly __wbg_get_profileproof_pokedex_seen: (a: number) => number;
  readonly __wbg_set_profileproof_pokedex_seen: (a: number, b: number) => void;
  readonly __wbg_get_profileproof_pokedex_caught: (a: number) => number;
  readonly __wbg_set_profileproof_pokedex_caught: (a: number, b: number) => void;
  readonly __wbg_get_profileproof_shiny_pokemon_battled: (a: number) => number;
  readonly __wbg_set_profileproof_shiny_pokemon_battled: (a: number, b: number) => void;
  readonly __wbg_get_profileproof_recipes_collected: (a: number) => number;
  readonly __wbg_set_profileproof_recipes_collected: (a: number, b: number) => void;
  readonly __wbg_sandwichproof_free: (a: number) => void;
  readonly __wbg_get_sandwichproof_name: (a: number, b: number) => void;
  readonly __wbg_set_sandwichproof_name: (a: number, b: number, c: number) => void;
  readonly __wbg_proof_free: (a: number) => void;
  readonly __wbg_get_proof_pokemon_legends_arceus: (a: number) => number;
  readonly __wbg_set_proof_pokemon_legends_arceus: (a: number, b: number) => void;
  readonly __wbg_get_proof_pokemon_lets_go: (a: number) => number;
  readonly __wbg_set_proof_pokemon_lets_go: (a: number, b: number) => void;
  readonly __wbg_get_proof_pokemon_sword_shield: (a: number) => number;
  readonly __wbg_set_proof_pokemon_sword_shield: (a: number, b: number) => void;
  readonly __wbg_get_proof_picross_s4: (a: number) => number;
  readonly __wbg_set_proof_picross_s4: (a: number, b: number) => void;
  readonly __wbg_get_proof_new_pokemon_snap: (a: number) => number;
  readonly __wbg_set_proof_new_pokemon_snap: (a: number, b: number) => void;
  readonly __wbg_get_proof_pokemon_go: (a: number) => number;
  readonly __wbg_set_proof_pokemon_go: (a: number, b: number) => void;
  readonly __wbg_get_proof_pokemon_scarlet_violet: (a: number) => number;
  readonly __wbg_set_proof_pokemon_scarlet_violet: (a: number, b: number) => void;
  readonly __wbg_get_proof_kirby_forgotten_land: (a: number) => number;
  readonly __wbg_set_proof_kirby_forgotten_land: (a: number, b: number) => void;
  readonly __wbg_get_proof_width: (a: number) => number;
  readonly __wbg_set_proof_width: (a: number, b: number) => void;
  readonly __wbg_get_proof_height: (a: number) => number;
  readonly __wbg_set_proof_height: (a: number, b: number) => void;
  readonly ocr: (a: number, b: number, c: number, d: number) => number;
  readonly __wbg_ocr_free: (a: number) => void;
  readonly __wbg_proofresult_free: (a: number) => void;
  readonly proofresult_value: (a: number) => number;
  readonly proofresult_log_count: (a: number) => number;
  readonly proofresult_log_entry_message: (a: number, b: number, c: number) => void;
  readonly proofresult_log_entry_bbox_exists: (a: number, b: number) => number;
  readonly proofresult_log_entry_bbox_x: (a: number, b: number, c: number) => void;
  readonly proofresult_log_entry_bbox_y: (a: number, b: number, c: number) => void;
  readonly proofresult_log_entry_bbox_w: (a: number, b: number, c: number) => void;
  readonly proofresult_log_entry_bbox_h: (a: number, b: number, c: number) => void;
  readonly ocr_new: (a: number, b: number) => number;
  readonly ocr_ocr: (a: number, b: number, c: number) => number;
  readonly __wbg_get_sandwichproof_times_made: (a: number) => number;
  readonly __wbg_set_sandwichproof_times_made: (a: number, b: number) => void;
  readonly __wbg_ps4proof_free: (a: number) => void;
  readonly __wbg_get_ps4proof_score: (a: number) => number;
  readonly __wbg_set_ps4proof_score: (a: number, b: number) => void;
  readonly __wbg_scoreproof_free: (a: number) => void;
  readonly __wbg_get_scoreproof_section: (a: number, b: number) => void;
  readonly __wbg_set_scoreproof_section: (a: number, b: number, c: number) => void;
  readonly __wbg_get_scoreproof_level: (a: number, b: number) => void;
  readonly __wbg_set_scoreproof_level: (a: number, b: number, c: number) => void;
  readonly __wbg_get_scoreproof_seconds: (a: number) => number;
  readonly __wbg_set_scoreproof_seconds: (a: number, b: number) => void;
  readonly __wbg_plaproof_free: (a: number) => void;
  readonly __wbg_get_plaproof_research: (a: number) => number;
  readonly __wbg_set_plaproof_research: (a: number, b: number) => void;
  readonly __wbg_get_plaproof_info: (a: number) => number;
  readonly __wbg_set_plaproof_info: (a: number, b: number) => void;
  readonly __wbg_get_plaproof_lost_and_found: (a: number) => number;
  readonly __wbg_set_plaproof_lost_and_found: (a: number, b: number) => void;
  readonly __wbg_get_plaproof_pokedex_cover: (a: number) => number;
  readonly __wbg_set_plaproof_pokedex_cover: (a: number, b: number) => void;
  readonly __wbg_researchtaskproof_free: (a: number) => void;
  readonly __wbg_get_researchtaskproof_tasks: (a: number, b: number) => void;
  readonly __wbg_set_researchtaskproof_tasks: (a: number, b: number, c: number) => void;
  readonly __wbg_infoproof_free: (a: number) => void;
  readonly __wbg_get_infoproof_dex: (a: number) => number;
  readonly __wbg_set_infoproof_dex: (a: number, b: number) => void;
  readonly __wbg_get_infoproof_weight_min: (a: number) => number;
  readonly __wbg_set_infoproof_weight_min: (a: number, b: number) => void;
  readonly __wbg_get_infoproof_weight_max: (a: number) => number;
  readonly __wbg_set_infoproof_weight_max: (a: number, b: number) => void;
  readonly __wbg_get_infoproof_height_min: (a: number) => number;
  readonly __wbg_set_infoproof_height_min: (a: number, b: number) => void;
  readonly __wbg_get_infoproof_height_max: (a: number) => number;
  readonly __wbg_set_infoproof_height_max: (a: number, b: number) => void;
  readonly __wbg_lostandfoundproof_free: (a: number) => void;
  readonly __wbg_pokedexcoverproof_free: (a: number) => void;
  readonly __wbg_get_pokedexcoverproof_total_seen: (a: number) => number;
  readonly __wbg_set_pokedexcoverproof_total_seen: (a: number, b: number) => void;
  readonly __wbg_get_pokedexcoverproof_total_caught: (a: number) => number;
  readonly __wbg_set_pokedexcoverproof_total_caught: (a: number, b: number) => void;
  readonly __wbg_get_pokedexcoverproof_fieldlands_seen: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_fieldlands_seen: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_fieldlands_caught: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_fieldlands_caught: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_mirelands_seen: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_mirelands_seen: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_mirelands_caught: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_mirelands_caught: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_coastlands_seen: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_coastlands_seen: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_coastlands_caught: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_coastlands_caught: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_highlands_seen: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_highlands_seen: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_highlands_caught: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_highlands_caught: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_icelands_seen: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_icelands_seen: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_icelands_caught: (a: number, b: number) => void;
  readonly __wbg_set_pokedexcoverproof_icelands_caught: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexcoverproof_research_points: (a: number) => number;
  readonly __wbg_set_pokedexcoverproof_research_points: (a: number, b: number) => void;
  readonly __wbg_get_researchtaskproof_dex: (a: number) => number;
  readonly __wbg_get_researchtaskproof_research_level: (a: number) => number;
  readonly __wbg_get_lostandfoundproof_total_mp: (a: number) => number;
  readonly __wbg_get_lostandfoundproof_retrieved_dropped_items: (a: number) => number;
  readonly __wbg_set_researchtaskproof_dex: (a: number, b: number) => void;
  readonly __wbg_set_researchtaskproof_research_level: (a: number, b: number) => void;
  readonly __wbg_set_lostandfoundproof_total_mp: (a: number, b: number) => void;
  readonly __wbg_set_lostandfoundproof_retrieved_dropped_items: (a: number, b: number) => void;
  readonly __wbg_pogoproof_free: (a: number) => void;
  readonly __wbg_get_pogoproof_pokedex_entry: (a: number) => number;
  readonly __wbg_set_pogoproof_pokedex_entry: (a: number, b: number) => void;
  readonly __wbg_pokedexentryproof_free: (a: number) => void;
  readonly __wbg_get_pokedexentryproof_dex: (a: number) => number;
  readonly __wbg_set_pokedexentryproof_dex: (a: number, b: number) => void;
  readonly __wbg_get_pokedexentryproof_variant: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_variant: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_seen: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_seen: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_caught: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_caught: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_lucky: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_lucky: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_purified: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_purified: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_weight_min: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_weight_min: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_weight_max: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_weight_max: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_height_min: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_height_min: (a: number, b: number, c: number) => void;
  readonly __wbg_get_pokedexentryproof_height_max: (a: number, b: number) => void;
  readonly __wbg_set_pokedexentryproof_height_max: (a: number, b: number, c: number) => void;
  readonly __wbg_kflproof_free: (a: number) => void;
  readonly __wbg_get_kflproof_coins: (a: number) => number;
  readonly __wbg_set_kflproof_coins: (a: number, b: number) => void;
  readonly __wbg_coinsproof_free: (a: number) => void;
  readonly __wbg_get_coinsproof_stage: (a: number, b: number) => void;
  readonly __wbg_set_coinsproof_stage: (a: number, b: number, c: number) => void;
  readonly __wbg_get_coinsproof_coins: (a: number) => number;
  readonly __wbg_set_coinsproof_coins: (a: number, b: number) => void;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
}

/**
* Synchronously compiles the given `bytes` and instantiates the WebAssembly module.
*
* @param {BufferSource} bytes
*
* @returns {InitOutput}
*/
export function initSync(bytes: BufferSource): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
