
let wasm;

const heap = new Array(32).fill(undefined);

heap.push(undefined, null, true, false);

function getObject(idx) { return heap[idx]; }

let heap_next = heap.length;

function dropObject(idx) {
    if (idx < 36) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}

const cachedTextDecoder = new TextDecoder('utf-8', { ignoreBOM: true, fatal: true });

cachedTextDecoder.decode();

let cachedUint8Memory0 = new Uint8Array();

function getUint8Memory0() {
    if (cachedUint8Memory0.byteLength === 0) {
        cachedUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachedUint8Memory0;
}

function getStringFromWasm0(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory0().subarray(ptr, ptr + len));
}

function isLikeNone(x) {
    return x === undefined || x === null;
}

function _assertClass(instance, klass) {
    if (!(instance instanceof klass)) {
        throw new Error(`expected instance of ${klass.name}`);
    }
    return instance.ptr;
}

let cachedInt32Memory0 = new Int32Array();

function getInt32Memory0() {
    if (cachedInt32Memory0.byteLength === 0) {
        cachedInt32Memory0 = new Int32Array(wasm.memory.buffer);
    }
    return cachedInt32Memory0;
}

let cachedUint32Memory0 = new Uint32Array();

function getUint32Memory0() {
    if (cachedUint32Memory0.byteLength === 0) {
        cachedUint32Memory0 = new Uint32Array(wasm.memory.buffer);
    }
    return cachedUint32Memory0;
}

function getArrayU32FromWasm0(ptr, len) {
    return getUint32Memory0().subarray(ptr / 4, ptr / 4 + len);
}

let WASM_VECTOR_LEN = 0;

function passArray32ToWasm0(arg, malloc) {
    const ptr = malloc(arg.length * 4);
    getUint32Memory0().set(arg, ptr / 4);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}

let cachedFloat64Memory0 = new Float64Array();

function getFloat64Memory0() {
    if (cachedFloat64Memory0.byteLength === 0) {
        cachedFloat64Memory0 = new Float64Array(wasm.memory.buffer);
    }
    return cachedFloat64Memory0;
}

function getArrayF64FromWasm0(ptr, len) {
    return getFloat64Memory0().subarray(ptr / 8, ptr / 8 + len);
}

function passArrayF64ToWasm0(arg, malloc) {
    const ptr = malloc(arg.length * 8);
    getFloat64Memory0().set(arg, ptr / 8);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}

const cachedTextEncoder = new TextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length);
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len);

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

function passArray8ToWasm0(arg, malloc) {
    const ptr = malloc(arg.length * 1);
    getUint8Memory0().set(arg, ptr / 1);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}
/**
* @param {Uint8Array} proof
* @param {string} game
* @returns {ProofResult}
*/
export function ocr(proof, game) {
    const ptr0 = passArray8ToWasm0(proof, wasm.__wbindgen_malloc);
    const len0 = WASM_VECTOR_LEN;
    const ptr1 = passStringToWasm0(game, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
    const len1 = WASM_VECTOR_LEN;
    const ret = wasm.ocr(ptr0, len0, ptr1, len1);
    return ProofResult.__wrap(ret);
}

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}
/**
*/
export class CoinsProof {

    static __wrap(ptr) {
        const obj = Object.create(CoinsProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_coinsproof_free(ptr);
    }
    /**
    * @returns {string}
    */
    get stage() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_coinsproof_stage(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
    /**
    * @param {string} arg0
    */
    set stage(arg0) {
        const ptr0 = passStringToWasm0(arg0, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_coinsproof_stage(this.ptr, ptr0, len0);
    }
    /**
    * @returns {number}
    */
    get coins() {
        const ret = wasm.__wbg_get_coinsproof_coins(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set coins(arg0) {
        wasm.__wbg_set_coinsproof_coins(this.ptr, arg0);
    }
}
/**
*/
export class CoverProof {

    static __wrap(ptr) {
        const obj = Object.create(CoverProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_coverproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get galar_caught() {
        const ret = wasm.__wbg_get_coverproof_galar_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set galar_caught(arg0) {
        wasm.__wbg_set_coverproof_galar_caught(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get galar_seen() {
        const ret = wasm.__wbg_get_coverproof_galar_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set galar_seen(arg0) {
        wasm.__wbg_set_coverproof_galar_seen(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get isle_of_armor_caught() {
        const ret = wasm.__wbg_get_coverproof_isle_of_armor_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set isle_of_armor_caught(arg0) {
        wasm.__wbg_set_coverproof_isle_of_armor_caught(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get isle_of_armor_seen() {
        const ret = wasm.__wbg_get_coverproof_isle_of_armor_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set isle_of_armor_seen(arg0) {
        wasm.__wbg_set_coverproof_isle_of_armor_seen(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get crown_tundra_caught() {
        const ret = wasm.__wbg_get_coverproof_crown_tundra_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set crown_tundra_caught(arg0) {
        wasm.__wbg_set_coverproof_crown_tundra_caught(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get crown_tundra_seen() {
        const ret = wasm.__wbg_get_coverproof_crown_tundra_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set crown_tundra_seen(arg0) {
        wasm.__wbg_set_coverproof_crown_tundra_seen(this.ptr, arg0);
    }
}
/**
*/
export class IndexProof {

    static __wrap(ptr) {
        const obj = Object.create(IndexProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_indexproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get pokemon_obtained() {
        const ret = wasm.__wbg_get_indexproof_pokemon_obtained(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set pokemon_obtained(arg0) {
        wasm.__wbg_set_indexproof_pokemon_obtained(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get pokemon_seen() {
        const ret = wasm.__wbg_get_indexproof_pokemon_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set pokemon_seen(arg0) {
        wasm.__wbg_set_indexproof_pokemon_seen(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get total_pokemon_caught() {
        const ret = wasm.__wbg_get_indexproof_total_pokemon_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set total_pokemon_caught(arg0) {
        wasm.__wbg_set_indexproof_total_pokemon_caught(this.ptr, arg0);
    }
}
/**
*/
export class InfoProof {

    static __wrap(ptr) {
        const obj = Object.create(InfoProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_infoproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get dex() {
        const ret = wasm.__wbg_get_infoproof_dex(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set dex(arg0) {
        wasm.__wbg_set_infoproof_dex(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get weight_min() {
        const ret = wasm.__wbg_get_infoproof_weight_min(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set weight_min(arg0) {
        wasm.__wbg_set_infoproof_weight_min(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get weight_max() {
        const ret = wasm.__wbg_get_infoproof_weight_max(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set weight_max(arg0) {
        wasm.__wbg_set_infoproof_weight_max(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get height_min() {
        const ret = wasm.__wbg_get_infoproof_height_min(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set height_min(arg0) {
        wasm.__wbg_set_infoproof_height_min(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get height_max() {
        const ret = wasm.__wbg_get_infoproof_height_max(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set height_max(arg0) {
        wasm.__wbg_set_infoproof_height_max(this.ptr, arg0);
    }
}
/**
*/
export class KFLProof {

    static __wrap(ptr) {
        const obj = Object.create(KFLProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_kflproof_free(ptr);
    }
    /**
    * @returns {CoinsProof | undefined}
    */
    get coins() {
        const ret = wasm.__wbg_get_kflproof_coins(this.ptr);
        return ret === 0 ? undefined : CoinsProof.__wrap(ret);
    }
    /**
    * @param {CoinsProof | undefined} arg0
    */
    set coins(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, CoinsProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_kflproof_coins(this.ptr, ptr0);
    }
}
/**
*/
export class LeagueProof {

    static __wrap(ptr) {
        const obj = Object.create(LeagueProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_leagueproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get curry_dex() {
        const ret = wasm.__wbg_get_coverproof_galar_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set curry_dex(arg0) {
        wasm.__wbg_set_coverproof_galar_caught(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get best_rally_score() {
        const ret = wasm.__wbg_get_coverproof_galar_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set best_rally_score(arg0) {
        wasm.__wbg_set_coverproof_galar_seen(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get pokemon_caught() {
        const ret = wasm.__wbg_get_coverproof_isle_of_armor_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set pokemon_caught(arg0) {
        wasm.__wbg_set_coverproof_isle_of_armor_caught(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get shiny_pokemon_found() {
        const ret = wasm.__wbg_get_coverproof_isle_of_armor_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set shiny_pokemon_found(arg0) {
        wasm.__wbg_set_coverproof_isle_of_armor_seen(this.ptr, arg0);
    }
}
/**
*/
export class LostAndFoundProof {

    static __wrap(ptr) {
        const obj = Object.create(LostAndFoundProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_lostandfoundproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get total_mp() {
        const ret = wasm.__wbg_get_infoproof_dex(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set total_mp(arg0) {
        wasm.__wbg_set_infoproof_dex(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get retrieved_dropped_items() {
        const ret = wasm.__wbg_get_infoproof_weight_min(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set retrieved_dropped_items(arg0) {
        wasm.__wbg_set_infoproof_weight_min(this.ptr, arg0);
    }
}
/**
*/
export class NPSProof {

    static __wrap(ptr) {
        const obj = Object.create(NPSProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_npsproof_free(ptr);
    }
    /**
    * @returns {PhotoProof | undefined}
    */
    get photo() {
        const ret = wasm.__wbg_get_npsproof_photo(this.ptr);
        return ret === 0 ? undefined : PhotoProof.__wrap(ret);
    }
    /**
    * @param {PhotoProof | undefined} arg0
    */
    set photo(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PhotoProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_npsproof_photo(this.ptr, ptr0);
    }
    /**
    * @returns {PhotoDexProof | undefined}
    */
    get photodex() {
        const ret = wasm.__wbg_get_npsproof_photodex(this.ptr);
        return ret === 0 ? undefined : PhotoDexProof.__wrap(ret);
    }
    /**
    * @param {PhotoDexProof | undefined} arg0
    */
    set photodex(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PhotoDexProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_npsproof_photodex(this.ptr, ptr0);
    }
}
/**
*/
export class OCR {

    static __wrap(ptr) {
        const obj = Object.create(OCR.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_ocr_free(ptr);
    }
    /**
    * @param {string} game
    */
    constructor(game) {
        const ptr0 = passStringToWasm0(game, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        const ret = wasm.ocr_new(ptr0, len0);
        return OCR.__wrap(ret);
    }
    /**
    * @param {Uint8Array} proof
    * @returns {ProofResult}
    */
    ocr(proof) {
        const ptr0 = passArray8ToWasm0(proof, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        const ret = wasm.ocr_ocr(this.ptr, ptr0, len0);
        return ProofResult.__wrap(ret);
    }
}
/**
*/
export class PLAProof {

    static __wrap(ptr) {
        const obj = Object.create(PLAProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_plaproof_free(ptr);
    }
    /**
    * @returns {ResearchTaskProof | undefined}
    */
    get research() {
        const ret = wasm.__wbg_get_plaproof_research(this.ptr);
        return ret === 0 ? undefined : ResearchTaskProof.__wrap(ret);
    }
    /**
    * @param {ResearchTaskProof | undefined} arg0
    */
    set research(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, ResearchTaskProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_plaproof_research(this.ptr, ptr0);
    }
    /**
    * @returns {InfoProof | undefined}
    */
    get info() {
        const ret = wasm.__wbg_get_plaproof_info(this.ptr);
        return ret === 0 ? undefined : InfoProof.__wrap(ret);
    }
    /**
    * @param {InfoProof | undefined} arg0
    */
    set info(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, InfoProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_plaproof_info(this.ptr, ptr0);
    }
    /**
    * @returns {LostAndFoundProof | undefined}
    */
    get lost_and_found() {
        const ret = wasm.__wbg_get_plaproof_lost_and_found(this.ptr);
        return ret === 0 ? undefined : LostAndFoundProof.__wrap(ret);
    }
    /**
    * @param {LostAndFoundProof | undefined} arg0
    */
    set lost_and_found(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, LostAndFoundProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_plaproof_lost_and_found(this.ptr, ptr0);
    }
    /**
    * @returns {PokedexCoverProof | undefined}
    */
    get pokedex_cover() {
        const ret = wasm.__wbg_get_plaproof_pokedex_cover(this.ptr);
        return ret === 0 ? undefined : PokedexCoverProof.__wrap(ret);
    }
    /**
    * @param {PokedexCoverProof | undefined} arg0
    */
    set pokedex_cover(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PokedexCoverProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_plaproof_pokedex_cover(this.ptr, ptr0);
    }
}
/**
*/
export class PLGProof {

    static __wrap(ptr) {
        const obj = Object.create(PLGProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_plgproof_free(ptr);
    }
    /**
    * @returns {IndexProof | undefined}
    */
    get index() {
        const ret = wasm.__wbg_get_plgproof_index(this.ptr);
        return ret === 0 ? undefined : IndexProof.__wrap(ret);
    }
    /**
    * @param {IndexProof | undefined} arg0
    */
    set index(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, IndexProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_plgproof_index(this.ptr, ptr0);
    }
    /**
    * @returns {PokedexProof | undefined}
    */
    get pokedex() {
        const ret = wasm.__wbg_get_plgproof_pokedex(this.ptr);
        return ret === 0 ? undefined : PokedexProof.__wrap(ret);
    }
    /**
    * @param {PokedexProof | undefined} arg0
    */
    set pokedex(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PokedexProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_plgproof_pokedex(this.ptr, ptr0);
    }
    /**
    * @returns {ParkProof | undefined}
    */
    get park() {
        const ret = wasm.__wbg_get_plgproof_park(this.ptr);
        return ret === 0 ? undefined : ParkProof.__wrap(ret);
    }
    /**
    * @param {ParkProof | undefined} arg0
    */
    set park(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, ParkProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_plgproof_park(this.ptr, ptr0);
    }
}
/**
*/
export class POGOProof {

    static __wrap(ptr) {
        const obj = Object.create(POGOProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pogoproof_free(ptr);
    }
    /**
    * @returns {PokedexEntryProof | undefined}
    */
    get pokedex_entry() {
        const ret = wasm.__wbg_get_pogoproof_pokedex_entry(this.ptr);
        return ret === 0 ? undefined : PokedexEntryProof.__wrap(ret);
    }
    /**
    * @param {PokedexEntryProof | undefined} arg0
    */
    set pokedex_entry(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PokedexEntryProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_pogoproof_pokedex_entry(this.ptr, ptr0);
    }
}
/**
*/
export class PS4Proof {

    static __wrap(ptr) {
        const obj = Object.create(PS4Proof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_ps4proof_free(ptr);
    }
    /**
    * @returns {ScoreProof | undefined}
    */
    get score() {
        const ret = wasm.__wbg_get_ps4proof_score(this.ptr);
        return ret === 0 ? undefined : ScoreProof.__wrap(ret);
    }
    /**
    * @param {ScoreProof | undefined} arg0
    */
    set score(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, ScoreProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_ps4proof_score(this.ptr, ptr0);
    }
}
/**
*/
export class PSWSHPokedexProof {

    static __wrap(ptr) {
        const obj = Object.create(PSWSHPokedexProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pswshpokedexproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get dex() {
        const ret = wasm.__wbg_get_coverproof_isle_of_armor_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set dex(arg0) {
        wasm.__wbg_set_coverproof_isle_of_armor_seen(this.ptr, arg0);
    }
    /**
    * @returns {string}
    */
    get region() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pswshpokedexproof_region(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
    /**
    * @param {string} arg0
    */
    set region(arg0) {
        const ptr0 = passStringToWasm0(arg0, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_pswshpokedexproof_region(this.ptr, ptr0, len0);
    }
    /**
    * @returns {number}
    */
    get number_battled() {
        const ret = wasm.__wbg_get_coverproof_crown_tundra_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set number_battled(arg0) {
        wasm.__wbg_set_coverproof_crown_tundra_caught(this.ptr, arg0);
    }
}
/**
*/
export class PSWSHProof {

    static __wrap(ptr) {
        const obj = Object.create(PSWSHProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pswshproof_free(ptr);
    }
    /**
    * @returns {LeagueProof | undefined}
    */
    get league() {
        const ret = wasm.__wbg_get_pswshproof_league(this.ptr);
        return ret === 0 ? undefined : LeagueProof.__wrap(ret);
    }
    /**
    * @param {LeagueProof | undefined} arg0
    */
    set league(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, LeagueProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_pswshproof_league(this.ptr, ptr0);
    }
    /**
    * @returns {PSWSHPokedexProof | undefined}
    */
    get pokedex() {
        const ret = wasm.__wbg_get_pswshproof_pokedex(this.ptr);
        return ret === 0 ? undefined : PSWSHPokedexProof.__wrap(ret);
    }
    /**
    * @param {PSWSHPokedexProof | undefined} arg0
    */
    set pokedex(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PSWSHPokedexProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_pswshproof_pokedex(this.ptr, ptr0);
    }
    /**
    * @returns {CoverProof | undefined}
    */
    get cover() {
        const ret = wasm.__wbg_get_pswshproof_cover(this.ptr);
        return ret === 0 ? undefined : CoverProof.__wrap(ret);
    }
    /**
    * @param {CoverProof | undefined} arg0
    */
    set cover(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, CoverProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_pswshproof_cover(this.ptr, ptr0);
    }
}
/**
*/
export class PScaVioProof {

    static __wrap(ptr) {
        const obj = Object.create(PScaVioProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pscavioproof_free(ptr);
    }
    /**
    * @returns {ProfileProof | undefined}
    */
    get profile() {
        const ret = wasm.__wbg_get_pscavioproof_profile(this.ptr);
        return ret === 0 ? undefined : ProfileProof.__wrap(ret);
    }
    /**
    * @param {ProfileProof | undefined} arg0
    */
    set profile(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, ProfileProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_pscavioproof_profile(this.ptr, ptr0);
    }
    /**
    * @returns {SandwichProof | undefined}
    */
    get sandwich() {
        const ret = wasm.__wbg_get_pscavioproof_sandwich(this.ptr);
        return ret === 0 ? undefined : SandwichProof.__wrap(ret);
    }
    /**
    * @param {SandwichProof | undefined} arg0
    */
    set sandwich(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, SandwichProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_pscavioproof_sandwich(this.ptr, ptr0);
    }
}
/**
*/
export class ParkProof {

    static __wrap(ptr) {
        const obj = Object.create(ParkProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_parkproof_free(ptr);
    }
    /**
    * @returns {Uint32Array}
    */
    get dex() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_parkproof_dex(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v0 = getArrayU32FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4);
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {Uint32Array} arg0
    */
    set dex(arg0) {
        const ptr0 = passArray32ToWasm0(arg0, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_parkproof_dex(this.ptr, ptr0, len0);
    }
    /**
    * @returns {Uint32Array}
    */
    get points() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_parkproof_points(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v0 = getArrayU32FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4);
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {Uint32Array} arg0
    */
    set points(arg0) {
        const ptr0 = passArray32ToWasm0(arg0, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_parkproof_points(this.ptr, ptr0, len0);
    }
    /**
    * @returns {Float64Array}
    */
    get time() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_parkproof_time(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v0 = getArrayF64FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 8);
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {Float64Array} arg0
    */
    set time(arg0) {
        const ptr0 = passArrayF64ToWasm0(arg0, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_parkproof_time(this.ptr, ptr0, len0);
    }
}
/**
*/
export class PhotoDexProof {

    static __wrap(ptr) {
        const obj = Object.create(PhotoDexProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_photodexproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get dex() {
        const ret = wasm.__wbg_get_photodexproof_dex(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set dex(arg0) {
        wasm.__wbg_set_photodexproof_dex(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get score() {
        const ret = wasm.__wbg_get_photodexproof_score(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set score(arg0) {
        wasm.__wbg_set_photodexproof_score(this.ptr, arg0);
    }
}
/**
*/
export class PhotoProof {

    static __wrap(ptr) {
        const obj = Object.create(PhotoProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_photoproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get dex() {
        const ret = wasm.__wbg_get_photodexproof_dex(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set dex(arg0) {
        wasm.__wbg_set_photodexproof_dex(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get stars() {
        const ret = wasm.__wbg_get_photodexproof_score(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set stars(arg0) {
        wasm.__wbg_set_photodexproof_score(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get score() {
        const ret = wasm.__wbg_get_photoproof_score(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set score(arg0) {
        wasm.__wbg_set_photoproof_score(this.ptr, arg0);
    }
}
/**
*/
export class PokedexCoverProof {

    static __wrap(ptr) {
        const obj = Object.create(PokedexCoverProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pokedexcoverproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get total_seen() {
        const ret = wasm.__wbg_get_pokedexcoverproof_total_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set total_seen(arg0) {
        wasm.__wbg_set_pokedexcoverproof_total_seen(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get total_caught() {
        const ret = wasm.__wbg_get_pokedexcoverproof_total_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set total_caught(arg0) {
        wasm.__wbg_set_pokedexcoverproof_total_caught(this.ptr, arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get fieldlands_seen() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_fieldlands_seen(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set fieldlands_seen(arg0) {
        wasm.__wbg_set_pokedexcoverproof_fieldlands_seen(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get fieldlands_caught() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_fieldlands_caught(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set fieldlands_caught(arg0) {
        wasm.__wbg_set_pokedexcoverproof_fieldlands_caught(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get mirelands_seen() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_mirelands_seen(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set mirelands_seen(arg0) {
        wasm.__wbg_set_pokedexcoverproof_mirelands_seen(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get mirelands_caught() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_mirelands_caught(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set mirelands_caught(arg0) {
        wasm.__wbg_set_pokedexcoverproof_mirelands_caught(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get coastlands_seen() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_coastlands_seen(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set coastlands_seen(arg0) {
        wasm.__wbg_set_pokedexcoverproof_coastlands_seen(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get coastlands_caught() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_coastlands_caught(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set coastlands_caught(arg0) {
        wasm.__wbg_set_pokedexcoverproof_coastlands_caught(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get highlands_seen() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_highlands_seen(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set highlands_seen(arg0) {
        wasm.__wbg_set_pokedexcoverproof_highlands_seen(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get highlands_caught() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_highlands_caught(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set highlands_caught(arg0) {
        wasm.__wbg_set_pokedexcoverproof_highlands_caught(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get icelands_seen() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_icelands_seen(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set icelands_seen(arg0) {
        wasm.__wbg_set_pokedexcoverproof_icelands_seen(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get icelands_caught() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexcoverproof_icelands_caught(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set icelands_caught(arg0) {
        wasm.__wbg_set_pokedexcoverproof_icelands_caught(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number}
    */
    get research_points() {
        const ret = wasm.__wbg_get_pokedexcoverproof_research_points(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set research_points(arg0) {
        wasm.__wbg_set_pokedexcoverproof_research_points(this.ptr, arg0);
    }
}
/**
*/
export class PokedexEntryProof {

    static __wrap(ptr) {
        const obj = Object.create(PokedexEntryProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pokedexentryproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get dex() {
        const ret = wasm.__wbg_get_pokedexentryproof_dex(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set dex(arg0) {
        wasm.__wbg_set_pokedexentryproof_dex(this.ptr, arg0);
    }
    /**
    * @returns {string | undefined}
    */
    get variant() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_variant(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {string | undefined} arg0
    */
    set variant(arg0) {
        var ptr0 = isLikeNone(arg0) ? 0 : passStringToWasm0(arg0, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_pokedexentryproof_variant(this.ptr, ptr0, len0);
    }
    /**
    * @returns {number | undefined}
    */
    get seen() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_seen(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set seen(arg0) {
        wasm.__wbg_set_pokedexentryproof_seen(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get caught() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_caught(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set caught(arg0) {
        wasm.__wbg_set_pokedexentryproof_caught(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get lucky() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_lucky(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set lucky(arg0) {
        wasm.__wbg_set_pokedexentryproof_lucky(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get purified() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_purified(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set purified(arg0) {
        wasm.__wbg_set_pokedexentryproof_purified(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get weight_min() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_weight_min(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set weight_min(arg0) {
        wasm.__wbg_set_pokedexentryproof_weight_min(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get weight_max() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_weight_max(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set weight_max(arg0) {
        wasm.__wbg_set_pokedexentryproof_weight_max(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get height_min() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_height_min(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set height_min(arg0) {
        wasm.__wbg_set_pokedexentryproof_height_min(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
    /**
    * @returns {number | undefined}
    */
    get height_max() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_pokedexentryproof_height_max(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number | undefined} arg0
    */
    set height_max(arg0) {
        wasm.__wbg_set_pokedexentryproof_height_max(this.ptr, !isLikeNone(arg0), isLikeNone(arg0) ? 0 : arg0);
    }
}
/**
*/
export class PokedexProof {

    static __wrap(ptr) {
        const obj = Object.create(PokedexProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_pokedexproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get dex() {
        const ret = wasm.__wbg_get_pokedexproof_dex(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set dex(arg0) {
        wasm.__wbg_set_pokedexproof_dex(this.ptr, arg0);
    }
    /**
    * @returns {boolean}
    */
    get alola() {
        const ret = wasm.__wbg_get_pokedexproof_alola(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set alola(arg0) {
        wasm.__wbg_set_pokedexproof_alola(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get number_caught() {
        const ret = wasm.__wbg_get_pokedexproof_number_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set number_caught(arg0) {
        wasm.__wbg_set_pokedexproof_number_caught(this.ptr, arg0);
    }
    /**
    * @returns {boolean}
    */
    get metric() {
        const ret = wasm.__wbg_get_pokedexproof_metric(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set metric(arg0) {
        wasm.__wbg_set_pokedexproof_metric(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get weight_min() {
        const ret = wasm.__wbg_get_pokedexproof_weight_min(this.ptr);
        return ret;
    }
    /**
    * @param {number} arg0
    */
    set weight_min(arg0) {
        wasm.__wbg_set_pokedexproof_weight_min(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get weight_max() {
        const ret = wasm.__wbg_get_pokedexproof_weight_max(this.ptr);
        return ret;
    }
    /**
    * @param {number} arg0
    */
    set weight_max(arg0) {
        wasm.__wbg_set_pokedexproof_weight_max(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get height_min() {
        const ret = wasm.__wbg_get_pokedexproof_height_min(this.ptr);
        return ret;
    }
    /**
    * @param {number} arg0
    */
    set height_min(arg0) {
        wasm.__wbg_set_pokedexproof_height_min(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get height_max() {
        const ret = wasm.__wbg_get_pokedexproof_height_max(this.ptr);
        return ret;
    }
    /**
    * @param {number} arg0
    */
    set height_max(arg0) {
        wasm.__wbg_set_pokedexproof_height_max(this.ptr, arg0);
    }
}
/**
*/
export class ProfileProof {

    static __wrap(ptr) {
        const obj = Object.create(ProfileProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_profileproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get badges_collected() {
        const ret = wasm.__wbg_get_profileproof_badges_collected(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set badges_collected(arg0) {
        wasm.__wbg_set_profileproof_badges_collected(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get pokedex_seen() {
        const ret = wasm.__wbg_get_profileproof_pokedex_seen(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set pokedex_seen(arg0) {
        wasm.__wbg_set_profileproof_pokedex_seen(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get pokedex_caught() {
        const ret = wasm.__wbg_get_profileproof_pokedex_caught(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set pokedex_caught(arg0) {
        wasm.__wbg_set_profileproof_pokedex_caught(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get shiny_pokemon_battled() {
        const ret = wasm.__wbg_get_profileproof_shiny_pokemon_battled(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set shiny_pokemon_battled(arg0) {
        wasm.__wbg_set_profileproof_shiny_pokemon_battled(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get recipes_collected() {
        const ret = wasm.__wbg_get_profileproof_recipes_collected(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set recipes_collected(arg0) {
        wasm.__wbg_set_profileproof_recipes_collected(this.ptr, arg0);
    }
}
/**
*/
export class Proof {

    static __wrap(ptr) {
        const obj = Object.create(Proof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_proof_free(ptr);
    }
    /**
    * @returns {PLAProof | undefined}
    */
    get pokemon_legends_arceus() {
        const ret = wasm.__wbg_get_proof_pokemon_legends_arceus(this.ptr);
        return ret === 0 ? undefined : PLAProof.__wrap(ret);
    }
    /**
    * @param {PLAProof | undefined} arg0
    */
    set pokemon_legends_arceus(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PLAProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_pokemon_legends_arceus(this.ptr, ptr0);
    }
    /**
    * @returns {PLGProof | undefined}
    */
    get pokemon_lets_go() {
        const ret = wasm.__wbg_get_proof_pokemon_lets_go(this.ptr);
        return ret === 0 ? undefined : PLGProof.__wrap(ret);
    }
    /**
    * @param {PLGProof | undefined} arg0
    */
    set pokemon_lets_go(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PLGProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_pokemon_lets_go(this.ptr, ptr0);
    }
    /**
    * @returns {PSWSHProof | undefined}
    */
    get pokemon_sword_shield() {
        const ret = wasm.__wbg_get_proof_pokemon_sword_shield(this.ptr);
        return ret === 0 ? undefined : PSWSHProof.__wrap(ret);
    }
    /**
    * @param {PSWSHProof | undefined} arg0
    */
    set pokemon_sword_shield(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PSWSHProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_pokemon_sword_shield(this.ptr, ptr0);
    }
    /**
    * @returns {PS4Proof | undefined}
    */
    get picross_s4() {
        const ret = wasm.__wbg_get_proof_picross_s4(this.ptr);
        return ret === 0 ? undefined : PS4Proof.__wrap(ret);
    }
    /**
    * @param {PS4Proof | undefined} arg0
    */
    set picross_s4(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PS4Proof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_picross_s4(this.ptr, ptr0);
    }
    /**
    * @returns {NPSProof | undefined}
    */
    get new_pokemon_snap() {
        const ret = wasm.__wbg_get_proof_new_pokemon_snap(this.ptr);
        return ret === 0 ? undefined : NPSProof.__wrap(ret);
    }
    /**
    * @param {NPSProof | undefined} arg0
    */
    set new_pokemon_snap(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, NPSProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_new_pokemon_snap(this.ptr, ptr0);
    }
    /**
    * @returns {POGOProof | undefined}
    */
    get pokemon_go() {
        const ret = wasm.__wbg_get_proof_pokemon_go(this.ptr);
        return ret === 0 ? undefined : POGOProof.__wrap(ret);
    }
    /**
    * @param {POGOProof | undefined} arg0
    */
    set pokemon_go(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, POGOProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_pokemon_go(this.ptr, ptr0);
    }
    /**
    * @returns {PScaVioProof | undefined}
    */
    get pokemon_scarlet_violet() {
        const ret = wasm.__wbg_get_proof_pokemon_scarlet_violet(this.ptr);
        return ret === 0 ? undefined : PScaVioProof.__wrap(ret);
    }
    /**
    * @param {PScaVioProof | undefined} arg0
    */
    set pokemon_scarlet_violet(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, PScaVioProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_pokemon_scarlet_violet(this.ptr, ptr0);
    }
    /**
    * @returns {KFLProof | undefined}
    */
    get kirby_forgotten_land() {
        const ret = wasm.__wbg_get_proof_kirby_forgotten_land(this.ptr);
        return ret === 0 ? undefined : KFLProof.__wrap(ret);
    }
    /**
    * @param {KFLProof | undefined} arg0
    */
    set kirby_forgotten_land(arg0) {
        let ptr0 = 0;
        if (!isLikeNone(arg0)) {
            _assertClass(arg0, KFLProof);
            ptr0 = arg0.ptr;
            arg0.ptr = 0;
        }
        wasm.__wbg_set_proof_kirby_forgotten_land(this.ptr, ptr0);
    }
    /**
    * @returns {number}
    */
    get width() {
        const ret = wasm.__wbg_get_proof_width(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set width(arg0) {
        wasm.__wbg_set_proof_width(this.ptr, arg0);
    }
    /**
    * @returns {number}
    */
    get height() {
        const ret = wasm.__wbg_get_proof_height(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set height(arg0) {
        wasm.__wbg_set_proof_height(this.ptr, arg0);
    }
}
/**
*/
export class ProofResult {

    static __wrap(ptr) {
        const obj = Object.create(ProofResult.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_proofresult_free(ptr);
    }
    /**
    * @returns {Proof | undefined}
    */
    value() {
        const ret = wasm.proofresult_value(this.ptr);
        return ret === 0 ? undefined : Proof.__wrap(ret);
    }
    /**
    * @returns {number}
    */
    log_count() {
        const ret = wasm.proofresult_log_count(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} index
    * @returns {string}
    */
    log_entry_message(index) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.proofresult_log_entry_message(retptr, this.ptr, index);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
    /**
    * @param {number} index
    * @returns {boolean}
    */
    log_entry_bbox_exists(index) {
        const ret = wasm.proofresult_log_entry_bbox_exists(this.ptr, index);
        return ret !== 0;
    }
    /**
    * @param {number} index
    * @returns {number | undefined}
    */
    log_entry_bbox_x(index) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.proofresult_log_entry_bbox_x(retptr, this.ptr, index);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number} index
    * @returns {number | undefined}
    */
    log_entry_bbox_y(index) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.proofresult_log_entry_bbox_y(retptr, this.ptr, index);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number} index
    * @returns {number | undefined}
    */
    log_entry_bbox_w(index) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.proofresult_log_entry_bbox_w(retptr, this.ptr, index);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {number} index
    * @returns {number | undefined}
    */
    log_entry_bbox_h(index) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.proofresult_log_entry_bbox_h(retptr, this.ptr, index);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return r0 === 0 ? undefined : r1 >>> 0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}
/**
*/
export class ResearchTaskProof {

    static __wrap(ptr) {
        const obj = Object.create(ResearchTaskProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_researchtaskproof_free(ptr);
    }
    /**
    * @returns {number}
    */
    get dex() {
        const ret = wasm.__wbg_get_infoproof_height_min(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set dex(arg0) {
        wasm.__wbg_set_infoproof_height_min(this.ptr, arg0);
    }
    /**
    * @returns {Uint32Array}
    */
    get tasks() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_researchtaskproof_tasks(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v0 = getArrayU32FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 4);
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {Uint32Array} arg0
    */
    set tasks(arg0) {
        const ptr0 = passArray32ToWasm0(arg0, wasm.__wbindgen_malloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_researchtaskproof_tasks(this.ptr, ptr0, len0);
    }
    /**
    * @returns {number}
    */
    get research_level() {
        const ret = wasm.__wbg_get_infoproof_height_max(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set research_level(arg0) {
        wasm.__wbg_set_infoproof_height_max(this.ptr, arg0);
    }
}
/**
*/
export class SandwichProof {

    static __wrap(ptr) {
        const obj = Object.create(SandwichProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_sandwichproof_free(ptr);
    }
    /**
    * @returns {string}
    */
    get name() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_sandwichproof_name(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
    /**
    * @param {string} arg0
    */
    set name(arg0) {
        const ptr0 = passStringToWasm0(arg0, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_sandwichproof_name(this.ptr, ptr0, len0);
    }
    /**
    * @returns {number}
    */
    get times_made() {
        const ret = wasm.__wbg_get_profileproof_shiny_pokemon_battled(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set times_made(arg0) {
        wasm.__wbg_set_profileproof_shiny_pokemon_battled(this.ptr, arg0);
    }
}
/**
*/
export class ScoreProof {

    static __wrap(ptr) {
        const obj = Object.create(ScoreProof.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_scoreproof_free(ptr);
    }
    /**
    * @returns {string}
    */
    get section() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_scoreproof_section(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
    /**
    * @param {string} arg0
    */
    set section(arg0) {
        const ptr0 = passStringToWasm0(arg0, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_scoreproof_section(this.ptr, ptr0, len0);
    }
    /**
    * @returns {string}
    */
    get level() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_scoreproof_level(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
    /**
    * @param {string} arg0
    */
    set level(arg0) {
        const ptr0 = passStringToWasm0(arg0, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        wasm.__wbg_set_scoreproof_level(this.ptr, ptr0, len0);
    }
    /**
    * @returns {number}
    */
    get seconds() {
        const ret = wasm.__wbg_get_scoreproof_seconds(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} arg0
    */
    set seconds(arg0) {
        wasm.__wbg_set_scoreproof_seconds(this.ptr, arg0);
    }
}

async function load(module, imports) {
    if (typeof Response === 'function' && module instanceof Response) {
        if (typeof WebAssembly.instantiateStreaming === 'function') {
            try {
                return await WebAssembly.instantiateStreaming(module, imports);

            } catch (e) {
                if (module.headers.get('Content-Type') != 'application/wasm') {
                    console.warn("`WebAssembly.instantiateStreaming` failed because your server does not serve wasm with `application/wasm` MIME type. Falling back to `WebAssembly.instantiate` which is slower. Original error:\n", e);

                } else {
                    throw e;
                }
            }
        }

        const bytes = await module.arrayBuffer();
        return await WebAssembly.instantiate(bytes, imports);

    } else {
        const instance = await WebAssembly.instantiate(module, imports);

        if (instance instanceof WebAssembly.Instance) {
            return { instance, module };

        } else {
            return instance;
        }
    }
}

function getImports() {
    const imports = {};
    imports.wbg = {};
    imports.wbg.__wbg_new_693216e109162396 = function() {
        const ret = new Error();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_stack_0ddaca5d1abfb52f = function(arg0, arg1) {
        const ret = getObject(arg1).stack;
        const ptr0 = passStringToWasm0(ret, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        const len0 = WASM_VECTOR_LEN;
        getInt32Memory0()[arg0 / 4 + 1] = len0;
        getInt32Memory0()[arg0 / 4 + 0] = ptr0;
    };
    imports.wbg.__wbg_error_09919627ac0992f5 = function(arg0, arg1) {
        try {
            console.error(getStringFromWasm0(arg0, arg1));
        } finally {
            wasm.__wbindgen_free(arg0, arg1);
        }
    };
    imports.wbg.__wbindgen_object_drop_ref = function(arg0) {
        takeObject(arg0);
    };
    imports.wbg.__wbindgen_throw = function(arg0, arg1) {
        throw new Error(getStringFromWasm0(arg0, arg1));
    };

    return imports;
}

function initMemory(imports, maybe_memory) {

}

function finalizeInit(instance, module) {
    wasm = instance.exports;
    init.__wbindgen_wasm_module = module;
    cachedFloat64Memory0 = new Float64Array();
    cachedInt32Memory0 = new Int32Array();
    cachedUint32Memory0 = new Uint32Array();
    cachedUint8Memory0 = new Uint8Array();


    return wasm;
}

function initSync(bytes) {
    const imports = getImports();

    initMemory(imports);

    const module = new WebAssembly.Module(bytes);
    const instance = new WebAssembly.Instance(module, imports);

    return finalizeInit(instance, module);
}

async function init(input) {
    if (typeof input === 'undefined') {
        input = new URL('auto_proofer_bg.wasm', import.meta.url);
    }
    const imports = getImports();

    if (typeof input === 'string' || (typeof Request === 'function' && input instanceof Request) || (typeof URL === 'function' && input instanceof URL)) {
        input = fetch(input);
    }

    initMemory(imports);

    const { instance, module } = await load(await input, imports);

    return finalizeInit(instance, module);
}

export { initSync }
export default init;
