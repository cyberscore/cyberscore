// PRELOAD THE SPINNER AND SET THE PATH VARIABLES UP
ajaxIndicator  = "/skins/" + skin_folder + "/images/ajax16.gif";
defaultRefresh = "/skins/" + skin_folder + "/images/arrow_refresh.png";
	
function refreshDashBox(boxID) {	
	// PATH THE IMAGE
	refreshImg = $(".refreshbox img",boxID.parent());

	// CHANGE TO THE SPINNER
	refreshImg.attr("src",ajaxIndicator);

	parentBox = $("#" + boxID.parent().attr("id"));
	boxNumber = boxID.parent().attr("id").replace(/\D/g,"");

	// SEND THE POST REQUEST TO REFRESH THIS BOX
	$.post("/xhr/dashboard.php",{
		"action": "refresh",
		"box_id": boxNumber
	},function(responseJSON) {
		if (responseJSON.title == "ERROR") {
			// ERROR!
			$("#dialog").html("<p>We're sorry, but there was a problem refreshing the box.</p>").dialog({
				title: "Dashboard error",
				buttons: {
					"OK": function() {
						$(this).dialog("close");
					}
				},
				modal: true,
				overlay: defaultOverlay,
				width: defaultDialogWidth,
				height: defaultDialogHeight
			});
		} else {
			// SUCCESS!
			// SET THE HEADER
			parentBox.find("h2").text(responseJSON.title);

			// SET THE BODY
			parentBox.find(".dashboxinner").html(htmlEntityDecode(responseJSON.content));

			// HIGHLIGHT
			parentBox.effect("highlight",{ },2000);
		}

		// RESET THE IMAGE
		refreshImg.attr("src",defaultRefresh);
	},"json");
	
	return false;
}

$(document).ready(function() {	
	$("<img>").attr("src",ajaxIndicator);

	// HIDE ALL THE MANAGEMENT ICONS AND SET UP THEIR DEFAULT FUNCTIONALITY
	$(".changebox, .refreshbox").hide()
	
	// BIND THE REFRESH BOX
	$(".refreshbox").click(function() {
		return refreshDashBox($(this));
	});
	
	// BIND THE CHANGE BOX
	$(".changebox").click(function() {
		changeDiv = $(this);
	
		parentBox = $("#" + $(this).parent().attr("id"));
		boxNumber = $(this).parent().attr("id").replace(/\D/g,"");
		
		// LOAD THE SELECTION FORM
		$.post("/xhr/dashboard-selector.php",{
			"box_id": boxNumber
		},function(responseHTML) {
			if (responseHTML == "ERROR") {
				// ERROR!
				$("#dialog").html("<p>We're sorry, but there was a problem showing the selection form.</p>").dialog({
					title: "Dashboard error",
					buttons: {
						"OK": function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					overlay: defaultOverlay,
					width: defaultDialogWidth,
					height: defaultDialogHeight
				});
			} else {
				$("#dialog").html(responseHTML).dialog({
					title: "Select a new box",
					buttons: {
						"OK": function() {
							theDialog = $(this).dialog;
						
							// SUBMIT THE CHANGE
							$.post("/xhr/dashboard.php",{
								"action": "change",
								"box_id": boxNumber,
								"new_box_id": $("#new_box_id").val()							
							},function(responseJSON) {
								if (responseJSON.title == "OK") {
									// LOAD THE NEW BOX
									return refreshDashBox(changeDiv);
								}
							},"json");
								
							// CLOSE THE DIALOG
							$(this).dialog("close");
						},
						"Cancel": function() {
							$(this).dialog("close");
						}
					},
					modal: true,
					overlay: defaultOverlay,
					width: 300,
					height: 150
				});
			}
		},"html");
		
		return false;
	});
	
	// NOW MAKE THEM SHOW UP IF A USER HOVERS OVER THE DIV
	$(".dashboxsmall, .dashboxfull").hover(function() {
		$(".changebox, .refreshbox",this).show();
	},function() {
		$(".changebox, .refreshbox",this).hide();
	});
});