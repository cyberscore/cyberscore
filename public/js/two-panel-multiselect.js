function setupTwoPanelMultiselect(tpms) {
  const source = tpms.querySelector('.two-panel-multiselect__source');
  const target = tpms.querySelector('.two-panel-multiselect__target');

  tpms.querySelector('.two-panel-multiselect__add').addEventListener('click', (ev) => {
    ev.preventDefault();
    while (source.selectedOptions.length > 0) {
      let before = null;
      for (const option of target.options) {
        if (option.innerText > source.selectedOptions[0].innerText) {
          before = option;
          break;
        }
      }
      tpms.querySelector(`input[type=hidden][value="${source.selectedOptions[0].value}"]`).disabled = false;
      target.insertBefore(source.selectedOptions[0], before);
    }
  });

  tpms.querySelector('.two-panel-multiselect__remove').addEventListener('click', (ev) => {
    ev.preventDefault();
    while (target.selectedOptions.length > 0) {
      let before = null;
      for (const option of source.options) {
        if (option.innerText > target.selectedOptions[0].innerText) {
          before = option;
          break;
        }
      }
      tpms.querySelector(`input[type=hidden][value="${target.selectedOptions[0].value}"]`).disabled = true;
      source.insertBefore(target.selectedOptions[0], before);
    }
  });

  tpms.querySelector('.two-panel-multiselect__source-filter').addEventListener('input', debounce((ev) => {
    const text = ev.target.value;
    for (const option of source.options) {
      option.selected = false;

      if (option.innerText.toLowerCase().includes(text.toLowerCase())) {
        option.classList.add('-match');
        option.disabled = false;
      } else {
        option.classList.remove('-match');
        option.disabled = true;
      }
    }
  }, 200));
}
