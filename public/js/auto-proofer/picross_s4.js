export function registerScores(result, addScore, game) {
  const charts = charts_with_name(charts_in_groups(game, ""), result.score.level.toUpperCase());

  addScore(charts, result.score.seconds);
}

function charts_in_groups(game, group_name) {
  return game.chart_groups
    .filter(group => group.english_group_name.includes(group_name))
    .flatMap(group => group.charts.map(c => ({ ...c, group })));
}

function charts_with_name(charts, name) {
  return charts.filter(chart => chart.english_chart_name.includes(name));
}

export const info = {
  features: [
    'detects times for all levels',
  ],
  limitations: [
  ],
};
