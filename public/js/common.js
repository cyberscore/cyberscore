// DECODE HTML ENTITIES
function htmlEntityDecode(theText) {
  return theText.replace(/&lt;/g,"<").replace(/&gt;/g,">").replace(/&quot;/g,"\"");
}

$(document).ready(function() {
  expand_details();
  copy_to_clipboard();

  // LINKTIP HOVER
  $(".loggedin small a").hover(function() {
    $("#linktip").text($(this).attr("title"));
  },function() {
    $("#linktip").text("");
  });
});

function showSpoiler(element) {
  // TODO: var inner = element.closest('.spoiler').descend('inner');
  var inner = element.parentNode.getElementsByTagName("div")[0];
  if (inner.style.display == "none") {
    inner.style.display = "";
  } else {
    inner.style.display = "none";
  }
}

const tagsToReplace = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;'
};

function h(str) {
  return str.toString().replace(/[&<>]/g, (tag) => tagsToReplace[tag]);
}

function ToggleDivVisible(tid) {
  if (document.getElementById(tid).style.display == "none") {
    document.getElementById(tid).style.display = "";
  } else {
    document.getElementById(tid).style.display = "none";
  }
}

function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => { func.apply(this, args); }, timeout);
  };
}

// TODO: this could be replaced with:
//   hx-get='search.json?live'
//   hx-target='next .livesearch-results'
//   hx-trigger='input changed delay:200ms, search'
const LiveSearch = debounce(async function(str) {
  if (!window.matchMedia('(max-width: 660px)').matches) {
    const response = await fetch(`/search.json?q=${encodeURIComponent(str)}&live`);
    const results = await response.json();
    document.querySelector(".livesearch-results").innerHTML = results.rendered;
  }
}, 200);

function expand_details() {
  const elements = Array.from(document.querySelectorAll('details[data-expand]'));

  const actions = elements
    .map(e => ({ element: e, open: window.localStorage.getItem(`expanded-${e.dataset.expand}`) == 'true' }))
    .filter((x) => x.open);

  actions.forEach((action) => {
    action.element.setAttribute('open', 'open');
  });

  elements.forEach((element) => {
    element.addEventListener('click', (event) => {
      const details = event.target.parentNode;
      if (details.open) {
        window.localStorage.removeItem(`expanded-${details.dataset.expand}`);
      } else {
        window.localStorage.setItem(`expanded-${details.dataset.expand}`, 'true');
      }
    });
  });
}

function copy_to_clipboard() {
  const elements = Array.from(document.querySelectorAll('[data-clipboard]'));

  elements.forEach((e) => {
    e.addEventListener('click', (evt) => navigator.clipboard.writeText(e.dataset.clipboard));
  });
}


function dispatch(target, eventName, detail) {
  target.dispatchEvent(new CustomEvent(eventName, { bubbles: true, detail }));
  return false;
}

function rove(event, closest) {
  if (event.keyCode == 40 || event.keyCode == 38) {
    event.preventDefault();
    const options = Array.from(event.target.closest(closest).querySelectorAll('[data-rove]'));

    const idx = options.indexOf(event.target) + (event.keyCode == 40 ? 1 : -1);
    if (0 <= idx && idx < options.length) {
      const next = options[idx];
      event.target.tabindex = -1;
      next.tabindex = 0;
      next.focus();
    }
  }
}
