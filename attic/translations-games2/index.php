<?php

Authorization::authorize('GlobalMod');

$t->CacheGameNames();

$sort = $_GET['sort'] ?? 'game_names';
switch ($sort) {
case 'charts':  $sorting = "ranked_charts DESC"; break;
case 'records': $sorting = "ranked_subs DESC"; break;
case 'rpc':     $sorting = "ranked_rpc DESC"; break;
case 'players': $sorting = "ranked_players DESC, ranked_rpc DESC"; break;
case 'recent':  $sorting = "game_id DESC"; break;
default:        $sorting = "game_name ASC"; break;
}

$game_list = database_get_all(database_select("
  SELECT
    games.game_id, games.site_id,
    games.ranked_charts, games.ranked_subs, games.ranked_players,
    IF(games.ranked_charts = 0, 0, games.ranked_subs / games.ranked_charts) AS ranked_rpc,
    games.unranked_charts, games.unranked_subs, games.unranked_players,
    IF(games.unranked_charts = 0, 0, games.unranked_subs / games.unranked_charts) AS unranked_rpc,
    GROUP_CONCAT(languages.flag_code ORDER BY languages.language_name ASC SEPARATOR ' ') AS languages
  FROM games
  LEFT JOIN translation_games_progress ON games.game_id = translation_games_progress.game_id
  LEFT JOIN languages USING(language_id)
  GROUP BY games.game_id ORDER BY $sorting
", '', []));

$total_num_subs = array_sum(pluck($game_list, 'ranked_subs'));
$total_num_charts = array_sum(pluck($game_list, 'ranked_charts'));

$workshop_games = database_get_all(database_select("
  SELECT game_id, COUNT(1) AS chart_count
  FROM games
  LEFT JOIN levels USING (game_id)
  WHERE site_id = 4
  GROUP BY game_id
  ORDER BY game_id DESC
", '', []));

render('translations/games2/index', $t['general_games']);
