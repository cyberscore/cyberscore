<?php

$prediction = database_find_by('predictions_games', ['game_id' => $_GET['id']]);

$enrolment = database_find_by('predictions_users', ['game_id' => $prediction['game_id'], 'user_id' => $current_user['user_id']]);
$role = $enrolment['user_status'];

$t->CacheCountryNames();

$matches = database_get_all(database_select("
  SELECT
    predictions_matches.*,
    predictions_results.*
  FROM predictions_matches
  LEFT JOIN predictions_results USING (match_id)
  WHERE game_id = ?
  ORDER BY date ASC
", 's', [$prediction['game_id']]));

foreach ($matches as &$match) {
  $match['country_a'] = database_find_by('countries', ['country_code' => $match['country_code_A']]);
  $match['country_b'] = database_find_by('countries', ['country_code' => $match['country_code_B']]);
}
unset($match);

render_with('predictions/show', [
  'page_title' => $prediction['game_name'],
  'prediction' => $prediction,
  'matches' => $matches,
  'enrolment' => $enrolment,
  'role' => $role,
]);
