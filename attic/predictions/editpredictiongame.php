<?php
set_include_path('/var/www/html/');
require_once("includes/startup.php");
$page_title = $t['predictions_game_edit'];
$name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = $g_game_id");
$rules = db_extract("SELECT rules FROM predictions_games WHERE game_id = $g_game_id");
require_once("includes/html_top.php");
?>
	<div id="pagefull">
		<div id="breadcrumb" class="clearfix">
			<?php require_once("predictions/nav.php");?>
		</div><br /><br />
<form action="predictionscript.php" method="post">
<input type="hidden" name="function" value="editgame">
<input type="hidden" name="game_id" value="<?php echo $g_game_id;?>">
<?php echo $t['games_game_name'];?>: <input type="text" name="game_name" value="<?php echo $name;?>"><br /><br />
<?php echo $t['gamerequests_rules'];?>: <textarea name="game_rules" value="<?php echo $rules;?>"><?php echo $rules;?></textarea><br /><br />
<input type="submit" name="submit" value="<?php echo $t['predictions_game_edit'];?>">
</form>

	</div>
<?php 
require_once("includes/html_bottom.php");?>
