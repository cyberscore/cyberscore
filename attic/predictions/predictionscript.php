<?php 
set_include_path('/var/www/html/');
require_once("includes/startup.php");

if ($user_id == 0) {
  $cs->WriteNote(false, str_replace(array('[link]', '[/link]'), array('<a href="../register.php">', '</a>'), $t['scripts_register_required']));
  $cs->RedirectToPreviousPage();
}

//-----------------------------------------------------------------------------------------------------------------------------------------------
if ($_POST['function'] == "creategame") {
  $game_id = database_insert('predictions_games', ['admin_user_id' => $user_id, 'game_name' => $_POST['game_name'], 'rules' => '']);
  database_insert('predictions_users', ['game_id' => $game_id, 'user_id' => $user_id, 'user_status' => 'administrator']);

  $cs->WriteNote(true, $t['scripts_predictions_game_created']);
  $cs->RedirectToPreviousPage();

} else if ($_POST['function'] == "enrol") {
  $game_id = intval($_POST['game_id']);
  if (database_find_by('predictions_users', ['game_id' => $game_id, 'user_id' => $user_id]) == NULL) {
    database_insert('predictions_users', ['game_id' => $game_id, 'user_id' => $user_id, 'user_status' => 'enrolled']);
    $game_name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = $game_id");
    $cs->WriteNote(true, str_replace('[game]', h($game_name) . ".", $t['scripts_predictions_enrolled']));
  } else {
    $cs->WriteNote(false, "Can't enrol.");
  }

  $cs->RedirectToPreviousPage();

} else if ($_POST['function'] == "unenrol") {
  $game_id = intval($_POST['game_id']);

  $game = database_find_by('predictions_games', ['game_id' => $game_id]);

  $enrollment = database_find_by('predictions_users', ['game_id' => $game_id, 'user_id' => $user_id]);
  $admin_count = database_single_value("SELECT COUNT(1) FROM predictions_users WHERE user_status = 'administrator' AND game_id = ?", 'i', [$game_id]);

  if ($enrollment['user_status'] == 'enrolled' || ($enrollment['user_status'] == 'administrator' && $admin_count > 1)) {
    db_query("DELETE FROM predictions_users WHERE game_id = $game_id AND user_id = $user_id");
    $cs->WriteNote(false, str_replace('[game]', $game['game_name'] . ".", $t['scripts_predictions_unenrolled']));
  } else {
    $cs->WriteNote(false, str_replace('[game]', $game['game_name'] . ".", $t['scripts_predictions_unenrolled_error']));
  }

  $cs->RedirectToPreviousPage();

} else if ($g_function == "delete") {
  $lead_admin = db_extract("SELECT admin_user_id FROM predictions_games WHERE game_id = $g_game_id");
  $game_name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = $g_game_id");
  if($lead_admin == $user_id);{
  db_query("DELETE FROM predictions_games WHERE game_id = $g_game_id");
  db_query("DELETE FROM predictions_matches WHERE game_id = $g_game_id");
  db_query("DELETE FROM predictions_records WHERE game_id = $g_game_id");
  db_query("DELETE FROM predictions_users WHERE game_id = $g_game_id");
  $cs->WriteNote(false, str_replace('[game]', $game_name.".", $t['scripts_predictions_deleted']));}
  $cs->RedirectToPreviousPage();

} else if ($p_function == "newcoreadmin") {
  $lead_admin = db_extract("SELECT admin_user_id FROM predictions_games WHERE game_id = $p_game_id");
  $game_name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = $p_game_id");
  $new_username = db_extract("SELECT username FROM users WHERE user_id = $p_new_admin_id");
  if ($lead_admin == $user_id);{
  db_query("UPDATE predictions_games SET admin_user_id = $p_new_admin_id WHERE game_id = $p_game_id");
  $cs->WriteNote(true, str_replace(array('[user]', '[game]'), array($new_username, $game_name), $t['scripts_predictions_promote']));
  $cs->RedirectToPreviousPage();}
}

//---------------------------------------------------------------------------------------------------------------
else if($p_function == "changerole")
{
$role = db_extract("SELECT user_status FROM predictions_users WHERE game_id = $p_game_id AND user_id = $user_id");
$lead_admin = db_extract("SELECT admin_user_id FROM predictions_games WHERE game_id = $p_game_id");
$changing_role = db_extract("SELECT user_status FROM predictions_users WHERE game_id = $p_game_id AND user_id = $p_change_user_id");
$game_name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = $p_game_id");
$new_username = db_extract("SELECT username FROM users WHERE user_id = $p_change_user_id");

if($lead_admin == $user_id && $role == 'administrator') {   db_query("UPDATE predictions_users SET user_status = '$p_role' WHERE game_id = $p_game_id AND user_id = $p_change_user_id");
                                                            $cs->WriteNote(true, str_replace(array('[user]', '[role]'), array($new_username, $p_role), $t['scripts_predictions_role_change']));
                            }// can do anything
else if($role == 'administrator') {
                                  } //can do anything except demote administrators
else if($role == 'barred' || 'enrolled') {$cs->WriteNote(false, "Well done on getting this far you cheeky devil. But I am too powerful!"); $cs->RedirectToPreviousPage();} // Send home

$cs->RedirectToPreviousPage();
}

//---------------------------------------------------------------------------------------------------------------
else if($p_function == "editgame")
{
$game_name = db_extract("SELECT game_name FROM predictions_games WHERE game_id = $p_game_id");
$role = db_extract("SELECT user_status FROM predictions_users WHERE game_id = $p_game_id AND user_id = $user_id");
if($role == 'administrator');{
db_query("UPDATE predictions_games SET game_name = '$p_game_name', rules = '$p_game_rules' WHERE game_id = $p_game_id");
$cs->WriteNote(true, str_replace('[game]', $game_name, $t['scripts_predictions_game_info_edit']));
$cs->RedirectToPreviousPage();}
}

//---------------------------------------------------------------------------------------------------------------
else if($p_function == "addmatch")
{
$country_code = db_extract("SELECT country_code FROM countries WHERE country_id = $p_country_id");
$country_code2 = db_extract("SELECT country_code FROM countries WHERE country_id = $p_country_id2");
$role = db_extract("SELECT user_status FROM predictions_users WHERE game_id = $p_game_id AND user_id = $user_id");
if($p_match_type == "") {$p_match_type = 'group';}


if(empty($p_teamA)) {}
else if(empty($p_teamB)) {}
else if(empty($p_game_name)) {}
else if(empty($p_game_datetime)) {}
else if($role == 'administrator'){
    db_query("INSERT INTO predictions_matches(game_id, match_name, team_A, team_B, country_code_A, country_code_B, date, game_type) VALUES($p_game_id, '$p_game_name', '$p_teamA', '$p_teamB', '$country_code', '$country_code2', '$p_game_datetime', '$p_match_type')");
    $cs->WriteNote(true, $t['scripts_predictions_match_create']);
    $cs->RedirectToPreviousPage();
}   

$cs->WriteNote(false, $t['scripts_predictions_match_create_error']);
$cs->RedirectToPreviousPage();
}

//---------------------------------------------------------------------------------------------------------------
else if($p_function == "editmatch")
{
$game_id = db_extract("SELECT game_id FROM predictions_matches WHERE match_id = $p_match_id");
$country_code = db_extract("SELECT country_code FROM countries WHERE country_id = $p_country_id");
$country_code2 = db_extract("SELECT country_code FROM countries WHERE country_id = $p_country_id2");
$role = db_extract("SELECT user_status FROM predictions_users WHERE game_id = $game_id AND user_id = $user_id");
if($p_match_type == "") {$p_match_type = 'group';}

if(empty($p_teamA)) {}
else if(empty($p_teamB)) {}
else if(empty($p_game_name)) {}
else if(empty($p_game_datetime)) {}
else if($role == 'administrator'){
    db_query("UPDATE predictions_matches SET match_name = '$p_game_name', team_A = '$p_teamA', team_B = '$p_teamB', country_code_A = '$country_code', country_code_B = '$country_code2', date = '$p_game_datetime', game_type = '$p_match_type' WHERE match_id = $p_match_id");
    $cs->WriteNote(true, $t['scripts_predictions_match_edit']);
    $cs->RedirectToPreviousPage();
}   

$cs->WriteNote(false, $t['scripts_predictions_match_edit_error']);
$cs->RedirectToPreviousPage();
}

//---------------------------------------------------------------------------------------------------------------
else if($p_function == "makeprediction") {
  $time = db_extract("SELECT date FROM predictions_matches WHERE match_id = $p_match_id");
  $game = database_find_by("predictions_matches", ['match_id' => $_POST['match_id']]);
  if (new DateTime() > new DateTime($time)) {
    $cs->WriteNote(false, $t['scripts_predictions_prediction_error']);
    $cs->RedirectToPreviousPage();
    exit;
  }
  $existing = db_extract("SELECT record_id FROM predictions_records WHERE user_id = $user_id AND match_id = $p_match_id");

  if (empty($p_submission_A) && $p_submission_A != 0) {
  } else if (empty($p_submission_B) && $p_submission_B != 0) {
  } else if (isset($existing)) {
    db_query("UPDATE predictions_records SET submission_A = '$p_submission_A', submission_B = '$p_submission_B', winning_team = '$p_winner' WHERE record_id = $existing AND user_id = $user_id AND match_id = $p_match_id");
    $cs->WriteNote(true, $t['scripts_predictions_prediction']);
    $cs->RedirectToPreviousPage();
  } else {
    // TODO: make winning_team and points_awarded nullable
    if ($game['game_type'] != 'knockout') {
      if ($p_submission_A > $p_submission_B) {
        $p_winner = 'A';
      } else if ($p_submission_A < $p_submission_B) {
        $p_winner = 'B';
      } else {
        $p_winner = 'Draw';
      }
    }
    db_query("INSERT INTO predictions_records(match_id, user_id, game_id, submission_A, submission_B, winning_team, points_awarded) VALUES($p_match_id, $user_id, ${game['game_id']}, '$p_submission_A', '$p_submission_B', '$p_winner', 0)");

    exit;
    $cs->WriteNote(true, $t['scripts_predictions_prediction']);
    $cs->RedirectToPreviousPage();
  }

  $cs->WriteNote(false, $t['predictions_prediction_error2']);
  $cs->RedirectToPreviousPage();
}

//---------------------------------------------------------------------------------------------------------------
else if($p_function == "editresult") {
  $time = db_extract("SELECT date FROM predictions_matches WHERE match_id = $p_match_id");
  list($game_id, $match_type) = db_extract("SELECT game_id, game_type FROM predictions_matches WHERE match_id = $p_match_id");
  $existing = db_extract("SELECT result_id FROM predictions_results WHERE match_id = $p_match_id");
  if($p_finished != 'yes') $finished = 'no'; else $finished = 'yes';

  //Handle points
  db_query("UPDATE predictions_records SET points_awarded = 0 WHERE match_id = $p_match_id");
  if($finished == 'yes' && $match_type == 'knockout') {
    db_query("UPDATE predictions_records SET points_awarded = 1 WHERE winning_team = '$p_winner' AND match_id = $p_match_id");
    if($p_submission_A > $p_submission_B){
      db_query("UPDATE predictions_records SET points_awarded = 2 WHERE submission_A > submission_B AND match_id = $p_match_id");
      db_query("UPDATE predictions_records SET points_awarded = 3 WHERE submission_A > submission_B AND winning_team = '$p_winner' AND match_id = $p_match_id");
    }
    else if($p_submission_A < $p_submission_B) {
      db_query("UPDATE predictions_records SET points_awarded = 2 WHERE submission_A < submission_B AND match_id = $p_match_id");
      db_query("UPDATE predictions_records SET points_awarded = 3 WHERE submission_A < submission_B AND winning_team = '$p_winner' AND match_id = $p_match_id");
    }
    else if($p_submission_A == $p_submission_B) {
      db_query("UPDATE predictions_records SET points_awarded = 2 WHERE submission_A = submission_B AND match_id = $p_match_id");
      db_query("UPDATE predictions_records SET points_awarded = 3 WHERE submission_A = submission_B AND winning_team = '$p_winner' AND match_id = $p_match_id");
    }
    db_query("UPDATE predictions_records SET points_awarded = 3 WHERE submission_A = '$p_submission_A' AND submission_B = '$p_submission_B' AND match_id = $p_match_id");
    db_query("UPDATE predictions_records SET points_awarded = 4 WHERE submission_A = '$p_submission_A' AND submission_B = '$p_submission_B' AND winning_team = '$p_winner' AND match_id = $p_match_id");
  } else if($finished == 'yes' && $match_type == 'group') {
    if($p_submission_A > $p_submission_B){
      db_query("UPDATE predictions_records SET points_awarded = 2 WHERE submission_A > submission_B AND match_id = $p_match_id");
    }
    else if($p_submission_A < $p_submission_B) {
      db_query("UPDATE predictions_records SET points_awarded = 2 WHERE submission_A < submission_B AND match_id = $p_match_id");
    }
    else if($p_submission_A == $p_submission_B) {
      db_query("UPDATE predictions_records SET points_awarded = 2 WHERE submission_A = submission_B AND match_id = $p_match_id");
    }

    db_query("UPDATE predictions_records SET points_awarded = 3 WHERE submission_A = '$p_submission_A' AND submission_B = '$p_submission_B' AND match_id = $p_match_id");
  }

  //Add result
  if(empty($p_submission_A) && $p_submission_A != 0){}
  else if(empty($p_submission_B) && $p_submission_B != 0){}
  else if(empty($p_winner)){}
  else if(isset($existing)) { db_query("UPDATE predictions_results SET result_A = '$p_submission_A', result_B = '$p_submission_B', winner = '$p_winner',
    sub_result_A = '$p_tiebreakersubmission_A', sub_result_B = '$p_tiebreakersubmission_B',
    result_finished = '$finished'
    WHERE result_id = $existing AND match_id = $p_match_id");

  $cs->WriteNote(true, $t['scripts_predictions_result_edit']);
  $cs->RedirectToPreviousPage();}
  else{
    db_query("INSERT INTO predictions_results(match_id, result_A, result_B, winner, sub_result_A, sub_result_B, result_finished) VALUES($p_match_id, '$p_submission_A', '$p_submission_B', '$p_winner', '$p_tiebreakersubmission_A', '$p_tiebreakersubmission_B', '$finished')");
    $cs->WriteNote(true, $t['scripts_predictions_result_edit']);
    $cs->RedirectToPreviousPage();
  }

  $cs->WriteNote(false, $t['scripts_predictions_result_edit_error']);
  $cs->RedirectToPreviousPage();
}





else {$cs->WriteNote(false, "%$%%%%");
$cs->RedirectToPreviousPage();}
?>
