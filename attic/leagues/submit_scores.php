<?php 
set_include_path('/var/www/html/');
require_once("includes/startup.php");

$league_id = intval($_GET['league'] ?? 0);

if (($logged_in) && ($round != "") && ($league_id != 0) && (db_num_rows(database_select("SELECT * FROM league_maybe WHERE league_id = ? AND user_id = ?", 'ii', [$league_id, $user_id])) == 1)) {
  //save each of the values

  for ($number = 1; $number <= 3; $number++) {
    $score = ${"p_score$number"};

    if (db_num_rows(database_select("SELECT * FROM my_leagues WHERE league_id = ? AND user_id = ? AND round = ? AND number = ?", 'iiii', [$league_id, $user_id, $round, $number])) == 1) {
      database_update(
        'my_leagues',
        ['score' => $score],
        'league_id = ? AND user_id = ? AND round = ? AND number = ?',
        'iiii',
        [$league_id, $user_id, $round, $number]
      );
    } else {
      database_insert('my_leagues', [
        'league_id' => $league_id,
        'user_id' => $user_id,
        'round' => $round,
        'number' => $number,
        'score' => $score,
      ]);
    }
  }

  if (db_num_rows(database_select("SELECT * FROM league_scores WHERE league_id = ? AND user_id = ?", 'ii', [$league, $user_id])) == 0) {
    $league = db_get_result(database_select("SELECT * FROM league_manager WHERE league_id = ?", 'i', [$league_id]));

    database_insert('league_scores', [
      'league_id' => $league_id,
      'game_id' => $league['game_id'],
      'user_id' => $user_id,
    ]);
  }

  header("Location: /leagues/view.php?league=$league_id");
  exit;
} else {
  header("Location: /index.php");
  exit;
}
?>
