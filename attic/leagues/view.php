<?php
require_once("../includes/startup.php");

$page_title = 'View League';

//check that this league exists
$league = intval($_GET['league'] ?? 0);
if (db_num_rows(database_select("SELECT * FROM league_manager WHERE league_id = ?", 'i', [$league])) != 1) {
  header("Location: /index.php");
  exit;
}
?>

<?php require_once("includes/html_top.php"); ?>

<div id="pageleft">
  <div id="breadcrumb" class="clearfix">
    <a href="/">Home</a> &rarr; <a href="/leagues/select.php">Leagues</a> &rarr; View League
  </div>

  <h1>View League</h1>

<?php
//if this is the league manager
$league_info = database_get(database_select("SELECT * FROM league_manager WHERE league_id = ?", 'i', [$league]));
if ($league_info['manager_id'] == $user_id && $league_info['finished'] == 0) {
?>

<h2>Manage League</h2>

<form action="/leagues/update.php?league=<?= $league ?>" method="post">
  <input type="hidden" name="section" value="settings" />

  League Manager : You <br><br>

  <h3>User List</h3>
  <?php
  $user_query = database_get_all(database_select("SELECT users.username FROM league_maybe, users WHERE league_maybe.user_id = users.user_id AND league_id = ?", 'i', [$league]));
  foreach ($user_query as $user_list) {
  ?>
  <?php echo $user_list['username']?><br>
  <?php } ?>
  <br><br>

  <label for="league_name">League Name</label><br>
  <input type="text" name="league_name" value="<?php echo $league_info['name']?>" size="15" maxlength="15" /><br><br>

  <label for="length">Number of rounds (min. 3 / recommended 4 / max. 10)</label><br>
  <input type="text" name="length" value="<?php echo $league_info['length']?>" size="2" maxlength="2"/>     <br><br>

  <label for="start">Days until the start (min. 1 / max. 14)</label><br>
  <input type="text" name="start" value="<?php echo $league_info['gap']?>" size="2" maxlength="2"/>  <br><br>

  <label for="round_length">Days for each round (min. 3 / max. 14)</label><br>
  <input type="text" name="round_length" value="<?php echo $league_info['time']?>" size="2" maxlength="2"/> <br><br>

  <label for="notes">Enter a full description of the league here along with any proof requirements</label><br>
  <textarea type="text" name="notes" cols="50" rows="5" maxlength="255"><?php echo $league_info['notes']?></textarea> <br><br>

  <input type="submit" value="Save" />
</form>

<?php
// set the charts for the challenges
if ($league_info['name'] != "" && $league_info['length'] != 0 && $league_info['gap'] != 0 && $league_info['time'] != 0) {
?>

<h2>Challenges for each round</h2>
Type a short description for each challenge followed by the Chart ID (if applicable).
You do not need to use all of these boxes, but they cannot be changed once the league has started.

<form action="/leagues/update.php?league=<?php echo $league?>" method="post">
  <input type="hidden" name="section" value="challenges" />

<?php
$gap = $league_info['gap'];
$roundtime = $league_info['time'];
$i = 1;

while($i < $league_info['length'] + 1){

?>

<h3>Round <?php echo $i?>: <?php echo date('F j', strtotime("+" . ($gap+1+($roundtime*($i-1))) . "days"))?> to <?php echo date('F j', strtotime("+" . ($gap+($roundtime*$i)) . "days"))?></h3>
<?php for ($number = 1; $number <= 3; $number++) {
$weekly_info = [];
if (db_num_rows(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = ?", 'iii', [$league, $i, $number])) == 1){
$weekly_info = database_get(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = ?", 'iii', [$league, $i, $number]));
}
?>
Challenge <?php echo $number; ?>:
<input
  type="text"
  name="round<?php echo $i?>t<?php echo $number; ?>a"
  value="<?php echo $weekly_info['information']?>"
  size="50"
  maxlength="127" /> |
Chart ID:
<input
  type="text"
  name="round<?php echo $i?>t<?php echo $number; ?>b"
  value="<?php echo $weekly_info['chart_id']?>"
  size="8"
  maxlength="8" />
<br>
<?php } ?>

<?php
$i++;
}
?>

<input type="submit" value="Save" />
</form>
<?php
}

//ready to publish?
$j = 0;
$i=1;
while($i < $league_info['length'] + 1){
$check1 = "";
$check1 = database_get(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = 1", 'ii', [$league, $i]));
if($check1['information'] != ""){
$j++;
}
$i++;
}

$interested = db_num_rows(database_select("SELECT * FROM league_maybe WHERE league_id = ?", 'i', [$league]));

if ($j == $league_info['length'] && $league_info['length'] != 0 && $interested > 2) {
?>
<br><br><br>
Once published a league cannot be edited further.
<form action="/leagues/publish.php?league=<?php echo $league?>" method="post">
<input type="submit" value="Publish" />
</form>
<?php
  }
}

if(($league_info['manager_id'] != $user_id)&&($league_info['finished'] == 0)){
?>

This league is still being worked on. A league must have at least 3 competitors and then be published by the league manager. If you have applied to the league you will be notified when it is published. Some of the settings so far can be viewed below.<br><br><br>

<h3>User List</h3>
<?php
$user_query = database_get_all(database_select("SELECT users.username FROM league_maybe, users WHERE league_maybe.user_id = users.user_id AND league_id = ?", 'i', [$league]));
foreach ($user_query as $user_list) {
?>
<?php echo $user_list['username']?><br>
<?php
}
?>
<br><br>
<?php
$r = $league_info['length'];
$s = $league_info['time'];
if($r == 0){
$r = "TBC";
}
if($s == 0){
$s = "TBC";
}

?>
Number of rounds: <?php echo $r?><br>
Length of each round: <?php echo $s?> days<br>
Description: <?php echo $league_info['notes']?><br>
<?php
}


if($league_info['finished'] == 1){

?>
<h3><?php echo $league_info['name']?></h3>
<?php
echo $league_info['notes'];
?>
<br>
<br>
<?php

//PUT HERE THE OVERALL SCORES


//PUT THE INPUT BOXES FOR THE POINTS
?>
<form action="/leagues/submit_totals.php?league=<?php echo $league?>" method="post">
  <table id="allgames" class="zebra">
    <tr>
      <td>User</td>
      <?php for ($p = 1; $p < $league_info['length'] + 1; $p++) { ?>
      <td>R<?php echo $p?></td>
      <?php } ?>
      <td>Total</td>
    </tr>
    <?php
    $extra_list_needed = 0;
    $all_users = database_get_all(database_select("SELECT league_scores.user_id, league_scores.total, users.user_id, users.username FROM users, league_scores WHERE users.user_id = league_scores.user_id AND league_id = ? ORDER BY league_scores.total DESC", 'i', [$league]));
    if (count($all_users) == 0){
      $extra_list_needed = 1;
    }
    foreach ($all_users as $user_list) {
      $this_user = $user_list['username'];
      $this_user_id = $user_list['user_id'];
      $total_score = 0;
    ?>
    <tr>
      <td><?php echo $user_list['username']?></td>
      <?php
      for ($q = 1; $q < $league_info['length'] + 1; $q++) {
        if (db_num_rows(database_select("SELECT * FROM league_scores WHERE league_id = ? AND user_id = ?", 'ii', [$league, $this_user_id])) == 1) {
          $old_data = database_get(database_select("SELECT * FROM league_scores WHERE league_id = ? AND user_id = ?", 'ii', [$league, $this_user_id]));
          $this_data = $old_data['round' . $q];
          $this_total = $old_data['total'];
        } else {
          $this_total = 0;
          $this_data = 0;
        }
      ?>
        <td>
        <?php if ($league_info['manager_id'] == $user_id) { ?>
          <input type="text" name="points<?php echo $user_list['user_id']?>n<?php echo $q?>" value="<?php echo $this_data ?>" size="3" maxlength="3"/>     <br><br>
        <?php } else { ?>
          <?php echo $this_data; ?>
        <?php } ?>
        </td>
      <?php } ?>
      <td><?php echo $this_total?></td>
    </tr>
    <?php } ?>
  </table>
  <?php
  if ($extra_list_needed == 1) {
    $user_query2 = database_get_all(database_select("SELECT users.username FROM league_maybe, users WHERE league_maybe.user_id = users.user_id AND league_id = ?", 'i', [$league]));
    foreach ($user_query2 as $user_list2) {
  ?>
  <?php echo $user_list2['username']?><br>
  <?php
    }
  }

  if($league_info['manager_id'] == $user_id) {
  ?>
  <input type="submit" value="Save" />
  <?php } ?>
</form>

<br><br>

<?php
$gap = $league_info['gap'];
$roundtime = $league_info['time'];
$published = $league_info['start'];
$publish_date = date_create($published);

$i = 1;
while($i < $league_info['length'] + 1){
$date1 = date_create($published);
$date2 = date_create($published);
date_add($date1, date_interval_create_from_date_string(($gap+1+($roundtime*($i-1))) . "days"));
date_add($date2, date_interval_create_from_date_string(($gap+($roundtime*$i)) . "days"));
?>
<h3>Round <?php echo $i?>: <?php echo date_format($date1, "F j")?> to <?php echo date_format($date2, "F j")?></h3>

<?php
$date1b = date_format($date1,"Y-m-d");
$date2b = date_format($date2,"Y-m-d");
$today = date('Y-m-d');

if(($today >= $date1b)&&($today <= $date2b)){

//THIS DISPLAYS THE INPUT BOXES FOR THE CURRENT ROUND

?>


<form action="/leagues/submit_scores.php?league=<?php echo $league?>&round=<?php echo $i?>" method="post">
<?php
  $j = 1;
  while ($j < 4) {
    // load in the challenge information
    if (db_num_rows(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = ?", 'iii', [$league, $i, $j])) == 1) {
      $weekly_info = database_get(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = ?", 'iii', [$league, $i, $j]));
      if (($weekly_info['chart_id'] != 0)&&($weekly_info['chart_id'] != "")) {
?>
<a href="/chart/<?php echo $weekly_info['chart_id']?>"><?php echo $weekly_info['information']?></a>
<?php
} else {
  echo $weekly_info['information'];
}
$my_score = "";
if (db_num_rows(database_select("SELECT * FROM my_leagues WHERE league_id = ? AND round = ? AND user_id = ? AND number = ?", 'iiii', [$league, $i, $user_id, $j])) == 1) {
  $my_score = database_get(database_select("SELECT * FROM my_leagues WHERE league_id = ? AND round = ? AND user_id = ? AND number = ?", 'iiii', [$league, $i, $user_id, $j]));
}
?><br>
Your score: <input type="text" name="score<?php echo $j?>" value="<?php echo $my_score['score']?>" size="15" maxlength="18"/>     <br><br>
<?php
}
$j++;
}
?>
  <input type="submit" value="Save" />
</form> <br><br>

<?php
} else if($today > $date2b)  {
  $j = 1;
  while ($j < 4){

//load in the challenge information
if(db_num_rows(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = ?", 'iii', [$league, $i, $j])) == 1){
$weekly_info = db_get_result(database_select("SELECT * FROM league_charts WHERE league_id = ? AND round = ? AND number = ?", 'iii', [$league, $i, $j]));
?>
Score <?php echo $j?>: <?php echo $weekly_info['information']?><br>
<?php
}
$j++;
}

?>

<br><br>
<table id="allgames" class="zebra">
<tr>
<td>Username</td>
<td>Score 1</td>
<td>Score 2</td>
<td>Score 3</td>
<td>Total</td>
<td>Points</td>
</tr>
<?php
$all_users = database_select("SELECT my_leagues.user_id, users.username FROM my_leagues, users WHERE users.user_id = my_leagues.user_id AND league_id = ? GROUP BY my_leagues.user_id", 'i', [$league]);
while ($user_list = db_get_result($all_users)){
$this_user = $user_list['user_id'];
$total_score = 0;
?>
<tr>
<td>
<?php echo $user_list['username']?>
</td>
<?php
$k = 1;
while($k < 4){
?>
<td>
<?php
if(db_num_rows(database_select("SELECT * FROM my_leagues WHERE league_id = ? AND user_id = ? AND round = ? AND number = ?", 'iiii', [$league, $this_user, $i, $k])) == 1){
$this_score = db_get_result(database_select("SELECT * FROM my_leagues WHERE league_id = ? AND user_id = ? AND round = ? AND number = ?", 'iiii', [$league, $this_user, $i, $k]));
echo $this_score['score'];
$total_score = $total_score + $this_score['score'];
} else {
?>
-
<?php } ?>
</td>
<?php
$k++;
}
?>
<td><?php echo $total_score?></td>

<!-- THIS IS WHERE I NEED TO PUT THE POINTS AWARDED BY THE MANAGER -->
<?php
$points = 0;
if(db_num_rows(database_select("SELECT * FROM league_scores WHERE league_id = ? AND user_id = ?", 'ii', [$league, $this_user])) == 1){
$points_for_this_round = db_get_result(database_select("SELECT * FROM league_scores WHERE league_id = ? AND user_id = ?", 'ii', [$league, $this_user]));
$points_for_this = $points_for_this_round['round' . $i];
} else {
$points_for_this = 0;
}
?>
<td><?php echo $points_for_this?></td>
<?php } ?>
</table><br><br><br>

<?php } else if($today < $date1b) { ?>
  <br>Upcoming challenges<br><br>
<?php } else { ?>
  <br>Date error<br><br>
<?php
}
$i++;
}
}

//manager of the league but it is now published

//if you are in the league
?>
</div>

<?php require_once("includes/html_bottom.php");?>
