<?php 
require_once("../includes/startup.php");

$game_id = intval($_GET['game'] ?? 0);

if ($logged_in && $game != "") {
  // each manager can only have one running league for each game
  if (db_num_rows(database_select("SELECT * FROM league_manager WHERE game_id = ? AND finished < 2 AND manager_id = ?"), 'ii', [$game_id, $user_id]) == 0) {
    //create league
    database_insert('league_manager', ['manager_id' => $user_id, 'game_id' => $game_id, 'name' => '', 'notes' => '']);

    $new_league = db_get_result(database_select("SELECT * FROM league_manager WHERE game_id = ? AND finished < 2 AND manager_id = ?", 'ii', [$game_id, $user_id]));

    database_insert('league_maybe', ['user_id' => $user_id, 'game_id' => $game_id, 'league_id' => $new_league['league_id']]);
  }

  header("Location: /leagues/select.php");
  exit;
} else {
  header("Location: /index.php");
  exit;
}
?>
