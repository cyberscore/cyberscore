<?php 
set_include_path('/var/www/html/');
require_once("includes/startup.php");

$league = intval($_GET['league'] ?? 0);
$league_info = db_get_result(database_select("SELECT * FROM league_manager WHERE league_id = ?", 'i', [$league]));
$game = $league_info['game_id'];

if ($logged_in && $league != "" && db_num_rows(database_select("SELECT * FROM league_manager WHERE league_id = ? and manager_id = ? AND finished < 2", 'ii', [$league, $user_id])) == 1) {
  //save each of the values

  //cycle through each of the users who have signed up
  $user_query = database_query("SELECT user_id FROM league_maybe WHERE league_id = ?", 'i', [$league]);
  while ($user_list = db_get_result($user_query)) {
    $this_user = $user_list['user_id'];

    //if there isn't a score entry then create one
    if(db_num_rows(database_select("SELECT * FROM league_scores WHERE user_id = ? AND league_id = ?", 'ii', [$this_user, $league])) == 0){
      database_insert('league_scores', [
        'league_id' => $league,
        'game_id' => $game,
        'user_id' => $this_user,
      ]);
    }

    //cycle through each of the rounds
    $total = 0;
    for ($round = 1; $round <= 10; $round++) {
      $points = intval(${"p_points${this_user}n${round}"});
      $total += $points;

      if ($points > 0 && $points < 101) {
        database_update(
          'league_scores',
          ["round$round" => $points],
          'user_id = ? AND league_id = ?',
          'ii',
          [$this_user, $league]
        );
      }
    }

    database_update(
      'league_scores',
      ['total' => $total],
      'user_id = ? AND league_id = ?',
      'ii',
      [$this_user, $league]
    );
  }

  header("Location: /leagues/view.php?league=$league");
  exit;
} else {
  header("Location: /index.php");
  exit;
}
?>
