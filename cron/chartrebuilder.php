<?php

set_include_path(__DIR__ . "/..");
require_once("includes/startup.php");

$charts = database_get_all(database_select("SELECT chart_id FROM chartrebuilder LIMIT 3000", '', []));
foreach ($charts as $chart) {
  ChartCacheRebuilder::RebuildChart($chart['chart_id']);
}
