<?php

set_include_path(__DIR__ . "/..");
require_once("includes/startup.php");

$games = database_get_all(database_select("SELECT game_id FROM cacherebuilder LIMIT 25", '', []));
foreach ($games as $game) {
  GameCacheRebuilder::RebuildGame($game['game_id']);
  database_delete_by('cacherebuilder', ['game_id' => $game['game_id']]);
}
