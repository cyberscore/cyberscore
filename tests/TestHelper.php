<?php

set_include_path(__DIR__ . '/..');
require_once __DIR__ . '/../vendor/autoload.php';
require_once("includes/chart_icons.php");

require_once("includes/config.php");
global $config;
$config = load_config(__DIR__ . '/../config.ini.test');

require_once("includes/router.php");
require_once("src/router.php");
require_once("includes/common.php");
require_once("includes/charts.php");
require_once("includes/modifiers.php");
require_once("includes/cs_class_x.php");
require_once("includes/helpers.php");
require_once("includes/uploads.php");
require_once("includes/user.php");
require_once("src/lib/tabbed_modules.php");
require_once("src/lib/rankbutton_generator.php");

require_once("src/repositories/rankbuttons.php");
require_once("src/repositories/api_tokens.php");
require_once("src/repositories/users.php");

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../src/templates');

global $twig;
$twig = new \Twig\Environment($loader, [
  'cache' => __DIR__ . '/../cache',
  'debug' => true,
]);

database_open();

require_once("src/lib/twig_functions.php");

function render_test($__target, $__params) {
  ob_start();

  $__target->run($__params);

  $contents = ob_get_contents();
  ob_end_clean();

  return $contents;
}

function test_post($path, $params) {
  global $_POST;
  global $_SERVER;

  $_POST = $params;
  $_SERVER['REQUEST_METHOD'] = 'POST';
  $_SERVER['REQUEST_URI'] = $path;

  return test_handle_request();
}

function test_get($path) {
  global $_SERVER;

  $_SERVER['REQUEST_METHOD'] = 'GET';
  $_SERVER['REQUEST_URI'] = $path;

  return test_handle_request();
}

function test_handle_request() {
  global $_SERVER;

  $matched_route = Router::instance()->match(
    $_SERVER['REQUEST_METHOD'],
    $_SERVER['REQUEST_URI']
  );

  extract_request_params($matched_route[1]);
  foreach ($matched_route[2] as $mw) {
    ($mw)();
  }

  return render_test($matched_route[0], $matched_route[1]);
}

function fake_user($params = []) {
  $faker = Faker\Factory::create();

  $id = UsersRepository::create(array_merge([
    'username' => $faker->name(),
    'timezone' => $faker->timezone(),
    'pword_new' => '',
    'country_id' => 1,
    'team_code' => '',
  ], $params));

  return UsersRepository::get($id);
}
