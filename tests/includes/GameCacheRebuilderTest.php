<?php

test('GameCacheRebuilder::BuildGameIncrementalCache', function () {
  $expected = database_filter_by('gsb_cache_incremental', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = GameCacheRebuilder::FetchRecordsWithModifiers(2006);

  $actual = GameCacheRebuilder::BuildGameIncrementalCache(2006, $records);

  expect($expected)->toBe($actual);
})->skip();

test('GameCacheRebuilder::BuildGameArcadeCache', function () {
  $expected = database_filter_by('gsb_cache_arcade', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = GameCacheRebuilder::FetchRecordsWithModifiers(2006);

  $actual = GameCacheRebuilder::BuildGameArcadeCache(2006, $records);

  expect($expected)->toBe($actual);
})->skip();

test('GameCacheRebuilder::BuildGameCollectibleCache', function () {
  $expected = database_filter_by('gsb_cache_collectible', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = GameCacheRebuilder::FetchRecordsWithModifiers(2006);

  $actual = GameCacheRebuilder::BuildGameCollectibleCache(2006, $records);

  expect($expected)->toBe($actual);
})->skip();

test('GameCacheRebuilder::BuildGameSpeedrunCache', function () {
  $expected = database_filter_by('gsb_cache_speedrun', ['game_id' => 2961]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = GameCacheRebuilder::FetchRecordsWithModifiers(2961);

  $actual = GameCacheRebuilder::BuildGameSpeedrunCache(2961, $records);

  expect($expected)->toBe($actual);
})->skip();

test('GameCacheRebuilder::BuildGameSolutionCache', function () {
  $expected = database_filter_by('gsb_cache_solution', ['game_id' => 629]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = GameCacheRebuilder::FetchRecordsWithModifiers(629);

  $actual = GameCacheRebuilder::BuildGameSolutionCache(629, $records);

  expect($expected)->toBe($actual);
})->skip();

test('GameCacheRebuilder::BuildGameProofCache', function () {
  $expected = database_filter_by('gsb_cache_proof', ['game_id' => 2006]);
  $expected = array_map(fn($r) => array_diff_key($r, ['cache_id' => 0]), $expected);

  $records = GameCacheRebuilder::FetchRecordsWithModifiers(2006);

  $actual = GameCacheRebuilder::BuildGameProofCache(2006, $records);

  expect($expected)->toBe($actual);
})->skip();
