<?php

require_once("tests/TestHelper.php");

test("database_where_sql_bind()", function() {
  expect(database_where_sql_bind(['cool' => true]))->toBe(["cool = ?", [true]]);
  expect(database_where_sql_bind(['name' => 'wiki']))->toBe(["name = ?", ["wiki"]]);
  expect(database_where_sql_bind(['name' => null]))->toBe(["name IS NULL", []]);
  expect(database_where_sql_bind(['user' => [1, 2, 3, 4]]))->toBe(["user IN (?,?,?,?)", [1, 2, 3, 4]]);
  expect(database_where_sql_bind(['cool' => true, 'user' => 42]))->toBe(["cool = ? AND user = ?", [true, 42]]);
  expect(database_where_sql_bind(['name' => [null]]))->toBe(["name IS NULL", []]);
  expect(database_where_sql_bind(['name' => ['wiki', 'tiki', null]]))->toBe(["(name IN (?,?) OR name IS NULL)", ['wiki', 'tiki']]);

  expect(database_where_sql_bind(['user' => database_not(42)]))->toBe(["NOT (user = ?)", [42]]);
  expect(database_where_sql_bind(['user' => database_not(42), 'name' => 'wiki']))->toBe(["NOT (user = ?) AND name = ?", [42, 'wiki']]);
  expect(database_where_sql_bind(['time' => database_now()]))->toBe(["time = NOW()", []]);
  expect(database_where_sql_bind(['time' => database_not(database_now())]))->toBe(["NOT (time = NOW())", []]);
  expect(database_where_sql_bind(['user' => database_literal("SOMESQL()")]))->toBe(["user = SOMESQL()", []]);
  expect(database_where_sql_bind(database_or(['name' => 'wiki'], ['cool' => true])))->toBe(["(name = ?) OR (cool = ?)", ['wiki', true]]);
  expect(database_where_sql_bind(database_or(['name' => database_not('wiki')], ['cool' => true])))->toBe(["(NOT (name = ?)) OR (cool = ?)", ['wiki', true]]);

  expect(database_where_sql_bind(['name' => "bobby tables' OR 1=1 --"]))->toBe(["name = ?", ["bobby tables' OR 1=1 --"]]);
});

test("database_insert_all()", function() {
  db_query("DROP TABLE IF EXISTS poop", []);
  db_query("CREATE TABLE poop(id INT, score INT)", []);
  database_insert_all('poop', [['id' => 1, 'score' => 42], ['id' => 2, 'score' => 34]]);

  expect(database_value("SELECT COUNT(1) FROM poop", []))->toBe(2);
  expect(database_value("SELECT score FROM poop WHERE id = 1", []))->toBe(42);
  expect(database_value("SELECT score FROM poop WHERE id = 2", []))->toBe(34);
});
