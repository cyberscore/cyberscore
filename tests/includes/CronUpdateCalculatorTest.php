<?php

require_once("includes/cron_update_calculator.php");

test('Cron::time_to_next', function () {
  $cron = new Cron([0], "*");

  expect($cron->time_to_next(1721135472))->toBe(49);
  expect($cron->time_to_next(1721135520))->toBe(48);
  expect($cron->time_to_next(1721135520 + 60*30))->toBe(18);
  expect($cron->time_to_next(1721135520 + 60*38))->toBe(10);
  expect($cron->time_to_next(1721135520 + 60*48))->toBe(0);
  expect($cron->time_to_next(1721135520 + 60*49))->toBe(59);
});
