<?php

require_once("tests/TestHelper.php");
require_once('src/repositories/notifications.php');

beforeEach(function() {
  for ($i = 0; $i < 10; $i++) {
    fake_user(['user_groups' => 32]);
  }

  $this->user = fake_user(['user_groups' => 32]);

  database_insert('notification_prefs', [
    'user_id' => $this->user['user_id'],
    'new_grequest' => true,
  ]);
});

test("DeliverToUser", function() {
  $notification_id = Notification::NewGameRequest(null, $this->user['user_id'])
    ->DeliverToUser($this->user['user_id']);

  $notification = NotificationsRepository::get($notification_id);

  expect($notification['type'])->toEqual('new_grequest');
  expect($notification['user_id'])->toEqual($this->user['user_id']);
});

test("DeliverToUser when notification is disabled", function() {
  database_update_by('notification_prefs', ['new_grequest' => false], ['user_id' => $this->user['user_id']]);

  $notification_id = Notification::NewGameRequest(null, $this->user['user_id'])
    ->DeliverToUser($this->user['user_id']);

  $notifications = NotificationsRepository::all();

  expect(count($notifications))->toEqual(0);
});

test("DeliverToGroups", function() {
  $developers_count = database_value('SELECT COUNT(1) FROM users WHERE user_groups & 32', []);

  $notification_ids = Notification::NewGameRequest(null, $this->user['user_id'])
    ->DeliverToGroups(['Dev']);

  expect(count($notification_ids))->toEqual($developers_count);
});

test("DeliverToGroups except ourselves", function() {
  $developers_count = database_value('SELECT COUNT(1) FROM users WHERE user_groups & 32', []);

  $notification_ids = Notification::NewGameRequest(null, $this->user['user_id'])
    ->DeliverToGroups(['Dev'], ['except' => [$this->user['user_id']]]);

  expect(count($notification_ids))->toEqual($developers_count - 1);
});
