<?php

require_once("tests/TestHelper.php");
require_once("src/repositories/games.php");

beforeEach(function () {
  for ($i = 1; $i < 100; $i++) {
    database_insert('platforms', ['platform_id' => $i]);
  }

  $this->game_id = GamesRepository::create([
    'site_id' => 1,
    'notes' => '',
    'game_name' => "Test game",
    'mod_notes' => '',
    'game_patches' => '',
    'date_published' => database_now(),
    'all_charts' => 1,
    'all_subs' => 0,
  ]);
  database_insert('levels', [
    'game_id' => $this->game_id,
    'level_name' => "Test level",
    'level_rules' => "",
    'chart_type' => 0,
    'score_prefix' => '',
    'score_suffix' => '',
    'chart_type2' => 0,
    'score_prefix2' => '',
    'score_suffix2' => '',
    'entity_lock' => '',
    'level_pos' => 0,
    'name_pri' => '',
    'name_sec' => '',
    'name1' => '',
    'name2' => '',
    'name3' => '',
  ]);
});

beforeEach(function () {
  $user = fake_user(['user_groups' => 128, 'auth_verified' => true, 'banned' => false]);

  CurrentUser::load_from_user_id($user['user_id']);
});

describe("Backwards compatibility", function() {
  test('Sets backwards compatible platform', function () {
    $contents = test_post("/games/{$this->game_id}", [
      'platform_id' => [40],
    ]);

    $game_platforms = database_filter_by('game_platforms', ['game_id' => $this->game_id]);

    expect(pluck($game_platforms, 'platform_id'))->toBe([40, 71]);
  });

  test('When backwards compatible is specified as main platform', function () {
    $contents = test_post("/games/{$this->game_id}", [
      'platform_id' => [40, 71],
    ]);

    $game_platforms = database_filter_by('game_platforms', ['game_id' => $this->game_id]);

    expect(pluck($game_platforms, 'platform_id'))->toBe([40, 71]);
  });

  test('when backwards compatible is specified as secondary platform', function () {
    $contents = test_post("/games/{$this->game_id}", [
      'platform_id' => [40],
      'platform_id2' => [71],
    ]);

    $game_platforms = database_filter_by('game_platforms', ['game_id' => $this->game_id]);

    expect(pluck($game_platforms, 'platform_id'))->toBe([40, 71]);
  });
});
