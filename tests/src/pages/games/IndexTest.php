<?php

require_once("tests/TestHelper.php");
require_once("src/repositories/games.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/games/index', function () {
  $platform = database_insert('platforms', []);

  $game_id = GamesRepository::create([
    'site_id' => 1,
    'notes' => '',
    'game_name' => "Test game",
    'mod_notes' => '',
    'game_patches' => '',
    'date_published' => database_now(),
    'all_charts' => 1,
    'all_subs' => 0,
  ]);
  database_insert('game_platforms', ['game_id' => $game_id, 'platform_id' => $platform]);
  database_insert('levels', [
    'game_id' => $game_id,
    'level_name' => "Test level",
    'level_rules' => "",
    'chart_type' => 0,
    'score_prefix' => '',
    'score_suffix' => '',
    'chart_type2' => 0,
    'score_prefix2' => '',
    'score_suffix2' => '',
    'entity_lock' => '',
    'level_pos' => 0,
    'name_pri' => '',
    'name_sec' => '',
    'name1' => '',
    'name2' => '',
    'name3' => '',
  ]);

  global $_SESSION;
  $_SESSION = [];

  ob_start();
  require("src/pages/games/index.php");

  $contents = ob_get_contents();
  ob_end_clean();

  $crawler = new Crawler($contents);

  expect($crawler->filter('main #allgames tr')->count())
    ->toBe(1 + database_value("select count(1) from games where site_id != 4", []));
});
