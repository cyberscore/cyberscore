<?php

require_once("tests/TestHelper.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/home/index', function () {
  database_insert('stats', [
    'users'     => 0,
    'games'     => 0,
    'records'   => 0,
    'charts'    => 0,
    'approvals' => 0,
  ]);

  $response = test_get("/");
  $crawler = new Crawler($response);

  expect($crawler->filter('main > .index-dashboard .scoreboard-widget')->count())->toBe(15);
});
