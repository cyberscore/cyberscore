<?php

require_once("tests/TestHelper.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/users/show/trophy', function () {
  $user = fake_user(['auth_verified' => true, 'banned' => false]);

  // create a bunch of games with groups and levels, including chart modifiers, platforms, etc
  // create a bunch of submissions from this user to those games

  CurrentUser::load_from_user_id($user['user_id']);

  $contents = test_get("/users/{$user['user_id']}/trophy");
  $crawler = new Crawler($contents);

  expect(http_response_code())->toBe(200);
  expect($crawler->filter('main .user-show-contents tr')->count())->toBeGreaterThan(4);
})->skip();
