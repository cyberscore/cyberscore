<?php

require_once("tests/TestHelper.php");

use Symfony\Component\DomCrawler\Crawler;

test('pages/users/settings/password', function () {
  $user = fake_user(['auth_verified' => true, 'banned' => false]);

  CurrentUser::load_from_user_id($user['user_id']);

  $contents = test_get("/settings/password");
  $crawler = new Crawler($contents);

  expect($crawler->filter('main .user-settings-form-security')->count())->toBe(1);
});
