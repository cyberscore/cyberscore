# Cyberscore

The world’s foremost video game records community. Visit us at
[cyberscore.me.uk](https://cyberscore.me.uk).


## License

Cyberscore
Copyright (C) 2022  The Cyberscore Authors

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

See the LICENSE file for full details.


## Documentation

Cyberscore's Development is documented using 
[the inbuilt GitLab Wiki feature](https://gitlab.com/cyberscore/cyberscore/-/wikis/Cyberscore-Development-Wiki).

## Contributing

Cyberscore is Open-Source, but Merge functionality is only available to those with 
the Developer role according to Cyberscore's staff structure. We strongly recommend
getting in touch with us on [Discord](https://discord.gg/0ltq5mHzEqHnRc6N) if you
wish to contribute to Cyberscore's development.

