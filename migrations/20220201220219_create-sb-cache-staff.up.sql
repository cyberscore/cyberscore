CREATE TABLE sb_cache_staff(
  user_id MEDIUMINT(8) UNSIGNED PRIMARY KEY NOT NULL,
  scoreboard_pos INT NOT NULL,
  teamwork_power INT NOT NULL,
  total_tasks    INT NOT NULL,

  games_added               INT NOT NULL,
  record_investigated       INT NOT NULL,
  record_moved              INT NOT NULL,
  record_deleted            INT NOT NULL,
  photo_proof_approved      INT NOT NULL,
  video_proof_approved      INT NOT NULL,
  proof_refused             INT NOT NULL,
  rankbutton_uploaded       INT NOT NULL,
  support_request_archived  INT NOT NULL,
  support_request_deleted   INT NOT NULL,
  news_article_written      INT NOT NULL,
  record_reinstated         INT NOT NULL,
  record_reverted           INT NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT
);
