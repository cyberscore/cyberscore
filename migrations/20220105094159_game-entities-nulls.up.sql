ALTER TABLE game_entities MODIFY inherited_entity_id INT(11) NULL DEFAULT NULL;

UPDATE game_entities SET inherited_entity_id = NULL WHERE inherited_entity_id = 0;
