ALTER TABLE users ADD COLUMN user_status BOOLEAN AFTER banned;
UPDATE users SET user_status = (1 - banned) * 2;
ALTER TABLE users DROP COLUMN banned;
