SET SESSION sql_mode = 'ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
ALTER TABLE staff_history MODIFY end_date DATETIME NULL;
UPDATE staff_history SET end_date = NULL WHERE end_date = '0000-00-00 00:00:00';
