ALTER TABLE notification_prefs ADD COLUMN bool_followed_posted    BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_blog_unfollow      BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_blog_follow        BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_userpage_comments  BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_reply              BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_referral_created   BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_referral_rejected  BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_referral_confirmed BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_article_comment    BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_reported_rec       BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_new_game           BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_greq_comment       BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_new_grequest       BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_pstring_edited     BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_pstring_added      BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_supp               BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_proof_refused      BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN rec_deleted             BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN rec_reported            BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_rec_approved       BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_trophy_gained      BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_trophy_lost        BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
ALTER TABLE notification_prefs ADD COLUMN bool_ygb                BOOLEAN NOT NULL DEFAULT TRUE AFTER user_id;
UPDATE notification_prefs
SET bool_ygb                = (ygb = 'yes'),
    bool_trophy_lost        = (trophy_lost = 'yes'),
    bool_trophy_gained      = (trophy_gained = 'yes'),
    bool_rec_approved       = (rec_approved = 'yes'),
    rec_reported            = TRUE,
    rec_deleted             = TRUE,
    bool_proof_refused      = (proof_refused = 'yes'),
    bool_supp               = (supp = 'yes'),
    bool_pstring_added      = (pstring_added = 'yes'),
    bool_pstring_edited     = (pstring_edited = 'yes'),
    bool_new_grequest       = (new_grequest = 'yes'),
    bool_greq_comment       = (greq_comment = 'yes'),
    bool_new_game           = (new_game = 'yes'),
    bool_reported_rec       = (reported_rec = 'yes'),
    bool_article_comment    = (article_comment = 'yes'),
    bool_referral_confirmed = (referral_confirmed = 'yes'),
    bool_referral_rejected  = (referral_rejected = 'yes'),
    bool_referral_created   = (referral_created = 'yes'),
    bool_reply              = (reply = 'yes'),
    bool_userpage_comments  = (userpage_comments = 'yes'),
    bool_blog_follow        = (blog_follow = 'yes'),
    bool_blog_unfollow      = (blog_unfollow = 'yes'),
    bool_followed_posted    = (followed_posted = 'yes');
ALTER TABLE notification_prefs DROP COLUMN followed_posted;
ALTER TABLE notification_prefs DROP COLUMN blog_unfollow;
ALTER TABLE notification_prefs DROP COLUMN blog_follow;
ALTER TABLE notification_prefs DROP COLUMN userpage_comments;
ALTER TABLE notification_prefs DROP COLUMN reply;
ALTER TABLE notification_prefs DROP COLUMN referral_created;
ALTER TABLE notification_prefs DROP COLUMN referral_rejected;
ALTER TABLE notification_prefs DROP COLUMN referral_confirmed;
ALTER TABLE notification_prefs DROP COLUMN article_comment;
ALTER TABLE notification_prefs DROP COLUMN reported_rec;
ALTER TABLE notification_prefs DROP COLUMN new_game;
ALTER TABLE notification_prefs DROP COLUMN greq_comment;
ALTER TABLE notification_prefs DROP COLUMN new_grequest;
ALTER TABLE notification_prefs DROP COLUMN pstring_edited;
ALTER TABLE notification_prefs DROP COLUMN pstring_added;
ALTER TABLE notification_prefs DROP COLUMN supp;
ALTER TABLE notification_prefs DROP COLUMN proof_refused;
ALTER TABLE notification_prefs DROP COLUMN rec_approved;
ALTER TABLE notification_prefs DROP COLUMN trophy_gained;
ALTER TABLE notification_prefs DROP COLUMN trophy_lost;
ALTER TABLE notification_prefs DROP COLUMN ygb;
ALTER TABLE notification_prefs RENAME COLUMN bool_followed_posted    TO followed_posted;
ALTER TABLE notification_prefs RENAME COLUMN bool_blog_unfollow      TO blog_unfollow;
ALTER TABLE notification_prefs RENAME COLUMN bool_blog_follow        TO blog_follow;
ALTER TABLE notification_prefs RENAME COLUMN bool_userpage_comments  TO userpage_comments;
ALTER TABLE notification_prefs RENAME COLUMN bool_reply              TO reply;
ALTER TABLE notification_prefs RENAME COLUMN bool_referral_created   TO referral_created;
ALTER TABLE notification_prefs RENAME COLUMN bool_referral_rejected  TO referral_rejected;
ALTER TABLE notification_prefs RENAME COLUMN bool_referral_confirmed TO referral_confirmed;
ALTER TABLE notification_prefs RENAME COLUMN bool_article_comment    TO article_comment;
ALTER TABLE notification_prefs RENAME COLUMN bool_reported_rec       TO reported_rec;
ALTER TABLE notification_prefs RENAME COLUMN bool_new_game           TO new_game;
ALTER TABLE notification_prefs RENAME COLUMN bool_greq_comment       TO greq_comment;
ALTER TABLE notification_prefs RENAME COLUMN bool_new_grequest       TO new_grequest;
ALTER TABLE notification_prefs RENAME COLUMN bool_pstring_edited     TO pstring_edited;
ALTER TABLE notification_prefs RENAME COLUMN bool_pstring_added      TO pstring_added;
ALTER TABLE notification_prefs RENAME COLUMN bool_supp               TO supp;
ALTER TABLE notification_prefs RENAME COLUMN bool_proof_refused      TO proof_refused;
ALTER TABLE notification_prefs RENAME COLUMN bool_rec_approved       TO rec_approved;
ALTER TABLE notification_prefs RENAME COLUMN bool_trophy_gained      TO trophy_gained;
ALTER TABLE notification_prefs RENAME COLUMN bool_trophy_lost        TO trophy_lost;
ALTER TABLE notification_prefs RENAME COLUMN bool_ygb                TO ygb;
