ALTER TABLE user_prefs ADD COLUMN show_full_names_bool BOOLEAN;
ALTER TABLE user_prefs ADD COLUMN show_entity_icons_bool BOOLEAN;
ALTER TABLE user_prefs ADD COLUMN userpage_comments_bool BOOLEAN;
ALTER TABLE user_prefs ADD COLUMN show_friends_list_bool BOOLEAN;
ALTER TABLE user_prefs DROP COLUMN show_dlc;
ALTER TABLE user_prefs DROP COLUMN popups_shown;
UPDATE user_prefs
SET show_full_names_bool = (show_full_names = 'yes'),
    show_entity_icons_bool = (show_entity_icons = 'yes'),
    userpage_comments_bool = (userpage_comments = 'yes'),
    show_friends_list_bool = (show_friends_list = 'yes');
ALTER TABLE user_prefs DROP COLUMN show_full_names;
ALTER TABLE user_prefs DROP COLUMN show_entity_icons;
ALTER TABLE user_prefs DROP COLUMN userpage_comments;
ALTER TABLE user_prefs DROP COLUMN show_friends_list;
ALTER TABLE user_prefs RENAME COLUMN show_full_names_bool TO show_full_names;
ALTER TABLE user_prefs RENAME COLUMN show_entity_icons_bool TO show_entity_icons;
ALTER TABLE user_prefs RENAME COLUMN userpage_comments_bool TO userpage_comments;
ALTER TABLE user_prefs RENAME COLUMN show_friends_list_bool TO show_friends_list;
