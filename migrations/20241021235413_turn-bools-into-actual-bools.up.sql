ALTER TABLE users ADD COLUMN auth_verified_bool BOOLEAN AFTER auth_verified;
UPDATE users SET auth_verified_bool = (auth_verified = 'y');
ALTER TABLE users DROP COLUMN auth_verified;
ALTER TABLE users RENAME COLUMN auth_verified_bool TO auth_verified;
