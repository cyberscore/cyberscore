ALTER TABLE users DROP COLUMN mattian_user;
ALTER TABLE users DROP COLUMN mattian_rss;
ALTER TABLE users DROP COLUMN show_sifr;
ALTER TABLE users DROP COLUMN show_avatar;
ALTER TABLE user_prefs ADD COLUMN email_pm BOOLEAN;
ALTER TABLE user_prefs ADD COLUMN lastseen_hidden BOOLEAN;
UPDATE user_prefs INNER JOIN users ON user_prefs.user_id = users.user_id
SET user_prefs.email_pm = (users.email_pm = 'y'),
    user_prefs.lastseen_hidden = (users.lastseen_hidden = 'y');
ALTER TABLE users DROP COLUMN email_pm;
ALTER TABLE users DROP COLUMN lastseen_hidden;
