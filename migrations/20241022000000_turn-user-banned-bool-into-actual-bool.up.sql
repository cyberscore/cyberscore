ALTER TABLE users ADD COLUMN banned BOOLEAN AFTER user_status;
UPDATE users SET banned = (user_status != '2');
ALTER TABLE users DROP COLUMN user_status;
