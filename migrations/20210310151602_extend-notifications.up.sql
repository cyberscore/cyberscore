-- +-----------------+-------------------------------+------+-----+---------+----------------+
-- | Field           | Type                          | Null | Key | Default | Extra          |
-- +-----------------+-------------------------------+------+-----+---------+----------------+
-- | notification_id | int(10) unsigned              | NO   | PRI | NULL    | auto_increment |
-- | type            | enum('ygb','followed_posted') | NO   |     | NULL    |                |
-- | user_id         | int(10) unsigned              | NO   | MUL | NULL    |                |
-- | chart_id        | int(10) unsigned              | NO   |     | NULL    |                |
-- | note_time       | datetime                      | NO   |     | NULL    |                |
-- | note_seen       | enum('y','n')                 | NO   |     | n       |                |
-- | note_details    | text                          | NO   |     | NULL    |                |
-- | sender_user_id  | int(11)                       | NO   |     | NULL    |                |
-- +-----------------+-------------------------------+------+-----+---------+----------------+

ALTER TABLE notifications ADD COLUMN game_id int(11) NULL;
ALTER TABLE notifications ADD COLUMN game_request_id int(11) NULL;
ALTER TABLE notifications ADD COLUMN record_id int(11) NULL;
ALTER TABLE notifications ADD COLUMN support_id int(11) NULL;
ALTER TABLE notifications ADD COLUMN news_id int(11) NULL;
ALTER TABLE notifications ADD COLUMN referral_id int(11) NULL;
ALTER TABLE notifications ADD COLUMN target_user_id int(11) NULL;
ALTER TABLE notifications ADD COLUMN userpage_id int(11) NULL;
ALTER TABLE notifications MODIFY sender_user_id int(11) NULL;
ALTER TABLE notifications MODIFY chart_id int(11) NULL;

ALTER TABLE staff_tasks MODIFY tasks_type enum(
  'game_added',
  'record_investigated',
  'record_moved',
  'record_deleted',
  'proof_approved',
  'proof_refused',
  'rankbutton_uploaded',
  'support_request_archived',
  'support_request_deleted',
  'news_article_written',
  'record_reinstated',
  'record_reverted',
  'record_terminated'
) NOT NULL;

UPDATE notifications
SET sender_user_id = NULL
WHERE sender_user_id = 0;

-- new_game: backfill game_id
UPDATE notifications
SET game_id = (
  SELECT games.game_id
  FROM games
  WHERE games.game_id = notifications.note_details
)
WHERE notifications.type = 'new_game' AND game_id IS NULL;

-- new_game: backfill sender_user_id
UPDATE notifications
SET sender_user_id = (
  SELECT DISTINCT staff_tasks.user_id
  FROM staff_tasks
  WHERE staff_tasks.task_id = notifications.game_id
  AND tasks_type = 'game_added'
  -- Both nielske and Groudon are marked as having created this game.
  -- Picked one based on discord logs.
  AND (notifications.game_id != 1792 OR staff_tasks.user_id = 133)
  -- LIMIT 1
)
WHERE notifications.type = 'new_game' AND sender_user_id IS NULL;

-- new_grequest: backfill game_request_id
UPDATE notifications
SET game_request_id = (
  SELECT DISTINCT game_requests.gamereq_id
  FROM game_requests
  WHERE notifications.note_details = game_requests.game_name
    AND DATE(notifications.note_time) = game_requests.date_requested
    AND notifications.sender_user_id = game_requests.submitter_id
)
WHERE notifications.type = 'new_grequest' AND game_request_id IS NULL;

-- reported_rec: backfill record_id
UPDATE notifications
SET record_id = chart_id
WHERE notifications.type = 'reported_rec' AND record_id IS NULL;

-- rec_approved: backfill game_id
UPDATE notifications
SET game_id = (
  SELECT levels.game_id
  FROM levels
  WHERE notifications.chart_id = levels.level_id
)
WHERE notifications.type = 'rec_approved' AND game_id IS NULL;

-- rec_approved: backfill record_id
UPDATE notifications
SET record_id = (
  SELECT records.record_id
  FROM records
  WHERE notifications.chart_id = records.level_id
  AND notifications.user_id = records.user_id
  LIMIT 1
)
WHERE notifications.type = 'rec_approved' AND record_id IS NULL;

-- proof_refused: backfill game_id
UPDATE notifications
SET game_id = (
  SELECT levels.game_id
  FROM levels
  WHERE notifications.chart_id = levels.level_id
)
WHERE notifications.type = 'proof_refused' AND game_id IS NULL;

-- proof_refused: backfill record_id
UPDATE notifications
SET record_id = (
  SELECT records.record_id
  FROM records
  WHERE notifications.chart_id = records.level_id
  AND notifications.user_id = records.user_id
)
WHERE notifications.type = 'proof_refused' AND record_id IS NULL;

-- rec_deleted: backfill game_id
UPDATE notifications
SET game_id = (
  SELECT levels.game_id
  FROM levels
  WHERE notifications.chart_id = levels.level_id
)
WHERE notifications.type = 'rec_deleted' AND game_id IS NULL;

-- rec_reported: backfill game_id
UPDATE notifications
SET game_id = (
  SELECT levels.game_id
  FROM levels
  WHERE notifications.chart_id = levels.level_id
)
WHERE notifications.type = 'rec_reported' AND game_id IS NULL;

-- rec_reported: backfill record_id
UPDATE notifications
SET record_id = (
  SELECT records.record_id
  FROM records
  WHERE notifications.chart_id = records.level_id
  AND notifications.user_id = records.user_id
  LIMIT 1
)
WHERE notifications.type = 'rec_reported' AND record_id IS NULL;

-- supp: backfill support_id
UPDATE notifications
SET support_id = (
  SELECT support_id
  FROM support
  WHERE ABS(notifications.note_time - support.message_date) < 2
)
WHERE notifications.type = 'supp' AND support_id IS NULL;

-- article_comment: backfill news_id
UPDATE notifications
SET news_id = (
  SELECT news_id
  FROM news
  WHERE notifications.chart_id = news.news_id
)
WHERE notifications.type = 'article_comment' and news_id IS NULL;

-- followed_posted: backfill news_id
UPDATE notifications
SET news_id = chart_id
WHERE notifications.type = 'followed_posted' and news_id IS NULL;

-- greq_comment.game_request_id
UPDATE notifications
SET game_request_id = chart_id
WHERE notifications.type = 'greq_comment' and game_request_id IS NULL;

-- pstring_added.sender_user_id
UPDATE notifications
SET sender_user_id = (
  SELECT suggester_id
  FROM translation_pages
  WHERE translation_pages.key = notifications.note_details
  AND language_id = 1
)
WHERE notifications.type = 'pstring_added' and sender_user_id IS NULL;

-- ygb.sender_user_id
UPDATE notifications
SET sender_user_id = (
  SELECT user_id
  FROM records
  WHERE records.level_id = notifications.chart_id
  AND chart_pos = 1
  ORDER BY last_update ASC
  LIMIT 1
)
WHERE notifications.type = 'ygb' and sender_user_id IS NULL;

-- ygb.game_id
UPDATE notifications
SET game_id = (
  SELECT game_id
  FROM levels
  WHERE levels.level_id = notifications.chart_id
)
WHERE notifications.type = 'ygb' and game_id IS NULL;

-- referral_rejected.referral_id
UPDATE notifications
SET referral_id = (
  SELECT referral_id
  FROM referrals
  WHERE (referrals.referrer_id = notifications.user_id AND referrals.user_id = notifications.chart_id)
  OR (referrals.referrer_id = notifications.chart_id AND referrals.user_id = notifications.user_id)
)
WHERE notifications.type = 'referral_rejected' and referral_id IS NULL;

-- referral_confirmed.referral_id
UPDATE notifications
SET referral_id = (
  SELECT referral_id
  FROM referrals
  WHERE (referrals.referrer_id = notifications.user_id AND referrals.user_id = notifications.chart_id)
  OR (referrals.referrer_id = notifications.chart_id AND referrals.user_id = notifications.user_id)
)
WHERE notifications.type = 'referral_confirmed' and referral_id IS NULL;

-- reply.news_id
UPDATE notifications
SET news_id = chart_id
WHERE notifications.type = 'reply' and note_details = 'news' and news_id IS NULL;

UPDATE notifications
SET game_request_id = chart_id
WHERE notifications.type = 'reply' and note_details = 'game_requests' and game_request_id IS NULL;

UPDATE notifications
SET target_user_id = chart_id
WHERE notifications.type = 'reply' and note_details = 'userpage' and target_user_id IS NULL;
