ALTER TABLE user_prefs ADD COLUMN preferred_chart_icon VARCHAR(255) AFTER chart_icon_id;

UPDATE user_prefs SET preferred_chart_icon = 'proof-gemstones@10000' WHERE chart_icon_id = 1;
UPDATE user_prefs SET preferred_chart_icon = 'proof-gemstones@5000'  WHERE chart_icon_id = 2;
UPDATE user_prefs SET preferred_chart_icon = 'proof-gemstones@2500'  WHERE chart_icon_id = 3;
UPDATE user_prefs SET preferred_chart_icon = 'proof-gemstones@1000'  WHERE chart_icon_id = 4;
UPDATE user_prefs SET preferred_chart_icon = 'proof-gemstones@500'   WHERE chart_icon_id = 5;
UPDATE user_prefs SET preferred_chart_icon = 'proof-gemstones@250'   WHERE chart_icon_id = 6;
UPDATE user_prefs SET preferred_chart_icon = 'proof-gemstones@100'   WHERE chart_icon_id = 7;

UPDATE user_prefs SET preferred_chart_icon = 'ambassador-trophies@2' WHERE chart_icon_id = 8;
UPDATE user_prefs SET preferred_chart_icon = 'ambassador-trophies@5' WHERE chart_icon_id = 9;
UPDATE user_prefs SET preferred_chart_icon = 'ambassador-trophies@10' WHERE chart_icon_id = 10;
UPDATE user_prefs SET preferred_chart_icon = 'ambassador-trophies@25' WHERE chart_icon_id = 11;

UPDATE user_prefs SET preferred_chart_icon = 'total-submission-sashes@500'   WHERE chart_icon_id = 12;
UPDATE user_prefs SET preferred_chart_icon = 'total-submission-sashes@1000'  WHERE chart_icon_id = 13;
UPDATE user_prefs SET preferred_chart_icon = 'total-submission-sashes@2500'  WHERE chart_icon_id = 14;
UPDATE user_prefs SET preferred_chart_icon = 'total-submission-sashes@5000'  WHERE chart_icon_id = 15;
UPDATE user_prefs SET preferred_chart_icon = 'total-submission-sashes@10000' WHERE chart_icon_id = 16;
UPDATE user_prefs SET preferred_chart_icon = 'total-submission-sashes@20000' WHERE chart_icon_id = 17;

UPDATE user_prefs SET preferred_chart_icon = 'standard-submission-sashes@500' WHERE chart_icon_id = 18;
UPDATE user_prefs SET preferred_chart_icon = 'standard-submission-sashes@1000' WHERE chart_icon_id = 19;
UPDATE user_prefs SET preferred_chart_icon = 'standard-submission-sashes@2500' WHERE chart_icon_id = 20;
UPDATE user_prefs SET preferred_chart_icon = 'standard-submission-sashes@5000' WHERE chart_icon_id = 21;
UPDATE user_prefs SET preferred_chart_icon = 'standard-submission-sashes@10000' WHERE chart_icon_id = 22;
UPDATE user_prefs SET preferred_chart_icon = 'standard-submission-sashes@20000' WHERE chart_icon_id = 23;

UPDATE user_prefs SET preferred_chart_icon = 'arcade-submission-sashes@500'   WHERE chart_icon_id = 24;
UPDATE user_prefs SET preferred_chart_icon = 'arcade-submission-sashes@1000'  WHERE chart_icon_id = 25;
UPDATE user_prefs SET preferred_chart_icon = 'arcade-submission-sashes@2500'  WHERE chart_icon_id = 26;
UPDATE user_prefs SET preferred_chart_icon = 'arcade-submission-sashes@5000'  WHERE chart_icon_id = 27;
UPDATE user_prefs SET preferred_chart_icon = 'arcade-submission-sashes@10000' WHERE chart_icon_id = 28;
UPDATE user_prefs SET preferred_chart_icon = 'arcade-submission-sashes@20000' WHERE chart_icon_id = 29;

UPDATE user_prefs SET preferred_chart_icon = 'speedrun-submission-sashes@500'   WHERE chart_icon_id = 30;
UPDATE user_prefs SET preferred_chart_icon = 'speedrun-submission-sashes@1000'  WHERE chart_icon_id = 31;
UPDATE user_prefs SET preferred_chart_icon = 'speedrun-submission-sashes@2500'  WHERE chart_icon_id = 32;
UPDATE user_prefs SET preferred_chart_icon = 'speedrun-submission-sashes@5000'  WHERE chart_icon_id = 33;
UPDATE user_prefs SET preferred_chart_icon = 'speedrun-submission-sashes@10000' WHERE chart_icon_id = 34;
UPDATE user_prefs SET preferred_chart_icon = 'speedrun-submission-sashes@20000' WHERE chart_icon_id = 35;

UPDATE user_prefs SET preferred_chart_icon = 'challenge-submission-sashes@500'   WHERE chart_icon_id = 36;
UPDATE user_prefs SET preferred_chart_icon = 'challenge-submission-sashes@1000'  WHERE chart_icon_id = 37;
UPDATE user_prefs SET preferred_chart_icon = 'challenge-submission-sashes@2500'  WHERE chart_icon_id = 38;
UPDATE user_prefs SET preferred_chart_icon = 'challenge-submission-sashes@5000'  WHERE chart_icon_id = 39;
UPDATE user_prefs SET preferred_chart_icon = 'challenge-submission-sashes@10000' WHERE chart_icon_id = 40;
UPDATE user_prefs SET preferred_chart_icon = 'challenge-submission-sashes@20000' WHERE chart_icon_id = 41;

UPDATE user_prefs SET preferred_chart_icon = 'unranked-submission-sashes@500' WHERE chart_icon_id = 42;
UPDATE user_prefs SET preferred_chart_icon = 'unranked-submission-sashes@1000' WHERE chart_icon_id = 43;
UPDATE user_prefs SET preferred_chart_icon = 'unranked-submission-sashes@2500' WHERE chart_icon_id = 44;
UPDATE user_prefs SET preferred_chart_icon = 'unranked-submission-sashes@5000' WHERE chart_icon_id = 45;
UPDATE user_prefs SET preferred_chart_icon = 'unranked-submission-sashes@10000' WHERE chart_icon_id = 46;
UPDATE user_prefs SET preferred_chart_icon = 'unranked-submission-sashes@20000' WHERE chart_icon_id = 47;

UPDATE user_prefs SET preferred_chart_icon = 'starboard-rosettes@1' WHERE chart_icon_id = 48;
UPDATE user_prefs SET preferred_chart_icon = 'starboard-rosettes@2' WHERE chart_icon_id = 49;
UPDATE user_prefs SET preferred_chart_icon = 'starboard-rosettes@3' WHERE chart_icon_id = 50;

UPDATE user_prefs SET preferred_chart_icon = 'standard-rosettes@1' WHERE chart_icon_id = 51;
UPDATE user_prefs SET preferred_chart_icon = 'standard-rosettes@2' WHERE chart_icon_id = 52;
UPDATE user_prefs SET preferred_chart_icon = 'standard-rosettes@3' WHERE chart_icon_id = 53;

UPDATE user_prefs SET preferred_chart_icon = 'trophy-rosettes@1' WHERE chart_icon_id = 54;
UPDATE user_prefs SET preferred_chart_icon = 'trophy-rosettes@2' WHERE chart_icon_id = 55;
UPDATE user_prefs SET preferred_chart_icon = 'trophy-rosettes@3' WHERE chart_icon_id = 56;

UPDATE user_prefs SET preferred_chart_icon = 'solution-rosettes@1' WHERE chart_icon_id = 57;
UPDATE user_prefs SET preferred_chart_icon = 'solution-rosettes@2' WHERE chart_icon_id = 58;
UPDATE user_prefs SET preferred_chart_icon = 'solution-rosettes@3' WHERE chart_icon_id = 59;

UPDATE user_prefs SET preferred_chart_icon = 'proof-rosettes@1' WHERE chart_icon_id = 60;
UPDATE user_prefs SET preferred_chart_icon = 'proof-rosettes@2' WHERE chart_icon_id = 61;
UPDATE user_prefs SET preferred_chart_icon = 'proof-rosettes@3' WHERE chart_icon_id = 62;

UPDATE user_prefs SET preferred_chart_icon = 'vproof-rosettes@1' WHERE chart_icon_id = 63;
UPDATE user_prefs SET preferred_chart_icon = 'vproof-rosettes@2' WHERE chart_icon_id = 64;
UPDATE user_prefs SET preferred_chart_icon = 'vproof-rosettes@3' WHERE chart_icon_id = 65;

UPDATE user_prefs SET preferred_chart_icon = 'submissions-rosettes@1' WHERE chart_icon_id = 66;
UPDATE user_prefs SET preferred_chart_icon = 'submissions-rosettes@2' WHERE chart_icon_id = 67;
UPDATE user_prefs SET preferred_chart_icon = 'submissions-rosettes@3' WHERE chart_icon_id = 68;

UPDATE user_prefs SET preferred_chart_icon = 'arcade-rosettes@1' WHERE chart_icon_id = 69;
UPDATE user_prefs SET preferred_chart_icon = 'arcade-rosettes@2' WHERE chart_icon_id = 70;
UPDATE user_prefs SET preferred_chart_icon = 'arcade-rosettes@3' WHERE chart_icon_id = 71;

UPDATE user_prefs SET preferred_chart_icon = 'speedrun-rosettes@1' WHERE chart_icon_id = 72;
UPDATE user_prefs SET preferred_chart_icon = 'speedrun-rosettes@2' WHERE chart_icon_id = 73;
UPDATE user_prefs SET preferred_chart_icon = 'speedrun-rosettes@3' WHERE chart_icon_id = 74;

UPDATE user_prefs SET preferred_chart_icon = 'challenge-rosettes@1' WHERE chart_icon_id = 75;
UPDATE user_prefs SET preferred_chart_icon = 'challenge-rosettes@2' WHERE chart_icon_id = 76;
UPDATE user_prefs SET preferred_chart_icon = 'challenge-rosettes@3' WHERE chart_icon_id = 77;

UPDATE user_prefs SET preferred_chart_icon = 'proof-percent-gemstones@100' WHERE chart_icon_id = 78;
UPDATE user_prefs SET preferred_chart_icon = 'proof-percent-gemstones@75' WHERE chart_icon_id = 79;
UPDATE user_prefs SET preferred_chart_icon = 'proof-percent-gemstones@50' WHERE chart_icon_id = 80;
UPDATE user_prefs SET preferred_chart_icon = 'proof-percent-gemstones@25' WHERE chart_icon_id = 81;
