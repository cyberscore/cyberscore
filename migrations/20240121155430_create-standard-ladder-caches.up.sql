CREATE TABLE gsb_cache_ladder (
  cache_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

  user_id        MEDIUMINT(8) UNSIGNED NOT NULL,
  game_id        SMALLINT(5) UNSIGNED NOT NULL,
  split_index    INT(10) UNSIGNED NOT NULL,
  dlc_index      TINYINT(4) NOT NULL,

  scoreboard_pos INT NOT NULL,
  sr             DOUBLE NOT NULL,
  percentage     DOUBLE NOT NULL,
  num_subs       INT UNSIGNED NOT NULL,
  num_approved   INT UNSIGNED NOT NULL,
  num_approved_v INT UNSIGNED NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT,
  FOREIGN KEY (game_id) REFERENCES games(game_id) ON DELETE RESTRICT
);

CREATE TABLE sb_cache_ladder (
  cache_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,

  user_id        MEDIUMINT(8) UNSIGNED NOT NULL,

  scoreboard_pos INT NOT NULL,
  sr             DOUBLE NOT NULL,
  num_subs       INT UNSIGNED NOT NULL,
  num_approved   INT UNSIGNED NOT NULL,
  num_approved_v INT UNSIGNED NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE RESTRICT
);