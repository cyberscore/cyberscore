CREATE TABLE auto_proofer_usages(
  auto_proofer_usage_id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT,
  user_id INT UNSIGNED NOT NULL,
  record_id INT UNSIGNED NOT NULL,
  chart_id INT UNSIGNED NOT NULL,
  game_id SMALLINT UNSIGNED NOT NULL,

  file_path VARCHAR(255) NOT NULL,
  created_at DATETIME NOT NULL
);
