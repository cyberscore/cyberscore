CREATE TABLE internal_events(
  internal_event_id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  user_id MEDIUMINT UNSIGNED NULL,
  user_ip VARCHAR(48) NOT NULL,
  request_uuid VARCHAR(36) NOT NULL,
  request_method VARCHAR(8) NOT NULL,
  request_uri TEXT NOT NULL,

  name VARCHAR(255) NOT NULL,
  properties JSON NOT NULL,

  created_at DATETIME NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE SET NULL
);

CREATE INDEX internal_events_name ON internal_events(name);
