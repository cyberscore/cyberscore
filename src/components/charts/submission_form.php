<?php
$record_inputs = $cs->GetRecordInputs(
  $chart_info['level_id'],
  $current_user['user_id'],
  $chart_info['chart_type'],
  $chart_info['chart_type2'],
  $chart_info['game_id']);

[$def_entity_ids, $def_platform_id, $chosen_patch, $input1, $input2, $def_comment, $rec_status, $extra1, $extra2, $extra3, $def_other_user, $record_exists] = $record_inputs;

$investigation = $rec_status == 1 || $rec_status == 2 || $rec_status == 5 || $rec_status == 6;

$results = database_get_all(database_select("
  SELECT submission, rank_name
  FROM game_ranks
  WHERE game_id = ?
  ORDER BY submission DESC
", 'i', [$game_id]));

$ranks = index_by($results, 'submission', 'rank_name');

$patch_list = $t->GetGamePatches(database_single_value("
  SELECT game_patches FROM games WHERE game_id = ?
", 's', [$game_id]));
?>
<script type="text/javascript">
  window.addEventListener("DOMContentLoaded", function () {
    let submissionForm = document.querySelector('.labelled-form');

    submissionForm.querySelectorAll('input[type=number]').forEach(input => {
      input.addEventListener('keydown', function(e) {
        if (e.keyCode == 38 || e.keyCode == 40) {
          e.preventDefault();
        }
      });
    });

    submissionForm.addEventListener('submit', function(event) {
      let form = event.target;

      if (form.entity_id1.value == -1 || form.entity_id2.value == -1 || form.entity_id3.value == -1 || form.entity_id4.value == -1 || form.entity_id5.value == -1) {
        event.preventDefault();
        alert('Please select an entity.');
      };
    });
  });
</script>

<form action="/records" method="post" enctype="multipart/form-data" class="labelled-form">
  <input type="hidden" name="level_id" value="<?= h($chart_info['level_id']) ?>" />

  <?php if ($investigation) { ?>
    <div class='labelled-form-separator'>
      <p><?= $t['chart_error_investigation'];?></p>
    </div>
  <?php } else { ?>
    <?php if ($chart_info['conversion_factor'] != 0) { ?>
      <div class='labelled-form-separator'>
        <p><?= $t['chart_notice_conversion'] ?></p>
      </div>
    <?php } ?>

    <?php if ($chart_info['conversion_factor'] != 0) { ?>
      <div class="labelled-form-label">
        <label><?= $chart_info['name_pri'] ?>:</label>
      </div>
      <div class="labelled-form-value">
        <?= chart_submit_submission_tags($chart_info, true, $input1, $ranks) ?>
      </div>

      <div class="labelled-form-label">
        <label><?= $chart_info['name_sec'] ?>:</label>
      </div>
      <div class="labelled-form-value">
        <?= chart_submit_submission_tags($chart_info, false, $input2, $ranks) ?>
      </div>
    <?php } else { ?>
      <div class="labelled-form-label">
        <div><?= $chart_info['name_pri'] ?>:</div>
      </div>
      <div class="labelled-form-value">
        <?= chart_submit_submission_tags($chart_info, true, $input1, $ranks) ?>
      </div>

      <?php if ($chart_info['chart_type2'] != 0) { ?>
        <div class="labelled-form-label">
          <div><?= $chart_info['name_sec'] ?>:</div>
        </div>
        <div class="labelled-form-value">
          <?= chart_submit_submission_tags($chart_info, false, $input2, $ranks) ?>
        </div>
      <?php } ?>
    <?php } ?>

    <?php if ($chart_info['name1'] != "") { ?>
      <div class="labelled-form-label">
        <label for="extra1"><?= $chart_info['name1'] ?>:</label>
      </div>
      <div class="labelled-form-value">
        <input type="text" name="extra1" id="extra1" value="<?= $extra1 ?>" size="6" autocomplete="off" />
      </div>
    <?php } ?>

    <?php if ($chart_info['name2'] != "") { ?>
      <div class="labelled-form-label">
        <label for="extra2"><?= $chart_info['name2'] ?>:</label>
      </div>
      <div class="labelled-form-value">
        <input type="text" name="extra2" id="extra2" value="<?= $extra2 ?>" size="6" autocomplete="off" />
      </div>
    <?php } ?>

    <?php if ($chart_info['name3'] != "") { ?>
      <div class="labelled-form-label">
        <label for="extra3"><?= $chart_info['name3'] ?>:</label>
      </div>
      <div class="labelled-form-value">
        <input type="text" name="extra3" id="extra3" value="<?= $extra3 ?>" size="6" autocomplete="off" />
      </div>
    <?php } ?>

    <?php if (count($platforms) > 1) { ?>
      <div class="labelled-form-label">
        <label for="platform_id"><?= $t['general_platform'] . $t['general_colon'];?></label>
      </div>
      <div class="labelled-form-value">
        <select name="platform_id" id="platform_id">
          <option value="-1"><?= h($t['chart_select_platform'] . $t['general_colon']) ?></option>

          <?php foreach ($platforms as $platform) { ?>
            <option <?php if ($platform['platform_id'] == $def_platform_id) echo 'selected="selected" ';?>value="<?= $platform['platform_id'] ?>">
              <?= $platform['platform_name'] ?>
            </option>
          <?php } ?>
        </select>
      </div>
    <?php } else { ?>
      <?php $platform = $platforms[0] ?>
      <div class="labelled-form-label">
        <input type="hidden" name="platform_id" value="<?= $platform['platform_id'] ?>">
        <?= $t['general_platform'] . $t['general_colon'] ?>
      </div>
      <div class="labelled-form-value">
        <?= $platform['platform_name'] ?>
      </div>
    <?php } ?>

    <?php if (count($patch_list) > 0) { ?>
      <div class="labelled-form-label">
        <label for="game_patch"><?= h($t['general_game_patch'] . $t['general_colon']) ?></label>
      </div>
      <div class="labelled-form-value">
        <select name="game_patch" id="game_patch">
          <option value=""><?= $t['chart_select_patch'] . $t['general_colon'] ?></option>
          <?php foreach ($patch_list as $patch) { ?>
            <option <?= option_selected($patch, $chosen_patch) ?> value="<?= h($patch) ?>"><?= h($patch) ?></option>
          <?php } ?>
        </select>
      </div>
    <?php } ?>

    <?php if (count($entities[1]) == 1) { ?>
      <div class="labelled-form-label">
        <?php $entity = $entities[1][0]; ?>
        <input type="hidden" name="entity_id1" value="<?= $entity['game_entity_id'] ?>">
        <?= $t['chart_using'] ?>
      </div>
      <div class="labelled-form-value">
        <?= h($t->GetEntityName($entity['game_entity_id'])) ?>
      </div>
    <?php } ?>

    <?php for ($i = 1; $i <= $chart_info['max_entities']; $i++) { ?>
      <?php if (count($entities[$i]) > 1) { ?>
        <div class="labelled-form-value">
          <select name="entity_id<?= $i ?>" id="entity_id<?= $i ?>">
            <option value="-1"><?= $t['chart_select_entity'] . $t['general_colon'] ?></option>
            <?php if ($chart_info['min_entities'] < $i) { ?>
              <option <?php if (0 == $def_entity_ids[$i]) echo 'selected="selected" ';?>value=""></option>
            <?php } ?>

            <?php foreach ($entities[$i] as $entity) {
              if ($entity['inherited_entity_id'] != 0) {
                $parent_entity = database_get(database_select("SELECT game_entity_id, entity_name FROM game_entities WHERE game_entity_id = ?", 's', [$entity['inherited_entity_id']]));
                $entity['game_entity_id2'] = $parent_entity['game_entity_id'];
                $entity['entity_name'] = $parent_entity['entity_name'];
            ?>
                <option <?php if($entity['game_entity_id'] == $def_entity_ids[$i]) echo 'selected="selected" ';?>value="<?= $entity['game_entity_id'] ?>"><?= h($t->GetEntityName($entity['game_entity_id2'])) ?></option>
              <?php } else { ?>
                <option <?php if ($entity['game_entity_id'] == $def_entity_ids[$i]) echo 'selected="selected" ';?>value="<?= $entity['game_entity_id'] ?>"><?= h($t->GetEntityName($entity['game_entity_id'])) ?></option>
              <?php } ?>
            <?php } ?>
          </select>
        </div>
      <?php } ?>
    <?php } ?>

    <?php if ($chart_info['players'] >= 2) { ?>
      <div class='labelled-form-separator'>
        <b><?= $t['chart_notice_multi_1'] ?></b>
        <b><?= $t['chart_notice_multi_2'] ?></b>
      </div>
      <div class="labelled-form-label">
        <label for="other_user"><?= $t['chart_other_users'] ?></label>
      </div>
      <div class="labelled-form-value">
        <input type="text" name="other_user" id="other_user" value="<?= $def_other_user ?>" size="15" autocomplete="off" />
      </div>
    <?php } ?>

    <div class="labelled-form-label">
      <label for="comment"><?= $t['chart_comment'] ?></label>
    </div>
    <div class="labelled-form-value">
      <textarea name="comment" id="comment" rows="2" maxlength="300"><?= h($def_comment) ?></textarea>
    </div>
  <?php } ?>

  <div class="labelled-form-separator">
    <?= $t['chart_proof_cyberscore'] ?>
  </div>
  <div class="labelled-form-value">
    <input type="file" name="proof_file" size="30" />
  </div>

  <div class="labelled-form-separator">
    <strong>OR</strong>
    <?= $t['chart_proof_link'] ?>
  </div>
  <div class="labelled-form-value">
    <input type="text" name="proof" autocomplete="off" />
  </div>

  <div class='labelled-form-separator'>
    <input type="submit" value="Submit" />
  </div>
</form>
