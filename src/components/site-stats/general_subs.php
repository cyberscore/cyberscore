<?php

render_component_template('site-stats/general_subs', [
  'record_stats' => database_get(database_select("
    SELECT
      COUNT(*) AS all_records,
      SUM(IF(rec_status IN(1,5),1,0)) AS pending_records,
      SUM(IF(rec_status IN(2,6),1,0)) AS investigated_records,
      SUM(IF(rec_status = 3,1,0)) AS approved_records,
      SUM(IF((rec_status = 3 AND proof_type = 'p'),1,0)) AS photo_proofs,
      SUM(IF((rec_status = 3 AND proof_type = 'v'),1,0)) AS video_proofs,
      SUM(IF((rec_status = 3 AND proof_type = 's'),1,0)) AS livestream_proofs,
      SUM(IF((rec_status = 3 AND proof_type = 'r'),1,0)) AS replay_proofs
    FROM records
  ", '', [])),
  'num_proof_linked' => database_single_value("SELECT COUNT(record_id) FROM records WHERE records.rec_status = 3 AND linked_proof != ''", '', []),
]);
