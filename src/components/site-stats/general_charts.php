<?php

render_component_template('site-stats/general_charts', [
  'total_charts' => database_single_value("SELECT COUNT(level_id) AS num_levels FROM levels", '', []),
  'subbed_charts' => database_single_value("SELECT COUNT(DISTINCT level_id) AS num_levels FROM records", '', []),
  'time_charts' => database_single_value("SELECT COUNT(level_id) FROM levels WHERE chart_type <= 2", '', []),
  'score_charts' => database_single_value("SELECT COUNT(level_id) FROM levels WHERE chart_type >= 3", '', []),
]);
