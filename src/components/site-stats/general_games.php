<?php

render_component_template('site-stats/general_games', [
  'num_games' => database_single_value("SELECT COUNT(game_id) FROM games WHERE site_id != 4", '', []),
  'game_plats' => database_get_all(database_select("
    SELECT platforms.manufacturer, platforms.platform_name, COUNT(game_platforms.game_id) AS num_games
    FROM game_platforms, platforms
    WHERE game_platforms.platform_id = platforms.platform_id AND game_platforms.original = 1
    GROUP BY platforms.platform_id
    ORDER BY num_games DESC
  ", '', [])),
]);
