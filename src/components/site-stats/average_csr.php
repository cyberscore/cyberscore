<?php
$highestaveragecsr = database_get_all(database_select("
  SELECT AVG(POW((records.csp) / 100,4)) AS score, COUNT(records.user_id) AS num_subs, user_id
  FROM records
  WHERE ranked = 1
  GROUP BY user_id
  ORDER BY score DESC
", '', []));
?>
<h3 class="middle">Average CSR per record (Limited to more than 50 submissions)</h3>
<table class="usergames">
  <?php $rank = 1; ?>
  <?php foreach ($highestaveragecsr as $user) { ?>
    <?php if ($user['num_subs'] > 50 && $rank <= 100) { ?>
      <?php $username = database_single_value("SELECT username FROM users WHERE user_id = ?", 's', [$user['user_id']]); ?>
      <tr>
        <td class="rank"><?= h(nth($rank)) ?></td>
        <td class="name"><a href="/user/<?= h($user['user_id']) ?>"><?= h($username) ?></a></td>
        <td class="number"><?= h(round($user['score'],3)) ?></td>
      </tr>
      <?php $rank++; ?>
    <?php } ?>
  <?php } ?>
</table>
