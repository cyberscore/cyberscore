<?php

render_component_template('site-stats/general_plats', [
  'remainingplats' => database_get_all(database_select("
    SELECT COUNT(1) AS count, YEAR(last_update) AS year
    FROM records
    WHERE platinum = 1
    GROUP BY YEAR(last_update)
    ORDER BY YEAR(last_update) ASC
  ", '', [])),
]);
