<?php

$subs_history = database_get_all(database_select("
  SELECT
    COUNT(*) AS num_subs,
    MIN(original_date) AS original_date,
    YEAR(original_date) AS year,
    MONTH(original_date) AS month
  FROM records
  WHERE YEAR(original_date) >= 2000
  GROUP BY year, month
  ORDER BY year DESC, month DESC
", '', []));

$result = database_get_all(database_select("
  SELECT COUNT(1) AS num_subs, YEAR(update_date) AS year, MONTH(update_date) AS month
  FROM record_history
  GROUP BY year, month
", '', []));

$subs_history_updates = [];
foreach ($result as $entry) {
  $subs_history_updates[$entry['year']][$entry['month']] = $entry['num_subs'];
}

render_component_template('site-stats/general_sub_history', [
  'subs_history' => $subs_history,
  'subs_history_updates' => $subs_history_updates,
]);
