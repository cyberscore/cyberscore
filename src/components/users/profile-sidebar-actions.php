<?php
$banned = (
  $user['banned'] &&
  $user['user_id'] != 16411 &&
  $user['user_id'] != 24634 &&
  $user['user_id'] != 25397
);

if ($current_user && $current_user['user_id'] != $user['user_id']) {
  $followed = database_single_value("
    SELECT user_id
    FROM user_blog_follows
    WHERE user_id = ? AND follow_id = ?
  ", 'ss', [$current_user['user_id'], $user['user_id']]);

  $friend = database_single_value("
    SELECT user_id
    FROM user_friends
    WHERE user_id = ? AND friend_id = ?
  ", 'ss', [$current_user['user_id'], $user['user_id']]);
}
?>
<?php if ($banned) { ?>
  <h2 style="color:red;"><?= h($t['profile_banned']) ?></h2>
<?php } ?>

<a
  href="/dashboard.php?id=<?= h($user['user_id']) ?>"
  style="text-decoration:none;"
>
  <img
    class="icon"
    src="<?= skin_image_url("dashboard.png") ?>"
    height="20"
    alt="<?= h($t['profile_view_dashboard']) ?>"
    title="<?= h($t['profile_view_dashboard']) ?>"
  />
</a>

<a
  href="/records/new?id=<?= h($user['user_id']) ?>"
  style="text-decoration:none;"
>
  <img
    class="icon"
    src="<?= skin_image_url("records_view_blue.png") ?>"
    height="20"
    alt="<?= h($t['loginbox_viewrecords']) ?>"
    title="<?= h($t['loginbox_viewrecords']) ?>"
  />
</a>

<?php if ($current_user && $current_user['user_id'] != $user['user_id']) { ?>
  <a
    href="<?= h(url_for_user_comparison($current_user['user_id'], $user['user_id'])) ?>"
    style="text-decoration:none;"
  >
    <img
      class="icon"
      src="/images/compare.png"
      height="20"
      alt="<?= h($t['compare_title']) ?>"
      title="<?= h($t['compare_title']) ?>"
    />
  </a>

  <a
    href="/csmail-messages/new?to=<?= h($user['user_id']) ?>"
    style="text-decoration:none;"
  >
    <img
      class="icon"
      src="<?= skin_image_url("inbox_larger_blue.png") ?>"
      height="20"
      alt="<?= h($t['profile_send_csmail']) ?>"
      title="<?= h($t['profile_send_csmail']) ?>"
    />
  </a>

  <?php if ($followed) { ?>
    <form method='post' action='/followings/<?= h($user['user_id']) ?>/delete'>
      <input
        type="image"
        src="<?= skin_image_url("unfollowuser.png") ?>"
        height="20"
        alt="<?= h($t['profilemodules_unfollow_blog']) ?>"
        title="<?= h($t['profilemodules_unfollow_blog']) ?>"
      />
    </form>
  <?php } else { ?>
    <form method='post' action='/followings'>
      <input type="hidden" name="user_id" value="<?= h($user['user_id']) ?>" />
      <input
        type="image"
        src="<?= skin_image_url("followuser.png") ?>"
        height="20"
        alt="<?= h($t['profilemodules_follow_blog']) ?>"
        title="<?= h($t['profilemodules_follow_blog']) ?>"
      />
    </form>
  <?php } ?>

  <?php if ($friend) { ?>
    <form action="/friendships/<?= h($user['user_id']) ?>/delete" method='post' >
      <?php $friend_icon = str_replace("[user]", $user['username'], $t['profilemodules_is_friend']); ?>
      <input
        type="image"
        class="icon"
        src="<?= skin_image_url('unfrienduser.png'); ?>"
        height="20"
        title="<?= h($friend_icon) ?>"
        alt="<?= h($friend_icon) ?>"
      />
    </form>
  <?php } else { ?>
    <form action="/friendships" method='post' >
      <input type='hidden' name='user_id' value='<?= h($user['user_id']) ?>' />
      <?php $friend_icon = str_replace("[user]", $user['username'], $t['profilemodules_is_not_friend']); ?>
      <input
        type="image"
        class="icon"
        src="<?= skin_image_url('frienduser.png') ?>"
        height="20"
        title="<?= h($friend_icon) ?>"
        alt="<?= h($friend_icon) ?>"
      />
    </form>
  <?php } ?>
<?php } ?>

<?php if (Authorization::has_access('UserMod')) { ?>
  <a
    href="/users/<?= h($user['user_id']) ?>/edit"
    style="text-decoration:none;"
  >
    <img
      class="icon"
      src="<?php echo skin_image_url('edituser.png'); ?>"
      height="20"
      alt="<?php echo $t['profile_edit_user']; ?>"
      title="<?php echo $t['profile_edit_user']; ?>"
    />
  </a>
<?php } ?>

<?php if (Authorization::has_access('Dev')) { ?>
  <form action="/impersonations" method='post'>
    <input type='hidden' value="<?= h($user['user_id']) ?>" name="user_id">
    <input type='submit' value="Impersonate">
  </form>
<?php } ?>
