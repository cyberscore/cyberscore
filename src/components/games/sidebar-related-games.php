<?php
$super_related_games = database_get_all(database_select("
  SELECT
    game_id,
    GROUP_CONCAT(game_platforms.platform_id, '_', platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ',') AS platforms
  FROM games
  JOIN game_platforms USING(game_id)
  JOIN platforms USING(platform_id)
  WHERE site_id <> 4 AND game_id IN (
    SELECT games_related.game_id
    FROM games_related
    WHERE games_related.related_game_id = ? AND super_related = 1
    UNION
    SELECT games_related.related_game_id AS game_id
    FROM games_related
    WHERE games_related.game_id = ? AND super_related = 1
  )
  AND game_platforms.original = 1
  GROUP BY game_id
  ORDER BY game_name ASC
", 'ss', [$game['game_id'], $game['game_id']]));

foreach ($super_related_games as &$g) {
  $g['platforms'] = array_map(function($e){return explode('_', $e);}, explode(',', $g['platforms']));
}
unset($g);

$related_games = database_get_all(database_select("
  SELECT game_id
  FROM games
  WHERE site_id <> 4 AND game_id IN (
    SELECT games_related.game_id FROM games_related WHERE games_related.related_game_id = ? AND super_related = 0
    UNION
    SELECT games_related.related_game_id AS game_id FROM games_related WHERE games_related.game_id = ? AND super_related = 0
  )
  ORDER BY game_name ASC
", 'ss', [$game['game_id'], $game['game_id']]));

$levels = implode(",", pluck(database_get_all(database_select("
  SELECT level_id
  FROM levels
  WHERE game_id = ?
", 's', [$game['game_id']])), 'level_id'));

$t->CacheGameNames();
?>

<?php if (count($super_related_games) > 0) { ?>
  <details class="sidebar-section" data-id="other-versions" open="" ontoggle="rememberDetailsState(this)">
        <summary class="sidebar-title"><?php echo $t['game_other_versions'];?></summary><br>

  <?php foreach ($super_related_games as $related_game) { ?>
    <div style="text-align:center;">
      <h3><?= link_to_game($related_game) ?></h3>

      <?php foreach($related_game['platforms'] as $platform) { ?>
        <img src="/uploads/platforms/<?= $platform[0] ?>.gif" alt="<?= h($platform[1]) ?>" title="<?= h($platform[1]) ?>" width="60" />
      <?php } ?>
    </div>
  <?php } ?>
      </details>
<?php } ?>

<?php if (count($related_games) > 0) { ?>
  <details class="sidebar-section--solution" data-id="related-games" open="" ontoggle="rememberDetailsState(this)">
        <summary class="sidebar-title"><?php echo $t['game_related_games'];?></summary><br>
  <ul class="arrows">
    <?php foreach ($related_games as $related_game) { ?>
      <li><?= link_to_game($related_game) ?></li>
    <?php } ?>
  </ul>
  </details>
<?php } ?>


