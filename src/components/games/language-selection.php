<?php
$languages = isset($game_id) ?
  $t->GetGameLanguages($game_id) :
  database_get_all(database_select("SELECT language_id, language_name, sub_language FROM languages WHERE language_id != 1 ORDER BY language_name LIKE 'English%' DESC, language_name ASC", '', []));

render_component_template('games/language-selection', [
  'languages' => $languages,
  'game_lang' => $t->GetGameLang(),
]);
