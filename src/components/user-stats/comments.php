<?php

$show_comments = database_single_value("
  SELECT userpage_comments FROM user_prefs WHERE user_id = ?
", 's', [$user['user_id']]) == "yes";

$comments = database_get_all(database_select("
  SELECT
    users.*,
    user_comments.*,
    user_comments.comment_time AS comment_date,
    IF(user_comments.comment_depth = 1, NULL, user_comments.reply_id) AS reply_id,
    'userpage' AS commentable_type,
    user_comments.userpage_id AS commentable_id
  FROM user_comments
  JOIN users USING(user_id)
  WHERE user_comments.userpage_id = ?
  ORDER BY user_comments.comment_time ASC
", 's', [$user['user_id']]));

$comments = tree_sort($comments, 'comment_id', 'reply_id');

render_component_template('user-stats/comments', [
  'user' => $user,
  'show_comments' => $show_comments,
  'comments' => $comments,
]);
