<?php

$friends = database_get_all(database_select("
  SELECT user_friends.friend_id AS user_id, users.username, users.forename, users.surname
  FROM user_friends
  JOIN users ON users.user_id = user_friends.friend_id
  WHERE user_friends.user_id = ?
  ORDER BY username ASC
", 's', [$user['user_id']]));

$friended = database_get_all(database_select("
  SELECT user_friends.user_id, user_friends.friend_id, users.username, users.forename, users.surname
  FROM user_friends, users
  WHERE user_friends.friend_id = ?
  AND users.user_id = user_friends.user_id
  ORDER BY username ASC
", 's', [$user['user_id']]));

$follows = database_get_all(database_select("
  SELECT user_blog_follows.follow_id AS user_id, users.username, users.forename, users.surname
  FROM user_blog_follows
  JOIN users ON users.user_id = user_blog_follows.follow_id
  WHERE user_blog_follows.user_id = ?
  ORDER BY username ASC
", 's', [$user['user_id']]));

$followers = database_get_all(database_select("
  SELECT user_blog_follows.user_id, user_blog_follows.follow_id, users.username, users.forename, users.surname
  FROM user_blog_follows, users
  WHERE user_blog_follows.follow_id = ? AND users.user_id = user_blog_follows.user_id
  ORDER BY username ASC
", 's', [$user['user_id']]));

$iamme = $current_user && $user['user_id'] == $current_user['user_id'];

render_component_template('user-stats/friends', [
  'user' => $user,
  'iamme' => $iamme,
  'friends' => $friends,
  'friended' => $friended,
  'follows' => $follows,
  'followers' => $followers,
]);
