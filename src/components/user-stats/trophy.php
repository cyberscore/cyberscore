<?php

$user_games = database_get_all(database_select("
  SELECT *
  FROM gsb_cache_trophy
  WHERE gsb_cache_trophy.user_id = ?
  ORDER BY trophy_points DESC
", 's', [$user['user_id']]));

foreach ($user_games as &$game) {
  $game['game_name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$boards = ['csp', 'standard', 'arcade', 'speedrun', 'solution', 'challenge', 'incremental', 'collectible'];

$trophy  = database_get(database_select("SELECT * FROM sb_cache_trophy WHERE user_id = ?", 's', [$user['user_id']]));
$type_totals = database_get(database_select("SELECT
SUM(csp_trophy_points) AS csp,
SUM(standard_trophy_points) AS standard,
SUM(arcade_trophy_points) AS arcade,
SUM(speedrun_trophy_points) AS speedrun,
SUM(solution_trophy_points) AS solution,
SUM(challenge_trophy_points) AS challenge,
SUM(incremental_trophy_points) AS incremental,
SUM(collectible_trophy_points) AS collectible,
SUM(CASE WHEN csp_trophy = 'platinum' THEN 1 END) AS platinum,
SUM(CASE WHEN csp_trophy = 'gold' OR csp_trophy = 'platinum' THEN 1 END) AS csp_gold,
SUM(CASE WHEN csp_trophy = 'silver' THEN 1 END) AS csp_silver,
SUM(CASE WHEN csp_trophy = 'bronze' THEN 1 END) AS csp_bronze,
SUM(CASE WHEN standard_trophy = 'gold' THEN 1 END) AS standard_gold,
SUM(CASE WHEN standard_trophy = 'silver' THEN 1 END) AS standard_silver,
SUM(CASE WHEN standard_trophy = 'bronze' THEN 1 END) AS standard_bronze,
SUM(CASE WHEN arcade_trophy = 'gold' THEN 1 END) AS arcade_gold,
SUM(CASE WHEN arcade_trophy = 'silver' THEN 1 END) AS arcade_silver,
SUM(CASE WHEN arcade_trophy = 'bronze' THEN 1 END) AS arcade_bronze,
SUM(CASE WHEN speedrun_trophy = 'gold' THEN 1 END) AS speedrun_gold,
SUM(CASE WHEN speedrun_trophy = 'silver' THEN 1 END) AS speedrun_silver,
SUM(CASE WHEN speedrun_trophy = 'bronze' THEN 1 END) AS speedrun_bronze,
SUM(CASE WHEN solution_trophy = 'gold' THEN 1 END) AS solution_gold,
SUM(CASE WHEN solution_trophy = 'silver' THEN 1 END) AS solution_silver,
SUM(CASE WHEN solution_trophy = 'bronze' THEN 1 END) AS solution_bronze,
SUM(CASE WHEN challenge_trophy = 'gold' THEN 1 END) AS challenge_gold,
SUM(CASE WHEN challenge_trophy = 'silver' THEN 1 END) AS challenge_silver,
SUM(CASE WHEN challenge_trophy = 'bronze' THEN 1 END) AS challenge_bronze,
SUM(CASE WHEN incremental_trophy = 'gold' THEN 1 END) AS incremental_gold,
SUM(CASE WHEN incremental_trophy = 'silver' THEN 1 END) AS incremental_silver,
SUM(CASE WHEN incremental_trophy = 'bronze' THEN 1 END) AS incremental_bronze,
SUM(CASE WHEN collectible_trophy = 'gold' THEN 1 END) AS collectible_gold,
SUM(CASE WHEN collectible_trophy = 'silver' THEN 1 END) AS collectible_silver,
SUM(CASE WHEN collectible_trophy = 'bronze' THEN 1 END) AS collectible_bronze
FROM gsb_cache_trophy
WHERE gsb_cache_trophy.user_id = ?
GROUP BY gsb_cache_trophy.user_id", 's', [$user['user_id']]));

render_component_template('user-stats/trophy', [
  'boards' => $boards,
  'trophy' => $trophy,
  'type_totals' => $type_totals,
  'user_games' => $user_games,
]);
