<?php
$sort = $_GET['sort'] ?? "percentage";
$order = 'percentage DESC, scoreboard_pos ASC, style_points DESC';
switch ($sort) {
case 'position':         $order = 'scoreboard_pos ASC, percentage DESC'; break;
case 'num_subs':         $order = 'num_subs DESC'; break;
case 'num_approved':	   $order = 'num_approved DESC, num_approved_v DESC, num_subs DESC'; break;
case 'num_approved_v':	 $order = 'num_approved_v DESC, num_approved DESC, num_subs DESC'; break;
case 'style_points':     $order = 'style_points DESC'; break;
case 'percentage':       $order = 'percentage DESC, scoreboard_pos ASC, style_points DESC'; break;
default:                 $order = 'percentage DESC, scoreboard_pos ASC, style_points DESC'; break;
}

$user_games = database_get_all(database_select("
  SELECT
    games.*,
    games.challenge_charts AS eligible_charts,
    scoreboard_pos, num_subs, style_points, num_approved, num_approved_v, percentage
  FROM gsb_cache_userchallenge
  LEFT JOIN games USING (game_id)
  WHERE user_id = ?
  ORDER BY $order
", 's', [$user['user_id']]));

$t->CacheGameNames();
foreach ($user_games as &$game) {
  $game['game_name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$cache = database_find_by('sb_cache_challenge', ['user_id' => $user['user_id']]);

$bars = [];
$values = [];
foreach ($user_games as &$game) {
  $bars []= [
    ['class' => '', 'value' => 100 * $game['num_subs'] / $game['eligible_charts']],
  ];
  $values []= [
    ['width' => 44, 'value' => number_format($game['style_points'], 1)],
  ];
}
unset($game);

render_component_template('user-stats/submissions-table', [
  'user' => $user,
  'cache' => $cache,
  'user_games' => $user_games,

  'chart_status' => 'challenge',
  'default_color' => 'challenge',
  'scoreboard' => 'challenge',
  'scoreboard_label' => t('general_challenge_table'),
  'columns' => [
    ['width' => 44, 'icon' => '/images/icon_challenge.png', 'alt' => t('general_challenge_points_plural'), 'sort' => 'style_points'],
  ],
  'totals' => [
    number_format($cache['cp'], 1),
  ],
  'total_bars' => [
    ['class' => 'subs_bar_challenge', 'value' => 100],
  ],
  'bars' => $bars,
  'values' => $values,
]);
