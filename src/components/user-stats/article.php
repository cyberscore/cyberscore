<?php

$a = new Article();
$featured_article_id = database_single_value("
  SELECT featured_article_id FROM user_info WHERE user_id = ?
", 's', [$user['user_id']]);

$news = database_get(database_select("
  SELECT news_id, news_date, article_type, username, user_id, edit_date, edit_user_id
  FROM news
  WHERE news_id = ?
", 's', [$featured_article_id]));

if ($news) {
  $news['headline'] = Article::RetrieveHeadline($news['news_id']);
  $news['text'] = Article::RetrieveText($news['news_id']);
  if ($news['edit_user_id']) {
    $news['editor_username'] = database_single_value("SELECT username from users WHERE user_id = ?", 's', [$news['edit_user_id']]);
  }

  $comments = database_get_all(database_select("
    SELECT
      users.*,
      news_comments.*,
      'news' AS commentable_type,
      news_comments.news_id AS commentable_id,
      news_comments.news_comments_id AS comment_id
    FROM news_comments
    JOIN users USING(user_id)
    WHERE news_comments.news_id = ?
    ORDER BY news_comments.comment_date ASC
  ", 's', [$news['news_id']]));
  $news['comments'] = tree_sort($comments, 'comment_id', 'reply_id');
}

render_component_template('user-stats/article', [
  'post' => $news,
]);
