<?php
$csr_rank              = database_single_value("SELECT scoreboard_pos FROM sb_cache             WHERE user_id = ?", 's', [$user['user_id']]);
$medal_rank            = database_single_value("SELECT scoreboard_pos FROM sb_cache_standard    WHERE user_id = ?", 's', [$user['user_id']]);
$trophy_rank           = database_single_value("SELECT scoreboard_pos FROM sb_cache_trophy      WHERE user_id = ?", 's', [$user['user_id']]);
$arcade_rank           = database_single_value("SELECT scoreboard_pos FROM sb_cache_arcade      WHERE user_id = ?", 's', [$user['user_id']]);
$speedrun_rank         = database_single_value("SELECT scoreboard_pos FROM sb_cache_speedrun    WHERE user_id = ?", 's', [$user['user_id']]);
$solution_rank         = database_single_value("SELECT scoreboard_pos FROM sb_cache_solution    WHERE user_id = ?", 's', [$user['user_id']]);
$challenge_rank        = database_single_value("SELECT scoreboard_pos FROM sb_cache_challenge   WHERE user_id = ?", 's', [$user['user_id']]);
$proof_rank            = database_single_value("SELECT scoreboard_pos FROM sb_cache_proof       WHERE user_id = ?", 's', [$user['user_id']]);
$vproof_rank           = database_single_value("SELECT scoreboard_pos FROM sb_cache_video_proof WHERE user_id = ?", 's', [$user['user_id']]);
$subs_rank             = database_single_value("SELECT scoreboard_pos FROM sb_cache_total_subs  WHERE user_id = ?", 's', [$user['user_id']]);
$rainbow_rank          = database_single_value("SELECT scoreboard_pos FROM sb_cache_rainbow     WHERE user_id = ?", 's', [$user['user_id']]);
$incremental_rank      = database_single_value("SELECT scoreboard_pos FROM sb_cache_collectible WHERE user_id = ?", 's', [$user['user_id']]);
$collectible_rank      = database_single_value("SELECT scoreboard_pos FROM sb_cache_incremental WHERE user_id = ?", 's', [$user['user_id']]);

$user_games = database_get_all(database_select("
  SELECT games.game_id, games.game_name
  FROM games
  JOIN gsb_cache_csp USING(game_id)
  WHERE gsb_cache_csp.user_id = ?
  ORDER BY games.site_id ASC, gsb_cache_csp.scoreboard_pos ASC
", 's', [$user['user_id']]));

render_component_template('user-stats/rankbuttons', [
  'user' => $user,
  'csr_rank' => $csr_rank,
  'medal_rank' => $medal_rank,
  'trophy_rank' => $trophy_rank,
  'arcade_rank' => $arcade_rank,
  'speedrun_rank' => $speedrun_rank,
  'solution_rank' => $solution_rank,
  'challenge_rank' => $challenge_rank,
  'proof_rank' => $proof_rank,
  'vproof_rank' => $vproof_rank,
  'subs_rank' => $subs_rank,
  'rainbow_rank' => $rainbow_rank,
  'incremental_rank' => $incremental_rank,
  'collectible_rank' => $collectible_rank,
  'user_games' => $user_games,
]);
