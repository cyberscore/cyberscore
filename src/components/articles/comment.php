<?php
  // expects: $comment
  $margin = ($comment['comment_depth'] - 1) * 20;
?>
<div class='comments-comment display' style="margin-left: <?= $margin ?>px">
  <div class="comments_userpic">
    <?= user_avatar_image_tag($comment, ['width' => 70, 'height' => 70]) ?>
    <br />
    <?= link_to_user($comment) ?>
    <?= user_star_image_tags($comment) ?>
    <br />
    <?= $t->FormatDateTime($comment['comment_date'], 'datetime') ?>
  </div>

  <div class="comments_functions">
    <?php if ($current_user) { ?>
      <a onclick="toggle_comment_mode(this, 'reply')">
        <img src="/images/Reply_new.png" height="24" title="Reply to comment" />
      </a>
    <?php } ?>

    <?php if (Authorization::has_access('GlobalMod', $comment['user_id'])) { ?>
      <a onclick="toggle_comment_mode(this, 'edit')">
        <img src="/images/Edit_new.png" height="24" title="Edit comment" />
      </a>
    <?php } ?>

    <?php if (Authorization::has_access('UserMod')) { ?>
      <form action="/comments/delete" method="post">
        <?= hidden_field('commentable_type', $comment) ?>
        <?= hidden_field('comment_id', $comment) ?>

        <input type="image" src="/images/delete_new.png" height="24" alt="Delete comment" />
      </form>
    <?php } ?>
  </div>

  <div class="comments_content comments_reply">
    <img src="/images/x.png" width=16 height=16 style="position: relative; left: 49%" onclick="toggle_comment_mode(this, 'display')">
    <br />
    <form action="/comments/create" method="post" onsubmit="this.elements['submit'].disabled=true;">
      <?= hidden_field('commentable_type', $comment) ?>
      <?= hidden_field('commentable_id', $comment) ?>
      <?= hidden_field('comment_id', $comment) ?>

      <textarea style="width: 80%; height: 70px;" placeholder="Remember, be nice!" name="comment"></textarea>
      <br />
      <input type="submit" name="submit" value="<?= h($t['general_post_comment']) ?>" />
    </form>
  </div>

  <div class="comments_content comments_edit">
    <img src="/images/x.png" width=16 height=16 style="position: relative; left: 49%" onclick="toggle_comment_mode(this, 'display')" />
    <br />
    <form action="/comments/update" method="post" onsubmit="this.elements['submit'].disabled=true;">
      <?= hidden_field('comment_id', $comment) ?>
      <?= hidden_field('commentable_type', $comment) ?>

      <textarea style="width: 80%; height: 70px;" name="comment"><?= $comment['comment'] ?></textarea>
      <br />
      <input type="submit" name="submit" value="Edit comment" />
    </form>
  </div>

  <div class="comments_content comments_message">
    <?= prettify($comment['comment']) ?>
  </div>
</div>
