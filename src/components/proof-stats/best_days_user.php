<?php
$best_user_days = database_get_all(database_select("
  SELECT
    DATE_FORMAT(approval_date, '%Y-%m-%d') AS date,
    users.username,
    COUNT(*) AS num_approved
  FROM record_approvals
  JOIN users USING(user_id)
  GROUP BY date, users.user_id, users.username
  ORDER BY COUNT(*) DESC
  LIMIT 50
", '', []));

render_component_template('proof-stats/best_days_user', [
  'best_user_days' => $best_user_days,
]);
