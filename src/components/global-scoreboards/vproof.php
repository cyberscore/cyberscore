<?php
  $template_entry = [
    'user_id' => 0,
    'num_subs' => 0,
    'total' => 0,
    'medal' => 0,
    'speedrun' => 0,
    'solution' => 0,
    'unranked' => 0,
    'incremental' => 0,
    'collectible' => 0,
    'challenge' => 0,
    'arcade' => 0,
  ];
?>
<tr>
  <th colspan="4"></th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => 'total']) ?>">
      <img src="/images/icon_proof2.png" height="75" alt="Video approved submissions"><br />
      <span tippy>
        <?= h($t['scoreboards_total_subs_video_approved']) ?>
        <div tippy-content>
          Sort by number of approved scores
        </div>
      </div>
    </a>
    (<a href="<?= current_url_with_query(['manual_sort' => 'percentage']) ?>">
      <span tippy>
        %
        <div tippy-content>Sort by percentage of approved scores</div>
      </span>
    </a>)
  </th>
</tr>
<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="total" style="text-align: center; padding-right: 1em; padding-top: 1em;">
      <canvas
          class="proof_chart"
          width="350"
          height="50"
          data-json="<?= h(json_encode(array_intersect_key($entry, $template_entry))) ?>"
      ></canvas>
      <small>
        <b><?= number_format($entry['total'], 0) ?></b>
        of
        <b><?= number_format($entry['num_subs'], 0) ?></b>
        (<?= number_format(100 * $entry['total'] / $entry['num_subs'], 2) ?>%)
      </small>
    </td>
  </tr>
<?php } ?>
<script>
const labels = [
  "Video approved standard submissions",
  "Video approved arcade submissions",
  "Video approved speedrun submissions",
  "Video approved solution submissions",
  "Video approved unranked submissions",
  "Video approved collectible submissions",
  "Video approved incremental submissions",
  "Video approved challenge submissions",
  "Video unproven submissions"
];

const colors = [
  "rgba(26, 108, 150, 1)", // standard
  "rgba(229, 116, 0, 1)",  // arcade
  "rgba(8, 113, 20, 1)",   // speedrun
  "rgba(255, 0, 0, 1)",    // solution
  "#000000",               // unranked
  "#1fbe96",               // collectible
  "#ceba05",               // incremental
  "#9700DB",               // challenge
  "rgba(0, 0, 0, 0.3)",    // unproven
];

const best = Math.max(...Array.from(document.querySelectorAll(".proof_chart")).map((chart) =>
  JSON.parse(chart.dataset.json).num_subs
));

Array.from(document.querySelectorAll(".proof_chart")).forEach((chart) => {
  const entry = JSON.parse(chart.dataset.json);

  const data = [
    entry.medal,
    entry.arcade,
    entry.speedrun,
    entry.solution,
    entry.unranked,
    entry.collectible,
    entry.incremental,
    entry.challenge,
    entry.num_subs - entry.total,
  ];

  // alternative representation using a stacked horizontal bar chart
  if (true) {
    var datasets = [];
    for (var i = 0; i < labels.length; i++) {
      datasets[i] = {
        label: labels[i],
        data: [data[i]],
        backgroundColor: [colors[i]],
        hoverBackgroundColor: [colors[i]],
      };
    }

    new Chart(chart, {
      type: 'horizontalBar',
      data: { datasets },
      options: {
        layout: { padding: { left: -10, bottom: 0, top: 10 } },
        legend: { display: false },
        tooltips: { mode: 'point' },
        scales: {
          xAxes: [{
            barPercentage: 1.0,
            categoryPercentage: 1.0,
            stacked: true,
            gridLines: {
                display: false
            },
            ticks: {
              display: false,
              min: 0,
              padding: 0,
              max: Number(data.reduce((a,b) => Number(a) + Number(b))),
            }
          }],
          yAxes: [{
            barPercentage: 1.0,
            categoryPercentage: 1.0,
            stacked: true,
            gridLines: {
                display: false
            },
            ticks: { display: false, padding: 0 },
          }],
        },
      }
    });
  } else {
    const datasets = [{
      data,
      backgroundColor: colors,
    }];

    new Chart(chart, {
      type: 'doughnut',
      data: { labels, datasets },
      options: {
        rotation: 0.9 * Math.PI,
        circumference: 1.2 * Math.PI,
        cutoutPercentage: 60,
        legend: {
          display: false
        },
        animation: {
          animateRotate: true
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        }
      }
    });
  }
});
</script>
