<tr>
  <th colspan="4"></th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => '1']) ?>">
      1st
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => '2']) ?>">
      2nd
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => '3']) ?>">
      3rd
    </a>
  </th>
  <th class="medals">
    <a href="<?= current_url_with_query(['manual_sort' => '4']) ?>">
      4th - 10th
    </a>
  </th>

</tr>
<?php foreach ($entries as $entry) { ?>
  <tr <?= $entry['tr_class'] ?>>
    <td class="pos"><?= $entry['pos_content'] ?></td>
    <td class="flag"><?= country_flag_image_tag($entry) ?></td>
    <td class="userpic"><?= user_avatar_image_tag($entry) ?></td>
    <td class="name">
      <a href="/user/<?= h($entry['user_id']) ?>"><?= $entry['display_name'] ?></a>
      <?= user_star_image_tags($entry) ?>
    </td>
    <td class="medals"><?= h(number_format($entry['1st'])) ?></td>
    <td class="medals"><?= h(number_format($entry['2nd'])) ?></td>
    <td class="medals"><?= h(number_format($entry['3rd'])) ?></td>
    <td class="medals"><?= h(number_format($entry['4th'])) ?></td>
  </tr>
<?php } ?>
