<?php

$scoreboard = $cs->GetGameMedalTable($game['game_id']);
$scoreboard = array_slice($scoreboard, 0, 5);

render_component_template('game-scoreboards/standard-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
]);
