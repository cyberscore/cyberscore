<?= h($t['leaderboards_unranked_group']) ?><br>
<?php echo str_replace(array('[url]', '[/url]'), array('<a href="https://www.videogamesrecords.net/">', '</a>'), $t['leaderboards_vgr']); ?>

<table class="scoreboard" id="scoreboard_classic">
  <tr>
    <th colspan="4"></th>
    <th class="medals"><img src="<?= skin_image_url('platinummedal.png') ?>" width="40" height="40" alt="platinum" /></th>
    <th class="medals"><img src="<?= skin_image_url('goldmedal.png') ?>" width="40" height="40" alt="gold medal" /></th>
    <th class="medals"><img src="<?= skin_image_url('silvermedal.png') ?>" width="40" height="40" alt="silver medal" /></th>
    <th class="medals"><img src="<?= skin_image_url('bronzemedal.png') ?>" width="40" height="40" alt="bronze medal" /></th>
  </tr>
  <?php foreach ($scoreboard['entries'] as $g) { ?>
    <tr<?= $g['tr_class'] ?>>
      <td class="pos">
        <?php if(is_numeric($g['pos_content'])) { echo nth($g['pos_content']); } 
              else echo $g['pos_content']; ?>

      <td class="flag"><?= country_flag_image_tag($g) ?></td>
      <td class="userpic"><?= user_avatar_image_tag($g) ?></td>
      <td class="name">
        <a href="/user/<?= h($g['user_id']) ?>"><?= $g['display_name'] ?></a>
        <?= user_star_image_tags($g) ?>
        <br />
      </td>
      <td class="medals"><?= h(number_format($g['platinum'])) ?></td>
      <td class="medals"><?= h(number_format($g['gold'])) ?></td>
      <td class="medals"><?= h(number_format($g['silver'])) ?></td>
      <td class="medals"><?= h(number_format($g['bronze'])) ?></td>
    </tr>
  <?php } ?>
</table>
