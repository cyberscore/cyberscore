<?php

$scoreboard = $cs->GetGameScoreboard($game['game_id']);
$scoreboard = array_filter($scoreboard, fn($entry) => $entry['scoreboard_pos'] == 1);
$boards = ['csp', 'standard', 'arcade', 'speedrun', 'solution', 'challenge', 'incremental', 'collectible'];

$is_csp = !empty($scoreboard);

if (empty($scoreboard)) {
  $scoreboard = $cs->GetGameTrophyCase($game['game_id']);
  $scoreboard = array_filter($scoreboard, fn($entry) => $entry['scoreboard_pos'] == 1);
}

foreach ($scoreboard as &$leader) {
  $trophies_to_display = [];

  $trophies = database_fetch_one("SELECT * FROM gsb_cache_trophy WHERE user_id = ? AND game_id = ?", [$leader['user_id'], $game['game_id']]);

  foreach ($boards AS $board) {
    switch($board) {
      case 'csp': $scoreboard_name = "Game scoreboard"; break;
      case 'standard': $scoreboard_name = "Game medal table"; break;
      case 'arcade': $scoreboard_name = "Game arcade board"; break;
      case 'speedrun': $scoreboard_name = "Game speedrun table"; break;
      case 'solution': $scoreboard_name = "Game solution hub"; break;
      case 'challenge': $scoreboard_name = "Game user challenge board"; break;
      case 'incremental': $scoreboard_name = "Game experience table"; break;
      case 'collectible': $scoreboard_name = "Game collector's cache"; break;
    }

    if ($trophies[$board.'_trophy'] == 'platinum') {
      $trophies_to_display []= [
        'title' => $scoreboard_name . ' - Platinum trophy',
        'number' => 1,
      ];
    } else if ($trophies[$board . '_trophy'] == 'gold') {
      $trophies_to_display []= [
        'title' => $scoreboard_name . ' - Gold trophy',
        'number' => 2,
      ];
    } else if ($trophies[$board . '_trophy'] == 'silver') {
      $trophies_to_display []= [
        'title' => $scoreboard_name . ' - Silver trophy',
        'number' => 3,
      ];
    } else if ($trophies[$board . '_trophy'] == 'bronze') {
      $trophies_to_display []= [
        'title' => $scoreboard_name . ' - Bronze trophy',
        'number' => 4,
      ];
    }
  }

  $leader['trophies'] = $trophies;
  $leader['trophy_case'] = $trophies_to_display;
}
unset($leader);

render_component_template('game-scoreboards/leader-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
  'is_csp' => $is_csp,
]);
