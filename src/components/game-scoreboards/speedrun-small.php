<?php

$scoreboard = $cs->GetGameSpeedrunTable($game['game_id']);
$scoreboard = array_slice($scoreboard, 0, 3);

render_component_template('game-scoreboards/speedrun-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
]);
