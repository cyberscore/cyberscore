<?php

$scoreboard = $cs->GetGameSolutionboard($game['game_id']);
$scoreboard = array_slice($scoreboard, 0, 3);

render_component_template('game-scoreboards/solution-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
]);
