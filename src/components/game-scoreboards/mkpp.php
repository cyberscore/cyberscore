<?= h($t['leaderboards_unranked_group']) ?><br>
<?php echo str_replace(array('[url]', '[/url]'), array('<a href="https://www.mariokart64.com/combinedranks/">', '</a>'), $t['leaderboards_mkpp']);
?>

<table class="scoreboard" id="scoreboard_classic">
<tr>
    <th colspan="4"></th>
    <th class="medals">AF</th>
</tr>
  <?php foreach($scoreboard['entries'] as $g) { ?>
    <tr<?= $g['tr_class'] ?>>
      <td class="pos"><?= $g['pos_content'] ?></td>
      <td class="flag"><?= country_flag_image_tag($g) ?></td>
      <td class="userpic"><?= user_avatar_image_tag($g) ?></td>
      <td class="name">
        <a href="/user/<?= h($g['user_id']) ?>"><?= $g['display_name'] ?></a>
        <?= user_star_image_tags($g) ?>
        <br />
        <?php if (isset($g['num_subs'])) { ?>
          <small><?= h($g['num_subs'] == 1 ? $t['general_submission_singular'] : str_replace('[subs]', $g['num_subs'], $t['general_submission_plural'])) ?></small>
        <?php } ?>
      </td>
      <td class="medals">
        <?= h(number_format($g['score'], 4)) ?>

      </td>
    </tr>
  <?php } ?>
</table>
