<?php
$num_charts = database_single_value("
  SELECT COUNT(1)
  FROM levels
  JOIN chart_modifiers2 USING(level_id)
  WHERE NOT unranked AND game_id = ?
", 's', [$game['game_id']]);
?>
<table class="scoreboard" id="scoreboard_classic">
  <?php foreach ($scoreboard['entries'] as $g) { ?>
    <tr<?= $g['tr_class'] ?>>

      <!-- Trophy and/or scoreboard position -->
      <td class="pos">
        <?php if(is_numeric($g['pos_content'])) { echo nth($g['pos_content']); } 
              else echo $g['pos_content']; ?>

      <!-- Trophy Points -->
      <?php if ($g['csp_trophy_points'] > 0) { ?>
      <br>
      <small>
          <img src="<?= skin_image_url('trophy_points_trophy.png') ?>" width="10" height="10" alt="<?= $t['general_trophy_points'] ?>" title="<?= $t['general_trophy_points'] ?>" /> 
          <?= number_format($g['csp_trophy_points'], 0) ?>
      </small>
        <?php } ?>
      </td>

      <!-- User identity -->
      <td class="flag"><?= country_flag_image_tag($g) ?></td>
      <td class="userpic"><?= user_avatar_image_tag($g) ?></td>
      <td class="name">
        <a href="/user/<?= h($g['user_id']) ?>"><?= $g['display_name'] ?></a>
        <?= user_star_image_tags($g) ?>
        <br />

        <!-- Submission & Proof display -->
        <?php render_component_with('game-scoreboards/counters', ['game_id' => $game['game_id'], 'counter' => $g, 'chart_type' => 'all_ranked']); ?>
      </td>
        <td class="data" style="text-align: left;">
          <?= number_format($g['total_csp'], 2) ?> <?= $t['general_csp'] ?>
        <small>
          <br /><img src="/images/CSStar.png" width="10" height="10" alt="CSR" />  <?= number_format($g['total_csr'], 2) ?> <?= $t['general_csr'] ?>
            <?php
              [$bonus_csr, $pos_factor, $sub_factor, $proof_factor, $video_proof_factor] = Award::BonusCSR(
                $g['total_csr'], $g['rpc'], $g['scoreboard_pos'], $g['num_subs'], $num_charts, $g['num_approved'], $g['num_approved_v']
              );
            ?>
            <span style="color:green">
              <br>
              <span class="tooltip_bonuscsr" tippy data-tippy-theme="bonuscsr">
                +<?= number_format($g['bonus_csr'], 2) ?> <?= $t['general_bonus_csr'] ?>
                <span class="tooltiptext" tippy-content>
                  <div class='bonus-csr-tooltip'>
                  <?= $t['game_submitted'] ?>: <?= number_format(100 * $sub_factor, 2) ?>%
                  <br><?= number_format(100 * $proof_factor * 3, 2) ?>% <?= $t['viewrecords_records_approved'] ?>: +<?= number_format(100 * $proof_factor, 2) ?>%
                  <br><?= number_format(100 * $video_proof_factor * 6, 2) ?>% <?= $t['scoreboards_total_subs_video_approved'] ?>: +<?= number_format(100 * $video_proof_factor, 2) ?>%
                  <br><?= $t['scoreboards_position'] ?> (<?= nth($g['scoreboard_pos']) ?>): x<?= number_format(100 * $pos_factor, 2) ?>%
                  <br><?= $t['scoreboards_final_modifier'] ?> = <?= number_format(100 * $pos_factor * ($sub_factor + $proof_factor + $video_proof_factor), 2) ?>% 
                  <br><br><?= number_format(100 * $pos_factor * ($sub_factor + $proof_factor + $video_proof_factor), 2) ?>% * <?= number_format($g['total_csr'], 2)?> <?= $t['general_csr'] ?> 
                  <br>= <b>+<?= number_format($g['bonus_csr'], 2) ?> <?= $t['general_bonus_csr'] ?></b>
                  <br><br><b><a href="/mechanics/bonus_csr" alt="Read more about Bonus CSR" title="Read more about Bonus CSR">Read more about Bonus CSR</a></b>
                </div>
                </span>
              </span>
            </span>
        </small>
      </td>
    </tr>
  <?php } ?>
</table>
