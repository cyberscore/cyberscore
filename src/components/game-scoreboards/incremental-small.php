<?php

$scoreboard = $cs->GetGameScoreboardIncremental($game['game_id']);
$scoreboard = array_slice($scoreboard, 0, 3);

render_component_template('game-scoreboards/incremental-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
]);
