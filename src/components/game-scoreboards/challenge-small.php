<?php

$scoreboard = $cs->GetGameChallengeBoard($game['game_id']);
$scoreboard = array_slice($scoreboard, 0, 3);

render_component_template('game-scoreboards/challenge-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
]);
