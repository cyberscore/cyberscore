<table class="scoreboard" id="scoreboard_classic">
  <?php foreach ($scoreboard['entries'] as $g) { ?>
    <tr<?= $g['tr_class'] ?>>

          <!-- Trophy and/or scoreboard position -->
          <td class="pos">
        <?php if(is_numeric($g['pos_content'])) { echo nth($g['pos_content']); } 
              else echo $g['pos_content']; ?>

      <!-- Trophy Points -->
      <?php if ($g['incremental_trophy_points'] > 0) { ?>
      <br>
      <small>
          <img src="<?= skin_image_url('trophy_points_trophy.png') ?>" width="10" height="10" alt="<?= $t['general_trophy_points'] ?>" title="<?= $t['general_trophy_points'] ?>" /> 
          <?= number_format($g['incremental_trophy_points'], 0) ?>
      </small>
        <?php } ?>
      </td>

      <td class="flag"><?= country_flag_image_tag($g) ?></td>
      <td class="userpic"><?= user_avatar_image_tag($g) ?></td>
      <td class="name">
        <a href="/user/<?= h($g['user_id']) ?>"><?= $g['display_name'] ?></a>
        <?= user_star_image_tags($g) ?>
        <br />

        <!-- Submission & Proof display -->
        <?php render_component_with('game-scoreboards/counters', ['game_id' => $game['game_id'], 'counter' => $g, 'chart_type' => 'incremental']); ?>
      </td>
      <td class="data">
        <?= h(number_format($g['cxp'],1)) ?> <?= h($t['general_cxp']) ?>
      </td>
      <td class="data">
        <?= h(number_format($g['vs_cxp'],0)) ?> <?= h($t['general_vs_cxp']) ?>
      </td>
    </tr>
  <?php } ?>
</table>

