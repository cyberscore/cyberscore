<?php

$scoreboard = $cs->GetGameCollectorsCache($game['game_id']);
$scoreboard = array_slice($scoreboard, 0, 3);

render_component_template('game-scoreboards/collectible-small', [
  'game' => $game,
  'scoreboard' => $scoreboard,
]);
