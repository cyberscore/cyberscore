<div class="notification-item__icon">
  <i class="xxx fas fa-user-friends"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_new_referral'] ?></strong>
  <br>
  <?= str_replace(['[user]', '[user 2]'], [$notification['referred_username'], $notification['referrer_username']], $t['notifications_type_new_referral_2']) ?>
</div>
