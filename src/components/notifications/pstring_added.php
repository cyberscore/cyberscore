<div class="notification-item__icon">
  <i class="xxx fas fa-globe-africa"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_pstring_added'] ?></strong>
  <br>
  <?= str_replace(['[name]', '[string]'], [h(first_line($notification['sender_username'])), "<q>" . h($notification['note_details']) . "</q>"], $t['notifications_type_pstring_added_2']) ?>
</div>
