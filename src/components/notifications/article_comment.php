<div class="notification-item__icon">
  <i class="xxx far fa-comment"></i>
</div>

<div class="notification-item__message">
  <strong>
    <?= $notification['sender_username'] ?> <?= $t['notifications_type_article_comment'] ?>
  </strong>
  <br>
  <?= str_replace('[name]', "<q>" . h($notification['news_headline']) . "</q>", $t['notifications_type_article_comment_2']) ?>
</div>
