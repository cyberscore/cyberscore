<div class="notification-item__icon">
  <i class="xxx fas fa-globe-asia"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_pstring_edited'] ?></strong>
  <br>

  <?= str_replace(['[name]', '[string]'], [h(first_line($notification['sender_username'])), "<q>" . h($notification['note_details']) . "</q>"], $t['notifications_type_pstring_edited_2']) ?>
</div>
