<div class="notification-item__icon">
  <i class="xxx fas fa-gamepad"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_new_grequest'] ?></strong>
  <br>
  <?= str_replace(['[username]', '[game_name]'], [h($notification['sender_username']), h(first_line($notification['game_request_name']))], $t['notifications_type_new_grequest_2']) ?>
</div>
