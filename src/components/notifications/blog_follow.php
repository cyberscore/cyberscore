<div class="notification-item__icon">
  <i class="xxx far fa-eye"></i>
</div>

<div class="notification-item__message">
  <strong><?= h(str_replace('[username]', $notification['sender_username'], $t['notifications_type_blog_follow'])) ?></strong>
  <br>
</div>
