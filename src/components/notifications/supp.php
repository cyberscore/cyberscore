<div class="notification-item__icon">
  <i class="xxx far fa-question-circle"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_support'] ?></strong>
  <br>
  <?= str_replace('[user]', $notification['sender_username'] ?? $t['notifications_type_support_3'], $t['notifications_type_support_2']) ?>
</div>
