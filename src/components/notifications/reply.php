<div class="notification-item__icon">
  <i class="xxx far fa-comments"></i>
</div>

<div class="notification-item__message">
  <strong>
    <?= h(str_replace('[user]', $notification['sender_username'], $t['notifications_type_reply'])) ?>
  </strong>
  <br>
  <?php if ($notification['note_details'] == 'news') { ?>
    <?= str_replace('[name]', "<q>".h($notification['news_headline'])."</q>", $t['notifications_type_reply_a']) ?>
  <?php } elseif ($notification['note_details'] == 'game_requests') { ?>
    <?= str_replace('[game]', "<q>".h(first_line($notification['game_request_name']))."</q>", $t['notifications_type_reply_gr']) ?>
  <?php } elseif ($notification['note_details'] == 'userpage') { ?>
    <?= str_replace('[name]', $notification['userpage_username'], $t['notifications_type_reply_u']) ?>
  <?php } ?>
</div>
