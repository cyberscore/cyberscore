<div class="notification-item__icon">
  <i class="xxx far fa-comment-alt"></i>
</div>

<div class="notification-item__message">
  <strong><?= $t['notifications_type_greq_comment_2'] ?></strong>
  <br>
  <?= str_replace(['[user]', '[request]'], [h($notification['sender_username']), h(first_line($notification['game_request_name']))], $t['notifications_type_greq_comment']) ?>
</div>
