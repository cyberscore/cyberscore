<?php

$flash = Flash::$messages;
Flash::Clear();

render_component_template('notifications', [
  'errors' => $flash['error'] ?? [],
  'successes' => $flash['success'] ?? [],
  'unverified' => $current_user && !User::IsVerified($current_user),
]);
