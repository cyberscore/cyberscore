<?php

$user_ids = [];
foreach ($scoreboard['entries'] as $level) {
  $user_ids = array_merge($user_ids, explode(",", $level['user_ids']));
}
$users = index_by(UsersRepository::where(['user_id' => $user_ids]), 'user_id');

foreach ($scoreboard['entries'] as &$level) {
  $level['users'] = array_map(fn($id) => $users[$id], explode(",", $level['user_ids']));
}
unset($level);
?>
<?php if (count($scoreboard['entries']) > 0) { ?>
  <p><?= h($t['groupstats_leaderboard_intro']) ?></p>

  <table class="zebra">
    <?php foreach ($scoreboard['entries'] as $g) { ?>
      <?php
        $class = Modifiers::FetchChartFlagColouration($g['level_id']);
        $group_class = Modifiers::GroupColouration(database_single_value("SELECT ranked FROM level_groups WHERE group_id = ?", 's', [$g['group_id']]));
      ?>
      <tr>
        <td>
          <span class="chartname_<?= $group_class ?>">
          <?= link_to_game_group($g) ?> </span> &rarr;
          <span class="chartname_<?= $class ?>">
          <?php if ($g['players'] > 1) { ?>[<?= $g['players'] ?>P]<?php } ?>
          <?= link_to_game_chart($g, ['class' => "chartname_$class"]) ?>
          </span>
          <br />
          <?= h($g['num_first']) ?>
            <?php if ($g['platinum'] && $class == 'standard') { ?>
              <img src="/images/icon-platinumstar.png" width="16" height="16" align="absmiddle" title="Platinum" />
            <?php } else if ($class == 'standard') { ?>
              <img src="/images/icon-goldstar-v2.png" width="16" height="16" align="absmiddle" title="Gold" />
            <?php } ?>
            /
            <?= h($g['chart_subs'] == 1 ? $t['general_submission_singular'] : str_replace('[subs]', $g['chart_subs'], $t['general_submission_plural'])) ?> &rarr;
          <b><?= h($cs->FormatSubmissions($g)) ?></b> &rarr;
          <?php foreach ($g['users'] as $user) { ?>
            <?php if ($current_user['user_id'] == $user['user_id']) { ?>
              <strong><?= link_to_user($user) ?></strong><?= $user == end($g['users']) ? "" : "," ?>
            <?php } else { ?>
              <?= link_to_user($user) ?><?= $user == end($g['users']) ? "" : "," ?>
            <?php } ?>
          <?php } ?>
        </td>
      </tr>
    <?php } ?>
  </table>
<?php } ?>
