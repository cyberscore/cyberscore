<?php

$entries = database_fetch_all("
  SELECT
    users.user_id,
    users.username,platinum, gold, silver, bronze, scoreboard_pos
  FROM sb_cache_standard
  JOIN users USING(user_id)
  WHERE scoreboard_pos >= COALESCE(
    (SELECT GREATEST(1, scoreboard_pos - 3) FROM sb_cache_standard WHERE user_id = ?),
    1
  )
  ORDER BY platinum DESC, gold DESC, silver DESC, bronze DESC
  LIMIT 7
", [$widget_user['user_id'] ?? null]);

render_component_template('widgets/standard-scoreboard', [
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
