<?php

$display = implode(', ', Article::ArticlePermissions($current_user));
$index_site_news = database_fetch_all("
  SELECT
    news.news_id,
    news.headline,
    news.news_date,
    news.article_type,
    COUNT(news_comments.news_id) AS num_comments
  FROM news
  LEFT JOIN news_comments USING(news_id)
  WHERE news.article_type IN ($display)
  GROUP BY news.news_id
  ORDER BY news.news_id DESC
  LIMIT 7
", []);

foreach ($index_site_news as &$article) {
  $article['stars'] = Article::TargettedArticleStars($article['article_type']);
}
unset($article);

render_component_template('widgets/site-news', [
  'entries' => $index_site_news,
]);
