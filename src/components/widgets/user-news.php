<?php
$index_user_news = database_get_all(database_select("
  SELECT
    news.news_id,
    news.user_id,
    news.headline,
    news.username,
    news.news_date,
    COUNT(news_comments.news_id) AS num_comments
  FROM news
  LEFT JOIN news_comments USING(news_id)
  WHERE news.article_type = 2
  GROUP BY news.news_id
  ORDER BY news.news_id DESC
  LIMIT 6
", '', []));

render_component_template('widgets/user-news', [
  'entries' => $index_user_news,
]);
