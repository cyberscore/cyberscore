<?php

if (isset($config['forum']['path'])) {
  require_once($config['forum']['path'] . "/SSI.php");
  $index_forum_entries = ssi_recentTopics(6, null, null, 'return');
} else {
  $index_forum_entries = [];
}
$forum_base_url = $config['forum']['url'];

foreach ($index_forum_entries as &$entry) {
  $entry['timestamp'] = intval($entry['timestamp']);
}
unset($entry);

render_component_template('widgets/latest-forum-posts', [
  'entries' => $index_forum_entries,
  'forum_base_url' => $forum_base_url,
]);
