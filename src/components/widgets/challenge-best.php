<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'challenge', 'top');

render_component_template('widgets/submissions', [
  'id' => 'challenge-best',
  'title' => t('widget_title_challenge_best'),
  'scoreboard' => 'challenge',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
