<?php
$entries = database_fetch_all("
  SELECT
    users.user_id,
    users.username,
    sb_cache_solution.scoreboard_pos,
    sb_cache_solution.brain_power
  FROM sb_cache_solution
  JOIN users USING(user_id)
  WHERE sb_cache_solution.scoreboard_pos >= COALESCE(
    (SELECT GREATEST(1, scoreboard_pos - 3) FROM sb_cache_solution WHERE user_id = ?),
    1
  )
  ORDER BY sb_cache_solution.scoreboard_pos ASC
  LIMIT 7
", [$widget_user['user_id'] ?? null]);

render_component_template('widgets/solution-scoreboard', [
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
