<?php

$entries = Dashboard::Submissions($widget_user['user_id'], 'standard', 'top');

render_component_template('widgets/submissions', [
  'id' => 'standard-best',
  'title' => t('widget_title_standard_best'),
  'scoreboard' => 'standard',
  'entries' => $entries,
  'widget_user' => $widget_user,
]);
