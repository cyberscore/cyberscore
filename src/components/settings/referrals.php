<?php

$referral = ReferralsRepository::find_by(['user_id' => $current_user['user_id']]);
$referrer = UsersRepository::get($referral['referrer_id']);

$referrals = database_get_all(database_select("
  SELECT
    referrals.confirmed,
    referrals.referrer_id,
    referrals.user_id,
    users.username
  FROM referrals
  JOIN users USING(user_id)
  WHERE referrals.referrer_id = ?
  AND referrals.deleted_at IS NULL
  ORDER BY referrals.confirmed DESC
", 'i', [$current_user['user_id']]));

render_component_template('settings/referrals', [
  'referrer' => $referrer,
  'referral' => $referral,
  'referrals' => $referrals,
]);
