<?php

$date_formatter = new DateFormatter($current_user['date_format'], $current_user['timezone']);
$date_format_preview = $date_formatter->Format(time(), 'date');

render_component_template('settings/display', [
  'date_format_preview' => $date_format_preview,
]);
