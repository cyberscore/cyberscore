<?php

$base_forum_url = $config['forum']['url'];
$smf_account = database_find_by('smf_accounts', ['user_id' => $current_user['user_id']]);

render_component_template('settings/forum', [
  'smf_account' => $smf_account,
  'base_forum_url' => $base_forum_url,
]);
