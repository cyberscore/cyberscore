<?php

require_once("src/scoreboards/filters.php");

class ChallengeScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_userchallenge
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function fetch($limit, $offset) {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(style_points) AS style_points
      FROM gsb_cache_userchallenge
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY user_id
      ORDER BY style_points DESC
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
