## Scoreboards

The files in this directory contains the logic necessary to build the scoreboards
in cyberscore.me.uk/scoreboard?board=x.

At some point, the `gsb_cache` rebuild functions may be moved from
`cs_class_x.php` into this directory as well.
