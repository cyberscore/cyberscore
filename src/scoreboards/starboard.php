<?php

require_once("src/scoreboards/filters.php");

class StarScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_csp
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function fetch($limit, $offset) {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(total_csr + bonus_csr) AS score,
        SUM(num_subs) AS num_subs
      FROM gsb_cache_csp
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY gsb_cache_csp.user_id
      ORDER BY score DESC, num_subs ASC
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
