<?php

require_once("src/scoreboards/filters.php");

class AmbassadorScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT referrer_id)
      FROM referrals
      JOIN users ON (referrals.referrer_id = users.user_id)
      WHERE deleted_at IS NULL AND $filters
     ", '', []);
  }

  public function fetch($limit, $offset) {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(confirmed) AS confirmed,
        SUM(!confirmed) AS unconfirmed,
        COUNT(*) AS total
      FROM referrals
      JOIN users ON (referrals.referrer_id = users.user_id)
      WHERE referrals.deleted_at IS NULL AND $filters
      GROUP BY referrals.referrer_id
      ORDER BY confirmed DESC
      LIMIT $limit
      OFFSET $offset
    ", '', []));

    return $entries;
  }
}
