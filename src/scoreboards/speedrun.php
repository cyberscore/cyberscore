<?php

require_once("src/scoreboards/filters.php");

class SpeedrunScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_speedrun
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      default:         return 'speedrun_points DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);

    $scoreboard = database_fetch_all("
      SELECT
        users.*,
        SUM(gsb_cache_speedrun.medal_points) AS speedrun_points
      FROM gsb_cache_speedrun
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY gsb_cache_speedrun.user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", []);

    foreach ($scoreboard as &$entry) {
      $entry['speedrun_time'] = Award::SpeedrunTime($entry['speedrun_points']);
    }
    unset($entry);

    return $scoreboard;
  }
}
