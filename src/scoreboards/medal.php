<?php

require_once("src/scoreboards/filters.php");

class MedalScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_medals
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      case 'platinum': return 'platinum DESC, gold DESC, silver DESC, bronze DESC';
      case 'gold': return 'gold DESC, silver DESC, bronze DESC';
      case 'silver': return 'silver DESC, platinum DESC, gold DESC, bronze DESC';
      case 'bronze': return 'bronze DESC, platinum DESC, gold DESC, silver DESC';
      default: return 'platinum DESC, gold DESC, silver DESC, bronze DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);

    return database_fetch_all("
      SELECT
        users.*,
        SUM(gsb_cache_medals.platinum) AS platinum,
        SUM(gsb_cache_medals.gold) AS gold,
        SUM(gsb_cache_medals.silver) AS silver,
        SUM(gsb_cache_medals.bronze) AS bronze
      FROM gsb_cache_medals
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
      GROUP BY gsb_cache_medals.user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", []);
  }
}
