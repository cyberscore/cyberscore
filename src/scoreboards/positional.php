<?php

require_once("src/scoreboards/filters.php");

class PositionalScoreboard {
  public $game_ids;
  public $country_ids;
  public $sort;

  public function __construct($game_ids, $country_ids, $sort) {
    $this->game_ids = $game_ids;
    $this->country_ids = $country_ids;
    $this->sort = $sort;
  }

  public function filters() {
    return ['game', 'country'];
  }

  public function count() {
    $filters = ScoreboardFilters::sql_filter($this);
    return database_single_value("
      SELECT COUNT(DISTINCT user_id)
      FROM gsb_cache_csp
      JOIN users USING (user_id)
      WHERE 1 = 1 AND $filters
     ", '', []);
  }

  public function sorting_sql() {
    switch ($this->sort) {
      case '1': return '1st DESC, 2nd DESC, 3rd DESC';
      case '2': return '2nd DESC, 1st DESC, 3rd DESC';
      case '3': return '3rd DESC, 1st DESC, 2nd DESC';
      case '4': return '4th DESC, 1st DESC, 2nd DESC, 3rd DESC';
      default: return '1st DESC, 2nd DESC, 3rd DESC';
    }
  }

  public function fetch($limit, $offset) {
    $sorting = $this->sorting_sql();
    $filters = ScoreboardFilters::sql_filter($this);
    return database_get_all(database_select("
      SELECT
        users.*,
        SUM(records.chart_pos = 1) AS '1st',
        SUM(records.chart_pos = 2) AS '2nd',
        SUM(records.chart_pos = 3) AS '3rd',
        SUM(records.chart_pos >= 4 AND records.chart_pos <= 10) AS '4th'
      FROM records
      JOIN users USING (user_id)
      WHERE $filters AND records.chart_pos <= 10
      GROUP BY user_id
      ORDER BY $sorting
      LIMIT $limit
      OFFSET $offset
    ", '', []));
  }
}
