<?php

class ScoreboardFilters {
  public static function sql_filter($scoreboard) {
    $filters = $scoreboard->filters();

    $sql_filters = ["1=1"];
    if (in_array('game', $filters)) {
      $game_filter = implode(",", $scoreboard->game_ids);
      $sql_filters []= "game_id IN ($game_filter)";
    }

    if (in_array('country', $filters)) {
      $country_filter = implode(",", $scoreboard->country_ids);
      $sql_filters []= "country_id IN ($country_filter)";
    }

    if (in_array('ym', $filters)) {
      [$year, $month] = $scoreboard->date;
      $sql_filters []= "year = $year AND month = $month";
    }

    return implode(" AND ", $sql_filters);
  }
}
