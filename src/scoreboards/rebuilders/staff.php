<?php

class StaffScoreboardRebuilder {
  public static function Game() { return false; }

  public static $modifiers = [
    'games_added'              => 40,
    'record_investigated'      => 20,
    'record_moved'             => 8,
    'record_deleted'           => 5,
    'photo_proof_approved'     => 2,
    'video_proof_approved'     => 12,
    'proof_refused'            => 4,
    'rankbutton_uploaded'      => 15,
    'support_request_archived' => 10,
    'support_request_deleted'  => 2,
    'news_article_written'     => 30,
    'record_reinstated'        => 5,
    'record_reverted'          => 5,
  ];

  public static function rebuild() {
    $staff_scoreboard = database_get_all(database_select("
      SELECT
        staff_tasks.user_id,
        SUM(IF(staff_tasks.tasks_type = 'game_added', 1, 0)) AS games_added,
        SUM(IF(staff_tasks.tasks_type = 'record_investigated', 1, 0)) AS record_investigated,
        SUM(IF(staff_tasks.tasks_type = 'record_moved', 1, 0)) AS record_moved,
        SUM(IF(staff_tasks.tasks_type = 'record_deleted', 1, 0)) AS record_deleted,
        SUM(IF(staff_tasks.tasks_type = 'proof_approved' AND staff_tasks.result = 'Record approved as a photo', 1, 0)) AS photo_proof_approved,
        SUM(IF(staff_tasks.tasks_type = 'proof_approved' AND (staff_tasks.result = 'Record approved as a video' OR staff_tasks.result = 'Record approved as a livestream' OR staff_tasks.result = 'Record approved as a replay'), 1, 0)) AS video_proof_approved,
        SUM(IF(staff_tasks.tasks_type = 'proof_refused', 1, 0)) AS proof_refused,
        SUM(IF(staff_tasks.tasks_type = 'rankbutton_uploaded', 1, 0)) AS rankbutton_uploaded,
        SUM(IF(staff_tasks.tasks_type = 'support_request_archived', 1, 0)) AS support_request_archived,
        SUM(IF(staff_tasks.tasks_type = 'support_request_deleted', 1, 0)) AS support_request_deleted,
        SUM(IF(staff_tasks.tasks_type = 'news_article_written', 1, 0)) AS news_article_written,
        SUM(IF(staff_tasks.tasks_type = 'record_reinstated', 1, 0)) AS record_reinstated,
        SUM(IF(staff_tasks.tasks_type = 'record_reverted', 1, 0)) AS record_reverted,
        COUNT(DISTINCT staff_tasks.entry_id) AS total_tasks
      FROM staff_tasks
      GROUP BY staff_tasks.user_id
    ", '', []));

    foreach ($staff_scoreboard as &$entry) {
      $entry['teamwork_power'] = 0;
      foreach (self::$modifiers as $key => $modifier) {
        $entry['teamwork_power'] += $modifier * $entry[$key];
      }
    }
    unset($entry);

    uasort($staff_scoreboard, fn($a, $b) => $b['teamwork_power'] <=> $a['teamwork_power']);

    $rank = new Aggregates\Rank();
    foreach ($staff_scoreboard as &$entry) {
      $entry['scoreboard_pos'] = $rank->update($entry['teamwork_power']);
    }
    unset($entry);

    database_delete_all('sb_cache_staff');
    database_insert_all('sb_cache_staff', $staff_scoreboard);
  }
}
