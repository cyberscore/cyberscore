<?php

require_once('includes/base_repository.php');

class StaffTasksRepository extends BaseRepository {
  protected static $table_name = 'staff_tasks';
  public static $primary_key = 'task_id';
  protected static $relations = [
    'user' => ['UsersRepository', 'user_id'],
  ];
}


// GameEntitiesRepository::get(1)['inherited_entity']['game_entity_id'];
// g = select * from game_entities where pkey = 1
// select * from game_entities where pkey = g.inherited_entity_id

