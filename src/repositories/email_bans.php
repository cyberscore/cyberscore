<?php

require_once('includes/base_repository.php');

class EmailBansRepository extends BaseRepository {
  protected static $table_name = 'email_bans';
  protected static $primary_key = 'email_ban_id';

  public static function is_banned($email_address) {
    foreach (self::all() as $ban) {
      if (str_ends_with($email_address, $ban['domain'])) {
        return true;
      }
    }
    return false;
  }
}
