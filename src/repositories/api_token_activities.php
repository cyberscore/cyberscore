<?php

require_once('includes/base_repository.php');

class ApiTokenActivityRepository extends BaseRepository {
  protected static $table_name = 'api_token_activities';
  protected static $primary_key = 'api_token_activity_id';
  protected static $has_timestamps = true;
}
