<?php

require_once('includes/base_repository.php');

class RulesRepository extends BaseRepository {
  protected static $table_name = 'rules';
  protected static $primary_key = 'rule_id';

  public static function create($params) {
    if (database_find_by(static::$table_name, ['rule_text' => $params['rule_text']])) {
      return false;
    } else {
      return parent::create($params);
    }
  }

  public static function update($rule_id, $params) {
    global $cs;

    parent::update($rule_id, $params);

    // Is the changed text identical to an existing rule?
    // If yes, merge them
    $same_text_rules = database_get_all(database_select("
      SELECT rules.rule_id
      FROM rules
      JOIN rules r2 USING (rule_text)
      WHERE rules.rule_id != ? AND r2.rule_id = ?
    ", 'ii', [$rule_id, $rule_id]));

    if (count($same_text_rules) > 0) {
      foreach ($same_text_rules as $dup_rule) {
        $dup_id = $dup_rule['rule_id'];
        database_update(
          'levels',
          ['level_rules' => database_literal("
          TRIM(',' FROM REPLACE(REPLACE(CONCAT(',', level_rules, ','), ?, ','), ?, ?))
          ")],
          "CONCAT(',', level_rules, ',') LIKE ?",
          'ssss',
          [",$rule_id,", ",$dup_id,", ",$rule_id,", "%,$dup_id,%"]
        );

        database_delete_by(static::$table_name, ['rule_id' => $dup_id]);
      }
    }

    return [
      'merged' => count($same_text_rules) > 0,
    ];
  }

  public static function delete($id) {
    parent::delete($id);

    database_update(
      'levels',
      ['level_rules' => database_literal("
        TRIM(',' FROM REPLACE(CONCAT(',', level_rules, ','), ?, ','))
      ")],
      "CONCAT(',', level_rules, ',') LIKE ?",
      's',
      ["%,$id,%"]
    );
  }
}
