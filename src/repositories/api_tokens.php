<?php

require_once('includes/base_repository.php');

class ApiTokenRepository extends BaseRepository {
  protected static $table_name = 'api_tokens';
  protected static $primary_key = 'api_token_id';
  protected static $has_timestamps = true;

  // soft delete
  public static function delete($id) {
    return static::update($id, ['deleted_at' => database_now()]);
  }

  public static function find_by($params, $includes = []) {
    return parent::find_by(array_merge(['deleted_at' => NULL], $params), $includes);
  }

  public static function where($params, $includes = []) {
    return parent::where(array_merge(['deleted_at' => NULL], $params), $includes);
  }
}
