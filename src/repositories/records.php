<?php

require_once('includes/base_repository.php');

class RecordsRepository extends BaseRepository {
  protected static $table_name = 'records';
  protected static $primary_key = 'record_id';
}

class DeletedRecordsRepository extends BaseRepository {
  protected static $table_name = 'records_del';
  protected static $primary_key = 'record_id';

  protected static $relations = [
    'record_note_mod' => ['UsersRepository', 'record_note_mod_id'],
    'chart' => ['LevelsRepository', 'level_id'],
    'deleted_chart' => ['DeletedLevelsRepository', 'level_id'],
    'investigation_task' => ['StaffTasksRepository', 'record_id', ['tasks_type' => 'record_investigated', 'result' => 'Record deleted']],
    'deleted_record_info' => ['DeletedRecordInfosRepository', 'record_id'],
  ];
}

class DeletedRecordInfosRepository extends BaseRepository {
  protected static $table_name = 'records_del_info';
  protected static $primary_key = 'record_id';

  protected static $relations = [
    'user' => ['UsersRepository', 'user_id'],
  ];
}
