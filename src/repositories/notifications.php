<?php

require_once('includes/base_repository.php');

class NotificationsRepository extends BaseRepository {
  protected static $table_name = 'notifications';
  protected static $primary_key = 'notification_id';

  public static $categories = [
    'community' => [
      'greq_comment',
      'article_comment',
      'referral_confirmed',
      'referral_rejected',
      'reply',
      'blog_follow',
      'blog_unfollow',
      'userpage_comments',
      'followed_posted',
    ],
    'competition' => [
      'ygb',
      'rec_reported',
      'rec_deleted',
      'trophy_lost',
      'trophy_gained',
    ],
    'proof' => [
      'rec_approved',
      'proof_refused',
    ],
    'staff' => [
      'supp',
      'pstring_added',
      'pstring_edited',
      'new_grequest',
      'new_game',
      'reported_rec',
      'referral_created',
    ],
  ];

  public static function delete($id) {
    return static::update($id, ['deleted_at' => database_now()]);
  }
}
