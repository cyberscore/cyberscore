<?php

# General pages
get('/', 'home/index');
get('/index', 'home/index');
get('/site_stats', 'home/stats');
get('/site_stats/{src}', 'home/stats');
get('/proof_stats', 'home/proof_stats');
get('/proof_stats/{src}', 'home/proof_stats');
get('/team', 'home/team');
get('/faq', 'home/faq');
get('/history', 'home/history');
get('/terms', 'home/terms');
get('/changelog', 'home/changelog');
get('/404', 'home/404');
get('/metrics', 'home/metrics');

post('/language', 'home/language');

# Documentation
get('/documentation', 'documentation/index');
get('/site_rules', 'home/site_rules');
get('/mechanics', 'home/mechanics');
get('/mechanics/{src}', 'home/mechanics');
get('/policies', 'home/policies');
get('/policies/{src}', 'home/policies');
get('/staff', 'home/staff');
get('/staff/{src}', 'home/staff');

get('/community', 'community/index');


# Search
get('/search', 'home/search');

# API
resources('chart-submission-history', ['only' => ['show']]);
resources('profile-api', ['only' => ['show']]);

scope('api', function() {
  // We should figure out a way to handle CORS here

  before(function() {
    // current_user is always loaded via cookies in startup.php,
    // so we need to undo all that work.
    // See that file for more information.
    CurrentUser::reset();
    CurrentUser::load_from_token();
  });

  resources('charts', ['only' => ['show']]);
  resources('games', ['only' => ['show']]);
  resources('notifications', ['only' => ['index']]);
  resources('records', ['only' => ['create']]);
});

# Personal Access Tokens
resources('api-tokens', ['only' => ['create', 'delete']]);

# Homepics
resources('homepics', ['only' => ['index', 'new', 'create']]);

# Boxarts
resources('boxarts', ['only' => ['new', 'create', 'delete']]);

# Account / session management

get('/register', 'accounts/new');
post('/register', 'accounts/create');
resources('accounts', ['only' => ['new', 'create']]);

get('/accounts/confirm/{token}', 'accounts/confirm');
get('passwords/recover');
get('/forgotpass', 'passwords/recover');
post('/passwords/recover/step2', 'passwords/recover_step2');
post('/passwords/recover/step3', 'passwords/recover_step3');
get('/passwords/recover/step3', 'passwords/recover_step3');
post('/passwords/recover/step4', 'passwords/recover_step4');

resources('passwords', ['only' => ['edit', 'update']]);
resources('impersonations', ['only' => ['create']]);
post('/impersonations/delete', 'impersonations/delete');
resources('sessions', ['only' => ['new', 'create', 'delete']]);
get('/login', 'sessions/new');
post('/login', 'sessions/create');

# Account settings
get('/settings', 'users/settings');
get('/user-settings-date-preview', 'user-settings-date-preview/index');
get('/settings/{src}', 'users/settings');
post('/settings', 'users/update_settings');

resources('dashboard', ['only' => ['index']]);
get('/dashboard/edit');
post('/dashboard', 'dashboard/update');
get('/dashboard-widgets/new');
get('/dashboard-widgets/game');

get('/games/autocomplete');


# Users
resources('user-stats-graphs', ['only' => ['show']]);
get('/external/stats_graph', 'user-stats-graphs/show');
get('/external/stats_graph_smaller', 'user-stats-graphs/show', ['small' => 1]);

resources('users', ['only' => ['index', 'edit', 'update', 'delete', 'show']]);
resources('user-avatars', ['only' => ['delete']]);
resources('user-banners', ['only' => ['delete']]);
resources('user-moderation-notes', ['only' => ['index']]);
resources('user-mergers', ['only' => ['new', 'create']]);

get('/external/forum_user_link', 'forum-users/show');

get('/user/{id}', 'users/show');
get('/stats', 'users/show');
get('/user/{id}/{src}', 'users/show');
get('/users/{id}/{src}', 'users/show'); # this must be after the resources('users') entry
post('/user-avatars/{id}/delete', 'user-avatars/delete');
post('/user-banners/{id}/delete', 'user-banners/delete');
post('users/resend-confirmation-email');

get('/queued-records-count', 'queued-records-count/index');

# deprecated routes, here to avoid breaking links
redirect('/userlist', '/users');
redirect('/mod_scripts/deleted_recs', '/records/deleted');
redirect('/record/{id}', '/records/{id}');
redirect('/chart/{id}', '/charts/{id}');

# User relationships
resources('friendships', ['only' => ['create', 'delete']]);
resources('followings', ['only' => ['create', 'delete']]);

# CSMail
resources('csmails', ['only' => ['index', 'show']]);
resources('csmail-messages', ['only' => ['new', 'show', 'create']]);

post('/csmail/folders', 'csmail/folders/create');
post('/csmail/folders/delete', 'csmail/folders/delete');
post('/csmail/update_messages', 'csmail/update_messages');

# Leagues
get('leagues/select');
# get("leagues/view");
# get('leagues/admin');
# get('leagues/check');
# get('leagues/create'); // TODO: make this a post
# get('leagues/join'); // TODO: make this a post
# post('leagues/publish');
# post('leagues/update');
# post('leagues/submit_scores');
# post('leagues/submit_totals');
# get('/leagues/league_check', 'leagues/check');   # deprecated
# get('/leagues/league_admin', 'leagues/admin');   # deprecated
# get("/leagues/view_league", 'leagues/view');     # deprecated
# get('/leagues/league_select', 'leagues/select'); # deprecated

# Support requests
get('/support_requests', 'support_requests/index');
get('/support', 'support_requests/new');
get('/support_requests/archived', 'support_requests/archived');
post('/support_requests', 'support_requests/create');
post('/support_requests/{id}/delete', 'support_requests/delete');
post('/support_requests/{id}', 'support_requests/update');

# Records
resources('records/multiple', ['only' => ['new', 'create', 'edit', 'update']]);
resources('user-records', ['only' => ['new', 'show', 'update']]);
resources('record-moderation-notes', ['only' => ['index', 'update']]);
resources('record-moderation-actions', ['only' => ['create']]);
resources('record-history-entries', ['only' => ['delete']]);

resources('record-auto-proofs', ['only' => ['new', 'create']]);
resources('record-savefiles', ['only' => ['new']]);
resources('record-auto-submissions', ['only' => ['create']]); // TODO: replace record-auto-proofs/create with this

get('/latest_subs', 'latest-submissions/index');
resources('latest-submissions', ['only' => ['index']]);
get('/game-rss', 'latest-submissions/show', ['format' => 'rss']);

get('/latest_subs_stats', 'latest-submissions-stats/index');
resources('latest-submissions-stats', ['only' => ['index']]);

get('/records/deleted', 'records/deleted');

resources('records', ['only' => ['index', 'new', 'show', 'create', 'update']]);
resources('reported-records', ['only' => ['index', 'create']]);
resources('record-chart-transfers', ['only' => ['create']]);
resources('deleted-record-chart-transfers', ['only' => ['create']]);

get('/compare', 'records/comparison');

# Scoreboards

get('/game_scoreboard', 'scoreboards/game');
get('/group/{game_id}/{group_id}', 'scoreboards/group');
get('/group_stats.php', 'scoreboards/group');
get('/scoreboards/country', 'scoreboards/country');
get('/scoreboards/monthly-challenge', 'scoreboards/first');
resources('scoreboards', ['only' => ['index', 'show']]);

# Translations

resources('translations', ['only' => ['index']]);
scope('translations', function() {
  get('faq');

  resources('games', ['only' => ['index']]);
  resources('current-language', ['only' => ['create']]);
  resources('games2', ['only' => ['index']]);

  resources('countries', ['only' => ['index', 'create']]);
  resources('country-suggestions', ['only' => ['index', 'create']]);

  resources('news', ['only' => ['index']]);
  resources('news/suggestions', ['only' => ['create', 'update', 'delete']]);
  resources('news/active-suggestions', ['only' => ['create']]);

  resources('rules', ['only' => ['index']]);
  resources('rule-suggestions', ['only' => ['index', 'create']]);
  resources('rule-suggestion-actions', ['only' => ['create']]);

  resources('proof-rules', ['only' => ['index']]);
  resources('proof-rule-suggestions', ['only' => ['index', 'create']]);
  resources('proof-rule-suggestion-actions', ['only' => ['create']]);

  resources('page-strings/votes', ['only' => ['create', 'delete']]);
  resources('page-strings/suggestions', ['only' => ['create', 'delete']]);
  resources('page-strings/active-suggestions', ['only' => ['create']]);
  resources('page-strings', ['only' => ['index', 'new', 'show', 'create', 'update', 'delete']]);

  resources('translators', ['only' => ['index', 'create', 'delete']]);
  resources('translators/languages', ['only' => ['create']]);
});

# Game rules
resources('rules', ['only' => ['index', 'create', 'update', 'edit', 'new', 'delete']]);
resources('proof-rules', ['only' => ['index', 'create', 'update', 'edit', 'new', 'delete']]);

# Games

resources('games', ['only' => ['index', 'create', 'show', 'edit', 'update']]);
get('/game/{id}', 'games/show');
get('/game/{id}/edit', 'games/edit');
get('/mod_scripts/edit_game', 'games/edit');
post('/games/{id}/delete', 'games/delete');
post('/games/{id}/name', 'games/update_name');

resources('game-rules', ['only' => ['show', 'update']]);
resources('game-proof-rules', ['only' => ['show', 'update']]);
resources('game-modifiers', ['only' => ['show', 'update']]);
resources('related-games', ['only' => ['show', 'update']]);
resources('game-chart-deletions', ['only' => ['show', 'update']]);
resources('game-chart-details', ['only' => ['show', 'update']]);
resources('game-social', ['only' => ['show', 'update']]);

get('/games/{game_id}/edit_charts', 'games/edit_charts');
get('/games/{game_id}/group/{group_id}/edit', 'games/edit_charts');
get('/mod_scripts/edit_game_groups', 'games/edit_groups');

resources('game-translation-statuses', ['only' => ['update']]);
resources('game-groups', ['only' => ['edit', 'update', 'delete', 'create']]);
resources('game-group-translations', ['only' => ['create']]);
resources('game-groups-groups', ['only' => ['update']]);

resources('record-improvements', ['only' => ['show']]);

resources('game-group-chart-translations', ['only' => ['create']]);
resources('game-group-charts', ['only' => ['new', 'create']]);
get('game-group-charts/form');

resources('game-entities', ['only' => ['edit', 'create', 'update']]);
resources('game-entity-translations', ['only' => ['create']]);
resources('game-entity-locks', ['only' => ['edit', 'update']]);
resources('game-entity-icons', ['only' => ['create']]);

# Game series

resources('game-series', ['only' => ['index', 'show', 'create', 'delete', 'update']]);

# Game boards
resources('game-boards', ['only' => ['show']]);
resources('game-board-topics', ['only' => ['new', 'create', 'show']]);
resources('game-board-messages', ['only' => ['edit', 'update', 'create']]);

# Rankbuttons
resources('rankbuttons', ['only' => ['index', 'new', 'create', 'delete']]);
resources('active-rankbuttons', ['only' => ['create']]);

get('/rankbutton-images/{type}/{user_id}', 'rankbutton-images/global');
resources('rankbutton-images', ['only' => ['show']]);

## musn't break these, they're linked externally
get('/external/game_rankbutton/{game_id}/{split_index}/{user_id}', 'rankbutton-images/game');
get('/external/game_rankbutton/{game_id}/{user_id}', 'rankbutton-images/game');
get('/external/rankbutton/{user_id}',           'rankbutton-images/global', ['type' => 'starboard']);
get('/external/medaltablebutton/{user_id}',     'rankbutton-images/global', ['type' => 'medal']);
get('/external/proofbutton/{user_id}',          'rankbutton-images/global', ['type' => 'proof']);
get('/external/videoproofbutton/{user_id}',     'rankbutton-images/global', ['type' => 'vproof']);
get('/external/trophytablebutton/{user_id}',    'rankbutton-images/global', ['type' => 'trophy']);
get('/external/arcadetablebutton/{user_id}',    'rankbutton-images/global', ['type' => 'arcade']);
get('/external/challengetablebutton/{user_id}', 'rankbutton-images/global', ['type' => 'challenge']);
get('/external/speedruntablebutton/{user_id}',  'rankbutton-images/global', ['type' => 'speedrun']);
get('/external/rainbowbutton/{user_id}',   'rankbutton-images/global', ['type' => 'rainbow']);
get('/external/subsbutton/{user_id}',           'rankbutton-images/global', ['type' => 'subs']);
get('/external/solutiontablebutton/{user_id}',  'rankbutton-images/global', ['type' => 'solution']);
get('/external/collectibletablebutton/{user_id}',  'rankbutton-images/global', ['type' => 'collectible']);
get('/external/incrementaltablebutton/{user_id}',  'rankbutton-images/global', ['type' => 'incremental']);

# Discord servers
post('discords/create');
post('discords/update');
post('discords/delete');

# Notifications
post('notifications/read-all');
post('notifications/unread-all');
post('notifications/delete-all');
post('notifications/delete-obsolete');

resources('notifications', ['only' => ['index', 'show']]);

# Charts
get('/charts/index48');
resources('charts', ['only' => ['show']]);

resources('rockband-charts', ['only' => ['new', 'create']]);

# News articles
post('/article/preview', 'articles/preview');
get('/article/{id}', 'articles/show');
resources('articles', ['only' => ['create', 'delete', 'index', 'update', 'edit', 'new', 'show']]);
resources('news', ['only' => ['index']]);

# Comments
post('comments/create');
post('comments/update');
post('comments/delete');

resources('record-comments', ['only' => ['index']]);

# Game requests
resources('game_requests', ['only' => ['index', 'new', 'show', 'update', 'create']]);
resources('handled-game-requests', ['only' => ['index', 'delete', 'update']]);

# Proofs
get('/mod_scripts/pending_proofs', 'proofs/pending');
get('/proofs/pending', 'proofs/pending');
get('/proofs/rankings', 'proofs/rankings');
resources('game-proofs', ['only' => ['show']]);
resources('proofs', ['only' => ['new', 'create', 'show']]);

# Referrals
get('/referrals', 'referrals/index');
post('/referrals', 'referrals/update');

# Administration tools
resources('admin', ['only' => ['index']]);
scope('admin', function() {
  resources('access_management', ['only' => ['index', 'create', 'update']]);
  resources('banned_users', ['only' => ['index', 'update']]);
  resources('countries', ['only' => ['index', 'create', 'delete']]);
  resources('csmails', ['only' => ['index', 'new']]);
  resources('email-bans', ['only' => ['index', 'create', 'delete']]);
  resources('platforms', ['only' => ['index', 'create', 'edit', 'update', 'delete']]);
  resources('staff_logs', ['only' => ['index']]); // disabled create and delete

  get('rebuild_center');
  post('rebuild_cache');
});


# Predictions
resources('predictions', ['only' => ['index']]);
# get('/predictions/{id}', 'predictions/show');
# post('predictions', 'predictions/create');
# post('predictions/{id}/join', 'predictions/join');
# post('predictions/{id}/leave', 'predictions/leave');
# post('predictions/{id}/delete', 'predictions/delete');

# Internal endpoints
resources('email-notifications', ['only' => ['create']]);
