<?php

class ChartModifierRebuilder {
  public static function Rebuild() {
    $levels = database_get_all(database_select("SELECT level_id FROM levels", '', []));
    $games = database_get_all(database_select("SELECT game_id FROM games", '', []));

    $modifiers = database_get_all(database_select("
      SELECT *
      FROM chart_modifiers
      JOIN levels USING (level_id)
    ", '', []));

    $new_modifiers = [];
    foreach ($levels as $level) {
      $new_modifiers[$level['level_id']] = [
        'level_id' => $level['level_id'],

        'speedrun' => false,
        'solution' => false,
        'unranked' => false,
        'challenge' => false,
        'collectible' => false,
        'incremental' => false,

        'regional_differences' => false,
        'gameplay_differences_device' => false,
        'significant_regional_differences' => false,
        'significant_device_differences' => false,
        'patience' => false,
        'computer_generated' => false,
        'premium_upgrades_and_consumables_earnable' => false,
        'premium_upgrades_and_consumables_premium_only' => false,
        'premium_dlc' => false,
        'premium_upgrades_and_consumables_inaccessible' => false,
        'premium_upgrades_and_consumables_conditionally_accessible' => false,
        'gameplay_differences_game_version' => false,
        'accessibility_issues_inaccessible' => false,
        'accessibility_issues_conditionally_accessible' => false,
        'accessibility_issues_geographical' => false,
      ];
    }

    foreach ($modifiers as $modifier) {
      $new_modifiers[$modifier['level_id']][self::Identifier($modifier)] = true;
    }

    foreach ($new_modifiers as &$modifier) {
      $modifier['arcade'] = (
        $modifier['regional_differences'] ||
        $modifier['gameplay_differences_device'] ||
        $modifier['significant_regional_differences'] ||
        $modifier['significant_device_differences'] ||
        $modifier['patience'] ||
        $modifier['computer_generated'] ||
        $modifier['premium_upgrades_and_consumables_earnable'] ||
        $modifier['premium_upgrades_and_consumables_premium_only'] ||
        $modifier['premium_dlc'] ||
        $modifier['premium_upgrades_and_consumables_inaccessible'] ||
        $modifier['premium_upgrades_and_consumables_conditionally_accessible'] ||
        $modifier['gameplay_differences_game_version'] ||
        $modifier['accessibility_issues_inaccessible'] ||
        $modifier['accessibility_issues_conditionally_accessible'] ||
        $modifier['accessibility_issues_geographical']
      );
      $modifier['standard'] = (
        !$modifier['speedrun'] &&
        !$modifier['solution'] &&
        !$modifier['unranked'] &&
        !$modifier['challenge'] &&
        !$modifier['collectible'] &&
        !$modifier['incremental'] &&
        !$modifier['arcade']
      );
    }
    unset($modifier);

    database_delete_all("chart_modifiers2");
    database_insert_all('chart_modifiers2', array_values($new_modifiers));

    foreach ($games as $game) {
      GameCacheRebuilder::BuildGameNumCharts($game['game_id']);
      Modifiers::UpdateChartFlooder($game['game_id']);
    }
  }

  static function Identifier($modifier) {
    switch ($modifier['chart_flag']) {
    case 1: return 'speedrun';
    case 2: return 'solution';
    case 3: return 'unranked';
    case 4: return 'challenge';
    case 5: return 'collectible';
    case 6: return 'incremental';
    case 7: return 'arcade';
    }

    switch ($modifier['csp_modifier']) {
    case 1: return 'regional_differences';
    case 2: return 'gameplay_differences_device';
    case 3: return 'significant_regional_differences';
    case 4: return 'significant_device_differences';
    case 5: return 'patience';
    case 6: return 'computer_generated';
    case 7: return 'premium_upgrades_and_consumables_earnable';
    case 8: return 'premium_upgrades_and_consumables_premium_only';
    case 9: return 'premium_dlc';
    case 10: return 'premium_upgrades_and_consumables_inaccessible';
    case 11: return 'premium_upgrades_and_consumables_conditionally_accessible';
    case 12: return 'gameplay_differences_game_version';
    case 13: return 'accessibility_issues_inaccessible';
    case 14: return 'accessibility_issues_conditionally_accessible';
    case 15: return 'accessibility_issues_geographical';
    }
  }
}
