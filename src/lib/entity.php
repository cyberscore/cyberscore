<?php

class Entity {
  private static $cache = [];

  public static function cache_game_entities($game_id) {
    if (isset(static::$cache[$game_id])) {
      return;
    }

    $game_entities = database_filter_by('game_entities', ['game_id' => $game_id]);
    $inherited_ids = array_compact(pluck($game_entities, 'inherited_entity_id'));
    $parent_entities = index_by(database_filter_by('game_entities', ['game_entity_id' => $inherited_ids]), 'game_entity_id');
    $game_entities = index_by($game_entities, 'game_entity_id');

    static::$cache[$game_id] = [
      'entities' => $game_entities,
      'parent_entities' => $parent_entities,
    ];
  }

  public static function info($game_id, $entity_id) {
    global $config;
    global $t;

    self::cache_game_entities($game_id);

    $entity = static::$cache[$game_id]['entities'][$entity_id] ?? NULL;
    if ($entity == NULL) {
      return NULL;
    }

    $name = self::normalize_name($entity['entity_name']);

    if (file_exists($config['app']['root'] . "/public/uploads/entities/{$entity['game_id']}/{$name}.png")) {
      return [
        'image_path' => "/uploads/entities/{$entity['game_id']}/{$name}.png",
        'name' => $t->GetEntityName($entity_id),
      ];
    } else if (file_exists($config['app']['root'] . "/public/uploads/entities/{$entity['game_id']}/{$entity['game_entity_id']}.png")) {
      return [
        'image_path' => "/uploads/entities/{$entity['game_id']}/{$entity['game_entity_id']}.png",
        'name' => $t->GetEntityName($entity_id),
      ];
    } else if ($entity['inherited_entity_id'] != 0) {
      $parent_entity = static::$cache[$game_id]['parent_entities'][$entity['inherited_entity_id']];
      $name = self::normalize_name($parent_entity['entity_name']);

      if (file_exists($config['app']['root'] . "/public/uploads/entities/{$parent_entity['game_id']}/{$name}.png")) {
        return [
          'image_path' => "/uploads/entities/{$parent_entity['game_id']}/{$name}.png",
          'name' => $t->GetEntityName($parent_entity['game_entity_id']),
        ];
      } else if (file_exists($config['app']['root'] . "/public/uploads/entities/{$parent_entity['game_id']}/{$parent_entity['game_entity_id']}.png")) {
        return [
          'image_path' => "/uploads/entities/{$parent_entity['game_id']}/{$parent_entity['game_entity_id']}.png",
          'name' => $t->GetEntityName($parent_entity['game_entity_id']),
        ];
      }
    }

    return ['image_path' => null, 'name' => $t->GetEntityName($entity_id)];
  }

  public static function image_path($entity, $parent_entity) {
    global $config;
    global $t;

    $name = self::normalize_name($entity['entity_name']);

    if (file_exists($config['app']['root'] . "/public/uploads/entities/{$entity['game_id']}/{$name}.png")) {
      return "/uploads/entities/{$entity['game_id']}/{$name}.png";
    } else if (file_exists($config['app']['root'] . "/public/uploads/entities/{$entity['game_id']}/{$entity['game_entity_id']}.png")) {
      return "/uploads/entities/{$entity['game_id']}/{$entity['game_entity_id']}.png";
    } else if ($parent_entity) {
      return self::image_path($parent_entity, NULL);
    }
  }

  private static function normalize_name($name) {
    return str_replace(
      [' ', '/', '&rsquo;', 'é', '♂', '♀'],
      ['_', '_', '_', 'e', '(m)', '(f)'],
      $name);
  }
}
