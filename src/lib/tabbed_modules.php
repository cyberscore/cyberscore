<?php

function validate_module($sidebar, $name, $module) {
  $modules = array_merge(...array_map(fn($x) => array_keys($x), array_values($sidebar)));

  if ($module) {
    if (in_array($module, $modules)) {
      return $module;
    } else {
      HTTPResponse::PageNotFound();
    }
  } else {
    redirect_to("/{$name}/{$modules[0]}");
  }
}
