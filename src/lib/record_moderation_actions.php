<?php

function SendCSMail($from_user_id, $to_user_id, $subject, $message) {
  database_insert('csmail', [
    'from_id' => $from_user_id,
    'to_id' => $to_user_id,
    'message_date' => database_now(),
    'subject' => trim($subject),
    'message' => trim($message),
    'to_status' => 0,
    'from_status' => 4,
    'to_deleted' => 0,
    'from_deleted' => 0,
    'csmail_folder_id' => 0,
  ]);
}

class RecordModerationActions {
  public static function InvestigateRecord($moderator, $record_id, $required_proof_type, $mail_note) {
    $record = RecordsRepository::get($record_id);
    $realname = $moderator['forename'] . " " . $moderator['surname'];

    database_update_by('records', [
      'rec_status' => 2,
      'proof_deadline' => date("Y-m-d 00:00:00", strtotime("+14 days")),
      'proof_mod' => $realname,
    ], ['record_id' => $record['record_id']]);

    database_insert('proofmailer', [
      'user_id' => $record['user_id'],
      'level_id' => $record['level_id'],
      'admin_name' => $realname,
      'admin_addy' => $moderator['email'],
      'proof_type' => $required_proof_type,
      'date_added' => database_now(),
      'mail_note' => $mail_note,
    ]);

    database_insert('staff_tasks', [
      'user_id' => $moderator['user_id'],
      'tasks_type' => 'record_investigated',
      'task_id' => $record['record_id'],
      'result' => 'Record investigated',
    ]);

    Notification::RecordProofRequired($record['record_id'], $moderator['user_id'])->DeliverToUser($record['user_id']);
  }

  public static function ResetRecord($moderator, $record_id) {
    RecordManager::ResetRecordStatus($record_id);
  }

  public static function DeleteRecord($moderator, $record_id) {
    $record = RecordsRepository::get($record_id);

    RecordManager::DeleteRecord($record_id, $moderator['user_id'], $record['level_id']);
    Notification::RecordDeleted($record_id, $moderator['user_id'])->DeliverToUser($record['user_id']);
  }

  public static function TerminateRecord($moderator, $record_id) {
    $record = RecordsRepository::get($record_id);

    RecordManager::TerminateRecord($record_id, $record['level_id'], $moderator['user_id']);
    Notification::RecordObliterated($record['level_id'], $record_id, $record['user_id'], $moderator['user_id'])
      ->DeliverToUser($record['user_id']);
  }

  public static function RevertRecord($moderator, $record_id, $history_id) {
    $history_id = database_single_value("SELECT history_id FROM record_history WHERE history_id = ?", 'i', [$history_id]);
    RecordManager::RevertRecord($record_id, $history_id, $moderator['user_id']);
  }

  public static function ReinstateRecord($moderator, $record_id) {
    $record = DeletedRecordsRepository::get($record_id);
    $result = RecordManager::ReinstateRecord($record_id, $moderator['user_id'], $record['user_id'], $record['level_id']);
  }

  public static function ApproveRecord($moderator, $record_id, $proof_type) {
    $record = RecordsRepository::get($record_id);

    RecordManager::ApproveRecord($record_id, $proof_type);
    switch ($proof_type) {
    case "p":
      $proof_result = "Record approved as a photo";
      break;

    case "v":
      $proof_result = "Record approved as a video";
      break;

    case "e":
      $proof_result = "Record approved as an eyewitness";
      break;

    case "s":
      $proof_result = "Record approved as a livestream";
      break;

    case "r":
      $proof_result = "Record approved as a replay";
      break;
    }

    database_insert('staff_tasks', ['user_id' => $moderator['user_id'], 'tasks_type' => 'proof_approved', 'task_id' => $record_id, 'result' => $proof_result]);
    database_update_by('staff_tasks', ['result' => $proof_result], ['tasks_type' => 'record_investigated', 'task_id' => $record_id]);
    database_update_by('staff_tasks', ['result' => $proof_result], ['tasks_type' => 'proof_refused', 'task_id' => $record_id]);

    if ($record['rec_status'] != 3) {
      RecordManager::TrackApproval($record_id, $moderator['user_id']);
      RecordManager::AddRecordHistoryEntry($record_id, 'a');
      Notification::RecordApproval($record_id, $moderator['user_id'])->DeliverToUser($record['user_id']);
    }
    ChartCacheRebuilder::QueueChartForRebuild($record['level_id']);
    GameCacheRebuilder::QueueGameForRebuild($record['game_id']);
  }

  public static function RefuseProofRecord($moderator, $record_id, $csmail_body) {
    $record = RecordsRepository::get($record_id);

    $params = ['linked_proof' => ""];
    if ($record['rec_status'] >= 4) {
      $params['rec_status'] = $record['rec_status'] - 4;
    }

    database_update_by('records', $params, ['record_id' => $record_id]);

    database_insert('staff_tasks', [
      'user_id' => $moderator['user_id'],
      'tasks_type' => 'proof_refused',
      'task_id' => $record_id,
      'result' => 'Proof refused',
    ]);

    database_update_by(
      'staff_tasks',
      ['result' => 'Proof refused'],
      ['tasks_type' => 'record_investigated', 'task_id' => $record_id]
    );

    Notification::ProofRefused($record_id, $moderator['user_id'])->DeliverToUser($record['user_id']);
    ChartCacheRebuilder::QueueChartForRebuild($record['level_id']);
    GameCacheRebuilder::QueueGameForRebuild($record['game_id']);
    if ($csmail_body != "") {
      SendCSMail($moderator['user_id'], $record['user_id'], "Refused proof(s)", $csmail_body);
    }
  }

  public static function DeleteProofRecord($moderator, $record_id) {
    global $config;
    $root = $config['app']['root'];

    $record = RecordsRepository::get($record_id);

    $params = ['linked_proof' => ""];
    if ($record['rec_status'] >= 4) {
      $params['rec_status'] = $record['rec_status'] - 4;
    }

    database_update_by('records', $params, ['record_id' => $record_id]);
  }

  public static function LockProofRecord($moderator, $record_id) {
    database_insert('record_approval_locks', [
      'record_id' => $record_id,
      'modname' => $moderator['username'],
    ]);
  }

  public static function UnlockProofRecord($moderator, $record_id) {
    database_delete_by('record_approval_locks', ['record_id' => $record_id]);
  }
}
