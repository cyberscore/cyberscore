<?php

$twig->addFunction(new \Twig\TwigFunction('t', function($key, $bindings = []) { return t($key, $bindings); }));

$twig->addFunction(new \Twig\TwigFunction('render_component_with', function($component, $variables) { return render_component($component, $variables); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('render_partial', function($partial, $variables) { return render_partial($partial, $variables); }, ['is_safe' => ['html']]));

$twig->addFunction(new \Twig\TwigFunction('skin_stylesheet', function($sheet) { return skin_stylesheet($sheet); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('javascript_tag', function($url) { return javascript_tag($url); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('skin_image_url', function($image) { return skin_image_url($image); }));
$twig->addFunction(new \Twig\TwigFunction('classnames', function($attrs) { return classnames($attrs); }));

$twig->addFunction(new \Twig\TwigFunction('url_for', function($attrs) { return url_for($attrs); }));
$twig->addFunction(new \Twig\TwigFunction('edit_url_for', function($attrs) { return edit_url_for($attrs); }));
$twig->addFunction(new \Twig\TwigFunction('delete_url_for', function($attrs) { return delete_url_for($attrs); }));
$twig->addFunction(new \Twig\TwigFunction('link_to_game', function($attrs) { return link_to_game($attrs); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('link_to_user', function($attrs, $fullname = false, $options = []) { return link_to_user($attrs, $fullname, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('link_to_chart', function($attrs, $options = []) { return link_to_chart($attrs, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('link_to_record', function($attrs, $options = []) { return link_to_record($attrs, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('link_to_game_group', function($attrs, $options = []) { return link_to_game_group($attrs, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('link_to_news_article', function($attrs, $options = []) { return link_to_news_article($attrs, $options); }, ['is_safe' => ['html']]));

$twig->addFunction(new \Twig\TwigFunction('format_submission', function($submission, $prefix = "") { return CSClass::FormatSubmissions($submission, $prefix); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('format_submission_part', fn($submission, $part, $prefix = '') => CSClass::FormatSubmissionPart($submission, $part, $prefix), ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('chart_submit_submission_tags', fn($a, $b, $c, $d, $e = false) => chart_submit_submission_tags($a, $b, $c, $d, $e), ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('record_proof', function($record) { return record_proof($record); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('csp2csr', function($csp) { return csp2csr($csp); }));

$twig->addFunction(new \Twig\TwigFunction('current_url_with_query', function($opts) { return current_url_with_query($opts); }));

$twig->addFunction(new \Twig\TwigFunction('user_avatar_image_tag', function($user, $attrs) { return user_avatar_image_tag($user, $attrs); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('user_banner_image_tag', function($user, $attrs) { return user_banner_image_tag($user, $attrs); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('user_star_image_tags', function($user) { return user_star_image_tags($user); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('country_flag_image_tag', function($country, $attrs = []) { return country_flag_image_tag($country, $attrs); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('chart_icon_image_tag', function($user) { return chart_icon_image_tag($user); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('award_image_tag', function($award) { return award_image_tag($award); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('boxart_image_tag', fn($boxart, $options = []) => boxart_image_tag($boxart, $options), ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('game_rankbutton', fn($x, $y, $z) => game_rankbutton($x, $y, $z), ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('global_rankbutton', fn($x, $y) => global_rankbutton($x, $y), ['is_safe' => ['html']]));

//User Input
$twig->addFunction(new \Twig\TwigFunction('option_selected', function($a, $b) { return option_selected($a, $b); }));
$twig->addFunction(new \Twig\TwigFunction('has_access', function($role_or_roles, $user_id = -1) { return Authorization::has_access($role_or_roles, $user_id); }));
$twig->addFunction(new \Twig\TwigFunction('hidden_field', function($name, $obj, $options = []) { return hidden_field($name, $obj, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('text_field', function($name, $obj, $options = []) { return text_field($name, $obj, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('email_field', function($name, $obj, $options = []) { return email_field($name, $obj, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('password_field', function($name, $obj, $options = []) { return password_field($name, $obj, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('timezone_select_field', function($name, $obj, $options = []) { return timezone_select_field($name, $obj, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('select_field', function($name, $obj, $options = []) { return select_field($name, $obj, $options); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('slider_field', function($name, $obj) { return slider_field($name, $obj); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('radio_field', function($name, $obj, $yes) { return radio_field($name, $obj, $yes); }, ['is_safe' => ['html']]));

$twig->addFunction(new \Twig\TwigFunction('prettify', function($text) { return prettify($text); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('highlight_text', function($text, $pattern) { return highlight_text($text, $pattern); }, ['is_safe' => ['html']]));

$twig->addFunction(new \Twig\TwigFunction('format_month', function($time, $apply_timezone = true) { global $t; return $t->FormatDateTime($time, 'month', $apply_timezone); }));
$twig->addFunction(new \Twig\TwigFunction('format_date', function($time, $apply_timezone = true) { global $t; return $t->FormatDateTime($time, 'date', $apply_timezone); }));
$twig->addFunction(new \Twig\TwigFunction('format_datetime', function($time, $apply_timezone = true) { global $t; return $t->FormatDateTime($time, 'datetime', $apply_timezone); }));

$twig->addFunction(new \Twig\TwigFunction('distance_of_time_in_words', function($time_a, $time_b) { return distance_of_time_in_words($time_a, $time_b); }, ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('nth', function($n) { return nth($n); }));

//Aliases for better readibility
$twig->addFunction(new \Twig\TwigFunction('FormatAsNth', function($n) { return nth($n); }));
$twig->addFunction(new \Twig\TwigFunction('GetLocalisedString', function($key, $bindings = []) { return t($key, $bindings); }));

function sort_url($sort, $column, $default_direction = 'ASC') {
  $other_direction = $default_direction == 'ASC' ? 'DESC' : 'ASC';

  $params = ['sort' => $sort == "{$column}{$default_direction}" ? "{$column}{$other_direction}" : "{$column}{$default_direction}"];

  return current_url_with_query($params);
}

function sort_arrow($sort, $column) {
  if ($sort === "{$column}ASC") {
    return '<img src="/images/sort_up.gif" width="12" height="12" alt="sorted in ascending order"/>';
  } else if ($sort === "{$column}DESC") {
    return '<img src="/images/sort_down.gif" width="12" height="12" alt="sorted in descending order"/>';
  }
}
$twig->addFunction(new \Twig\TwigFunction('sort_url', fn($sort, $column, $default_direction = 'ASC') => sort_url($sort, $column, $default_direction), ['is_safe' => ['html']]));
$twig->addFunction(new \Twig\TwigFunction('sort_arrow', fn($sort, $column, $default_direction = 'ASC') => sort_arrow($sort, $column), ['is_safe' => ['html']]));

function truncate($string, $limit) {
  if (strlen($string) > $limit) {
    return substr($string, 0, $limit - 3) . '…';
  }
  return $string;
}

$twig->addFilter(new \Twig\TwigFilter('truncate', fn($string, $limit) => truncate($string, $limit)));
