<?php

namespace Controllers\Home;

class Index extends \Controller {
  public function action() {
    \Authorization::authorize('Guest');

    global $t;
    $t->CacheGameNames();

    return [
      'page_title' => t('general_home'),
    ];
  }
}
