<?php

namespace Controllers\Home;

class Team extends \Controller {
  public function action() {
    \Authorization::authorize('Guest');

    $staff_users = $this->staff();
    $translators = $this->translators();

    $sections = [
      [
        'title' => 'Owner',
        'star'  => '/images/Star_Pink16.png',
        'users' => $this->filter_by_user_group($staff_users, 'Owner'),
        'note'  => t('general_team3'),
      ],
      [
        'title' => 'Administrators',
        'star'  => '/images/Star_Red16.png',
        'users' => $this->filter_by_user_group($staff_users, 'Admin'),
      ],
      [
        'title' => 'Global Moderators',
        'star'  => '/images/Star_Blue16.png',
        'users' => $this->filter_by_user_group($staff_users, 'GlobalMod'),
      ],
      [
        'title' => 'Developers',
        'star'  => '/images/Star_Violet16.png',
        'users' => $this->filter_by_user_group($staff_users, 'Dev'),
      ],
      [
        'title' => 'Proof Moderators',
        'star'  => '/images/Star_LightGreen16.png',
        'users' => $this->filter_by_user_group($staff_users, 'ProofMod'),
      ],
      [
        'title' => 'Game Moderators',
        'star'  => '/images/Star_Green16.png',
        'users' => $this->filter_by_user_group($staff_users, 'GameMod'),
      ],
      [
        'title' => 'Designers',
        'star'  => '/images/Star_Yellow16.png',
        'users' => $this->filter_by_user_group($staff_users, 'Designer'),
      ],
      [
        'title' => 'Newswriters',
        'star'  => '/images/Star_Black16.png',
        'users' => $this->filter_by_user_group($staff_users, 'Newswriter'),
      ],
      [ 'title' => 'Translators',
        'star'  => '/images/Star_Orange16.png',
        'users' => $translators,
      ],
      [
        'title' => 'Retired staff members',
        'users' => array_filter($staff_users, function($user) { return $user['end_date'] != 'present'; }),
        'note'  => t('general_team4'),
      ],
    ];

    return [
      'page_title' => t('general_footer_team'),
      'sections' => $sections,
    ];
  }

  private function staff() {
    $staff_users = database_fetch_all("
      SELECT users.*, staff_history.start_date, staff_history.end_date
      FROM users
      INNER JOIN staff_history ON users.user_id = staff_history.staff_id
      ORDER by users.owner_check DESC, users.user_groups DESC, users.username ASC
    ", []);

    foreach ($staff_users as &$user) {
      $user['start_date'] = date('F, Y', strtotime($user['start_date']));
      if ($user['end_date']) {
        $user['end_date']   = date('F, Y', strtotime($user['end_date']));
      } else {
        $user['end_date'] = 'present';
      }

      if ($user['owner_check']) {
        $user['start_date'] = 'April, 2010';
      }
    }
    unset($user);

    return $staff_users;
  }

  private function translators() {
    return database_fetch_all("
      SELECT users.*
      FROM users
      INNER JOIN translator_info USING(user_id)
      GROUP BY user_id
      ORDER by users.owner_check DESC, users.user_groups DESC, users.username ASC
    ", []);
  }

  private function filter_by_user_group($users, $group) {
    return array_filter(
      $users,
      function($user) use ($group) { return \Authorization::user_has_role($user, $group); }
    );
  }
}
