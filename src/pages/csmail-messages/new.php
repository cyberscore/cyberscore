<?php

Authorization::authorize('User');

$to_id = $_GET['to'] ?? NULL;
$reply_to_id = $_GET['reply_id'] ?? NULL;
$is_forward = ($_GET['forward'] ?? NULL) == 'true';

$body = "";
$subject = "";
$to_username = "";

if ($to_id != NULL) {
  $to_id = explode(",", $to_id);
  $to = UsersRepository::where(['user_id' => $to_id]);

  if (count($to) != count($to_id)) {
    $cs->LeavePage('/csmails', "Couldn't find recipient user");
  }

  $to_username = implode(", ", pluck($to, 'username'));
}

// TODO: if it's a reply, should we be ignoring `params.to`?
if ($reply_to_id != NULL) {
  $original_message = CSMailMessagesRepository::get($reply_to_id);

  if ($original_message == NULL) {
    $cs->LeavePage('/csmails', "Couldn't find message to reply to");
  }

  if ($original_message['from_id'] != $current_user['user_id'] && $original_message['to_id'] != $current_user['user_id']) {
    $cs->LeavePage('/csmails', "Couldn't find message to reply to");
  }

  $subject = str_replace(["FW: ","Re: "],"",$original_message['subject']);
  $body = "[quote]" . $original_message['message'] . "[/quote]";

  if ($is_forward) {
    $subject = "Re: $subject";
  } else {
    $subject = "FW: $subject";
    $to_username = $to['username'];
  }
}

render_with('csmail-messages/new', [
  'page_title' => $t['csmail_compose_title'],
  'reply_to_id' => $reply_to_id,
  'is_forward' => $is_forward,
  'to_username' => $to_username,
  'subject' => $subject,
  'body' => $body,
]);
