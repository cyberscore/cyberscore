<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::number('reply_to_id'),
  Params::str('forward'),
  Params::str('message'),
  Params::str('subject'),
  Params::str('to'),
]);

$reply_to_id = $params['reply_to_id'] ?? "";
$to_username = $params['to'];
$subject = trim($params['subject']);
$body = trim($params['message']);
$is_forward = ($params['forward'] ?? "") == "true";

if (empty($body) || empty($subject)) {
  $cs->WriteNote(false, $t['csmail_error_no_sbj_or_msg']);
  render_with('csmail-messages/new', [
    'page_title' => t('csmail_compose_title'),
    'reply_to_id' => $reply_to_id,
    'is_forward' => $is_forward,
    'to_username' => $to_username,
    'subject' => $subject,
    'body' => $body,
  ]);
  $cs->Exit();
}

// TODO: if it's a reply, should we be ignoring `params.to`?
$recipients = [];
foreach (explode(",", $params['to']) as $recipient_id) {
  $to = UsersRepository::find_by(['username' => trim($recipient_id)]);

  if ($to == NULL) {
    $cs->WriteNote(false, $t['csmail_error_faulty_username']);
    render_with('csmail-messages/new', [
      'page_title' => t('csmail_compose_title'),
      'reply_to_id' => $reply_to_id,
      'is_forward' => $is_forward,
      'to_username' => $to_username,
      'subject' => $subject,
      'body' => $body,
    ]);
    $cs->Exit();
  }

  if ($to['user_id'] == $current_user['user_id']) {
    $cs->WriteNote(false, $t['csmail_error_send_self']);
    render_with('csmail-messages/new', [
      'page_title' => t('csmail_compose_title'),
      'reply_to_id' => $reply_to_id,
      'is_forward' => $is_forward,
      'to_username' => $to_username,
      'subject' => $subject,
      'body' => $body,
    ]);
    $cs->Exit();
  }

  $recipients []= $to;
}

$original_message = NULL;
if ($reply_to_id) {
  $original_message = CSMailMessagesRepository::get($reply_to_id);

  if ($original_message == NULL || ($original_message['from_id'] != $current_user['user_id'] && $original_message['to_id'] != $current_user['user_id'])) {
    $cs->WriteNote(false, "Couldn't find message to reply to");
    render_with('csmail-messages/new', [
      'page_title' => t('csmail_compose_title'),
      'reply_to_id' => $reply_to_id,
      'is_forward' => $is_forward,
      'to_username' => $to_username,
      'subject' => $subject,
      'body' => $body,
    ]);
    $cs->Exit();
  }
}

foreach ($recipients as $recipient) {
  $message_id = database_insert('csmail', [
    'from_id' => $current_user['user_id'],
    'to_id' => $recipient['user_id'],
    'message_date' => database_now(),
    'subject' => $subject,
    'message' => $body,
    'to_status' => 0,
    'from_status' => 4,
    'to_deleted' => 0,
    'from_deleted' => 0,
    'csmail_folder_id' => 0,
  ]);

  if ($original_message) {
    $cur_status = $original_message['to_status'];
    if (($cur_status == 2 && $is_forward) || ($cur_status == 3 && !$is_forward)) {
      $new_status = 5;
    } else if ($cur_status == 3) {
      $new_status = 3;
    } else if ($cur_status == 2) {
      $new_status = 2;
    } else if ($cur_status == 1 && $is_forward) {
      $new_status = 3;
    } else if ($cur_status == 1 && !$is_forward) {
      $new_status = 2;
    }
    database_update_by('csmail', ['to_status' => $new_status], [
      'csmail_id' => $original_message['csmail_id'],
      'to_id' => $current_user['user_id'],
    ]);
  }

  if ($recipient['email_pm'] == "y") {
    send_email(
      [$recipient['email']],
      'emails/csmail_notification',
      'Cyberscore - Someone sent you a message',
      [
        'message' => str_replace(
          ['[to username]', '[from username]', '[subject]', '[message]', '[link]'],
          [
            $recipient['username'],
            $current_user['username'],
            $subject,
            $message,
            $config['app']['base_url'] . "/csmail-messages/$message_id",
          ],
          t('csmail_automatic_mail')
        ),
        'automated_warning' => t('general_automatic_email_2'),
        'signature' => t('general_automatic_email_1'),
      ]
    );
  }
}

$cs->LeavePage("/csmails/1");
