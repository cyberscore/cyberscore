<?php

$article = database_get(database_select("
  SELECT
    news.*,
    editor.username AS edit_username
  FROM news
  LEFT JOIN users editor ON (news.edit_user_id = editor.user_id)
  WHERE news_id = ?
", 'i', [$_GET['id']]));

if ($article['user_id'] == $current_user['user_id']) {
  Authorization::authorize('User');
} else {
  Authorization::authorize('Newswriter');
}

$article['blog_name'] = Article::BlogName($article['user_id']);

render_with('articles/edit', [
  'page_title' => 'Edit article',
  'article' => $article,
]);
