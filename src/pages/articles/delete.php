<?php

$article = database_find_by('news', ['news_id' => $_GET['id']]);

if (!$article) {
  $cs->PageNotFound();
}

Authorization::authorize('GlobalMod', $article['user_id']);
Article::DeleteArticle($article['news_id']);

$cs->WriteNote(true, "You have deleted the article.");
$cs->RedirectToPreviousPage();
