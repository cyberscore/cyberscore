<?php

$news = database_get_all(database_select("
  SELECT news.*, COUNT(news_comments.news_id) AS num_comments
  FROM news
  LEFT JOIN news_comments USING(news_id)
  WHERE news.article_type = 2
  GROUP BY news.news_id
  ORDER BY news.news_id DESC
", '', []));

foreach ($news as &$article) {
  $article['headline'] = $t->GetNewsHeadline($article['news_id']);
  $article['blog_name'] = Article::BlogName($article['user_id']);
}
unset($article);

render_with('articles/index', [
  'page_title' => t('index_user_articles'),
  'news' => $news,
]);
