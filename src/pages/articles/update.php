<?php

Authorization::authorize('User');

$article = database_find_by('news', ['news_id' => $_GET['id']]);

$poster = $current_user;

if ($_POST['poster']) {
  if (!Authorization::has_access('GlobalMod')) {
    $cs->LeavePage("You do not have permission to post as someone else.");
  }

  $poster = UsersRepository::get($_POST['poster']);
}

if (Authorization::has_access("GlobalMod") || $article['user_id'] == $current_user['user_id']) {
  Article::EditArticle(
    $poster,
    $_POST['headline'],
    $_POST['news_text'],
    $current_user['user_id'],
    $article['news_id'],
  );
  $cs->WriteNote(true, "You have successfully edited the article.");
} else {
  $cs->LeavePage("You do not have permission to edit this article.");
}

switch ($article['article_type']) {
case 2:
  redirect_to('/articles');
  break;
case 13:
  redirect_to('/changelog');
  break;
default:
  redirect_to('/news');
  break;
}
