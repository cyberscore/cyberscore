<?php

$params = filter_post_params([
  Params::number('article_type'),
  Params::str('headline'),
  Params::str('news_text'),
  Params::str('usergroup'),
]);

if ($params['article_type'] != 2) {
  Authorization::authorize('Newswriter');
} else {
  Authorization::authorize('User');
}

$poster = $current_user;

if ($_POST['poster']) {
  if (!Authorization::has_access('GlobalMod')) {
    $cs->LeavePage("You do not have permission to post as someone else.");
  }

  $poster = UsersRepository::get($_POST['poster']);
}

$article_id = Article::AddArticle(
  $poster,
  $params['article_type'],
  $params['headline'],
  $params['news_text'],
  $params['usergroup']
);

switch ($params['article_type']) {
case 2:
  redirect_to('/articles');
  break;
case 13:
  redirect_to('/changelog');
  break;
default:
  redirect_to('/news');
  break;
}
