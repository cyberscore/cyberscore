<?php

Authorization::authorize('UserMod');

$record_stats = database_get(database_select("
  SELECT
    SUM(IF(rec_status IN(1,5),1,0)) AS pending_records,
    SUM(IF(rec_status IN(2,6),1,0)) AS investigated_records
  FROM records
", '', []));

$reported_records = database_get_all(database_select("
  SELECT *
  FROM records
  JOIN levels USING (level_id)
  INNER JOIN users ON records.user_id = users.user_id
  WHERE rec_status IN (1,2,5,6)
  ORDER by rec_status ASC, proof_deadline, users.username
", '', []));

foreach ($reported_records as &$record) {
  $record['moderator'] = database_get(database_select("
    SELECT user_id, username
    FROM staff_tasks
    JOIN users USING (user_id)
    WHERE tasks_type = 'record_investigated' AND task_id = ?
    ORDER BY entry_id DESC LIMIT 1
  ", 's', [$record['record_id']]));

  $record['reporter'] = database_get(database_select("
    SELECT user_id, username
    FROM users
    WHERE user_id = ?
  ", 's', [$record['rec_reporter']]));
}
unset($record);

render_with('reported-records/index', [
  'page_title' => 'Reported records',
  'reported_records' => $reported_records,
  'record_stats' => $record_stats,
]);
