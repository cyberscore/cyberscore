<?php

Authorization::authorize('GameMod');

$root = $config['app']['root'];

// Make sure it's a gif, jpg or png
$icon_file = $_FILES['icon_file'];

$params = filter_post_params([
  Params::number('entity_id'),
  Params::number('game_id'),
]);

$game_id = $params['game_id'];
$entity_id = $params['entity_id'];

if (!$entity_id) {
  $cs->WriteNote(false, "No entity ID was included.");
  http_response_code(400);
  render_json(['error' => 'No entity ID was included.']);
  return;
}

if (!is_uploaded_file($icon_file['tmp_name'])) {
  http_response_code(400);
  render_json(['error' => "You didn't specify a file to upload!"]);
  return;
}

if ($icon_file['size'] > 8192000) {
  http_response_code(400);
  render_json(['error' => "The uploaded file is too big. Pictures must have a size of 8 megabyte or lower."]);
  return;
}

$file_info = getimagesize($icon_file['tmp_name']);
if ($file_info == false || $file_info[2] > 3) {
  http_response_code(400);
  render_json(['error' => "The uploaded file could not be loaded for processing. Please ensure you use a JPEG, PNG or GIF."]);
  return;
}

// Create user directory if needed or try to delete the old picture
if (!is_dir("$root/public/uploads/entities/$game_id")) {
  mkdir("$root/public/uploads/entities/$game_id", 0777);
} else if (file_exists("$root/uploads/entities/$game_id/$entity_id.png")) {
  unlink("$root/public/uploads/entities/$game_id/$entity_id.png");
}

// Prepare the icon
$image = new Imagick($icon_file['tmp_name']);
if ($image->getImageWidth() > 256 || $image->getImageHeight() > 256) {
  $image->resizeImage(256, 256, imagick::FILTER_LANCZOS, 1, true);
}
$image->writeImage("$root/public/uploads/entities/$game_id/$entity_id.png");

render_json(['success' => true]);
