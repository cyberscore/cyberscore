<?php

// TODO: game-groups-groups is a stupid name, but game-groups/update was taken.
// I probably need to rename that action first and then rename this one.

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);
if (!$game) {
  HTTPResponse::PageNotFound();
}

$params = Params::ParsePost([
  'lang_id' => Params::integer(),
  'groups'  => Params::array(
    Params::object([
      'group_id'   => Params::integer(),
      'group_pos'  => Params::float(),
      'ranked'     => Params::integer(),
      'group_name' => Params::string(),
    ]),
  ),
]);

usort($params['groups'], fn($a, $b) => $a['group_pos'] <=> $b['group_pos']);

foreach ($params['groups'] as $i => $group) {
  database_update_by(
    'level_groups',
    [
      'group_pos' => $i + 1,
      'ranked'    => $group['ranked'],
    ],
    ['group_id' => $group['group_id']]);

  $t->ChangeGroupName($params['lang_id'], $group['group_id'], $group['group_name']);
}

$cs->WriteNote(true, $t->GetChanges());
redirect_to_back();
