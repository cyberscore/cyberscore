<?php

$sort = $_GET['sort'] ?? 'submissions';

$show_updates = $_GET['updates'] ?? 'no';

switch ($sort) {
case 'rpc':
  $sorting = "rpc DESC, recent_subs DESC";
  $sorting_updates = "rpc DESC, recent_subs_updates_games DESC";
  break;
default:
  $sorting = "recent_subs DESC, rpc DESC";
  $sorting_updates = "recent_subs_updates_games DESC, rpc DESC";
}

$now = strtotime("now");

$days_span = $_GET['days'] ?? 7;
switch ($days_span) {
case 'week':
  $date_from = strtotime("monday last week 00:00:00");
  $date_to = strtotime("sunday last week 23:59:59");

  $title = 'Top of the week from ' . date('Y-m-d', $date_from) . ' to ' . date('Y-m-d', $date_to);
  break;
case 'newweek':
  $date_from = strtotime("first day of this week 00:00:00");
  $date_to = $now;

  $title = 'Top of the week from ' . date('Y-m-d', $date_from) . ' to now';
  break;
case 'month':
  $date_from = strtotime("first day of last month 00:00:00");
  $date_to = strtotime("last day of last month 23:59:59");

  $title = 'Top of the month from ' . date('Y-m-d', $date_from) . ' to ' . date('Y-m-d', $date_to);
  break;
case 'newmonth':
  $date_from = strtotime("first day of this month 00:00:00");
  $date_to = $now;

  $title = 'Top of the month from ' . date('Y-m-d', $date_from) . ' to now';
  break;
default:
  $days_span = intval($days_span);
  $date_from = strtotime("-$days_span days");
  $date_to = $now;

  $title = str_replace('[days]', $days_span, $t['latestsubstats_header']);
  break;
}

$debug = date('Y-m-d H:i:s', $date_from) . " to " . date('Y-m-d H:i:s', $date_to);

$interval = "records.original_date >= '" . date('Y-m-d H:i:s', $date_from) . "' AND records.original_date <= '" . date('Y-m-d H:i:s', $date_to) . "'";
$update_interval = "update_date >= '" . date('Y-m-d H:i:s', $date_from) . "' AND update_date <= '" . date('Y-m-d H:i:s', $date_to) . "'";

// Game ID 2000 is the development test game.
$game_list = database_get_all(database_select("
  SELECT
    COUNT(records.game_id) AS recent_subs,
    records.game_id,
    (games.all_charts) AS num_charts,
    COUNT(records.game_id) / (games.all_charts) AS rpc
  FROM records
  JOIN games USING(game_id)
  WHERE $interval AND game_id != 2000
  GROUP BY records.game_id
  ORDER BY $sorting LIMIT 100
", '', []));

// Game ID 2000 is the development test game.
$top_users = database_get_all(database_select("
  SELECT
    COUNT(records.user_id) AS recent_subs,
    records.user_id,
    users.username
  FROM records
  JOIN users USING(user_id)
  WHERE $interval AND game_id != 2000
  GROUP BY records.user_id
  ORDER BY recent_subs DESC LIMIT 100
", '', []));

// Game ID 2000 is the development test game.
if ($show_updates == "yes") {
  $result = database_get_all(database_select("
    SELECT game_id, COUNT(*) AS count
    FROM record_history
    WHERE $update_interval AND update_type IN ('u', 'f') AND game_id != 2000
    GROUP BY game_id
    ORDER BY game_id
  ", '', []));

  $updates_by_game_id = index_by($result, 'game_id');

  // Game ID 2000 is the development test game.
  $result = database_get_all(database_select("
    SELECT user_id, COUNT(*) AS count
    FROM record_history
    WHERE $update_interval AND update_type IN ('u', 'f') AND game_id != 2000
    GROUP BY user_id
    ORDER BY user_id
  ", '', []));

  $updates_by_user_id = index_by($result, 'user_id');
} else {
  $updates_by_game_id = [];
  $updates_by_user_id = [];
}

$t->CacheGameNames();

render_with('latest-submissions-stats/index', [
  'page_title' => t('latestsubstats_title'),
  'title' => $title,
  'top_users' => $top_users,
  'debug' => $debug,
  'days_span' => $days_span,
  'game_list' => $game_list,
  'show_updates' => $show_updates,
  'updates_by_game_id' => $updates_by_game_id,
  'updates_by_user_id' => $updates_by_user_id,
]);
