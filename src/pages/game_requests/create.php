<?php

Authorization::authorize('User');

//Data validation to ensure all required fields are entered.
if (empty($_POST['Game_Name'])) {
  $cs->LeavePage('/game_requests', $t['gamerequests_error1']);
}

if (empty($_POST['Platforms'])) {
  $cs->LeavePage('/game_requests', $t['gamerequests_error2']);
}

if (empty($_POST['Charts'])) {
  $cs->LeavePage('/game_requests', $t['gamerequests_error3']);
}

$game_request_id = database_insert('game_requests', [
  'handled' => 0,
  'submitter_id' => $current_user['user_id'],
  'date_requested' => database_now(),
  'game_name' => $_POST['Game_Name'],
  'platforms' => $_POST['Platforms'],
  'entities' => $_POST['Entities'],
  'charts' => $_POST['Charts'],
  'rules' => $_POST['Rules'],
  'regiondiff' => $_POST['Region_Differences'],
  'platformdiff' => $_POST['Platform_Differences'],
  'patches' => $_POST['Patches'],
  'boxart' => $_POST['Boxarts'],
  'extra_info' => $_POST['Extra_Information'],
  'chartcount' => $_POST['Chart_Count'],
  'exampleproof' => $_POST['Example_Proof'],
  'game_id' => $_POST['game_id'],
]);

Notification::NewGameRequest($game_request_id, $current_user['user_id'])
  ->DeliverToGroups(['GameMod', 'GlobalMod', 'Admin'], ['except' => [$current_user['user_id']]]);

$cs->WriteNote(true, $t['gamerequests_submitted']);
$cs->Leavepage('/game_requests');
