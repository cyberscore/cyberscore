<?php

$request_id = $_GET['id'];

$request = database_get(database_select("
  SELECT
    gamereq_id,
    submitter_id,
    submitter.user_id,
    submitter.username,
    game_name, date_requested, platforms, entities,
    charts,
    rules, regiondiff, platformdiff, patches, boxart, extra_info,
    game_id, handled, chartcount, exampleproof
  FROM game_requests
  LEFT JOIN users submitter ON submitter.user_id = game_requests.submitter_id
  WHERE gamereq_id = ?
  ORDER BY gamereq_id ASC
", 's', [$request_id]));

if (!$request) {
  $cs->LeavePage("/game_requests", "Couldn't find that game request");
}

$comments = tree_sort(database_get_all(database_select("
  SELECT
    users.*,
    request_comments.*,
    request_comments.comment_time AS comment_date,
    'game_requests' AS commentable_type,
    request_comments.gamereq_id AS commentable_id
  FROM request_comments
  JOIN users USING(user_id)
  WHERE request_comments.gamereq_id = ?
  ORDER BY request_comments.comment_time ASC
", 'i', [$request['gamereq_id']])), 'comment_id', 'reply_id');

$can_edit = Authorization::has_access('GameMod') || ($request['handled'] == 0 && $current_user && $current_user['user_id'] == $request['submitter_id']);

render_with('game_requests/show', [
  'page_title' => 'View game requests',
  'request' => $request,
  'comments' => $comments,
  'can_edit' => $can_edit,
]);
