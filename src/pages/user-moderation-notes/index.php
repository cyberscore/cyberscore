<?php

Authorization::authorize('UserMod');

$users = database_get_all(database_select("
  SELECT users.*, moderators.username as moderator_username
  FROM users
  LEFT JOIN users moderators ON users.mod_notes_id = moderators.user_id
  WHERE users.mod_notes != ''
  ORDER BY users.username ASC
", '', []));

render_with('user-moderation-notes/index', [
  'page_title' => 'Moderator Notes',
  'users' => $users,
]);
