<?php

Authorization::authorize('Guest');

$user_search = $_GET['user_search'] ?? '';
$sort = $_GET['sort'] ?? 'usernameASC';
$letter = $_GET['letter'] ?? '';
$country_id = $_GET['country'] ?? 0;

$filters = [];

if (strlen($letter) == 1) {
  if ($letter == '0') {
    $filters['username'] = database_regexp('^[0-9]+');
  } elseif ($letter >= 'A' && $letter <= 'Z') {
    $filters['username'] = database_like("$letter%");
  }
} else if ($user_search != '') {
  $filters["username"] = database_like("%" . database_escape_like($user_search) . "%");
}

$order = [
  'countryASC'      => 'ORDER BY country_code_filter ASC, country_code ASC, username ASC',
  'emailASC'        => 'ORDER BY email ASC',
  'useridASC'       => 'ORDER BY user_id ASC',
  'usernameASC'     => 'ORDER BY username ASC',
  'userroleDESC'    => 'ORDER BY user_groups DESC, username ASC',
  'recordcountDESC' => 'ORDER BY num_records DESC, username ASC',
  'joindateDESC'    => 'ORDER BY date_joined DESC',
  'lastseenDESC'    => 'ORDER BY last_seen_sort DESC',

  'countryDESC'     => 'ORDER BY country_code_filter ASC, country_code DESC, username ASC',
  'emailDESC'       => 'ORDER BY email DESC',
  'useridDESC'      => 'ORDER BY user_id DESC',
  'usernameDESC'    => 'ORDER BY username DESC',
  'userroleASC'     => 'ORDER BY user_groups ASC, username ASC',
  'recordcountASC'  => 'ORDER BY num_records ASC, username ASC',
  'joindateASC'     => 'ORDER BY date_joined ASC',
  'lastseenASC'     => 'ORDER BY last_seen_sort ASC',
][$sort] ?? 'ORDER BY username ASC';

$t->CacheCountryNames();

$countries = pluck(database_fetch_all('SELECT country_id FROM countries ORDER BY country_id', []), 'country_id');

$country_id = array_search($country_id, $countries) != false ? $country_id : 0;

if ($country_id != 0) {
  $filters["users.country_id"] = $country_id;
}

[$where_sql, $where_binds] = database_where_sql_bind($filters);

$num_users = database_value("SELECT COUNT(*) FROM users WHERE $where_sql", $where_binds);
$num_pages = ceil($num_users / 100);

$page = isset($_GET['page']) && $_GET['page'] <= $num_pages ? $_GET['page'] : 1;
$offset = ($page - 1) * 100;

$users = database_fetch_all("
  SELECT
    users.user_id,
    users.username,
    users.forename,
    users.surname,
    users.email, users.auth_verified,
    users.date_joined,
    users.date_lastseen,
    users.country_code,
    users.country_id,
    IF(country_code='--', 1, 0) AS country_code_filter,
    COUNT(records.user_id) AS num_records,
    users.user_groups,
    users.owner_check,
    user_prefs.lastseen_hidden,
    IF(lastseen_hidden, '2000-01-01', date_lastseen) AS last_seen_sort
  FROM users
  LEFT JOIN user_prefs USING (user_id)
  LEFT JOIN records USING(user_id)
  WHERE $where_sql
  GROUP BY users.username $order
  LIMIT 100
  OFFSET $offset
", $where_binds);

foreach ($users as &$user) {
  $user_stars = $cs->GetStarIcons($user);
  $user_roles = implode(', ', pluck($user_stars, 'title'));
  if ($user_roles === "") {
    $user_roles = 'User';
  }

  $user['user_roles'] = $user_roles;
}
unset($user);


$country_options = [0 => $t['general_countries_all']] + $t->GetCountryNames();

render_with('users/index', [
  'page_title' => 'User list',
  'page' => $page,
  'num_pages' => $num_pages,
  'num_users' => $num_users,
  'user_search' => $user_search,
  'users' => $users,
  'country_options' => $country_options,
  'country_id' => $country_id,
  'sort' => $sort,
  'letter' => $letter,
]);
