<?php

global $current_user;
global $_GET;

Authorization::authorize('Guest');

if (is_numeric($_GET['id'])) {
  $id = intval($_GET['id']);
  $user = database_get(database_select("
    SELECT * FROM users
    LEFT JOIN user_info USING(user_id)
    LEFT JOIN user_prefs USING(user_id)
    WHERE user_id = ?", 'i', [$id]));
} else {
  $id = $_GET['id'];
  $user = database_get(database_select("
    SELECT * FROM users
    LEFT JOIN user_info USING(user_id)
    LEFT JOIN user_prefs USING(user_id)
    WHERE username = ?", 's', [$id]));

  // TODO: workaround because many modules use $id instead of referring to $user.
  $id = $user['user_id'];
}

if ($user == null) {
  $cs->LeavePage('/', 'The requested user does not exist.');
}

$iamme = $current_user != NULL && $user['user_id'] == $current_user['user_id'];

//  HOW TO ADD NEW MODULES:
//
//  1. Pick an identifier for your module. Something like 'submissions' or
//     'blog'.
//  3. Add it to the $sidebar variable below
//  4. NOW you can code your new module. Write your module in a
//     separate file: stats/modules/user/yourmodule.php.
//     Have fun x)

$friends_pref = $user['show_friends_list'];
$comment_pref = $user['userpage_comments'];
$applet_pref = $user['applet'];

$derpy_applet_mapping = [
  'ContactMe'   => 'contactdetails',
  'Submissions' => 'submissions',
  'CSR'         => 'starboard',
  'Ranked'      => 'starboard',
  'Arcade'      => 'arcade',
  'Speedrun'    => 'speedrun',
  'Solution'    => 'solution',
  'Badges'      => 'badges',
  'Blog'        => 'blog',
  'Comments'    => 'comments',
  'Feature'     => 'article',
];
$applet_pref = $derpy_applet_mapping[$applet_pref] ?? 'contactdetails';

$num_csp = database_single_value("SELECT COUNT(*) FROM gsb_cache_csp WHERE user_id = ?", 'i', [$user['user_id']]);
$num_medals = database_single_value('SELECT COUNT(*) FROM gsb_cache_medals WHERE user_id = ?', 'i', [$user['user_id']]);
$num_arcade = database_single_value('SELECT COUNT(*) FROM gsb_cache_arcade WHERE user_id = ?', 'i', [$user['user_id']]);
$num_speedrun = database_single_value('SELECT COUNT(*) FROM gsb_cache_speedrun WHERE user_id = ?', 'i', [$user['user_id']]);
$num_uc = database_single_value('SELECT COUNT(*) FROM gsb_cache_userchallenge WHERE user_id = ?', 'i', [$user['user_id']]);
$num_solution = database_single_value('SELECT COUNT(*) FROM gsb_cache_solution WHERE user_id = ?', 'i', [$user['user_id']]);
$num_collectible = database_single_value('SELECT COUNT(*) FROM gsb_cache_collectible WHERE user_id = ?', 'i', [$user['user_id']]);
$num_incremental = database_single_value('SELECT COUNT(*) FROM gsb_cache_incremental WHERE user_id = ?', 'i', [$user['user_id']]);
$has_submissions = $num_csp > 0 || $num_medals > 0 || $num_arcade > 0 || $num_speedrun > 0 || $num_solution > 0 || $num_uc > 0 || $num_collectible > 0 || $num_incremental > 0;

$sort = $_GET['sort'] ?? '';

$sidebar = [
  'General' => [
    'contactdetails' => 'Contact Details',
  ],
];

if ($has_submissions) {
  $sidebar['Records & submissions'] = [
    'submissions' => 'Submission statistics & latest submissions',
  ];

  if ($num_csp > 0) $sidebar['Records & submissions']['trophy'] = t('profilemodules_trophy_case');
  if ($num_csp > 0) $sidebar['Records & submissions']['starboard'] = 'All ranked submissions';
  if ($num_medals > 0) $sidebar['Records & submissions']['medal'] = 'Standard submissions';
  if ($num_arcade > 0) $sidebar['Records & submissions']['arcade'] = 'Arcade submissions';
  if ($num_speedrun > 0) $sidebar['Records & submissions']['speedrun'] = 'Speedrun submissions';
  if ($num_solution > 0) $sidebar['Records & submissions']['solution'] = 'Solution submissions';
  if ($num_uc > 0) $sidebar['Records & submissions']['challenge'] = 'User Challenge submissions';
  if ($num_collectible > 0) $sidebar['Records & submissions']['collectible'] = 'Collectible submissions';
  if ($num_incremental > 0) $sidebar['Records & submissions']['incremental'] = 'Incremental submissions';
}

$sidebar['Accomplishments'] = ['badges' => 'Cyberscore awards & Achievements'];
if ($iamme) $sidebar['Accomplishments']['rankbuttons'] = 'Your rankbuttons';

$sidebar['Community'] = ['blog' => 'Blog'];
if ($user['featured_article_id']) $sidebar['Community']['article'] = 'Featured article';
if ($comment_pref) $sidebar['Community']['comments'] = 'Comments';
if ($friends_pref) $sidebar['Community']['friends'] = 'Friends and followers';

$src = validate_module($sidebar, "user/{$user['user_id']}", $_GET['src'] ?? $applet_pref);

render_with('users/show', [
  'page_title' => t('profile_stats_title', ['username' => $user['username']]),
  'user' => $user,
  'has_banner' => Uploads::exists('user-banner', $user['user_id'], 'legacy'),
  'sidebar' => $sidebar,
  'src' => $src,
  'iamme' => $iamme,
]);
