<?php

Authorization::authorize('UserMod');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

if ($user['mod_notes_id']) {
  $moderator = UsersRepository::get($user['mod_notes_id']);
} else {
  $moderator = null;
}

$user_games = database_get_all(database_select("
  SELECT records.game_id, COUNT(records.game_id) AS num_subs, games.game_name
  FROM records, games
  WHERE records.game_id = games.game_id AND records.user_id = ?
  GROUP BY records.game_id
  ORDER BY game_name ASC
", 's', [$user['user_id']]));

$has_avatar = Uploads::exists('user-avatar', $user['user_id'], 'variant');
$has_banner = Uploads::exists('user-banner', $user['user_id'], 'legacy');

render_with('users/edit', [
  'page_title' => 'Edit user',
  'user' => $user,
  'moderator' => $moderator,
  'user_games' => $user_games,
  'has_avatar' => $has_avatar,
  'has_banner' => $has_banner,
]);
