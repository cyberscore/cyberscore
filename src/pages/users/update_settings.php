<?php

Authorization::authorize('UnverifiedUser');

if ($_POST['verify'] ?? NULL) {
  User::SendConfirmationEmail($current_user['user_id']);

  $cs->WriteNote(true, 'Verification email sent. Check your inbox and follow the instructions on the message.');
  redirect_to_back();
}

$user_settings_params = Params::ParsePost([
  // Public information
  'forename'   => Params::string()->optional(),
  'surname'    => Params::string()->optional(),
  'gender'     => Params::string()->optional(),
  'dob'        => Params::date()->optional()->nullable(),
  'profile'    => Params::string()->optional(),
  'country_id' => Params::string()->optional(),
  // Display settings
  'timezone' => Params::string()->optional(),
  // Contact details
  'im_aol'   => Params::string()->optional(),
  'im_icq'   => Params::string()->optional(),
  'im_yahoo' => Params::string()->optional(),
  'im_skype' => Params::string()->optional(),
  'website'  => Params::string()->optional(),

  // `email_ygb` is not supported anymore.
  // We still have the data in the database, in case it comes back.
  //
  //'email_ygb'            => Params::post('boolean')->optional(),
]);

$user_info_params = Params::ParsePost([
  // Contact details
  'twitter'       => Params::string()->optional(),
  'discord_tag'   => Params::string()->optional(),
  'youtube'       => Params::string()->optional(),
  'twitch'        => Params::string()->optional(),
  'steam_id'      => Params::string()->optional(),
  '3ds_fc'        => Params::string()->optional(),
  'wiiu_fc'       => Params::string()->optional(),
  'switch_fc'     => Params::string()->optional(),
  'pokemon_go_fc' => Params::string()->optional(),
  'psn_gamertag'  => Params::string()->optional(),
  'xbl_gamertag'  => Params::string()->optional(),
  'backloggery'   => Params::string()->optional(),

  // Site preferences, move to user_prefs?
  'featured_article_id' => Params::integer()->optional(),
  'blog_name'           => Params::string()->optional(),
]);

$user_prefs_params = Params::ParsePost([
  'medal_display'        => Params::enum(['plats', 'golds'])->optional(),
  'speedrun_display'     => Params::enum(['speedrunpoints', 'plats', 'golds'])->optional(),
  'trophy_display'       => Params::enum(['trophypoints', 'plats', 'golds'])->optional(),
  'date_format'          => Params::string()->optional(),
  'applet'               => Params::enum(['ContactMe', 'Submissions', 'CSR', 'Ranked', 'Arcade', 'Speedrun', 'Solution', 'Badges', 'Blog', 'Comments', 'Feature'])->optional(),
  'show_full_names'      => Params::boolean()->optional(),
  'show_entity_icons'    => Params::boolean()->optional(),
  'userpage_comments'    => Params::boolean()->optional(),
  // Chart icons
  'preferred_chart_icon' => Params::string()->optional(),

  'email_pm'             => Params::boolean()->optional(),
  'lastseen_hidden'      => Params::boolean()->optional(),

  // not available in the UI: Params::number('chart_sort'),
  // not used: Params::number('chart_limit'),
  // not used: Params::checkbox('index_forum_tags', 1, 0),
  // not used: Params::number('version'),
]);

$notification_params = Params::ParsePost([
  'ygb'                => Params::boolean()->optional(),
  'trophy_lost'        => Params::boolean()->optional(),
  'trophy_gained'      => Params::boolean()->optional(),
  'rec_approved'       => Params::boolean()->optional(),
  // rec_reported can't be turned off
  // rec_deleted can't be turned off
  'proof_refused'      => Params::boolean()->optional(),
  'supp'               => Params::boolean()->optional(),
  'pstring_edited'     => Params::boolean()->optional(),
  'pstring_added'      => Params::boolean()->optional(),
  'new_grequest'       => Params::boolean()->optional(),
  'greq_comment'       => Params::boolean()->optional(),
  'new_game'           => Params::boolean()->optional(),
  'reported_rec'       => Params::boolean()->optional(),
  'article_comment'    => Params::boolean()->optional(),
  'referral_confirmed' => Params::boolean()->optional(),
  'referral_rejected'  => Params::boolean()->optional(),
  //'referral_created' => Params::boolean()->optional(),
  'reply'              => Params::boolean()->optional(),
  'userpage_comments'  => Params::boolean()->optional(),
  'blog_follow'        => Params::boolean()->optional(),
  'blog_unfollow'      => Params::boolean()->optional(),
  'followed_posted'    => Params::boolean()->optional(),
]);

$password_params = Params::ParsePost([
  'current_password'      => Params::password()->optional(),
  'password'              => Params::password()->optional(),
  'password_confirmation' => Params::password()->optional(),
]);

$referral_params = Params::ParsePost([
  'referrer' => Params::integer()->optional(),
]);

/* Forum account temporarily disabled
$forum_params = Params::ParsePost([
  'forum_username' => Params::string()->optional(),
  'forum_password' => Params::password()->optional(),
]);
*/

database_update_by(
  'users',
  $user_settings_params,
  ["user_id" => $current_user['user_id']]
);

$country_code = database_single_value("
  SELECT countries.country_code FROM users JOIN countries USING (country_id) WHERE user_id = ?
", 'i', [$current_user['user_id']]);

database_update_by(
  'users',
  ['country_code' => $country_code],
  ["user_id" => $current_user['user_id']]
);

database_update_by(
  'user_info',
  $user_info_params,
  ["user_id" => $current_user['user_id']]
);

database_update_by(
  'notification_prefs',
  $notification_params,
  ["user_id" => $current_user['user_id']]
);

// validate user preferences
{
  if (isset($user_prefs_params['preferred_chart_icon'])) {
    $good = false;
    foreach (ChartIcon::all_selections($current_user) as [$label, $selections]) {
      $selections = array_filter($selections, fn($s) => $s['available']);
      if (in_array($user_prefs_params['preferred_chart_icon'], pluck($selections, 'id'))) {
        $good = true;
      }
    }

    if (!$good) {
      $cs->WriteNote(false, "You haven't unlocked that chart icon yet!");
      redirect_to_back();
    }
  }

  database_update_by(
    'user_prefs',
    $user_prefs_params,
    ["user_id" => $current_user['user_id']]
  );
}

if ($_POST['delete-avatar'] ?? NULL) {
  Uploads::remove('user-avatar', $current_user['user_id']);
}

if ($_FILES['avatar']) {
  Uploads::save('user-avatar', $current_user['user_id'], $_FILES['avatar']);
}

if ($_POST['delete-banner'] ?? NULL) {
  Uploads::remove('user-banner', $current_user['user_id']);
}

if ($_FILES['banner']) {
  Uploads::save('user-banner', $current_user['user_id'], $_FILES['banner']);
}

if (!empty($password_params['current_password'])) {
  $current_password = $password_params['current_password'];
  $password = $password_params['password'];
  $password_confirmation = $password_params['password_confirmation'];

  list($good_password, $error) = Authentication::validate_password(
    $password,
    $password_confirmation
  );

  if (!Authentication::password_verify($current_password, $current_user)) {
    $cs->WriteNote(false, 'Current password incorrect!');
  } else if (!$good_password) {
    $cs->WriteNote(false, $error);
  } else {
    Authentication::store_password($password, $current_user);

    $cs->WriteNote(true, 'Password changed');
  }
}

if (!empty($referral_params['referrer'])) {
  $referrer = $referral_params['referrer'];
  $referrer = database_get(database_select(
    'SELECT user_id, username FROM users WHERE user_id = ? OR username = ?',
    'is',
    [intval($referrer), $referrer]
  ));

  if ($referrer !== NULL) {
    referral_create($referral_params['referrer'], $current_user['user_id']);
    $cs->WriteNote(true, 'You have successfully set ' . h($referrer['username']) . ' (#' . $referrer['user_id'] . ') as your referral.');
  } else {
    $cs->WriteNote(false, 'You did not input a valid user ID or username. Please verify the user ID and try again.');
  }
}

/* Forum account temporarily disabled
if (!empty($forum_params['forum_username'])) {
  include($config['forum']['path'] . "/SSI.php");

  $has_linked_account = database_single_value('SELECT 1 FROM smf_accounts WHERE user_id = ?', 'i', [$current_user['user_id']]);
  $smf_member_id = database_single_value('SELECT id_member FROM smf_members WHERE member_name = ?', 's', [$forum_params['forum_username']]);
  $right_password = ssi_checkPassword($forum_params['forum_username'], $forum_params['forum_password'], true);

  if ($has_linked_account) {
    $cs->WriteNote(false, 'You already have a linked forum account');
  } else if ($smf_member_id === NULL) {
    $cs->WriteNote(false, 'Forum account not found');
  } else if (!$right_password) {
    sleep(3);
    $cs->WriteNote(false, 'Forum password was incorrect');
  } else {
    database_insert('smf_accounts', [
      'user_id' => $current_uset['user_id'],
      'smf_member_id' => $smf_member_id,
    ]);
    $cs->WriteNote(true, 'Forum account synched up');
  }
}
  */

database_update_by('users',
  ['updated_at' => database_now()],
  ["user_id" => $current_user['user_id']]);

// Reload current user
CurrentUser::load_from_user_id($current_user['user_id']);

redirect_to_back();
