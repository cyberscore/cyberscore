<?php

Authorization::authorize('Guest');

if ($current_user != NULL) {
  render_with('accounts/new', [
    'page_title' => t['register_title'],
    'params' => [],
  ]);
  $cs->Exit();
}

$params = filter_post_params([
  Params::str('username'),
  Params::str('email'),
  Params::str('password'),
  Params::str('password_confirmation'),
  Params::str('referrer'),
]);

$errors = [];

// username should be alphanumeric 3-20 chars
if (!preg_match("/^[a-zA-Z0-9]{3,20}$/", $params['username'])) {
  $errors []= t('register_error_bad_username');
}

if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
  $errors []= t('register_error_bad_email');
}

if ($params['password'] === '' || $params['password'] != $params['password_confirmation']) {
  $errors []= t('register_error_pw_missmatch');
}

if (database_find_by('users', ['username' => $params['username']]) != null) {
  $errors []= t('register_error_used_username');
}

if (database_find_by('users', ['email' => $params['email']]) != null) {
  $errors []= t('register_error_used_email');
}

if (!recaptcha_verify()) {
  $errors []= t('register_error_captcha');
}

if (count($errors) > 0) {
  $error_head = count($errors) == 1 ? t('register_error') : t('register_errors');
  $cs->WriteNote(false, $error_head, implode('<br/>', $errors));

  render_with('accounts/new', [
    'page_title' => t('register_title'),
    'params' => $params,
  ]);
} elseif (EmailBansRepository::is_banned($params['email'])) {
  // if the registration is using a banned email domain,
  // we pretend to register it but silently ignore it.

  log_event('accounts/shadowban', [
    'username' => $params['username'],
    'email' => $params['email'],
  ]);

  sleep(1);
  render_with("accounts/create", [
    'page_title' => t('regthanks_title'),
  ]);
} else {
  log_event('accounts/create', [
    'username' => $params['username'],
    'email' => $params['email'],
  ]);

  // Step 1: insert into cyberscore users database
  $user_id = database_insert('users', [
    'username' => $params['username'],
    'pword_new' => password_hash($params['password'], PASSWORD_DEFAULT),
    'email' => $params['email'],
    'register_ip' => get_ip(),
    'pword' => 'unused',
    'date_lastseen' => database_now(),
    'auth_verified' => false,
    'banned' => false,
    'auth_seed' => 7, // ???
    'date_joined' => database_now(),
    'secret_q' => '',
    'secret_q_answer' => '',
    'access' => 1,
    'skin_id' => 2,
    'email_ygb' => 'y',
    'country_code' => '--',
    'country_id' => 1,
    'dob' => NULL,
    'team_code' => '',
    'timezone' => 'America/New_York',
    'email_confirmation_required' => true,
  ]);

  database_insert("user_prefs", ["user_id" => $user_id, 'applet' => '', 'email_pm' => true]);
  database_insert("notification_prefs", ["user_id" => $user_id]);
  database_insert("user_info", ["user_id" => $user_id, 'blog_name' => '']);

  // Step 2: Referral handling
  if ($params['referrer'] != '') {
    if (is_numeric($params['referrer'])) {
      $referrer = database_find_by('users', ['user_id' => intval($params['referrer'])]);
    } else {
      $referrer = database_find_by('users', ['username' => $params['referrer']]);
    }

    if ($referrer != NULL) {
      referral_create($referrer['user_id'], $user_id);
    } else {
      $cs->WriteNote(false, 'We did not recognize the user ID in the referral field. You can try again by accessing your user settings later.');
    }
  }

  // Step 3: email this shiznit to the new person
  // User::SendConfirmationEmail($user_id);

  render_with("accounts/create", [
    'page_title' => t('regthanks_title'),
  ]);
}
