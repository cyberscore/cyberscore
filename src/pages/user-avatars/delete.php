<?php

Authorization::authorize('UserMod');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

Uploads::remove('user-avatar', $user['user_id']);

$cs->WriteNote(true, "{$user['username']}'s avatar has been successfully removed.");
redirect_to_back();
