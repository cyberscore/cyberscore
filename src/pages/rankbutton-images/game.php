<?php

$im = RankbuttonGenerator::game_rankbutton($_GET['game_id'] ?? null, $_GET['user_id'] ?? null);

if ($im) {
  header("Content-type: image/gif");
  imagegif($im);
  imagedestroy($im);
}
