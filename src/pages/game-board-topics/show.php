<?php

$topic = database_find_by('game_board_topics', ['topic_id' => $_GET['id']]);

if (!$topic) {
  $cs->PageNotFound();
}

$game = GamesRepository::get($topic['game_id']);

$messages = database_get_all(database_select("
  SELECT users.user_id, users.username, users.gender, users.user_groups,
  game_board_messages.message_id, game_board_messages.message_text, game_board_messages.message_date, game_board_messages.edit_date
  FROM game_board_messages
  JOIN users USING(user_id)
  WHERE game_board_messages.topic_id = ?
", 's', [$topic['topic_id']]));

$game_name = $t->GetGameName($game['game_id']);
$page_title = 'Discussion board for ' . $game_name . ' – ' . $topic['topic_title'];

render_with('game-board-topics/show', [
  'page_title' => $page_title,
  'topic' => $topic,
  'game' => $game,
  'game_name' => $game_name,
  'messages' => $messages,
]);
