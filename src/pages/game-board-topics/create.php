<?php

Authorization::authorize('User');

$game = GamesRepository::get($_POST['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

$params = filter_post_params([
  Params::str('topic_title'),
  Params::str('message_text'),
]);

if (empty($params['topic_title']) || empty($params['message_text'])) {
  $cs->Exit();
}

$topic_id = database_insert('game_board_topics', [
  'game_id' => $game['game_id'],
  'topic_title' => $params['topic_title'],
]);

database_insert('game_board_messages', [
  'game_id' => $game['game_id'],
  'topic_id' => $topic_id,
  'user_id' => $current_user['user_id'],
  'message_text' => $params['message_text'],
  'message_date' => database_now(),
]);

Flash::AddSuccess('Topic created');

redirect_to("/game-boards/{$game['game_id']}");
