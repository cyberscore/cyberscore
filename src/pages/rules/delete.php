<?php

Authorization::authorize('GameMod');

$rule_id = intval($_GET['id']);

if (!RulesRepository::exists($rule_id)) {
  $cs->PageNotFound();
  $cs->Exit();
}

$result = RulesRepository::delete($rule_id);

$cs->WriteNote(true, "The rule has been deleted.");

header("Location: /rules");
