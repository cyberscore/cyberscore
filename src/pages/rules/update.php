<?php

Authorization::authorize('GameMod');

$rule_id = intval($_GET['id']);
$params = filter_post_params([
  Params::str('rule_text'),
]);

if (!RulesRepository::exists($rule_id)) {
  $cs->PageNotFound();
}

$result = RulesRepository::update($rule_id, $params);

$cs->WriteNote(true, "The rule has been updated.");

if ($result['merged']) {
  $cs->WriteNote(
    false,
    "The rule's text you updated has matched an existing rule's text. As a
    result, the two rules have been merged into one."
  );
}

header("Location: /rules/$rule_id/edit");
