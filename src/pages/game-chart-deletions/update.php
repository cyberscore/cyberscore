<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->LeavePage('/games', 'The requested game does not exist.');
}

// There are multiple ways to select data in the form, via group or via individual charts. 
// We annotate groups with 'g' (for "group") and charts with 'l' (for "level"). 
// This code disseminates the information passed by the form into a functional set of chart IDs to action.
$chart_ids = [];
foreach ($_POST['delete'] as $chart_id) { $chart_ids []= $chart_id; }

// Error reporting for empty form.
if (empty($chart_ids)) {
  $cs->WriteNote(false, "No charts selected");
  redirect_to_back();
}

$records = RecordsRepository::where(['game_id' => $game['game_id'], 'level_id' => $chart_ids]);

foreach($chart_ids AS $single_chart) {
  // Fetch a bit more important info regarding the chart being deleted, because...
  $chart_info = LevelsRepository::get($single_chart);

  // ... we keep a minified entry of deleted charts in order to reference information when a chart is accidentally deleted and we need to reinstate records
  database_insert('levels_del', ['level_id' => $single_chart, 'game_id' => $game['game_id'], 'group_id' => $chart_info['group_id'], 'level_name' => $chart_info['level_name']]);
}

// Now, we're ready to start removing things. Record deletion handles its own redundancy, so we're just going to let it handle itself.
foreach ($records as $record) {
  RecordManager::DeleteRecord($record['record_id'], $current_user['user_id'], $record['level_id']);
}

// Be condemned to the void!
database_delete_by('translation_games', ['table_field' => 'chart_name', 'table_id' => $chart_ids]);
database_delete_by('chart_modifiers', ['level_id' => $chart_ids]);
database_delete_by('chart_modifiers2', ['level_id' => $chart_ids]);
database_delete_by('levels', ['level_id' => $chart_ids]);
database_delete_by('proofmailer', ['level_id' => $chart_ids]);

//Chart deletions need to prompt essentially a full rebuild of the game from the chart level.
Modifiers::UpdateChartFlooder($game['game_id']);
ChartCacheRebuilder::QueueGameChartsForRebuild($game['game_id']);
GameCacheRebuilder::QueueGameForRebuild($game['game_id']);


//Report what was deleted
$cs->WriteNote(false, count($chart_ids) . " selected charts have been deleted, including " . count($records) . " submitted records.");
redirect_to("/mod_scripts/edit_game_groups?game_id={$game['game_id']}");
