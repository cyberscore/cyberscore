<?php

$game = GamesRepository::get($_GET['game_id'] ?? $_GET['id']);

if (!$game) {
  HTTPResponse::PageNotFound();
}

switch ($_GET['type'] ?? 'p') {
case 'v':		$type_name = 'Video'; $proof_type = 'v'; break;
case 'p':		$type_name = 'Image'; $proof_type = 'p'; break;
default:		$type_name = 'Image'; $proof_type = 'p'; break;
}

$users = database_fetch_all("
  SELECT users.*, COUNT(1) AS record_count
  FROM users
  LEFT JOIN records USING (user_id)
  WHERE records.game_id = ? AND records.rec_status = 3 AND records.proof_type = ? AND records.linked_proof != ''
  GROUP BY users.user_id
  ORDER BY record_count DESC
  ", [$game['game_id'], $proof_type]);

$t->CacheGame($game['game_id']);
$game_name = $t->GetGameName($game['game_id']);

render_with('game-proofs/show', [
  'page_title' => 'Proofs for ' . $game_name,
  'users' => $users,
  'type_name' => $type_name,
  'proof_type' => $proof_type,
  'game' => $game,
]);
