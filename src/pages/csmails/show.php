<?php

Authorization::authorize('User');

$folder_id = intval($_GET['id'] ?? 0);

// Get folder (0 -> inbox, 1 -> outbox)
if ($folder_id == 0) {
  $folder = ['csmail_folder_id' => 0, 'user_id' => $current_user['user_id'], 'folder_name' => $t['csmail_inbox']];
} else if ($folder_id == 1) {
  $folder = ['csmail_folder_id' => 1, 'user_id' => $current_user['user_id'], 'folder_name' => $t['csmail_sent_items']];
} else {
  $folder = database_get(database_select("SELECT * FROM csmail_folders WHERE csmail_folder_id = ?", 's', [$folder_id]));

}

// check if the folder belongs to them
if (($folder['user_id'] ?? NULL) != $current_user['user_id']) {
  $cs->LeavePage('/csmails', $t['csmail_error_faulty_folder']);
}

$custom_folders = database_get_all(database_select("
  SELECT * FROM csmail_folders WHERE user_id = ?
", 's', [$current_user['user_id']]));

if ($folder_id == 1) {
  $messages = database_get_all(database_select("
    SELECT
      csmail_id,
      from_id,
      to_id,
      subject,
      message_date,
      to_status,
      from_user.username AS from_username,
      from_user.user_groups AS from_user_groups,
      to_user.username AS to_username,
      to_user.user_groups AS to_user_groups
    FROM csmail
    INNER JOIN users from_user ON csmail.from_id = from_user.user_id
    INNER JOIN users to_user ON csmail.to_id = to_user.user_id
    WHERE from_id = ? AND from_deleted = 0
    ORDER BY message_date DESC
  ", 's', [$current_user['user_id']]));
} else {
  $messages = database_get_all(database_select("
    SELECT
      csmail_id,
      from_id,
      to_id,
      subject,
      message_date,
      to_status,
      from_user.username AS from_username,
      from_user.user_groups AS from_user_groups,
      to_user.username AS to_username,
      to_user.user_groups AS to_user_groups
    FROM csmail
    INNER JOIN users from_user ON csmail.from_id = from_user.user_id
    INNER JOIN users to_user ON csmail.to_id = to_user.user_id
    WHERE to_id = ? AND to_deleted = 0 AND csmail_folder_id = ?
    ORDER BY message_date DESC
  ", 'ss', [$current_user['user_id'], $folder_id]));
}

function message_status($message, $folder) {
  if ($folder['csmail_folder_id'] == 1) {
    // if the folder is a sent items folder,
    // still display whether the message has been read,
    // but no further detail.

    if ($message['to_status'] == 0) {
      return "unread";
    } else {
      return "read";
    }
  }

  switch($message['to_status']) {
    case 0: return "unread";
    case 1: return "read";
    case 2: return "replied";
    case 3: return "forwarded";
    case 4: return "sent";
    case 5: return "replied-forwarded";
  }
}

foreach ($messages as &$message) {
  $message['from'] = [
    'user_id' => $message['from_id'],
    'user_groups' => $message['from_user_groups'],
    'username' => $message['from_username'],
  ];
  $message['to'] = [
    'user_id' => $message['to_id'],
    'user_groups' => $message['to_user_groups'],
    'username' => $message['to_username'],
  ];

  $message['status'] = message_status($message, $folder);
}
unset($message);

render_with('csmails/show', [
  'page_title' => $t['csmail_title'],
  'folder' => $folder,
  'custom_folders' => $custom_folders,
  'messages' => $messages,
]);
