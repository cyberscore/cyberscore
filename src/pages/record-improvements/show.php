<?php

Authorization::authorize('User');

// This page displays the charts for the selected game where you're either
// tied for first place, or are in second place.

$game_id = intval($_GET['id']);
$ties = ($_GET['untied-only'] ?? false);
$min = intval($_GET['min'] ?? 1);
$max = intval($_GET['max'] ?? 2);

$filters = [
  'min' => $min,
  'max' => $max,
  'ties' => $ties,
];

$game = database_find_by('games', ['game_id' => $game_id]);

if (!$game) {
  $cs->LeavePage('/games', 'The requested game does not exist.');
}

if (isset($_GET['group_ids'])) {
  $selected_group_ids = array_map(fn($id) => intval($id), $_GET['group_ids']);
  $group_filter = implode(", ", $selected_group_ids);
  $group_filter = "AND group_id IN ($group_filter)";
} else {
  $selected_group_ids = [];
  $group_filter = "";
}


$game_groups = database_fetch_all('SELECT * FROM level_groups WHERE game_id = ? ORDER BY group_pos', [$game_id]);

foreach ($game_groups as &$group) {
  $group['name'] = $t->GetGroupName($group['group_id']);
}
unset($group);

$my_scores = database_get_all(database_select("
  SELECT *
  FROM records
  JOIN levels USING(level_id)
  JOIN level_groups USING(group_id)
  WHERE user_id = ? AND records.game_id = ? $group_filter
", 'ss', [$current_user['user_id'], $game_id]));

$first_places = database_get_all(database_select("
  SELECT *
  FROM records
  JOIN levels USING(level_id)
  JOIN users USING(user_id)
  WHERE chart_pos = 1 AND records.game_id = ? $group_filter
", 's', [$game_id]));

$first_places = group_by($first_places, fn ($place) => $place['level_id']);

$possible_improvements = [];

foreach ($my_scores as $score) {
  $first = $first_places[$score['level_id']];

  // we don't care about charts where we're below 2nd (default) or the defined max if it's passed
  if ($score['chart_pos'] > $max) continue;

  // we don't care about charts where we're better than a defined min if it's passed
  if ($score['chart_pos'] < $min) continue;

  // we don't care about charts where we're the only first place
  if ($score['chart_pos'] == 1 && count($first) == 1) continue;

  // if we've chosen to disable tied first places, we don't care about any first places at all
  if ($score['chart_pos'] == 1 && $ties == TRUE) continue;

  $possible_improvements[] = [
    'score' => $score,
    'first' => $first,
    'class' => Modifiers::FetchChartFlagColouration($score['level_id']),
  ];
}

usort($possible_improvements, fn($a, $b) => (
  [$a['score']['chart_pos'], $a['score']['group_pos'], $a['score']['level_pos']] <=>
  [$b['score']['chart_pos'], $b['score']['group_pos'], $b['score']['level_pos']]
));

render_with('record-improvements/show', [
  'page_title' => "Improve your scores",
  'game' => $game,
  'filters' => $filters,
  'groups' => $game_groups,
  'selected_group_ids' => $selected_group_ids,
  'possible_improvements' => $possible_improvements,
]);
