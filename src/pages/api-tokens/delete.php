<?php

Authorization::authorize('User');

$token = ApiTokenRepository::get($_GET['id']);

if (!$token) {
  $cs->FlashError("Couldn't delete token: no such token");
  redirect_to_back();
}

ApiTokenRepository::delete($token['api_token_id']);
$cs->FlashSuccess("Token successfully deleted.");

redirect_to_back();
