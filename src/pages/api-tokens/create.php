<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::str('description'),
  Params::array_of_strs('permissions'),
]);

foreach ($params['permissions'] as $permission) {
  if (!in_array($permission, ['profile:read', 'records:read', 'records:create', 'notifications:read'])) {
    Flash::AddError("Unknown permission");
    redirect_to_back();
  }
}

$token = ApiTokenRepository::create([
  'user_id' => $current_user['user_id'],
  'description' => $params['description'],
  'permissions' => implode(',', $params['permissions']),
  'token' => bin2hex(random_bytes(32)),
]);

redirect_to_back();
