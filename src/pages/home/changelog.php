<?php

Authorization::authorize('Guest');

$posts = database_get_all(database_select("
  SELECT * FROM news WHERE article_type = 13 ORDER BY news_id DESC
", '', []));

foreach ($posts as &$post) {
  $post['editor'] = database_find_by('users', ['user_id' => $post['edit_user_id']]);
  $post['headline'] = Article::RetrieveHeadline($post['news_id']);
  $post['body'] = Article::RetrieveText($post['news_id']);
}
unset($post);

render_with('home/changelog', [
  'page_title' => t('general_dev_changelog'),
  'posts' => $posts,
]);
