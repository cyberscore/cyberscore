<?php

Authorization::authorize('Guest');

// How to add new modules:
// - Create a new module in src/components/policies/module_name.php
// - Create the twig template in src/templates/components/policies/module_name.php
// - Add an entry to the $sidebar variable below

$sidebar = [
  
  'Policies' => [
    'diversity' => t('documentation_policies_diversity_title'),
    'personal-data' => t('documentation_policies_personal_data_title'),
  ],
  'Processes' => [
    'verification' => t('documentation_policies_verification_title'),
    'investigations' => t('documentation_policies_investigations_title'),
    'anonymization' => t('documentation_policies_anonymization_title'),
  ],
  'Terminology' => [
    'verification-types' => t('documentation_policies_verification_types_title'),
    'staff-roles' => t('documentation_policies_staff_roles_title'),
    'glossary' => t('documentation_policies_glossary_title'),
  ],
];

$src = validate_module($sidebar, "policies", $_GET['src']);

render_with("home/policies", [
  'page_title' => t('documentation_policies_title'),
  'sidebar' => $sidebar,
  'src' => $src,
]);
