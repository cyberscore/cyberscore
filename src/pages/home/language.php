<?php

Authorization::authorize('Guest');

// If the user is not logged in,
// we store these settings in cookies for a year.

if (isset($_POST['site_lang'])) {
  if ($current_user) {
    database_update_by(
      'user_prefs',
      ['site_lang' => $_POST['site_lang']],
      ['user_id' => $current_user['user_id']]
    );
  } else {
    Cookies::Store('site_lang', $_POST['site_lang']);
  }
}

if (isset($_POST['game_lang'])) {
  if ($current_user)	{
    database_update_by(
      'user_prefs',
      ['game_lang' => $_POST['game_lang']],
      ['user_id' => $current_user['user_id']],
    );
  } else {
    Cookies::Store('game_lang', $_POST['game_lang']);
  }
}

redirect_to_back();
