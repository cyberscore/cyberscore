<?php

Authorization::authorize('Guest');

// How to add new modules:
// - Create a new module in src/components/staff/module_name.php
// - Create the twig templaate in src/templates/components/staff/module_name.php
// - Add an entry to the $sidebar variable below

$sidebar = [
  'Cyberscore staff' => [
    'staff_expectations' => $t['documentation_staff_expectations_title'],
  ],
  'Game moderation' => [
    'speedrun' => $t['documentation_staff_speedrun_title'],
    'solution' => $t['documentation_staff_solution_title'],
    'user_challenge' => $t['documentation_staff_userchallenge_title'],
    'collectible' => $t['documentation_staff_collectible_title'],
    'incremental' => $t['documentation_staff_incremental_title'],
    'unranked' => $t['documentation_staff_unranked_title'],
  ],
  'User moderation' => [
    'support' => $t['documentation_staff_support_title'],
    'record_management' => $t['documentation_staff_record_management_title'],
    'user_management' => $t['documentation_staff_user_management_title'],
    'moderation_actions' => $t['documentation_staff_moderation_actions_title'],
  ],
];

$src = validate_module($sidebar, "staff", $_GET['src']);

render_with("home/staff", [
  'page_title' => t('documentation_staff_title'),
  'sidebar' => $sidebar,
  'src' => $src,
]);
