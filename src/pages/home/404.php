<?php

Authorization::authorize('Guest');

render_with('home/404', [
  'page_title' => t('404_title'),
]);
