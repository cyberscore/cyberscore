<?php

Authorization::authorize('Guest');

// How to add new modules:
// - Create a new module in src/components/mechanics/module_name.php
// - Create the twig templaate in src/templates/components/mechanics/module_name.php
// - Add an entry to the $sidebar variable below

$sidebar = [
  'Primary Awards' => [
    'csp' => t('documentation_mechanics_csp_title'),
    'bonus_csr' => t('documentation_mechanics_bonus_csr_title'),
    'medal' => t('documentation_mechanics_medal_title'),
    'trophy' => t('documentation_mechanics_trophy_title'),
    'rainbow' => t('documentation_mechanics_rainbow_title'),
    'modifiers' => t('documentation_mechanics_modifier_title'),
  ],
  'Secondary Awards' => [
    'secondary_awards' => t('documentation_mechanics_secondary_awards_title'),
    'arcade' => t('documentation_mechanics_token_title'),
    'speedrun' => t('documentation_mechanics_speedrun_title'),
    'solution' => t('documentation_mechanics_solution_title'),
    'user_challenge' => t('documentation_mechanics_style_title'),
    'collectible' => t('documentation_mechanics_cyberstar_title'),
    'incremental' => t('documentation_mechanics_experience_title'),
  ],
];

$src = validate_module($sidebar, "mechanics", $_GET['src']);

render_with("home/mechanics", [
  'page_title' => t('documentation_mechanics_title'),
  'sidebar' => $sidebar,
  'src' => $src,
]);
