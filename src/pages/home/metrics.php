<?php

$requests = database_fetch_all("SELECT * FROM internal_events WHERE created_at > DATE_FORMAT(NOW(), '%Y-%m-%d %H:00:00') AND name = 'page_request'", []);

$nanoseconds = 0;

foreach ($requests as $request) {
  $nanoseconds += json_decode($request["properties"], true)["execution_time"] ?? 0;
}

echo "# TYPE cyberscore_requests_execution_time_nanoseconds counter\n";
echo "cyberscore_requests_execution_time_nanoseconds $nanoseconds\n";
