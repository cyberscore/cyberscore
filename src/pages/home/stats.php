<?php

Authorization::authorize('Guest');

// How to add new modules:
// - Create a new module in src/components/site-stats/module_name.php
// - Create the twig templaate in src/templates/components/site-stats/module_name.php
// - Add an entry to the $sidebar variable below

$sidebar = [
  'General' => [
    'general_users' => 'User statistics',
    'general_subs' => 'Submission statistics',
    'general_sub_history' => 'Submission history',
    'general_charts' => 'Chart statistics',
    'general_games' => 'Game statistics',
    'general_plats' => 'Remaining platinum medals (year)',
  ],
  'User achievements' => [
    'video_proven_platinums' => 'Platinum medals with video proof',
    'proven_platinums' => 'Platinum medals with proof',
    'proven_golds' => 'Gold medals with proof',
    'average_csr' => 'Average CSR per record',
    'highest_sub_days' => 'Number of submissions in a day',
    'oldest_plats' => 'Oldest platinum medals',
  ],
  'Chart achievements' => [
    'highest_sub_charts' => 'Number of submissions',
    'highest_proven_sub_charts' => 'Number of proven submissions',
    'most_tied_charts' => 'Number of submissions tied for first place',
  ],
  'Game achievements' => [
    'highest_CSR' => 'Awarded CSR',
    'highest_bonus_CSR' => 'Awarded bonus CSR',
    'highest_total_CSR' => 'Awarded total CSR',
    'most_platinum_medal_holders' => 'Number of platinum medal holders',
    'most_player_game' => 'Number of players',
    'most_medal_holders' => 'Number of medal holders',
    'most_proven_submissions' => 'Number of proven submissions',
  ],
];

$src = validate_module($sidebar, "site_stats", $_GET['src']);

render_with("home/stats", [
  'page_title' => t('sitestats_title'),
  'sidebar' => $sidebar,
  'src' => $src,
]);
