<?php

Authorization::authorize(['GameMod', 'ProofMod']);

$rule_id = intval($_GET['id']);
$params = filter_post_params([
  Params::str('rule_text'),
]);

if (!ProofRulesRepository::exists($rule_id)) {
  $cs->PageNotFound();
}

$result = ProofRulesRepository::update($rule_id, $params);

$cs->WriteNote(true, "The proof guideline has been updated.");

if ($result['merged']) {
  $cs->WriteNote(
    false,
    "The guideline's text you updated has matched an existing rule's text. As a
    result, the two guidelines have been merged into one."
  );
}

header("Location: /proof-rules/$rule_id/edit");
