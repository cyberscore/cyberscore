<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->LeavePage('/games', 'The requested game does not exist.');
}

$affected_charts = [];

// Handle selecting the correct charts.
foreach ($_POST['gc_ids'] as $gc_id) {
  $id = substr($gc_id, 1);
  switch (substr($gc_id, 0, 1)) {
  case 'g':
    $charts = database_fetch_all(
      "SELECT level_id FROM levels WHERE game_id = ? AND group_id = ?",
      [$game['game_id'], $id]
    );

    foreach ($charts as $chart) {
      $affected_charts []= $chart['level_id'];
    }
  break;
  case 'c':
    $affected_charts []= intval($id);
    break;
  }
}

$num_decimals2 = $_POST['chart_type2'] == 0 ? -1 : $_POST['num_decimals2'];
$conversion_factor = isset($_POST['use_conversion']) && $_POST['chart_type2'] != 0 ? $_POST['conversion_factor'] : 0;

database_update_by('levels', [
  'players' => $_POST['players'],
  'dlc' => $_POST['dlc'] ?? 0,
  'chart_type' => $_POST['chart_type'],
  'num_decimals' => $_POST['num_decimals'],
  'score_prefix' => $_POST['score_prefix'],
  'score_suffix' => $_POST['score_suffix'],
  'chart_type2' => $_POST['chart_type2'],
  'num_decimals2' => $num_decimals2,
  'score_prefix2' => $_POST['score_prefix2'],
  'score_suffix2' => $_POST['score_suffix2'],
  'conversion_factor' => $conversion_factor,
  'name_pri' => $_POST['name_pri'],
  'name_sec' => $_POST['name_sec'],
  'name1' => $_POST['name1'],
  'name2' => $_POST['name2'],
  'name3' => $_POST['name3'],
], [
  'level_id' => $affected_charts,
  'game_id' => $game['game_id'],
]);

// Entirely rebuild affected game
ChartCacheRebuilder::QueueGameChartsForRebuild($game['game_id']);
GameCacheRebuilder::QueueGameForRebuild($game['game_id']);

if(count($affected_charts) > 0) {
  $cs->WriteNote(TRUE, count($affected_charts) . " charts successfully updated and queued for rebuild");
  $cs->WriteNote(TRUE, "Game ID #" . $game['game_id'] . " additionally queued for immediate rebuild");
}

redirect_to_back();
