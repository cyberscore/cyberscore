<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);

if (!$cs->GameExists($game_id)) {
  $cs->LeavePage('/games.php', 'The requested game does not exist.');
}

$game_charts = database_get_all(database_select("
  SELECT
    levels.level_id,
    levels.group_id,
    level_name,
    group_name,
    CASE level_groups.ranked WHEN 1 THEN 'standard' ELSE 'standard' END as group_ranked,
    chart_modifiers2.*
  FROM levels
  LEFT JOIN level_groups USING (group_id)
  LEFT JOIN chart_modifiers2 USING (level_id)
  WHERE levels.game_id = ?
  ORDER BY level_groups.group_pos, levels.level_pos
", 'i', [$game_id]));

$selected_modifiers = [];

foreach ($game_charts as $chart) {
  $id = $chart['level_id'];
  $selected_modifiers[$id] = [];

  foreach (Modifiers::$chart_flags as [$chart_flag, $modifier]) {
    if ($chart[$chart_flag]) {
      $selected_modifiers[$id][$chart_flag] = $chart[$chart_flag];
    }
  }

  foreach (Modifiers::$csp_modifiers as [$csp_modifier, $modifier]) {
    if ($chart[$csp_modifier]) {
      $selected_modifiers[$id][$csp_modifier] = $chart[$csp_modifier];
    }
  }
}

$game_charts = group_by($game_charts, function($e) { return $e['group_id']; });

$flags = array_map(fn($flag) => [
  'identifier' => $flag[0],
  'label' => Modifiers::csp_modifier_label($flag[0]),
], Modifiers::$chart_flags);

$modifiers = array_map(fn($modifier) => [
  'identifier' => $modifier[0],
  'subcategory' => Modifiers::CSPModifierSubcategory($modifier[0]),
  'label' => Modifiers::csp_modifier_label($modifier[0]),
  'is_arcade_modifier' => $modifier[2],
], Modifiers::$csp_modifiers);

render_with("game-modifiers/show", [
  'page_title' => "Edit game modifiers",
  'game_id' => $game_id,
  'modifiers' => $modifiers,
  'flags' => $flags,
  'game_charts' => $game_charts,
  'selected_modifiers' => $selected_modifiers,
]);
