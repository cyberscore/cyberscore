<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);

if (!GamesRepository::exists($game_id)) {
  $cs->PageNotFound();
}

$charts = database_get_all(database_select("
  SELECT chart_modifiers2.*
  FROM levels
  LEFT JOIN chart_modifiers2 USING (level_id)
  WHERE game_id = ?
", 's', [$game_id]));


$keys = [];
$fields = [];
foreach ($charts as $chart) {
  $f = [];
  foreach (Modifiers::$chart_flags as [$chart_flag,]) {
    $f[$chart_flag] = isset($_POST['flags'][$chart['level_id']][$chart_flag]);
  }
  foreach (Modifiers::$csp_modifiers as [$csp_modifier,]) {
    $f[$csp_modifier] = isset($_POST['modifiers'][$chart['level_id']][$csp_modifier]);
  }

  $keys []= $chart['level_id'];
  $fields []= $f;
}

// TODO: enforce validation to avoid javascript shenanigans
database_update_all('chart_modifiers2', 'level_id', $keys, $fields);

Modifiers::UpdateChartFlooder($game_id);
ChartCacheRebuilder::QueueGameChartsForRebuild($game_id);
GameCacheRebuilder::QueueGameForRebuild($game_id);
$cs->WriteNote(TRUE, "Modifiers successfully updated. The entire game's charts have been queued to rebuild but this will have a slight delay.");
$cs->WriteNote(TRUE, "Game ID #" . $game_id . " queued for rebuild");

redirect_to_back();
