<?php

Authorization::authorize('GameMod');

$files = $cs->GetHomepics(true);
usort($files, fn($a, $b) => $b['mtime'] <=> $a['mtime']);
$file = $files[0];

render_with('homepics/new', [
  'page_title' => 'Upload homepic',
  'latest_file' => $file,
]);
