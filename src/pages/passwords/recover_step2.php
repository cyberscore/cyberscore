<?php

if (!recaptcha_verify()) {
  Session::Clear('password_reset');
  $cs->LeavePage("/passwords/recover?error=recaptcha-failed");
}

$user = UsersRepository::find_by([
  'username' => $_POST['username'],
  'email' => $_POST['email'],
]);

if ($user == null) {
  Session::Clear('password_reset');
  $cs->LeavePage("/passwords/recover?error=username-not-found");
}

$verification_code = random_int(100000, 999999);

Session::Store('password_reset', [
  'user_id' => $user['user_id'],
  'code' => $verification_code,
]);

send_email(
  [$user['email']],
  'emails/password_reset',
  "Cyberscore - Password verification code",
  [
    'username' => $user['username'],
    'vcode' => $verification_code,
    'signature' => t('general_automatic_email_1'),
    'automated_warning' => t('general_automatic_email_2'),
  ]
);

render_with('passwords/recover_step2', [
  'page_title' => 'Password Reset - Step 2',
]);
