<?php

Session::Clear('password_reset');

render_with('passwords/recover', [
  'page_title' => 'Forgotten Password',
  'error' => $_GET['error'] ?? null,
]);
