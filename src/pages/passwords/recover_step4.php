<?php

$password_reset = Session::Fetch('password_reset');
$user = UsersRepository::get($password_reset['authed'] ?? null);

if ($user == null) {
  Session::Clear('password_reset');
  $cs->LeavePage("/passwords/recover?error=username-not-found");
}

$params = filter_post_params([
  Params::str('password'),
  Params::str('password_confirmation'),
]);

if (mb_strlen($params['password']) < 8) {
  $cs->LeavePage("/passwords/recover/step3?error=invalid");
}

if ($params['password'] != $params['password_confirmation']) {
  $cs->LeavePage("/passwords/recover/step3?error=nomatch");
}

Session::Clear('password_reset');

$password_new = password_hash($params['password'], PASSWORD_DEFAULT);
UsersRepository::update($user['user_id'], ['pword_new' => $password_new]);

send_email(
  [$user['email']],
  'emails/password_reset_notification',
  'Cyberscore - Password change confirmation',
  [
    'username' => $user['username'],
    'signature' => t('general_automatic_email_1'),
    'automated_warning' => t('general_automatic_email_2'),
  ]
);

header("Location: /");
