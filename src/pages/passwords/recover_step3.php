<?php

$password_reset = Session::Fetch('password_reset');
$user = UsersRepository::get($password_reset['user_id'] ?? null);

if ($user == null) {
  Session::Clear('password_reset');
  $cs->LeavePage("/passwords/recover?error=username-not-found");
}

if (isset($password_reset['code'])) {
  if ($password_reset['code'] != $_POST['verification']) {
    Session::Clear('password_reset');
    $cs->LeavePage("/passwords/recover?error=wrong");
  } else {
    Session::Store('password_reset', [
      'authed' => $password_reset['user_id'],
    ]);
  }
}

render_with('passwords/recover_step3', [
  'page_title' => 'Password Reset - Step 3',
  'error' => $_GET['error'] ?? null,
]);
