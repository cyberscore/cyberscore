<?php

Authorization::authorize('User');

$game = GamesRepository::get($_GET['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

if (!$game['savefile_module']) {
  $cs->LeavePage("/games/{$game['game_id']}", "This feature is only enabled for a few select games");
}

render_with('record-savefiles/new', [
  'page_title' => "Upload records savefiles",
]);
