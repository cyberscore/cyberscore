<?php

Authorization::authorize('GlobalMod', 4531); # Mod or Groudon

$params = filter_post_params([
  Params::array_of_numbers('game_ids'),
  Params::str('chart_name'),
  Params::checkbox('alpha', true, false),
  Params::str('name1'),
  Params::str('name2'),
  Params::str('name3'),
  Params::str('rule'),
]);

if ($params['chart_name'] == '') {
  $cs->WriteNote(false, "Chart name is mandatory");
  redirect_to_back();
}

foreach ($params['game_ids'] as $game_id) {
  $letter = substr(ucfirst($params['chart_name']), 0, 1);

  $group_id = database_single_value("
    SELECT group_id
    FROM level_groups
    WHERE game_id = ? AND group_name = ?
  ", 'ss', [$game_id, $letter]);

  if ($group_id == null) {
    $group_id = database_single_value("
      SELECT group_id
      FROM level_groups
      WHERE game_id = ? AND group_name = '123'
    ", 's', [$game_id]);
  }

  $chart_pos = database_single_value("
    SELECT 1 + MAX(level_pos) FROM levels WHERE game_id = ? AND group_id = ?
  ", 'ss', [$game_id, $group_id]);

  $level_id = database_insert('levels', [
    'game_id' => $game_id,
    'group_id' => $group_id,
    'level_name' => $params['chart_name'],
    'level_rules' => $params['rule'] ?? '',
    'chart_type' => 4,
    'num_decimals' => 0,
    'chart_type2' => 0,
    'score_prefix' => '',
    'score_prefix2' => '',
    'score_suffix' => '',
    'score_suffix2' => '',
    'entity_lock' => '',
    'players' => 1,
    'name_pri' => '',
    'name_sec' => '',
    'name1' => $params['name1'],
    'name2' => $params['name2'],
    'name3' => $params['name3'],
    'level_pos' => $chart_pos,
    'dlc' => 1,
  ]);

  database_insert('chart_modifiers2', ['level_id' => $level_id, 'standard' => true]);

  if (isset($params['alpha'])) {
    RecordManager::SortChartsAlphabetically($game_id, $group_id);
  }

  GameCacheRebuilder::QueueGameForRebuild($game_id);
}

$cs->WriteNote(true, count($params['game_ids']) . " chart(s) added");
redirect_to_back();
