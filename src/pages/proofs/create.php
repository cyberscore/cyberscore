<?php

Authorization::authorize('User');

// "Link proofs from another website"
if (isset($_POST['proof_links'])) {
  $num_proofs_added = 0;

  foreach ($_POST['proof_links'] as $chart_id => $proof_link) {
    $record = database_get(database_select("
      SELECT record_id, rec_status, linked_proof
      FROM levels JOIN records USING (level_id)
      WHERE user_id = ? AND level_id = ?
    ", 'ss', [$current_user['user_id'], $chart_id]));

    if ($record && $record['linked_proof'] != $proof_link) {
      $result = ProofManager::ProcessLinkedProof($record['record_id'], $proof_link);

      if ($result['status'] == 'success') {
        ChartCacheRebuilder::QueueChartForRebuild($chart_id);
        $num_proofs_added++;
      } else {
        $cs->WriteNote(false, "There was an error and the proof was not stored ({$result['status']}).");
      }
    }
  }
  $cs->WriteNote(true, "$num_proofs_added proofs added");
  redirect_to_back();
}

// "Upload proofs from your device"
if (isset($_POST['chart_id']) && isset($_FILES['proof_file'])) {
  $record = database_get(database_select("
    SELECT record_id, level_id
    FROM records
    WHERE level_id = ? AND user_id = ?
  ", 'ss', [$_POST['chart_id'], $current_user['user_id']]));

  if (!$record) {
    $cs->WriteNote(false, "Record not found.");
  } else {
    $result = ProofManager::ProcessUploadedProof($record['record_id'], $_FILES['proof_file']);

    if ($result['status'] == 'success') {
      ChartCacheRebuilder::QueueChartForRebuild($_POST['chart_id']);
      $cs->WriteNote(true, "Proof uploaded.", "<img src='" . h($result['link']) . "' style='height: 200px;' />");
    } else {
      $cs->WriteNote(false, "There was an error and the proof was not uploaded.");
    }
  }

  redirect_to("/chart/{$record['level_id']}");
}

// "Verify multiple submissions with one proof"
if (isset($_POST['chart_ids'])) {
  $params = filter_post_params([
    Params::array_of_numbers('chart_ids'),
  ]);

  if (is_uploaded_file($_FILES['proof_file']['tmp_name'])) {
    $proof = ProofManager::StoreUploadedProof($_FILES['proof_file']);
  } else if (isset($_POST['proof_link'])) {
    $proof = ProofManager::StoreLinkedProof($_POST['proof_link']);
  } else {
    $proof = null;
  }

  if (!$proof) {
    $error = "No proof was provided.";
  } else if ($proof['status'] != 'success') {
    $error = "Error handling submitted proof ({$proof['status']})";
  } else {
    ChartCacheRebuilder::QueueMultipleChartsForRebuild($params['chart_ids']);
    $records = database_filter_by('records', ['user_id' => $current_user['user_id'], 'level_id' => $params['chart_ids']]);
    foreach ($records AS $record) {
      ProofManager::LinkProof($record['record_id'], $proof['link']);
    }

    if ($proof['action'] == 'linked') {
      $success = "Your proof has been linked for the specified chart(s).";
    } else {
      $success = "Your proof has been stored for the specified chart(s).";
    }
  }

  if (isset($error)) {
    $cs->WriteNote(false, $error);
  } else if (isset($success)) {
    if ($proof['action'] == 'stored') {
      $cs->WriteNote(true, $success, "<img src='" . h($proof['link']) . "' style='height:200px;' />");
    } else {
      $cs->WriteNote(true, $success);
    }
  }

  redirect_to_back();
}
