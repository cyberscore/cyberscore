<?php

Authorization::authorize('User');

$params = Params::ParseGet([
  'game_id' => Params::integer(),
  'proven'  => Params::integer()->default(0),
  'type'    => Params::enum(['link', 'upload', 'singleupload'])->default(null)->nullable(),
]);

$game = GamesRepository::get($params['game_id']);

if (!$game) {
  HTTPResponse::PageNotFound();
}

$t->CacheGame($game['game_id']);

$levels = database_fetch_all("
  SELECT
    records.ranked,
    records.submission,
    records.submission2,
    records.linked_proof,
    levels.*
  FROM records
  JOIN levels USING (level_id)
  LEFT JOIN level_groups USING (group_id)
  WHERE records.user_id = ? AND records.game_id = ? AND records.rec_status <= ?
  ORDER BY COALESCE(level_groups.group_pos, 99999999), levels.level_pos
", [$current_user['user_id'], $game['game_id'], $params['proven']]);

$game_name = $t->GetGameName($game['game_id']);

foreach ($levels as &$level) {
  $level['group_name'] = $t->GetGroupName($level['group_id']);
}
unset($level);

render_with('proofs/new', [
  'page_title' => t('multiproof_title'),
  'game' => $game,
  'type' => $params['type'],
  'hide_proven' => $params['proven'],
  'game_name' => $game_name,
  'levels' => $levels,
]);
