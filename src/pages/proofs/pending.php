<?php

Authorization::authorize('ProofMod');

$start = intval($_GET['start'] ?? 0);
$limit = intval($_GET['limit'] ?? 25);
$sort = $_GET['sort'] ?? 'normal';
$game_id = intval($_GET['game'] ?? 0);
$single_user_id = $_GET['user_id'] ?? 'user_default';
$date_sort = $_GET['date_sort'] ?? 'default';
$ucsp_sort = $_GET['ucsp_sort'] ?? 'default';
$own_proofs = $_GET['own_proofs'] ?? 'show';
$locked = $_GET['locked'] ?? 'all';
$proof_type = $_GET['proof_type'] ?? 'all';
$num_approvals = database_single_value("SELECT COUNT(1) FROM records WHERE records.rec_status = 3", '', []);
$approvals_month = database_single_value("SELECT COUNT(1) FROM record_approvals WHERE approval_date BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW()", '', []);


$where = [];

if ($game_id != 0) {
  $where['records.game_id'] = $game_id;
}

if ($single_user_id != "user_default") {
  $where['records.user_id'] = intval($single_user_id);
} else if ($own_proofs == "hide") {
  $where['records.user_id'] = database_not($current_user['user_id']);
}

if ($locked == 'lock') {
  $lock_filter = "modname IS NOT NULL";
}
else if ($locked == 'unlock') {
  $lock_filter = "modname IS NULL";
}
else $lock_filter = 1;

switch ($date_sort) {
case "dateDESC":
  $date_sorter = "MAX(record_history.update_date) DESC,";
  break;
case "dateASC":
  $date_sorter = "MAX(record_history.update_date) ASC,";
  break;
default:
  $date_sorter = "";
}

switch ($ucsp_sort) {
  case "ucspDESC":
    $ucsp_sorter = "records.ucsp DESC,";
    break;
  case "ucspASC":
    $ucsp_sorter = "records.ucsp ASC,";
    break;
  default:
    $ucsp_sorter = "";
  }

[$filter_sql, $filter_binds] = database_where_sql_bind($where);

$total_proofs_t = database_single_value("SELECT COUNT(1) FROM records WHERE rec_status >= 4", '', []);
$total_proofs = database_value("SELECT COUNT(1) FROM records WHERE rec_status >= 4 AND $filter_sql", $filter_binds);

$pending_records = database_fetch_all("
  SELECT
    records.*,
    mod_users.username AS mod_username,
    platforms.platform_name,
    users.username,
    levels.group_id,
    levels.chart_type,
    levels.chart_type2,
    levels.decimals,
    levels.num_decimals,
    levels.score_prefix,
    levels.score_suffix,
    MIN(record_approval_locks.modname) AS modname,
    levels.level_rules,
    levels.level_proof_rules,
    MAX(record_approvals.approval_date) AS latest_approval,
    MAX(record_history.update_date) AS latest_date
  FROM records
  JOIN users USING(user_id)
  JOIN levels USING(level_id)
  JOIN platforms USING(platform_id)
  LEFT JOIN record_approval_locks USING(record_id)
  LEFT JOIN record_approvals USING(record_id)
  JOIN record_history ON (records.record_id = record_history.record_id AND record_history.update_type = 'p')
  LEFT JOIN users as mod_users ON records.record_note_mod_id = mod_users.user_id
  LEFT JOIN level_groups USING(group_id)
  WHERE records.rec_status >= 4 AND $filter_sql AND $lock_filter
  GROUP BY records.record_id
  ORDER BY $date_sorter $ucsp_sorter records.game_id ASC, level_groups.group_pos ASC, levels.level_pos ASC, users.username ASC
  LIMIT $limit
  OFFSET $start
", $filter_binds);
foreach ($pending_records as &$record) {
  $t->CacheEntityNames($record['game_id']);

  $record['proof'] = ProofManager::ProofDetails($record);
  $record['colour'] = Modifiers::FetchChartFlagColouration($record['level_id']);
  $record['awards'] = fetch_chart_awards($record['level_id']);
  $record['entity_names'] = $t->GetEntityNames([$record['entity_id'], $record['entity_id2'], $record['entity_id3'], $record['entity_id4'], $record['entity_id5']]);
  $record['rules'] = $t->GetRuleText($record['level_rules']);
  $record['proof_rules'] = $t->GetProofRuleText($record['level_proof_rules']);
  $record['updated_after_note'] = $record['record_note_date'] != null && new DateTime($record['last_update']) > new DateTime($record['record_note_date']);
}
unset($record);

$games = database_get_all(database_select("
  SELECT
    records.game_id,
    COUNT(records.game_id) AS pending_proofs
  FROM records
  JOIN games USING(game_id)
  WHERE records.rec_status >= 4
  GROUP BY records.game_id
  ORDER BY games.game_name ASC
", '', []));
$t->CacheGameNames();
foreach ($games as &$game) {
  $game['name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$users = database_get_all(database_select("
  SELECT
    users.user_id,
    users.username,
    COUNT(records.user_id) AS pending_records
  FROM records
  JOIN users USING (user_id)
  WHERE records.rec_status >= 4
  GROUP BY users.user_id
  ORDER BY pending_records DESC, records.user_id ASC
", '', []));

render_with('proofs/pending', [
  'page_title' => 'Pending proofs',
  'games' => $games,
  'users' => $users,
  'num_approvals' => $num_approvals,
  'approvals_month' => $approvals_month,
  'total_proofs_t' => $total_proofs_t,
  'total_proofs' => $total_proofs,
  'pending_records' => $pending_records,

  'limit' => $limit,
  'start' => $start,
  'pages' => ceil($total_proofs / $limit),

  // filters
  'selected_user_id' => $single_user_id,
  'selected_game_id' => $game_id,
  'own_proofs' => $own_proofs,
  'locked' => $locked,
  'proof_type' => $proof_type,
  'date_sort' => $date_sort,
  'ucsp_sort' => $ucsp_sort,
]);
