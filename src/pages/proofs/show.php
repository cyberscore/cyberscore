<?php

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

global $config;

Authorization::authorize('Guest');

$ua = $_SERVER['HTTP_USER_AGENT'] ?? "";

if (str_contains($ua, "Bytespider") || (str_contains($ua, "bot") && !str_contains($ua, "archive.org"))) {
  http_response_code(403);
  self::Exit();
}

$record = RecordsRepository::get($_GET['id']);

if (!$record) {
  HTTPResponse::PageNotFound();
}

$proof = ProofManager::ProofDetails($record);

if (str_starts_with($proof['url'], 'https://s3.eu-west-2.amazonaws.com/cyberscoreproofs/')) {
  $bucket = "cyberscoreproofs";
  $key = explode("/cyberscoreproofs/", $proof['url'])[1];

  $s3 = new S3Client([
    'version' => '2006-03-01',
    'region'  => 'eu-west-2',
    'credentials' => [
      'key' => $config['email']['aws_access_key_id'],
      'secret' => $config['email']['aws_secret_access_key'],
    ],
  ]);

  $url = $s3->createPresignedRequest(
    $s3->getCommand('GetObject', ['Bucket' => $bucket, 'Key' => $key]),
    '+1 hour'
  )->getUri();

  header("Location: " . $url);
} else {
  header("Location: " . $proof['url']);
}
