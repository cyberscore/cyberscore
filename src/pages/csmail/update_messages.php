<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::array_of_numbers('csmail_ids'),
  Params::str('action'),
]);

$messages = CSMailMessagesRepository::where(['csmail_id' => $params['csmail_ids']]);

// Ensure that all csmail ids belong to the current user
if (count($messages) != count($params['csmail_ids'])) {
  $cs->LeavePage('/csmails', "You can't move this message, it does not exist or it's not yours to move");
}

foreach ($messages as $message) {
  if ($message['from_id'] != $current_user['user_id'] && $message['to_id'] != $current_user['user_id']) {
    $cs->LeavePage('/csmails', "You can't move this message, it does not exist or it's not yours to move");
  }
}

// Apply selected actions
switch ($params['action']) {
case "delete":
  CSMailMessagesRepository::update_all(
    ['to_deleted' => 1, 'to_status' => 1],
    ['to_id' => $current_user['user_id'], 'csmail_id' => $params['csmail_ids']]
  );
  CSMailMessagesRepository::update_all(
    ['from_deleted' => 1],
    ['from_id' => $current_user['user_id'], 'csmail_id' => $params['csmail_ids']]
  );

  break;
case "unread":
  CSMailMessagesRepository::update_all(
    ['to_status' => 0],
    ['to_id' => $current_user['user_id'], 'csmail_id' => $params['csmail_ids']]
  );
  break;
case "read":
  CSMailMessagesRepository::update_all(
    ['to_status' => 1],
    ['to_id' => $current_user['user_id'], 'csmail_id' => $params['csmail_ids']]
  );
  break;
}

// move messages to another folder
if (substr($action, 0, 5) == "move_") {
  $folder_id = intval(substr($action, 5));

  $folder = database_find_by('csmail_folders', ['csmail_folder_id' => $folder_id, 'user_id' => $current_user['user_id']]);

  // confirm folder ownership
  if ($folder_id != 0 && $folder == NULL) {
    $cs->LeavePage('/csmails', "The target folder does not exist or does not belong to you.");
  }

  // We can only move received messages, not sent ones.
  CSMailMessagesRepository::update_all(
    ['csmail_folder_id' => $folder_id],
    ['to_id' => $current_user['user_id'], 'csmail_id' => $params['csmail_ids']]
  );
}

$cs->RedirectToPreviousPage();
