<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->PageNotFound();
}

$total_entities = database_single_value("
  SELECT COUNT(*) FROM game_entities WHERE game_id = ? AND entity_group = 1
", 's', [$game['game_id']]);

$entity_locks = database_get_all(database_select("
  SELECT
    MIN(level_id) AS level_id,
    min_entities,
    max_entities,
    entity_lock
  FROM levels WHERE game_id = ?
  GROUP BY min_entities, max_entities, entity_lock
  ORDER BY min_entities ASC, max_entities ASC, entity_lock = '' ASC, CHAR_LENGTH(entity_lock) DESC, entity_lock ASC
", 's', [$game['game_id']]));

foreach ($entity_locks as &$lock) {
  $lock['groups'] = database_get_all(database_select("
    SELECT
      IF(levels.group_id = 0, 'Separate charts', level_groups.group_name) AS group_name,
      SUM(IF(levels.min_entities = ? AND levels.max_entities = ? AND levels.entity_lock = ?, 1, 0)) AS num_charts,
      COUNT(*) AS total_charts
    FROM levels LEFT JOIN level_groups USING(group_id) WHERE levels.game_id = ?
    GROUP BY levels.group_id HAVING num_charts > 0 ORDER BY level_groups.group_pos ASC
  ", 'ssss', [$lock['min_entities'], $lock['max_entities'], $lock['entity_lock'], $game['game_id']]));

  if ($lock['entity_lock'] == '') {
    $lock['num_entities'] = $total_entities;
  } else {
    $entities = [];
    $entity_ids = explode(',', $lock['entity_lock']);
    foreach ($entity_ids as $entity_id) {
      $inherited = database_single_value("SELECT inherited_entity_id FROM game_entities WHERE game_entity_id = ?", 's', [$entity_id]);
      if ($inherited != 0) {
        $entity_id = database_single_value("SELECT game_entity_id FROM game_entities WHERE game_entity_id = ?", 's', [$inherited]);
      }
      $entities []= $t->GetEntityName($entity_id, 1);
    }
    $lock['entities'] = $entities;
    $lock['num_entities'] = count($entities);
  }
}
unset($lock);

$game_groups = database_get_all(database_select("
  SELECT group_id, group_name
  FROM level_groups
  WHERE game_id = ?
  ORDER BY group_pos ASC
", 's', [$game['game_id']]));

foreach ($game_groups as &$group) {
  $group['levels'] = database_get_all(database_select("
    SELECT level_id, level_name
    FROM levels WHERE group_id = ?
    ORDER BY level_pos ASC
  ", 's', [$group['group_id']]));
}
unset($group);

$other_levels = database_get_all(database_select("
  SELECT level_id, level_name
  FROM levels
  WHERE game_id = ? AND group_id = 0
  ORDER BY level_pos ASC
", 's', [$game['game_id']]));

$all_entities = database_get_all(database_select("
  SELECT game_entity_id, entity_name, inherited_entity_id
  FROM game_entities
  WHERE game_id = ? AND entity_group = 1
  ORDER BY entity_name ASC
", 's', [$game['game_id']]));

foreach ($all_entities as &$entity) {
  if ($entity['inherited_entity_id'] != 0) {
    $parent_entity = database_get(database_select("
      SELECT game_entity_id, entity_name FROM game_entities WHERE game_entity_id = ?
    ", 's', [$entity['inherited_entity_id']]));

    $entity['game_entity_id'] = $parent_entity['game_entity_id'];
    $entity['entity_name'] = $parent_entity['entity_name'];
  }
}
unset($entity);


$chart_ids = [];
$entity_ids = [];
$min_entities = 1;
$max_entities = 1;

if (isset($_GET['edit'])) {
 $chart = database_get(database_select("
    SELECT min_entities, max_entities, entity_lock FROM levels WHERE level_id = ?
  ", 's', [$_GET['edit']]));

  $min_entities = $chart['min_entities'];
  $max_entities = $chart['max_entities'];
  $entity_lock = $chart['entity_lock'];

  $entity_ids = explode(',', $entity_lock);
  $locked_charts = database_get_all(database_select("
    SELECT level_id
    FROM levels
    WHERE game_id = ? AND min_entities = ? AND max_entities = ? AND entity_lock = ?
  ", 'ssss', [$game['game_id'], $min_entities, $max_entities, $entity_lock]));
  foreach ($locked_charts as $locked_chart) {
    $chart_ids []= $locked_chart['level_id'];
  }
}

$t->CacheEntityNames($game['game_id'], 1);

render_with('game-entity-locks/edit', [
  'page_title' => 'Edit entity locks',
  'entity_locks' => $entity_locks,
  'game' => $game,
  'game_groups' => $game_groups,
  'chart_ids' => $chart_ids,
  'other_levels' => $other_levels,
  'all_entities' => $all_entities,
  'entity_ids' => $entity_ids,
  'min_entities' => $min_entities,
  'max_entities' => $max_entities,
]);
