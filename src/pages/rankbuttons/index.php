<?php

Authorization::authorize('Designer');

$games = group_by(RankbuttonsRepository::all(), fn($r) => $r['game_id']);

$t->CacheGameNames();

render_with('rankbuttons/index', [
  'page_title' => 'View rankbuttons',
  'games' => $games,
]);
