<?php

Authorization::authorize('Designer');

$games = database_get_all(database_select("
  SELECT game_id, game_name, COUNT(rankbutton_id) > 0 AS rankbutton_exists
  FROM games
  LEFT JOIN rankbuttons USING (game_id)
  WHERE site_id = 1
  GROUP BY game_id
  ORDER BY rankbutton_exists, game_name
", '', []));
foreach ($games as &$game) {
  $game['name'] = $t->GetGameName($game['game_id']);
}
unset($game);

$latest_rankbuttons = database_get_all(database_select("
  SELECT *
  FROM rankbuttons
  ORDER BY created_at DESC
  LIMIT 7
", '', []));

$t->CacheGameNames();

render_with('rankbuttons/new', [
  'page_title' => 'Upload rankbutton',
  'games' => $games,
  'latest_rankbuttons' => $latest_rankbuttons,
]);
