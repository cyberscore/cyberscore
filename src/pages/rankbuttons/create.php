<?php

Authorization::authorize('Designer');

$game = GamesRepository::get($_POST['game_id']);

if (!$game) {
  $cs->LeavePage("/rankbuttons/new", "Game not found");
}

if (!is_uploaded_file($_FILES['rankbutton_file']['tmp_name'])) {
  $cs->LeavePage("/rankbuttons/new", "No file uploaded");
}

$image = imagecreatefromgif($_FILES['rankbutton_file']['tmp_name']);
if (!$image) {
  $cs->WriteNote(false, "File type not supported, rankbutton must be a GIF");
}
imagedestroy($image);

$sha256 = hash('sha256', file_get_contents($_FILES['rankbutton_file']['tmp_name']));

global $config;
$root = $config['app']['root'];
if (!move_uploaded_file($_FILES['rankbutton_file']['tmp_name'], "$root/public/uploads/rankbuttons/games/$sha256.gif")) {
  $cs->LeavePage("/rankbuttons/new", "Error uploading file.");
}

database_update_by('rankbuttons', ['active' => false], ['game_id' => $game['game_id']]);
$rankbutton_id = database_insert('rankbuttons', [
  'game_id' => $game['game_id'],
  'owner_id' => $current_user['user_id'],
  'active' => true,
  'sha256' => $sha256,
  'created_at' => database_now(),
  'archive_id' => 0,
  'split_index' => 0,
]);

database_insert('staff_tasks', [
  'user_id' => $current_user['user_id'],
  'tasks_type' => 'rankbutton_uploaded',
  'task_id' => $rankbutton_id,
  'result' => 'Rankbutton uploaded',
]);

$cs->WriteNote(true, "Rankbutton uploaded");
redirect_to("/rankbuttons/new");
