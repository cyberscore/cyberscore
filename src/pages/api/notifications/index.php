<?php

Authorization::require_all_api_permissions('notifications:read');
Authorization::authorize('User');

$notifications = database_get_all(database_select("
  SELECT
    notifications.*,
    senders.username AS sender_username,
    targets.username AS target_username,
    userpage.username AS userpage_username,
    referrals.referrer_id AS referrer_user_id,
    referrals.user_id AS referred_user_id,
    referrer.username AS referrer_username,
    referred.username AS referred_username,
    levels.level_name,
    levels.group_id,
    games.game_name,
    level_groups.group_name,
    game_requests.game_name AS game_request_name,
    news.headline AS news_headline
  FROM notifications
  LEFT JOIN users senders ON notifications.sender_user_id = senders.user_id
  LEFT JOIN users targets ON notifications.target_user_id = targets.user_id
  LEFT JOIN users userpage ON notifications.userpage_id = userpage.user_id
  LEFT JOIN levels ON notifications.chart_id = levels.level_id
  LEFT JOIN level_groups ON levels.group_id = level_groups.group_id
  LEFT JOIN games ON notifications.game_id = games.game_id
  LEFT JOIN game_requests ON notifications.game_request_id = game_requests.gamereq_id
  LEFT JOIN records ON notifications.record_id = records.record_id
  LEFT JOIN support ON notifications.support_id = support.support_id
  LEFT JOIN referrals ON notifications.referral_id = referrals.referral_id
  LEFT JOIN users referrer ON referrals.referrer_id = referrer.user_id
  LEFT JOIN users referred ON referrals.user_id = referred.user_id
  LEFT JOIN news ON notifications.news_id = news.news_id
  WHERE notifications.user_id = ?
  ORDER BY notifications.notification_id DESC
", 's', [$current_user['user_id']]));

render_json($notifications);
