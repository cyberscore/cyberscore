<?php

Authorization::require_all_api_permissions('records:create');
Authorization::authorize('User');

$game = GamesRepository::get($_POST['game_id']);
if ($game === NULL) {
  http_response_code(404);
  render_json(['error' => 'Game not found']);
  $cs->Exit();
}

$new_records = 0;
$updated_records = 0;
$unchanged_records = 0;

$queued_records = [];
foreach ($_POST['records'] as $record) {
  $chart = LevelsRepository::get($record['chart_id']);

  if ($chart === NULL) {
    $cs->LeavePage('/', 'Something went wrong: submitting to an unknown chart');
  }

  if ($chart['game_id'] !== $game['game_id']) {
    $cs->LeavePage('/', 'Something went wrong: submitting to a chart of another game');
  }

  $existing_record = RecordsRepository::find_by([
    'level_id' => $chart['level_id'],
    'user_id' => $current_user['user_id'],
  ]);

  if (RecordManager::IsUnderInvestigation($existing_record)) {
    $cs->LeavePage('/', 'Something went wrong: record under investigation');
  }

  $submission = $record['submission'] ?? NULL;
  $submission2 = $record['submission2'] ?? NULL;

  if ($submission !== NULL || $submission2 !== NULL) {
    $queued_records []= [
      'chart_id'    => $chart['level_id'],
      'user_id'     => $current_user['user_id'],
      'entity_id'   => ($record['entity_id1'] ?? null) ?: null,
      'entity_id2'  => ($record['entity_id2'] ?? null) ?: null,
      'entity_id2'  => ($record['entity_id3'] ?? null) ?: null,
      'entity_id2'  => ($record['entity_id4'] ?? null) ?: null,
      'entity_id2'  => ($record['entity_id5'] ?? null) ?: null,
      'entity_id2'  => ($record['entity_id6'] ?? null) ?: null,
      'platform_id' => $record['platform_id'] ?? null,
      'game_patch'  => $record['game_patch'] ?? null,
      'submission'  => $submission,
      'submission2' => $submission2,
      'extra1'      => $record['extra1'] ?? null,
      'extra2'      => $record['extra2'] ?? null,
      'extra3'      => $record['extra3'] ?? null,
      'comment'     => $record['comment'] ?? null,
      'created_at'  => database_now(),
    ];
  }
}

if (count($queued_records) > 100) {
  database_insert_all('queued_records', $queued_records);
  $cs->FlashSuccess("queued " . count($queued_records) . " records. These will all be submitted in a few minutes.");
} else {
  foreach ($queued_records as $queued_record) {
    [$result, $change, $record_id] = RecordManager::SubmitQueuedRecord($queued_record);

    if ($result == 'no_entity')           $cs->WriteNote(false, 'No entity was selected');
    else if ($result == 'error_entity')   $cs->WriteNote(false, 'There was an error with the entity');
    else if ($result == 'error_platform') $cs->WriteNote(false, 'There was an error with the platform');
    else {
      if ($change == 'add')            $new_records++;
      else if ($change == 'update')    $updated_records++;
      else if ($change == 'change')    $updated_records++;
      else if ($change == 'no change') $unchanged_recordss++;
    }
  }

  $cs->FlashSuccess("$new_records new records, $updated_records updated records.");
}

render_json($cs->notes);
