<?php

Authorization::authorize('UserMod');

$params = filter_post_params([
  Params::number('record_id'),
  Params::number('new_chart_id'),
]);

RecordManager::MoveDeletedRecord($params['record_id'], $params['new_chart_id'], $current_user['user_id']);

$cs->WriteNote(true, "Record moved successfully.");
redirect_to_back();
