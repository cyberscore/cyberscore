<?php

Authorization::authorize('GameMod');

$params = filter_post_params([
  Params::number('server_id'),
]);

DiscordServersRepository::delete($params['server_id']);
$cs->WriteNote(false, "Deleted discord server");

$cs->RedirectToPreviousPage();
