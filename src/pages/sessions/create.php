<?php

function internal_bbcode($text) {
  return str_replace([
    '[link_resetpassword]',
    '[link_contact]',
    '[/link]',
  ], [
    '<a href="/passwords/recover">',
    '<a href="/support.php">',
    '</a>',
  ], $text);
}

$params = filter_post_params([
  Params::str('username'),
  Params::str('password'),
  Params::str('referrer'),
]);

$user = Authentication::authenticate($params['username'], $params['password']);

log_event('login/attempt', ['username' => $params['username']]);

if ($user == null) {
  log_event('login/fail', ['username' => $params['username']]);
  $cs->WriteNote(false, $t['login_error'], internal_bbcode($t['login_error_wrong_password']));
} else if ($user['user_id'] == 24634 || $user['user_id'] == 16411) {
  log_event('login/frozen', ['user_id' => $user['user_id'], 'email' => $user['email'], 'username' => $user['username']]);
  $cs->WriteNote(false, $t['login_error'], internal_bbcode($t['login_error_user_frozen']));
} else if ($user['banned']) {
  log_event('login/suspended', ['user_id' => $user['user_id'], 'email' => $user['email'], 'username' => $user['username']]);
  $cs->WriteNote(false, $t['login_error'], internal_bbcode($t['login_error_user_suspended']));
} else {
  log_event('login/success', ['user_id' => $user['user_id'], 'email' => $user['email'], 'username' => $user['username']]);
  // Everything is fine, we can log this user in
  $user_id = $user['user_id'];

  Session::Store('user_id', $user_id);

  $cs->WriteNote(true, $t['login_success']);

  // Gets and stores the last known IP Address for a user, upon their log-in
  database_update_by('users', ['last_ip' => get_ip()], ['user_id' => $user_id]);

  // Rehash the password and store it on every login.
  // We're doing this because the hashed passwords were slightly wrong.
  // Check authentication.php for more details.
  Authentication::store_password($params['password'], $user);
}

if (!$user_id) {
  sleep(2);
  render_with('sessions/new', ['referrer' => $params['referrer']]);
} else {
  redirect_to($params['referrer']);
}
