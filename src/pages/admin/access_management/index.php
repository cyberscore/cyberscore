<?php

Authorization::authorize('Admin');

$users = database_get_all(database_select("
  SELECT users.*
  FROM users WHERE user_groups > 0
  ORDER by user_groups DESC, username ASC
", '', []));

$roles = [
  ['id' => 'Owner'     , 'label' => 'Owner'],
  ['id' => 'Admin'     , 'label' => 'Administrator'],
  ['id' => 'GlobalMod' , 'label' => 'Global Moderator'],
  ['id' => 'Dev'       , 'label' => 'Developer'],
  ['id' => 'Curator'   , 'label' => 'Curator'],
  ['id' => 'UserMod'   , 'label' => 'User Moderator'],
  ['id' => 'ProofMod'  , 'label' => 'Proof Moderator'],
  ['id' => 'GameMod'   , 'label' => 'Game Moderator'],
  ['id' => 'Newswriter', 'label' => 'Newswriter'],
  ['id' => 'Designer'  , 'label' => 'Designer'],
  ['id' => 'Translator', 'label' => 'Translator'],
];
foreach ($roles as &$role) {
  $role['value'] = Authorization::ReadUserGroup($role['id']);
}
unset($role);

foreach ($users as &$user) {
  $user['roles'] = [];
  foreach ($roles as $role) {
    $user['roles'][$role['id']] = Authorization::user_has_role($user, $role['id']);
  }
}
unset($user);

render_with('admin/access_management/index', [
  'page_title' => 'Access levels',
  'users' => $users,
  'roles' => $roles,
]);
