<?php

Authorization::authorize('GameMod');

if ($_POST['scoreboard'] ?? null) {
  $scoreboard = $_POST['scoreboard'];
  switch ($scoreboard) {
  case 'starboard':          ScoreboardCacheRebuilder::RebuildMainboard(); break;
  case 'medal':              ScoreboardCacheRebuilder::RebuildMedalTable(); break;
  case 'arcade':             ScoreboardCacheRebuilder::RebuildArcadeCache(); break;
  case 'speedrun':           ScoreboardCacheRebuilder::RebuildSpeedrunCache(); break;
  case 'solution':           ScoreboardCacheRebuilder::RebuildSolutionCache(); break;
  case 'trophy':             ScoreboardCacheRebuilder::RebuildTrophyTable(); break;
  case 'submissions':        ScoreboardCacheRebuilder::RebuildTotalSubmissionsCache(); break;
  case 'proof':              ScoreboardCacheRebuilder::RebuildProofCache(); break;
  case 'vproof':             ScoreboardCacheRebuilder::RebuildVideoProofCache(); break;
  case 'challenge':          ScoreboardCacheRebuilder::RebuildChallengeCache(); break;
  case 'incremental':        ScoreboardCacheRebuilder::RebuildIncrementalCache(); break;
  case 'collectible':        ScoreboardCacheRebuilder::RebuildCollectibleCache(); break;
  case 'rainbow':            ScoreboardCacheRebuilder::RebuildRainbowScoreboard(); break;
  default:
    $cs->PageNotFound();
  }

  Flash::AddSuccess("Rebuilt $scoreboard cache");
}

if ($_POST['game_id'] ?? null) {
  $game_id = intval($_POST['game_id']);

  GameCacheRebuilder::RebuildGame($game_id);
  database_delete_by('cacherebuilder', ['game_id' => $game_id]);

  Flash::AddSuccess("Rebuilt game ID #$game_id.");
}

if ($_POST['from_game_id'] ?? null && $_POST['to_game_id'] ?? null) {
  $from_game_id = intval($_POST['from_game_id']);
  $to_game_id = intval($_POST['to_game_id']);

  GameCacheRebuilder::QueueGameRangeForRebuild($from_game_id, $to_game_id);
  Flash::AddSuccess("Queued games between ID #$from_game_id & #$to_game_id for rebuild.");
}

if ($_POST['every_game'] ?? null) {
  GameCacheRebuilder::QueueAllGamesForRebuild();
  Flash::AddSuccess("Queued all games for rebuild.");
}

if ($_POST['chart_id'] ?? null) {
  $chart_id = intval($_POST['chart_id']);
  ChartCacheRebuilder::RebuildChart($chart_id);
  Flash::AddSuccess("Rebuilt chart ID #$chart_id.");
}

if ($_POST['from_chart_id'] ?? null && $_POST['to_chart_id'] ?? null) {
  $from_chart_id = intval($_POST['from_chart_id']);
  $to_chart_id = intval($_POST['to_chart_id']);

  ChartCacheRebuilder::QueueChartRangeForRebuild($from_chart_id, $to_chart_id);
  Flash::AddSuccess("Queued charts between ID #$from_chart_id & #$to_chart_id for rebuild.");
}

if ($_POST['every_chart'] ?? null) {
  ChartCacheRebuilder::QueueAllChartsForRebuild();
  Flash::AddSuccess("Queued all charts for rebuild.");
}

$cs->RedirectToPreviousPage();
