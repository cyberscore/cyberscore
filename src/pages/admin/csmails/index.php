<?php

Authorization::authorize('GlobalMod');

$csmails = database_get_all(database_select("
  SELECT csmail.subject, users.username AS from_name, users2.username AS to_name
  FROM csmail, users, users AS users2
  WHERE csmail.from_id = users.user_id AND csmail.to_id = users2.user_id
  ORDER BY csmail.csmail_id DESC
  LIMIT 100
", '', []));

render_with('admin/csmails/index', [
  'page_title' => 'CSMail monitor',
  'csmails' => $csmails,
]);
