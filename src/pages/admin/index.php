<?php

Authorization::authorize(['Admin', 'ProofMod', 'GameMod', 'Designer', 'Translator']);

$links = [
  [
    'title' => 'Administration tools',
    'role' => 'Admin',
    'icon' => '/images/Star_Red16.png',
    'links' => [
      ["/admin/access_management", 'tools_access_management'],
      ['/admin/email-bans', "tools_email_bans"],
      ["/users", 'tools_manage_users'],
      ["/admin/platforms", 'tools_platform_management'],
      ["/admin/countries", 'tools_country_management'],
    ],
  ],
  [
    'title' => 'Global moderation tools',
    'role' => 'GlobalMod',
    'icon' => '/images/Star_Blue16.png',
    'links' => [
      ['/record-comments', 'tools_comment_monitor'],
      ["/admin/csmails", 'tools_csmail_monitor'],
      // ["/admin/staff_logs", 'tools_staff_logs'],
    ],
  ],
  [
    'title' => 'User moderation tools',
    'role' => 'UserMod',
    'icon' => '/images/Star_LightBlue16.png',
    'links' => [
      ['/admin/banned_users', 'tools_banlist'],
      ["records/deleted", 'tools_deletedrecords'],
      ["/user-moderation-notes", 'tools_moderator_notes'],
      ["/referrals", 'tools_referral_management'],
      ["/reported-records", 'tools_reported_records'],
      ["/support_requests.php", 'tools_support_requests'],
    ],
  ],
  [
    'title' => 'Proof moderation tools',
    'role' => 'ProofMod',
    'icon' => '/images/Star_LightGreen16.png',
    'links' => [
      ["/proofs/pending", 'tools_pendingproofs'],
      ["/record-moderation-notes", 'tools_record_notes'],
      ["/proof-rules", 'tools_proof_guideline_editor'],
    ]
  ],
  [
    'title' => 'Game moderation tools',
    'role' => 'GameMod',
    'icon' => '/images/Star_Green16.png',
    'links' => [
      ["/boxarts/new", 'tools_boxartupload'],
      ["/game_requests", 'tools_gamerequests_gr'],
      ["/handled-game-requests", 'tools_handled_game_requests'],
      ["/homepics", 'tools_pic_uploader'],
      ["/admin/rebuild_center", 'tools_rebuild_centre'],
      ["/rules", 'tools_ruler'],
      ["/game-series", 'tools_series_management'],
    ],
  ],
  [
    'title' => 'Designer tools',
    'role' => 'Designer',
    'icon' => '/images/Star_Yellow16.png',
    'links' => [
      ["/rankbuttons", 'tools_view_rankbuttons'],
      ["/rankbuttons/new", 'tools_rb_uploader'],
    ],
  ],
  [
    'title' => 'Translator tools',
    'role' => 'Translator',
    'icon' => '/images/Star_Orange16.png',
    'links' => [
      ["/translations/games", 'tools_origin'],
    ],
  ],
];

$links = array_filter($links, fn ($section) => Authorization::has_access($section['role']));

$workshop_games = database_get_all(database_select("
  SELECT
    game_id,
    all_charts AS total_charts
  FROM games
  WHERE site_id = 4
  ORDER BY game_id DESC
", '', []));

render_with('admin/index', [
  'page_title' => t('general_site_tools'),
  'links' => $links,
  'workshop_games' => $workshop_games,
]);
