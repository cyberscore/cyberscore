<?php

Authorization::authorize('Admin');

$platform = database_find_by('platforms', ['platform_id' => $_GET['id']]);

render_with('admin/platforms/edit', [
  'page_title' => 'Edit platform',
  'platform' => $platform,
]);
