<?php

Authorization::authorize('Admin');

$platforms = database_fetch_all("
  SELECT
    platforms.*,
    COUNT(game_platforms.platform_id) AS num_games
  FROM platforms
  LEFT JOIN game_platforms USING (platform_id)
  GROUP BY platform_id
  ORDER BY platforms.manufacturer ASC, platforms.platform_name ASC
", []);

$platform_ids = pluck($platforms, 'platform_id');

$orphan_images = [];
$images = glob("{$config['app']['root']}/public/uploads/platforms/*.gif");
foreach ($images as $image) {
  if (!in_array(basename($image, ".gif"), $platform_ids)) {
    $orphan_images []= [
      'url' => explode("/public", $image)[1],
    ];
  }
}




render_with('admin/platforms/index', [
  'page_title' => 'Platforms',
  'platforms' => $platforms,
  'orphan_images' => $orphan_images,
]);
