<?php

Authorization::authorize('UserMod');

$banned_user_id = $_GET['id'];

$user = UsersRepository::get($banned_user_id);

if (!$user) {
  $cs->PageNotFound();
}

UsersRepository::update($user['user_id'], [
  'mod_notes' => $_POST['mod_notes'],
  'mod_notes_date' => database_now(),
  'mod_notes_id' => $current_user['user_id'],
]);

$cs->WriteNote(true, "Updated moderation note");

redirect_to('/admin/banned_users');
