<?php

Authorization::authorize('GameMod');

$game = GamesRepository::get($_GET['id']);

if (!$game) {
  $cs->LeavePage('/games.php', 'The requested game does not exist.');
}

$language_id = $_GET['lang_id'] ?? 1;

$game_name = $t->GetGameName($game['game_id']);
$game_entities = database_get_all(database_select("
  SELECT game_id, game_entity_id, entity_name, entity_group, inherited_entity_id
  FROM game_entities
  WHERE game_id = ?
  ORDER BY entity_group ASC, entity_name ASC, game_entity_id ASC, inherited_entity_id ASC
", 's', [$game['game_id']]));

foreach ($game_entities as &$entity) {
  if ($entity['inherited_entity_id'] != 0) {
    $entity['inherited_entity'] = database_get(database_select("
      SELECT game_entity_id, entity_name, game_id
      FROM game_entities
      WHERE game_entity_id = ?
      ORDER BY entity_name ASC
    ", 's', [$entity['inherited_entity_id']]));
  } else {
    $entity['inherited_entity'] = NULL;
  }

  $entity['image_path'] = Entity::image_path($entity, $entity['inherited_entity']);
  $entity['entity_name'] = $t->GetEntityName($entity['game_entity_id'], $language_id);
}
unset($entity);

$num_entities = count($game_entities);

$entity_names = pluck($game_entities, 'entity_name');

$languages = database_get_all(database_select("
  SELECT language_id, language_name
  FROM languages
  ORDER BY language_name ASC
", '', []));

$translation_languages = database_get_all(database_select("
  SELECT
    COUNT(*) AS num_entity_names,
    COUNT(*) / ? * 100 AS percentage,
    languages.language_name
  FROM translation_games
  JOIN languages USING (language_id)
  WHERE translation_games.game_id = ? AND table_field = 'entity_name'
  GROUP BY translation_games.language_id
  ORDER BY languages.language_name ASC
", 'ss', [$num_entities, $game['game_id']]));

$t->CacheEntityNames($game['game_id'], $language_id);

render_with('game-entities/edit', [
  'page_title' => 'Edit game entities',
  'game' => $game,
  'language_id' => $language_id,
  'languages' => $languages,
  'game_entities' => $game_entities,
  'entity_names' => $entity_names,
  'translation_languages' => $translation_languages,
  'game_name' => $game_name,
]);
