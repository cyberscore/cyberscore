<?php

$game = GamesRepository::get($_GET['id']);
if (!$game) {
  $cs->PageNotFound();
}

$game_name = $t->GetGameName($game['game_id']);

$topics = database_get_all(database_select("
  SELECT
    game_board_topics.game_id,
    game_board_topics.topic_id, game_board_topics.topic_title,
    COUNT(game_board_messages.topic_id) AS num_messages,
    MAX(game_board_messages.message_date) AS last_message_date
  FROM game_board_topics
  LEFT JOIN game_board_messages USING(topic_id)
  GROUP BY game_board_topics.topic_id
  ORDER BY last_message_date DESC
", '', []));

$t->CacheGameNames();
foreach ($topics as &$topic) {
  $topic['game_name'] = $t->GetGameName($topic['game_id']);
}
unset($topic);

$game_topics = array_filter($topics, fn($topic) => $topic['game_id'] == $game['game_id']);

$page_title = str_replace('[game]', $game_name, $t['gameboard_discussion_board_game']);

render_with('game-boards/show', [
  'page_title' => $page_title,
  'game_topics' => $game_topics,
  'topics' => $topics,
  'game' => $game,
]);
