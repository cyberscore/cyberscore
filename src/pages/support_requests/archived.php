<?php

Authorization::authorize('UserMod');

if (($_GET['sort'] ?? null) == 'archive_date') {
  $order = "support.mod_date DESC";
} else {
  $order = "support.message_date DESC";
}

$requests = database_get_all(database_select("
  SELECT
    users.username,
    support.user_id,
    support.support_id,
    support.message,
    support.message_date,
    users2.username AS mod_name,
    support.mod_date,
    support.mod_comment
  FROM support
  LEFT JOIN users USING(user_id)
  LEFT JOIN users AS users2 ON users2.user_id = support.mod_id
  WHERE support.status = 1
  ORDER BY $order
", '', []));

render_with('support_requests/archived', [
  'page_title' => 'Support requests',
  'requests' => $requests,
]);
