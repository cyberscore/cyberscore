<?php

Authorization::authorize('UserMod');

$support = database_find_by('support', ['support_id' => $_GET['id']]);

$params = ['mod_comment' => $_POST['comment']];

if (isset($_POST['archive'])) {
  $params['status'] = 1;
}

if (isset($_POST['unarchive'])) {
  $params['status'] = 0;
}

$params = array_merge($params, [
  'mod_id' => $current_user['user_id'],
  'mod_date' => database_now(),
]);

if (isset($_POST['archive'])) {
  database_insert('staff_tasks', [
    'user_id' => $current_user['user_id'],
    'tasks_type' => 'support_request_archived',
    'task_id' =>  $support['support_id'],
    'result' => 'Support request archived',
  ]);

  database_update_by('support', $params, ['support_id' => $support['support_id']]);
  $cs->WriteNote(true, "Support request status updated");
} elseif (isset($_POST['unarchive'])) {
  database_update_by('support', $params, ['support_id' => $support['support_id']]);
  $cs->WriteNote(true, "Support request comment unarchived");
} else {
  database_update_by('support', $params, ['support_id' => $support['support_id']]);
  $cs->WriteNote(true, "Support request comment updated");
}

$cs->LeavePage("/support_requests.php");
