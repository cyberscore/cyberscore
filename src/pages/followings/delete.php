<?php

Authorization::authorize('User');

$other = UsersRepository::get($_GET['id']);

if (!$other || $other['user_id'] == $current_user['user_id']) {
  $cs->WriteNote(false, "You must unfollow someone other than yourself");
  $cs->RedirectToPreviousPage();
}

$params = ['user_id' => $current_user['user_id'], 'follow_id' => $other['user_id']];
database_delete_by('user_blog_follows', $params);
Notification::BlogUnfollowed($other['user_id'], $current_user['user_id'])->DeliverToTarget();

$cs->WriteNote(true, "Unfollowed {$other['username']}");
$cs->RedirectToPreviousPage();
