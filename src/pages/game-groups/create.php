<?php

$params = filter_post_params([
  Params::str('group_names'),
  Params::number('group_pos'),
  Params::number('game_id'),
  Params::number('ranked'),
]);

$game = GamesRepository::get($params['game_id']);
if (!$game) {
  $cs->PageNotFound();
}

$group_names = array_filter(
  array_map(fn($p) => trim($p), explode("\n", $params['group_names'])),
  fn($name) => $name != ''
);

$num_groups = count($group_names);

// make room for new groups by moving existing groups forward
database_select("
  UPDATE level_groups
  SET group_pos = group_pos + ?
  WHERE game_id = ? AND group_pos >= ?
", 'sss', [$num_groups, $game['game_id'], $params['group_pos']]);

// create new groups
foreach ($group_names as $group_name) {
  database_insert('level_groups', [
    'game_id' => $game['game_id'],
    'group_name' => $group_name,
    'group_pos' => $params['group_pos'],
    'ranked' => $params['ranked'],
  ]);
  $params['group_pos']++;
}

$cs->WriteNote(true, "$num_groups group(s) added");
redirect_to_back();
