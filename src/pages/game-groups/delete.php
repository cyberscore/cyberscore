<?php

Authorization::authorize('GameMod');

$group = GroupsRepository::get($_GET['id']);

if (!$group) {
  $cs->PageNotFound();
}

$num_charts = database_single_value("
  SELECT COUNT(1)
  FROM levels
  WHERE group_id = ?
", 's', [$group['group_id']]);

if ($num_charts > 0) {
  $cs->WriteNote(false, "You can only delete groups without any charts.");
  $cs->RedirectToPreviousPage();
}

database_delete_by('translation_games', ['table_field' => 'group_name', 'table_id' => $group['group_id']]);
GroupsRepository::delete($group['group_id']);

$cs->WriteNote(true, "Group deleted");
redirect_to("/games/{$group['game_id']}");
