<?php

Authorization::authorize('GameMod');

// TODO: automatic "Separate charts" must go.
// We need to explicitly create a group_id for them.
if (str_starts_with($_GET['id'], 's')) {
  $group = ['group_id' => 0];
  $game = GamesRepository::get(substr($_GET['id'], 1));
  $pseudo_group_id = "s{$game['game_id']}";
} else {
  $group = GroupsRepository::get($_GET['id']);
  $game = GamesRepository::get($group['game_id']);
  $pseudo_group_id = $group['group_id'];
}

$language = database_find_by('languages', ['language_id' => $_GET['lang_id'] ?? 1]);

if (!$game || !$group || !$language) {
  $cs->LeavePage('/games.php', 'The requested game/group/language does not exist.');
}

$language_id = $language['language_id'];

$group_charts = database_get_all(database_select("
  SELECT
    levels.players, levels.level_id, levels.level_pos, levels.level_name,
    COUNT(records.level_id) AS num_records,
    MAX(records.last_update) AS last_update,
    levels.ranked, levels.dlc
  FROM levels
  LEFT JOIN records USING(level_id)
  WHERE levels.game_id = ? AND levels.group_id = ?
  GROUP BY levels.level_id
  ORDER BY levels.level_pos ASC, levels.level_name ASC
", 'ss', [$game['game_id'], $group['group_id']]));


$languages = database_get_all(database_select("
  SELECT language_id, language_name
  FROM languages
  ORDER BY language_name ASC
", '', []));

$chart_count_by_name = index_by(database_get_all(database_select("
  SELECT level_name, count(1) AS total
  FROM levels
  WHERE game_id = ?
  GROUP BY level_name
", 's', [$game['game_id']])), 'level_name', 'total');

$other_groups = database_get_all(database_select("
  SELECT levels.group_id
  FROM levels
  LEFT JOIN level_groups USING(group_id)
  WHERE levels.game_id = ? AND levels.group_id != ?
  GROUP BY levels.group_id
  ORDER BY levels.group_id != 0 DESC, level_groups.group_pos ASC
", 'ss', [$game['game_id'], $group['group_id']]));


$charts = database_get_all(database_select("
  SELECT levels.level_id, levels.group_id
  FROM levels
  LEFT JOIN level_groups USING(group_id)
  WHERE levels.game_id = ?
  ORDER BY levels.group_id != 0 DESC, level_groups.group_pos ASC, levels.level_pos ASC
", 's', [$game['game_id']]));

$t->CacheGame($game['game_id']);
$t->CacheChartNames($game['game_id'], $language['language_id']);

foreach ($other_groups as &$g) {
  $g['name'] = $t->GetGroupName($g['group_id']);
}
unset($g);

$group['name'] = $t->GetGroupName($group['group_id']);

foreach ($group_charts as &$chart) {
  $chart['name'] = $t->GetChartNameLang($chart['level_id'], $language_id);
}
unset($chart);

foreach ($charts as &$chart) {
  $chart['name'] = $t->GetChartNameLang($chart['level_id'], $language_id);
}
unset($chart);

render_with('game-groups/edit', [
  'page_title' => 'Edit Group',
  'pseudo_group_id' => $pseudo_group_id,
  'group' => $group,
  'game' => $game,
  'group_charts' => $group_charts,
  'languages' => $languages,
  'language' => $language,
  'language_id' => $language_id,
  'chart_count_by_name' => $chart_count_by_name,
  'other_groups' => $other_groups,
  'charts' => $charts,
  'english_names_clipboard' => implode("\n", pluck($group_charts, 'level_name')),
]);
