<?php

Authorization::authorize('Translator');

$games = database_get_all(database_select("
  SELECT games.game_id, games.game_name, languages.language_id, languages.language_name, translation_games_progress.status, COUNT(*) AS count
  FROM translation_games
  JOIN games USING(game_id)
  JOIN languages USING(language_id)
  LEFT JOIN translation_games_progress USING(game_id, language_id)
  GROUP BY translation_games.game_id, translation_games.language_id
  ORDER BY games.game_name ASC, languages.language_name ASC
", '', []));

$games = group_by($games, function($g) { return $g['game_id']; });

$t->CacheGameNames();

render_with('translations/games/index', [
  'page_title' => 'Translation info',
  'games' => $games,
]);
