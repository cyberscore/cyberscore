<?php

Authorization::authorize('GameMod');

$user = database_find_by('users', ['user_id' => $_GET['id']]);

if (!$user) {
  $cs->FlashError('User not found');
} else {
  database_delete_by('translator_info', ['user_id' => $user['user_id']]);

  $result = Authorization::RemoveFromUserGroup($user, 'Translator');
  if ($result) {
    $cs->FlashSuccess("{$user['username']} removed as translator");
  } else {
    $cs->FlashError("{$user['username']} is not a translator");
  }
}

$cs->RedirectToPreviousPage();
