<?php

Authorization::authorize('GameMod');

$user = database_find_by('users', ['username' => $_POST['username']]);

if (!$user) {
  $cs->WriteNote(false, 'User not found');
} else {
  $result = Authorization::AddToUserGroup($user, 'Translator');
  if ($result) {
    $cs->WriteNote(true, "{$user['username']} added as translator");
  } else {
    $cs->WriteNote(false, "{$user['username']} is already translator");
  }
}

$cs->RedirectToPreviousPage();
