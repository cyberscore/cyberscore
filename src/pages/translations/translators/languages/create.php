<?php

Authorization::authorize('GameMod');

$user = database_find_by('users', ['user_id' => $_POST['user_id']]);
$language = database_find_by('languages', ['language_code' => $_POST['language_code']]);

if (!$user || !$language) {
  $cs->WriteNote(false, 'User/language not found');
} else {
  $record = [
    'user_id' => $user['user_id'],
    'language_code' => $language['language_code'],
  ];

  $translator_language = database_find_by('translator_info', $record);

  if (!$translator_language) {
    database_insert('translator_info', $record);
  }

  $cs->WriteNote(true, "Added translator info");
}

$cs->RedirectToPreviousPage();
