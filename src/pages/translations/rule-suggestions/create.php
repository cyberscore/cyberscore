<?php

Authorization::authorize('Translator');

$language_id = intval($_POST['language_id']);

foreach ($_POST['text'] as $rule_id => $translation) {
  $t->SuggestTranslationDB(
    $language_id,
    'rule_text',
    $rule_id,
    $translation,
    $current_user['user_id']
  );
}

$cs->RedirectToPreviousPage();
