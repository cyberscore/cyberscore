<?php

Authorization::authorize('Translator');

$translate_lang_id = $t->GetTranslateLang();

$page = intval($_GET['p'] ?? 0);
$page_size = 50;

$start = $page * $page_size;
$total = database_single_value(
  "SELECT COUNT(1) FROM proof_rules", '', []
);

$rules = database_get_all(database_select("
  SELECT rule_id, rule_text
  FROM proof_rules
  ORDER BY rule_id ASC
  LIMIT $page_size
  OFFSET $start
", '', []));

foreach ($rules as &$rule) {
  $rule['translation'] = $t->GetProofRuleTextLang($rule['rule_id'], $translate_lang_id);

  $rule['suggested_translation'] =
    $t->GetSuggestedProofRuleTextLang($rule['rule_id'], $translate_lang_id) ??
    $rule['translation'];
}
unset($rule);

render_with('translations/proof-rules/index', [
  'page_title' => 'Translation',
  'language_name' => $t->GetLangName($translate_lang_id),
  'rules' => $rules,
  'current_page' => $page,
  'total_pages' => $total / $page_size,
  'translate_lang_id' => $translate_lang_id,
]);
