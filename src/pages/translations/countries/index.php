<?php

Authorization::authorize('Translator');

$translate_lang_id = $t->GetTranslateLang();

$countries = database_get_all(database_select("
  SELECT * FROM countries ORDER BY country_name ASC
", '', []));

$ld_edit = $t->RetrieveCountryNames($translate_lang_id);

$users_by_country_id = database_get_all(database_select("
  SELECT COUNT(1) AS total, country_id FROM users GROUP BY country_id
", '', []));

$users_by_country_id = index_by($users_by_country_id, 'country_id', 'total');

foreach ($countries as &$country) {
  $country['translation'] = $ld_edit[$country['country_id']] ?? '';
  $country['user_count'] = $users_by_country_id[$country['country_id']] ?? 0;
}
unset($country);

render_with('translations/countries/index', [
  'page_title' => 'Translation',
  'language_name' => $t->GetLangName($translate_lang_id),
  'translate_lang_id' => $translate_lang_id,
  'countries' => $countries,
]);
