<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::number('topic_id'),
  Params::str('message_text'),
]);

$topic = database_find_by('game_board_topics', ['topic_id' => $params['topic_id']]);

if (!$topic) {
  $cs->PageNotFound();
}

if (empty($params['message_text'])) {
  $cs->Exit();
}

database_insert('game_board_messages', [
  'game_id' => $topic['game_id'],
  'topic_id' => $topic['topic_id'],
  'user_id' => $current_user['user_id'],
  'message_text' => $params['message_text'],
  'message_date' => database_now(),
]);

Flash::AddSuccess('Message created');

redirect_to("/game-board-topics/{$topic['topic_id']}");
