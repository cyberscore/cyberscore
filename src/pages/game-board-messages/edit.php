<?php

Authorization::authorize('User');

$message = database_find_by('game_board_messages', ['message_id' => $_GET['id']]);

if (!$message) {
  $cs->PageNotFound();
}

Authorization::authorize('Admin', $message['user_id']);

$topic = database_find_by('game_board_topics', ['topic_id' => $message['topic_id']]);
$game = GamesRepository::get($message['game_id']);

$game_name = $t->GetGameName($game['game_id']);

render_with('game-board-messages/edit', [
  'page_title' => t('gameboard_edit_message'),
  'game' => $game,
  'topic' => $topic,
  'message' => $message,
]);
