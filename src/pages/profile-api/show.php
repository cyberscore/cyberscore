<?php
if (is_numeric($_GET['id'])) {
	$user = database_find_by('users', ['user_id' => $_GET['id']]);
} else {
	$user = database_find_by('users', ['username' => $_GET['id']]);
}
$id = $user['user_id'];

//Scoreboard information
$csr         = database_get(database_select("SELECT * FROM sb_cache WHERE user_id = ?", 's', [$id]));
$medal       = database_get(database_select("SELECT * FROM sb_cache_standard WHERE user_id = ?", 's', [$id]));
$arcade      = database_get(database_select("SELECT * FROM sb_cache_arcade WHERE user_id = ?", 's', [$id]));
$speedrun    = database_get(database_select("SELECT * FROM sb_cache_speedrun WHERE user_id = ?", 's', [$id]));
$solution    = database_get(database_select("SELECT * FROM sb_cache_solution WHERE user_id = ?", 's', [$id]));
$challenge   = database_get(database_select("SELECT * FROM sb_cache_challenge WHERE user_id = ?", 's', [$id]));
$incremental = database_get(database_select("SELECT * FROM sb_cache_incremental WHERE user_id = ?", 's', [$id]));
$collectible = database_get(database_select("SELECT * FROM sb_cache_collectible WHERE user_id = ?", 's', [$id]));
$subs        = database_get(database_select("SELECT * FROM sb_cache_total_subs WHERE user_id = ?", 's', [$id]));
$proof       = database_get(database_select("SELECT * FROM sb_cache_proof WHERE user_id = ?", 's', [$id]));
$video_proof = database_get(database_select("SELECT * FROM sb_cache_video_proof WHERE user_id = ?", 's', [$id]));
$rainbow     = database_get(database_select("SELECT * FROM sb_cache_rainbow WHERE user_id = ?", 's', [$id]));
$trophy      = database_get(database_select("SELECT * FROM sb_cache_trophy WHERE user_id = ?", 's', [$id]));

//Record status counters
$num_pi = database_single_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status = 1", 's', [$id]);
$num_ui = database_single_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status = 2", 's', [$id]);
$num_proofs = database_single_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status = 3", 's', [$id]);
$num_pending_approval = database_single_value("SELECT COUNT(*) FROM records WHERE user_id = ? AND rec_status >= 4", 's', [$id]);

//Other useful info calculations
$avg_rank = (
  $rainbow['starboard_pos'] +
  $rainbow['medal_pos'] +
  $rainbow['trophy_pos'] +
  $rainbow['arcade_pos'] +
  $rainbow['speedrun_pos'] +
  $rainbow['solution_pos'] +
  $rainbow['challenge_pos'] +
  $rainbow['incremental_pos'] +
  $rainbow['collectible_pos'] +
  $rainbow['proof_pos'] +
  $rainbow['video_proof_pos'] +
  $rainbow['total_submissions_pos']
) / 12;

// Wrangle rainbow scoreboard position into a more readable format.
$rainbow['rainbow_pos'] = $rainbow['scoreboard_pos'];
unset($rainbow['scoreboard_pos']);

render_json([
    'user_id' => $user['user_id'],
    'username' => $user['username'],
    'country' => $user['country_code'],
    'gender' => $user['gender'],
    'join_date' => $user['date_joined'],
    'last_seen' => $user['date_lastseen'],
    'sub_counts' => $subs,
    'proof_counts' => $proof,
    'video_proof_counts' => $video_proof,
    'positions' => $rainbow,
    'avg_rainbow_rank' => $avg_rank,
]);
