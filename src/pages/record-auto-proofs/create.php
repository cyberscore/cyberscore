<?php

Authorization::Authorize('User');

$new_records = 0;
$updated_records = 0;
$unchanged_records = 0;

$proof_files = [];
foreach ($_FILES['files']['name'] as $index => $file) {
  $file = [
    'name' => $_FILES['files']['name'][$index],
    'type' => $_FILES['files']['type'][$index],
    'size' => $_FILES['files']['size'][$index],
    'error' => $_FILES['files']['error'][$index],
    'tmp_name' => $_FILES['files']['tmp_name'][$index],
  ];

  $proof_files []= ProofManager::StoreUploadedProof($file);
}

foreach ($proof_files as $result) {
  if ($result['status'] != 'success') {
    Flash::AddError("Error uploading proof file");
    $cs->Exit();
  }
}

foreach ($_POST['records'] as $chart_id => $params) {
  $chart = LevelsRepository::get($chart_id);

  if ($chart === null) {
    Flash::AddError('Something went wrong: submitting to an unknown chart');
    break;
  }

  if (count($proof_files) <= $params['file_index']) {
    Flash::AddError('Something went wrong: probably tried to upload too many files at once');
    break;
  }

  $record = RecordsRepository::find_by([
    'level_id' => $chart_id,
    'user_id' => $current_user['user_id'],
  ]);

  $rec_status = $record['rec_status'] ?? 0;
  if ($rec_status == 1 || $rec_status == 2 || $rec_status == 5 || $rec_status == 6) {
    $cs->LeavePage('/', 'Something went wrong: record under investigation');
  }

  $submission = $cs->ReadInput(
    $chart['chart_type'],
    $params['input1'] ?? '',
    $params['input2'] ?? '',
    $params['input3'] ?? '',
    $params['input4'] ?? '',
  );

  if ($chart['chart_type2'] != 0) {
    $submission2 = $cs->ReadInput(
      $chart['chart_type2'],
      $params['input5'] ?? '',
      $params['input6'] ?? '',
      $params['input7'] ?? '',
      $params['input8'] ?? '',
    );
  } else {
    $submission2 = NULL;
  }

  if ($submission !== NULL || $submission2 !== NULL) {
    [$result, $change, $record_id] = RecordManager::SubmitRecord(
      $chart,
      $record,
      ($params['entity_id1'] ?? null) ?: null,
      ($params['entity_id2'] ?? null) ?: null,
      ($params['entity_id3'] ?? null) ?: null,
      ($params['entity_id4'] ?? null) ?: null,
      ($params['entity_id5'] ?? null) ?: null,
      ($params['entity_id6'] ?? null) ?: null,
      $params['platform_id'] ?? null,
      $submission,
      $submission2,
      $params['comment'] ?? null,
      $current_user['user_id'],
      '',
      $params['extra1'] ?? null,
      $params['extra2'] ?? null,
      $params['extra3'] ?? null,
      $params['game_patch'] ?? null
    );

    if ($result == 'success') {
      ProofManager::LinkProof($record_id, $proof_files[$params['file_index']]['link']);
    }

    if ($result == 'no_entity')           $cs->WriteNote(false, 'No entity was selected');
    else if ($result == 'error_entity')   $cs->WriteNote(false, 'There was an error with the entity');
    else if ($result == 'error_platform') $cs->WriteNote(false, 'There was an error with the platform');
    else {
      database_insert('auto_proofer_usages', [
        'user_id' => $current_user['user_id'],
        'record_id' => $record_id,
        'chart_id' => $chart['level_id'],
        'game_id' => $chart['game_id'],
        'file_path' => $proof_files[$params['file_index']]['link'],
        'created_at' => database_now(),
      ]);

      if ($change == 'add')            $new_records++;
      else if ($change == 'update')    $updated_records++;
      else if ($change == 'change')    $updated_records++;
      else if ($change == 'no change') $unchanged_recordss++;
    }
  }
}

Flash::AddSuccess("$new_records new records, $updated_records updated records.");

render_json([
  'new_records' => $new_records,
  'updated_records' => $updated_records,
  'unchanged_records' => $unchanged_records,
]);
