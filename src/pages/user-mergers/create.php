<?php

Authorization::authorize('GlobalMod');

$params = filter_post_params([
  Params::number('from_user_id'),
  Params::number('to_user_id'),
]);

$from_user = Users::get($params['from_user_id']);
$to_user = Users::get($params['to_user_id']);

if (!$from_user || !$to_user) {
  $cs->WriteNote(false, "Users not found");
  redirect_to_back();
}

$moved_recs = [];
$better_recs = [];
$equal_recs = [];
$worse_recs = [];

$records = RecordsRepository::where(['user_id' => $from_user['user_id']]);

foreach ($records as $record) {
  $existing_record = RecordsRepository::find_by(['user_id' => $to_user['user_id'], 'level_id' => $record['level_id']]);

  if ($existing_record === null) {
    $moved_recs []= $record['level_id'];
    database_update_by('records', ['user_id' => $to_user['user_id']], ['record_id' => $record['record_id']]);
    GameCacheRebuilder::QueueGameForRebuild($record['game_id']);
  } else {
    $new_chart_pos = $record['chart_pos'];
    $old_chart_pos = $existing_record['chart_pos'];

    if ($new_chart_pos < $old_chart_pos) {
      $better_recs []= $record['level_id'];
    } else if ($new_chart_pos == $old_chart_pos) {
      $equal_recs []= $record['level_id'];
    } else {
      $worse_recs []= $record['level_id'];
    }
  }
}

$cs->WriteNote(true, "Move report: Moved from " . h($from_user['username']) . " to " . h($to_user['username']));

$cs->WriteNote(true, "Records moved: " . count($moved_recs));
$cs->WriteNote(true, "Records better on target: " . count($better_recs));
$cs->WriteNote(true, "Records equal on both accounts: " . count($equal_recs));
$cs->WriteNote(true, "Records better on source: " . count($worse_recs));

foreach ($worse_recs as $chart_id) {
  $cs->WriteNote(true, '<a href="../chart/' . $chart_id . '" target="_blank">' . h($t->GetChartName($chart_id)) . '</a>');
}

redirect_to_back();
