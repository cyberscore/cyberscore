<?php

Authorization::authorize('Guest');

$record = database_get(database_select("
  SELECT *
  FROM records
  LEFT JOIN levels USING (level_id)
  JOIN chart_modifiers2 USING (level_id)
  WHERE record_id = ?
", 's', [$_GET['id']]));

if (!$record) {
  $cs->LeavePage('/index.php', 'The requested record does not exist.');
}

$chart = $cs->GetChart($record['level_id']);
$get_chart = array_values(array_filter($chart, fn($r) => $r['record_id'] == $record['record_id']))[0];
$get_chart['csr'] = csp2csr($get_chart['csp']);
$get_chart['ugh'] = [
  'submission1' => $cs->GetConvertedSubmission($get_chart['submission'],  $get_chart['submission2'], $record['conversion_factor'], 1, $record['num_decimals']),
  'submission2' => $cs->GetConvertedSubmission($get_chart['submission2'], $get_chart['submission'],  $record['conversion_factor'], 2, $record['num_decimals2']),
];
$get_chart['ugh']['formatted_submission1'] = $cs->FormatSubmission($get_chart['ugh']['submission1'], $record['chart_type'],  $record['num_decimals'],  $record['game_id'], $record['decimals']);
$get_chart['ugh']['formatted_submission2'] = $cs->FormatSubmission($get_chart['ugh']['submission2'], $record['chart_type2'], $record['num_decimals2'], $record['game_id'], $record['decimals']);

$rank = Modifiers::ChartFlagColouration($record);
$awards = chart_awards(...Modifiers::ChartModifiers($record));

$ranks = database_fetch_all("
  SELECT submission, rank_name
  FROM game_ranks
  WHERE game_id = ?
  ORDER BY submission DESC
", [$record['game_id']]);
$ranks = index_by($ranks, 'submission', 'rank_name');

if ($current_user) {
  $user_friends = $cs->GetUserFriends($current_user['user_id']);
} else {
  $user_friends = [];
}

$record['entity_id1'] = $record['entity_id'];
$record['user'] = UsersRepository::get($record['user_id']);
$record['game'] = GamesRepository::get($record['game_id']);
$record['reporter'] = UsersRepository::get($record['rec_reporter']);
$record['chart'] = LevelsRepository::get($record['level_id']);
$record['record_note_mod'] = UsersRepository::get($record['record_note_mod_id']);

$proof = ProofManager::ProofDetails($record);

$chart_id = $record['level_id'];

[$game_id, $group_id, $chart_info, $entities, $platforms] = $cs->GetChartInfo($chart_id);

foreach ($entities as &$entity_set) {
  foreach ($entity_set as &$entity) {
    $entity['entity_name'] = $t->GetEntityName($entity['game_entity_id']);
  }
  unset($entity);
}
unset($entity_set);

$t->CacheGame($game_id);
$game_name = $t->GetGameName($game_id);
$group_name = $t->GetGroupName($group_id);
$chart_name = $t->GetChartName($chart_id);
$t->CacheEntityNames($game_id);

// Hardcoded date in below query is intentional
$record_history = database_get_all(database_select("
  SELECT
    record_history.*,
    levels.*,
    proofmod.username AS proofmod_username
  FROM record_history
  LEFT JOIN record_approvals ON record_approvals.record_id = record_history.record_id AND record_approvals.approval_date = record_history.update_date
  LEFT JOIN users proofmod ON proofmod.user_id = record_approvals.user_id
  LEFT JOIN records ON records.record_id = record_history.record_id
  LEFT JOIN levels USING (level_id)
  WHERE record_history.record_id = ? AND update_date != '2018-06-10 00:00:00'
  ORDER BY update_date DESC
", 's', [$record['record_id']]));

foreach ($record_history as &$entry) {
  $entry['update_type_label'] = $t->ReadUpdateType($entry['update_type']);
}
unset($entry);

$patch_list = $t->GetGamePatches($record['game']['game_patches']);

$inputs = [$cs->GetSubmissionInputs($chart_info['chart_type'], $record['submission'])];
if ($chart_info['chart_type2'] != 0) {
  $inputs[] = $cs->GetSubmissionInputs($chart_info['chart_type2'], $record['submission2']);
}

render_with('records/show', [
  'page_title' => t('record_title'),
  'game_id' => $game_id,
  'game_name' => $game_name,
  'group_id' => $group_id,
  'group_name' => $group_name,
  'chart_id' => $chart_id,
  'chart_info' => $chart_info,
  'chart_name' => $chart_name,
  'record' => $record,
  'proof' => $proof,
  'record_history' => $record_history,
  'platforms' => $platforms,
  'patches' => $patch_list,
  'entities' => $entities,
  'inputs' => $inputs,
  'g' => $get_chart,
  'awards' => $awards,
  'rank' => $rank,
  'ranks' => $ranks,
  'user_friends' => $user_friends,
]);
