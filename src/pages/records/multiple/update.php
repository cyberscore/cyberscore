<?php

Authorization::authorize('User');

[$game_id] = explode('-', $_GET['id']);

$game = GamesRepository::get($game_id);
if ($game === NULL) {
  $cs->LeavePage('/', 'Something went wrong: submitting to an unknown game');
}

$game_id = $game['game_id'];

$new_records = 0;
$updated_records = 0;
$unchanged_records = 0;

$queued_records = [];
foreach ($_POST['records'] as $level_id => $params) {
  $chart = LevelsRepository::get($level_id);

  if ($chart === null) {
    $cs->LeavePage('/', 'Something went wrong: submitting to an unknown chart');
  }

  if ($chart['game_id'] !== $game_id) {
    $cs->LeavePage('/', 'Something went wrong: submitting to a chart of another game');
  }

  $record = RecordsRepository::find_by([
    'level_id' => $level_id,
    'user_id' => $current_user['user_id'],
  ]);

  $rec_status = $record['rec_status'] ?? 0;
  if ($rec_status == 1 || $rec_status == 2 || $rec_status == 5 || $rec_status == 6) {
    $cs->LeavePage('/', 'Something went wrong: record under investigation');
  }

  $submission = $cs->ReadInput(
    $chart['chart_type'],
    $params['input1'] ?? '',
    $params['input2'] ?? '',
    $params['input3'] ?? '',
    $params['input4'] ?? '',
  );

  if ($chart['chart_type2'] != 0) {
    $submission2 = $cs->ReadInput(
      $chart['chart_type2'],
      $params['input5'] ?? '',
      $params['input6'] ?? '',
      $params['input7'] ?? '',
      $params['input8'] ?? '',
    );
  } else {
    $submission2 = NULL;
  }

  if ($submission !== NULL || $submission2 !== NULL) {
    $queued_records []= [
      'chart_id'    => $level_id,
      'user_id'     => $current_user['user_id'],
      'entity_id'   => ($params['entity_id1'] ?? null) ?: null,
      'entity_id2'  => ($params['entity_id2'] ?? null) ?: null,
      'entity_id3'  => ($params['entity_id3'] ?? null) ?: null,
      'entity_id4'  => ($params['entity_id4'] ?? null) ?: null,
      'entity_id5'  => ($params['entity_id5'] ?? null) ?: null,
      'entity_id6'  => ($params['entity_id6'] ?? null) ?: null,
      'platform_id' => $params['platform_id'] ?? null,
      'game_patch'  => $params['patch'] ?? null,
      'submission'  => $submission,
      'submission2' => $submission2,
      'extra1'      => ($params['extra1'] ?? null) == "" ? null : ($params['extra1'] ?? null),
      'extra2'      => ($params['extra2'] ?? null) == "" ? null : ($params['extra2'] ?? null),
      'extra3'      => ($params['extra3'] ?? null) == "" ? null : ($params['extra3'] ?? null),
      'comment'     => $params['comment'] ?? null,
      'created_at'  => database_now(),
    ];
  }
}

if (count($queued_records) > 100) {
  database_insert_all('queued_records', $queued_records);
  $cs->FlashSuccess("queued " . count($queued_records) . " records. These will all be submitted in a few minutes.");
} else {
  foreach ($queued_records as $queued_record) {
    [$result, $change, $record_id] = RecordManager::SubmitQueuedRecord($queued_record);

    if ($result == 'no_entity')           $cs->WriteNote(false, 'No entity was selected');
    else if ($result == 'error_entity')   $cs->WriteNote(false, 'There was an error with the entity');
    else if ($result == 'error_platform') $cs->WriteNote(false, 'There was an error with the platform');
    else {
      if ($change == 'add')            $new_records++;
      else if ($change == 'update')    $updated_records++;
      else if ($change == 'change')    $updated_records++;
      else if ($change == 'no change') $unchanged_recordss++;
    }
  }

  $cs->FlashSuccess("$new_records new records, $updated_records updated records.");
}

redirect_to("/game/$game_id");
