<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::number('game_id'),
  Params::array_of_strs('level_ids'),
]);

$game_id = $params['game_id'];

$group_ids = array_filter($params['level_ids'], fn($l) => str_starts_with($l, "g"));
$group_ids = array_map(fn($l) => intval(substr($l, 1)), $group_ids);

$level_ids = array_filter($params['level_ids'], fn($l) => str_starts_with($l, "l"));
$level_ids = array_map(fn($l) => intval(substr($l, 1)), $level_ids);

$level_group_ids = index_by(LevelsRepository::where(['level_id' => $level_ids]), 'level_id', 'group_id');

$level_ids = array_filter($level_ids, fn($id) => !in_array($level_group_ids[$id], $group_ids));

$group_ids = implode(",", $group_ids);
$level_ids = implode(",", $level_ids);

$id = "$game_id-$group_ids-$level_ids";

$cs->LeavePage("/records/multiple/$id/edit");
