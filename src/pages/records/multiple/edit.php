<?php

Authorization::authorize('User');

$id = $_GET['id'];
[$game_id, $group_ids, $chart_ids] = explode("-", $id);

$group_ids = array_filter(explode(",", $group_ids), fn($gid) => $gid != "");
$chart_ids = array_filter(explode(",", $chart_ids), fn($cid) => $cid != "");

$game = GamesRepository::get($game_id);

$charts = LevelsRepository::where(
  database_or(['level_id' => $chart_ids], ['group_id' => $group_ids]),
  ['group'],
);

if (empty($charts)) {
  HTTPResponse::LeavePage("/records/multiple/new?game_id=$game_id", "You didn't select any charts!");
}

if (count($charts) > 1000) {
  HTTPResponse::LeavePage(
    "/records/multiple/new?game_id=$game_id",
    "Please pick 1000 or less charts to edit at once."
  );
}

usort($charts, function($a, $b) {
  if ($a['group_id'] != $b['group_id']) {
    return $a['group']['group_pos'] <=> $b['group']['group_pos'];
  }

  return $a['level_pos'] <=> $b['level_pos'];
});

$game_platforms = $cs->GetGamePlatforms($game_id);

$patch_list = $t->GetGamePatches(database_single_value("
  SELECT game_patches FROM games WHERE game_id = ?
", 's', [$game['game_id']]));

$ranks = database_get_all(database_select("
  SELECT submission, rank_name
  FROM game_ranks
  WHERE game_id = ?
  ORDER BY submission DESC
", 'i', [$game_id]));

$ranks = index_by($ranks, 'submission', 'rank_name');

$t->CacheGame($game_id);
$t->CacheEntityNames($game_id);

$max_entities = max(array_map(fn ($c) => $c['max_entities'], $charts));
$name1 = in_array(true, array_map(fn ($c) => $c['name1'] !== '', $charts));
$name2 = in_array(true, array_map(fn ($c) => $c['name2'] !== '', $charts));
$name3 = in_array(true, array_map(fn ($c) => $c['name3'] !== '', $charts));
$name_pri = in_array(true, array_map(fn ($c) => $c['name_pri'] !== '', $charts));
$name_sec = in_array(true, array_map(fn ($c) => $c['name_sec'] !== '', $charts));
$players = in_array(true, array_map(fn ($c) => $c['players'] !== '', $charts));
$score2 = in_array(true, array_map(fn ($c) => $c['conversion_factor'] == 0 && $c['chart_type2'] != 0, $charts));

foreach ($charts as &$chart) {
  $chart['group']['color'] = Modifiers::GroupColouration($chart['group']['ranked']);
  $chart['entities'] = CSClass::FetchChartEntities($chart);

  foreach ($chart['entities'] as &$entity_group) {
    foreach ($entity_group as &$entity) {
      $entity['name'] = $t->GetEntityName($entity['game_entity_id']);
    }
    unset($entity);
  }
  unset($entity_group);

  $chart['record'] = CSClass::GetRecordInputs($chart['level_id'], $user_id, $chart['chart_type'], $chart['chart_type2']);
  $chart['record'] = RecordsRepository::find_by(['user_id' => $current_user['user_id'], 'level_id' => $chart['level_id']]);
  $chart['inputs'] = [];
  if ($chart['record']) {
    $chart['inputs'][] = CSClass::GetSubmissionInputs($chart['chart_type'], $chart['record']['submission']);
    if ($chart['chart_type2'] != 0) {
      $chart['inputs'][] = CSClass::GetSubmissionInputs($chart['chart_type2'], $chart['record']['submission2']);
    }
  }
  $chart['default_platform'] = CSClass::FetchDefaultPlatform($current_user['user_id'], $chart['game_id']);
}
unset($chart);
$charts = array_filter($charts, fn($chart) => !$chart['record'] || !in_array($chart['record']['rec_status'], [1, 2, 5, 6]));
$charts_by_group_id = group_by($charts, fn($c) => $c['group_id']);

$game_entities = [];
foreach (range(1, $max_entities) as $i) {
  $game_entities[$i] = [];

  $seen = [];
  foreach ($charts as $chart) {
    foreach ($chart['entities'][$i] as $entity) {
      if (!in_array($entity['game_entity_id'], $seen)) {
        $game_entities[$i] []= $entity;
        $seen [] = $entity['game_entity_id'];
      }
    }
  }
}

render_with("records/multiple/edit", [
  'page_title' => 'Edit multiple records',
  'game' => $game,
  'id' => $id,
  'score2' => $score2,
  'name1' => $name1,
  'name2' => $name2,
  'name3' => $name3,
  'max_entities' => $max_entities,
  'game_platforms' => $game_platforms,
  'game_entities' => $game_entities,
  'patches' => $patch_list,
  'charts_by_group_id' => $charts_by_group_id,
  'ranks' => $ranks,
]);
