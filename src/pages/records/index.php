<?php

Authorization::authorize('Guest');

$params = Params::ParseGet([
  'user_id'      => Params::integer()->default($current_user['user_id'] ?? null),
  'sort'         => Params::integer()->default(7),
  'platinums'    => Params::boolean()->default(false),
  'proof_type'   => Params::enum(['all', 'pictures', 'videos'])->default('all'),
  'chartstatus'  => Params::enum(array_merge(array_map(fn($x) => $x[0], Modifiers::$chart_flags), ['all', 'all_ranked']))->default('all'),
  'rec_status'   => Params::integer()->default(null)->nullable(),
  'games'        => Params::array(Params::integer())->separator(",")->default([]),
  'filter_comp'  => Params::enum(['b', 'e', 'w', 'be', 'we'])->default('b'),
  'filter_field' => Params::enum(['csp', 'ucsp', 'chart_pos', 'chart_subs'])->default('chart_pos'),
  'filter_value' => Params::float()->default(null)->nullable(),
]);

$user = UsersRepository::get($params['user_id']);
if (!$user) {
  HTTPResponse::LeavePage('/', 'User not found.');
}

$filters = ["1=1"];
$title = [];

if ($params['chartstatus'] === 'all_ranked') {
  $filters []= "NOT chart_modifiers2.unranked";
} else if ($params['chartstatus'] !== 'all') {
  $filters []= "chart_modifiers2.{$params['chartstatus']}";
}

if ($params['rec_status'] !== null) {
  $status = [
    0 => [[0,1,2], "unproven records"],
    1 => [[1,5], "records pending investigation"],
    2 => [[2,6], "records under investigation"],
    3 => [[3], "approved records"],
    4 => [[4,5,6], "records pending approval"],
  ];

  $filters []= "records.rec_status IN (" . implode(",", $status[$params['rec_status']][0]) . ")";
  $title []= $status[$params['rec_status']][1];
} else {
  $title []= "records";
}

if ($params['filter_value'] !== null) {
  $filter_sign = [
    'b' => '>',
    'w' => '<',
    'e' => '=',
    'be' => '>=',
    'we' => '<=',
  ][$params['filter_comp']];

  $filters []= "records.{$params['filter_field']} {$filter_sign} {$params['filter_value']}";
}

$sort_order = [
  1 => "records.chart_pos ASC, records.platinum DESC, records.csp DESC",
  2 => "records.chart_pos DESC, records.platinum ASC, records.csp ASC",
  3 => "records.csp ASC",
  4 => "records.csp DESC",
  5 => "records.last_update ASC",
  6 => "records.last_update DESC",
  7 => "games.game_name ASC, level_groups.group_pos ASC, levels.level_pos ASC",
  8 => "games.game_name DESC, level_groups.group_pos DESC, levels.level_pos DESC",
  9 => "records.ucsp DESC",
  10 => "records.ucsp ASC",
][$params['sort']];

switch ($params['proof_type']) {
case "all":
  break;
case "pictures":
  $filters []= "records.proof_type = 'p' AND records.rec_status = 3";
  $title []= 'filtered by approved photo proofs';
  break;
case "videos":
  $filters []= "(records.proof_type = 'v' OR records.proof_type = 's' OR records.proof_type = 'r') AND records.rec_status = 3";
  $title []= 'filtered by approved video/livestream/replay proofs';
  break;
}

if ($params['platinums']) {
  $filters []= "records.platinum = 1";
  $title []= "awarding platinum";
}

if (!empty($params['games'])) {
  $game_filter = implode(",", $params['games']);
  $filters []= "records.game_id IN ($game_filter)";

  if (count($params['games']) == 1) {
    $title []= 'for ' . $t->GetGameName($params['games'][0]);
  } else {
    $title []= 'for ' . count($params['games']) . ' selected games';
  }
}

$query_filter = implode(" AND ", $filters);
$records = database_fetch_all("
    SELECT
        records.*,
        chart_modifiers2.*,
        levels.*,
        MAX(top_records.submission) as top_submission,
        MAX(top_records.submission2) as top_submission2
    FROM records
    JOIN games USING(game_id)
    JOIN levels USING(level_id)
    JOIN chart_modifiers2 USING(level_id)
    LEFT JOIN level_groups USING(group_id)
    LEFT JOIN records AS top_records ON (records.level_id = top_records.level_id AND top_records.chart_pos = 1)
    WHERE records.user_id = ? AND $query_filter
    GROUP BY records.record_id
    ORDER BY $sort_order
", [$user['user_id']]);

$t->CacheRecords($records);

foreach ($records as &$record) {
  $record['awards'] = chart_awards(...Modifiers::ChartModifiers($record));
  $record['rank'] = Modifiers::ChartFlagColouration($record);
  $record['proof_details'] = ProofManager::ProofDetails($record);
  $record['proof_type'] = $cs->ReadProofType($record['proof_type']);
}
unset($record);

$title = $user['username'] . "'s " . count($records) . " " . implode(' ', $title);

render_with('records/index', [
  'page_title' => $title,
  'title' => $title,
  'user' => $user,
  'records' => $records,
]);
