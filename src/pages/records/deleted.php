<?php

Authorization::authorize('UserMod');

$params = Params::ParseGet([
  'earliest' => Params::datetime()->default('2012-05-31T11:37:00'),
  'latest'   => Params::datetime()->default(date('Y-m-d\TH:i:s', strtotime('Tomorrow'))),
  'offset'   => Params::integer()->default(0),
  'game_ids' => Params::array(Params::integer())->separator(',')->default([]),
  'submitter' => Params::string()->empty_string_is_null()->default(null)->nullable(),
  'moderator' => Params::string()->empty_string_is_null()->default(null)->nullable(),
]);

// Fetch the actual deleted records that match our filters

$conditions = [];
if (isset($params['submitter'])) { $conditions['submitter.username'] = $params['submitter']; }
if (isset($params['moderator'])) { $conditions['deleter.username'] = $params['moderator']; }
if (count($params['game_ids']) > 0) { $conditions ["records_del.game_id"] = $params['game_ids']; }
[$where_sql, $where_binds] = database_where_sql_bind($conditions);

$deleted_recs = database_fetch_all("
  SELECT
    records_del.*,
    records_del_info.deleter_id,
    records_del_info.delete_date,
    submitter.username AS submitter,
    deleter.username AS deleter,
    reporter.username AS reporter,
    games.game_name
  FROM records_del
  LEFT JOIN games USING (game_id)
  LEFT JOIN users AS submitter ON records_del.user_id = submitter.user_id
  LEFT JOIN users AS reporter ON records_del.rec_reporter = reporter.user_id
  LEFT JOIN records_del_info ON records_del.record_id = records_del_info.record_id
  LEFT JOIN users AS deleter ON records_del_info.deleter_id = deleter.user_id
  WHERE delete_date BETWEEN ? AND ? AND $where_sql
  GROUP BY records_del.record_id
  ORDER by records_del_info.delete_date DESC, submitter.username
  LIMIT 100
  OFFSET {$params['offset']}
", array_merge([$params['earliest'], $params['latest']], $where_binds));

// Fetch additional information about the chart that the deleted record used to exist on,
// in case we need to reference that information or reinstate the record.
// It's possible that the record came from a chart that was deleted.
// Where possible, let's fetch information about that too.
// We want to also fetch information about the moderator who removed the record
DeletedRecordsRepository::eager_load($deleted_recs, [
  'record_note_mod',
  'chart' => ['group'],
  'deleted_chart' => ['group'],
  'investigation_task' => ['user'],
]);

// This query is separate from the larger data-pull in order to enable pagination
$num_records = database_value("
  SELECT COUNT(*)
  FROM records_del
  LEFT JOIN users AS submitter ON records_del.user_id = submitter.user_id
  LEFT JOIN records_del_info ON records_del.record_id = records_del_info.record_id
  LEFT JOIN users AS deleter ON records_del_info.deleter_id = deleter.user_id
  WHERE delete_date BETWEEN ? AND ? AND $where_sql
", array_merge([$params['earliest'], $params['latest']], $where_binds));

// Render the page
render_with('records/deleted', [
  'page_title' => 'Deleted records overview',
  'earliest' => $params['earliest'],
  'latest' => $params['latest'],
  'offset' => $params['offset'],
  'submitter' => $params['submitter'],
  'moderator' => $params['moderator'],
  'game' => implode(",", $params['game_ids']),
  'num_records' => $num_records,
  'deleted_records' => $deleted_recs,
]);
