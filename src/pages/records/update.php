<?php

Authorization::authorize('UserMod');

$record = RecordsRepository::get($_GET['id']);
$chart = LevelsRepository::get($record['level_id']);

if (!$record || !$chart) {
  $cs->PageNotFound();
}

// So many features, but most charts don't have them, so avoid php errors
$input1 = $_POST['input1'] ?? "";
$input2 = $_POST['input2'] ?? "";
$input3 = $_POST['input3'] ?? "";
$input4 = $_POST['input4'] ?? "";
$input5 = $_POST['input5'] ?? "";
$input6 = $_POST['input6'] ?? "";
$input7 = $_POST['input7'] ?? "";
$input8 = $_POST['input8'] ?? "";

$entity_id1 = $_POST['entity_id1'] ?? 0;
$entity_id2 = $_POST['entity_id2'] ?? 0;
$entity_id3 = $_POST['entity_id3'] ?? 0;
$entity_id4 = $_POST['entity_id4'] ?? 0;
$entity_id5 = $_POST['entity_id5'] ?? 0;
$entity_id6 = $_POST['entity_id6'] ?? 0;

$extra1 = $_POST['extra1'] ?? NULL;
$extra2 = $_POST['extra2'] ?? NULL;
$extra3 = $_POST['extra3'] ?? NULL;

$submission = $cs->ReadInput($chart['chart_type'], $input1, $input2, $input3, $input4);

if ($chart['chart_type2'] != 0) {
  $submission2 = $cs->ReadInput($chart['$chart_type2'], $input5, $input6, $input7, $input8);
} else {
  $submission2 = NULL;
}

if ($submission === NULL && $submission2 === NULL) {
  $cs->WriteNote(false, 'Empty or invalid score information was submitted. Please use only numbers and decimal points.');
} else {
  $result = RecordManager::ModEditRecord(
    $record['level_id'],
    $record['record_id'],
    [$entity_id1, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6],
    $_POST['platform_id'],
    $submission,
    $submission2,
    $_POST['comment'],
    $extra1, $extra2, $extra3,
    $_POST['linked_proof'],
    $_POST['game_patch']
  );

  if ($result == 'update') {
    $cs->WriteNote(true, 'Record updated');
  } else if ($result == 'change') {
    $cs->WriteNote(true, 'Submission info updated');
  }
}

redirect_to_back();
