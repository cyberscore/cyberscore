<?php

Authorization::authorize('User');

$params = filter_post_params([
  Params::number('level_id'), # level/chart id

  Params::str('other_user'), # for multiplayer charts
  Params::number('platform_id'),
  Params::str('comment'),
  Params::str('game_patch'),
  Params::str('proof'), # linked proof. proof_file is uploaded file

  Params::number('entity_id1'),
  Params::number('entity_id2'),
  Params::number('entity_id3'),
  Params::number('entity_id4'),
  Params::number('entity_id5'),
  Params::number('entity_id6'),
  Params::number('extra1'),
  Params::number('extra2'),
  Params::number('extra3'),

  Params::number('input1'),
  Params::number('input2'),
  Params::number('input3'),
  Params::str('input4'),
  Params::number('input5'),
  Params::number('input6'),
  Params::number('input7'),
  Params::str('input8'),
]);

$chart = LevelsRepository::get($params['level_id']);

$record = RecordsRepository::find_by([
  'level_id' => $params['level_id'],
  'user_id' => $current_user['user_id'],
]);

$rec_status = $record['rec_status'] ?? null;

if ($rec_status != 1 && $rec_status != 2 && $rec_status != 5 && $rec_status != 6) {
  $game_id     = $chart['game_id'];
  $players     = $chart['players'];
  $chart_type  = $chart['chart_type'];
  $chart_type2 = $chart['chart_type2'];

  // So many features, but most charts don’t have them, so avoid PHP errors
  $submission = $cs->ReadInput(
    $chart_type,
    $params['input1'] ?? null,
    $params['input2'] ?? null,
    $params['input3'] ?? null,
    $params['input4'] ?? null);

  if ($chart_type2 != 0) {
    $submission2 = $cs->ReadInput(
      $chart_type2,
      $params['input5'] ?? null,
      $params['input6'] ?? null,
      $params['input7'] ?? null,
      $params['input8'] ?? null);
  } else {
    $submission2 = NULL;
  }

  if ($submission === NULL && $submission2 === NULL) {
    $cs->WriteNote(false, 'Empty or invalid score information was submitted. Please use only numbers and decimal points.');
  } else {
    [$result, $change, $record_id] = RecordManager::SubmitRecord(
      $chart,
      $record,
      $params['entity_id1'] ?? null,
      $params['entity_id2'] ?? null,
      $params['entity_id3'] ?? null,
      $params['entity_id4'] ?? null,
      $params['entity_id5'] ?? null,
      $params['entity_id6'] ?? null,
      $params['platform_id'],
      $submission,
      $submission2,
      $params['comment'],
      $current_user['user_id'],
      $params['other_user'] ?? '',
      $params['extra1'] ?? null,
      $params['extra2'] ?? null,
      $params['extra3'] ?? null,
      $params['game_patch'] ?? '',
    );

    if ($result == 'no_entity') {
      $cs->WriteNote(false, 'No entity was selected');
    } else if ($result == 'error_entity') {
      $cs->WriteNote(false, 'There was an error with the entity');
    } else if ($result == 'error_platform') {
      $cs->WriteNote(false, 'There was an error with the platform. You may not have selected one.');
    } else {
      if ($change == 'add') {
        $cs->WriteNote(true, 'Record successfully submitted');
      } else if ($change == 'update') {
        $cs->WriteNote(true, 'Record updated');
      } else if ($change == 'change') {
        $cs->WriteNote(true, 'Submission info updated');
      }
    }
  }
}

$record = RecordsRepository::find_by([
  'level_id' => $params['level_id'],
  'user_id' => $current_user['user_id'],
]);

if ($record == null) {
  $cs->WriteNote(false, 'Record not found');
  $cs->RedirectToPreviousPage();
}

if (is_uploaded_file($_FILES['proof_file']['tmp_name'])) {
  $result = ProofManager::ProcessUploadedProof($record['record_id'], $_FILES['proof_file']);

  // We need to queue the game for rebuild on proof submission because otherwise bonus CSR can be wrong if 
  // the record itself wasn't edited but the user came back to add proof.
  GameCacheRebuilder::QueueGameForRebuild($game_id);

  switch ($result['status']) {
  case 'error_noupload':
    $cs->WriteNote(false, "You didn't specify a file to upload!");
    break;
  case 'error_not_image':
    $cs->WriteNote(false, 'The uploaded file could not be loaded for processing. Please ensure you use a JPEG, PNG or GIF.');
    break;
  case 'error_size':
    $cs->WriteNote(false, 'The uploaded file is too big. Pictures must have a size of 6 megabytes or lower.');
    break;
  case 'error_copy':
    $cs->WriteNote(false, 'Upload failed.');
    break;
  case 'success';
    $cs->WriteNote(true, 'Proof uploaded.', "<img src='" . h($result['link']) . "' style='height: 200px;' />");
    break;
  }
} else if ($params['proof'] != NULL) {
  $result = ProofManager::ProcessLinkedProof($record['record_id'], $params['proof']);

  // We need to queue the game for rebuild on proof submission because otherwise bonus CSR can be wrong if 
  // the record itself wasn't edited but the user came back to add proof.
  GameCacheRebuilder::QueueGameForRebuild($game_id);

  if ($result['status'] == 'success') {
    if ($result['action'] == 'linked') {
      $cs->WriteNote(true, 'Proof has been linked. If this isn’t a video, please link to the image directly instead.');
    } else if ($result['action'] == 'stored') {
      $cs->WriteNote(true, 'Proof copied and uploaded.');
    }
  } else {
    $cs->WriteNote(false, 'There was an error with this proof. Please make sure to link to the image directly, including "https://".');
  }
}

$cs->RedirectToPreviousPage();
