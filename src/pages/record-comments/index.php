<?php

Authorization::authorize('UserMod');

if (!empty($_GET['comment_filter'])) {
  $comment_filter = strtolower($_GET['comment_filter']);
  $query_extra = "";
} else {
  $comment_filter = null;
  $query_extra = "LIMIT 100";
}

$comment_count = database_single_value("SELECT COUNT(1) FROM records WHERE comment != ''", '', []);

$comments = database_get_all(database_select("
  SELECT
    users.username,
    records.game_id,
    records.level_id,
    records.record_id,
    records.user_id,
    records.chart_pos,
    records.chart_subs,
    records.submission,
    records.submission2,
    records.comment,
    levels.num_decimals,
    levels.score_prefix,
    levels.score_suffix,
    levels.num_decimals2,
    levels.score_prefix2,
    levels.score_suffix2,
    levels.group_id,
    levels.chart_type,
    levels.chart_type2,
    levels.decimals
  FROM records
  JOIN users USING(user_id)
  JOIN levels USING(level_id)
  WHERE records.comment != ''
  ORDER BY records.last_update DESC
  $query_extra
", '', []));

// TODO: filter in SQL instead
$comments = array_filter($comments, fn($c) => $comment_filter == null || substr_count(strtolower($c['comment']), $comment_filter));

render_with('record-comments/index', [
  'page_title' => 'Comment monitor',
  'comments' => $comments,
  'comment_count' => $comment_count,
]);
