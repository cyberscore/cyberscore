<?php

Authorization::authorize('GameMod');

// TODO: automatic "Separate charts" must go.
// We need to explicitly create a group_id for them.
if ($_GET['group_id'] == 0) {
  $game = GamesRepository::get($_GET['game_id']);
  $group = ['group_id' => 0, 'game_id' => $game['game_id']];
} else {
  $group = GroupsRepository::get($_GET['group_id']);
  $game = GamesRepository::get($group['game_id']);
}

$group['name'] = $t->GetGroupName($group['group_id']);

$chart_quick_selections = database_fetch_all("
  SELECT COUNT(1) AS count, MAX(level_id) AS level_id,
  players, standard, speedrun, solution, arcade, challenge, incremental, collectible, unranked,
  level_rules, name1, name2, name3,
  gameplay_differences_device, gameplay_differences_game_version,
  premium_upgrades_and_consumables_earnable, premium_upgrades_and_consumables_premium_only, premium_upgrades_and_consumables_inaccessible, premium_upgrades_and_consumables_conditionally_accessible,
  accessibility_issues_inaccessible, accessibility_issues_conditionally_accessible, accessibility_issues_geographical,
  full_game, part_game, body_of_work,
  chart_type, num_decimals, score_prefix, name_pri, score_suffix,
  chart_type2, num_decimals2, score_prefix2, name_sec, score_suffix2, conversion_factor
  FROM levels
  JOIN chart_modifiers2 USING (level_id)
  WHERE game_id = ? AND group_id = ?
  GROUP BY
  players, standard, speedrun, solution, arcade, challenge, incremental, collectible, unranked,
  level_rules, name1, name2, name3,
  gameplay_differences_device, gameplay_differences_game_version,
  premium_upgrades_and_consumables_earnable, premium_upgrades_and_consumables_premium_only, premium_upgrades_and_consumables_inaccessible, premium_upgrades_and_consumables_conditionally_accessible,
  accessibility_issues_inaccessible, accessibility_issues_conditionally_accessible, accessibility_issues_geographical,
  full_game, part_game, body_of_work,
  chart_type, num_decimals, score_prefix, name_pri, score_suffix,
  chart_type2, num_decimals2, score_prefix2, name_sec, score_suffix2, conversion_factor
  ORDER BY 1 DESC
  LIMIT 10
", [$group['game_id'], $group['group_id']]);

$empty_group = empty($chart_quick_selections);

if (empty($chart_quick_selections)) {
  $chart_quick_selections = database_fetch_all("
    SELECT COUNT(1) AS count, MAX(level_id) AS level_id,
    players, standard, speedrun, solution, arcade, challenge, incremental, collectible, unranked,
    level_rules, name1, name2, name3,
    gameplay_differences_device, gameplay_differences_game_version,
    premium_upgrades_and_consumables_earnable, premium_upgrades_and_consumables_premium_only, premium_upgrades_and_consumables_inaccessible, premium_upgrades_and_consumables_conditionally_accessible,
    accessibility_issues_inaccessible, accessibility_issues_conditionally_accessible, accessibility_issues_geographical,
    full_game, part_game, body_of_work,
    chart_type, num_decimals, score_prefix, name_pri, score_suffix,
    chart_type2, num_decimals2, score_prefix2, name_sec, score_suffix2, conversion_factor
    FROM levels
    JOIN chart_modifiers2 USING (level_id)
    WHERE game_id = ?
    GROUP BY
    players, standard, speedrun, solution, arcade, challenge, incremental, collectible, unranked,
    level_rules, name1, name2, name3,
    gameplay_differences_device, gameplay_differences_game_version,
    premium_upgrades_and_consumables_earnable, premium_upgrades_and_consumables_premium_only, premium_upgrades_and_consumables_inaccessible, premium_upgrades_and_consumables_conditionally_accessible,
    accessibility_issues_inaccessible, accessibility_issues_conditionally_accessible, accessibility_issues_geographical,
    full_game, part_game, body_of_work,
    chart_type, num_decimals, score_prefix, name_pri, score_suffix,
    chart_type2, num_decimals2, score_prefix2, name_sec, score_suffix2, conversion_factor
    ORDER BY 1 DESC
    LIMIT 10
  ", [$group['game_id']]);
}

foreach ($chart_quick_selections as &$selection) {
  $selection['level_rules'] = $t->GetRules($selection['level_rules']);
}
unset($selection);

render_with('game-group-charts/new', [
  'page_title' => 'Add Charts',
  'group' => $group,
  'game' => $game,

  'chart_flags' => Modifiers::$chart_flags,
  'csp_modifiers' => Modifiers::$csp_modifiers,
  'chart_quick_selections' => $chart_quick_selections,
  'empty_group' => $empty_group,
]);
