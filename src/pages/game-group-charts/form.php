<?php

Authorization::authorize('GameMod');

$selection = database_fetch_one("
  SELECT *
  FROM levels
  JOIN chart_modifiers2 USING (level_id)
  WHERE level_id = ?
", [$_GET['id']]);

$selection['level_rules'] = $t->GetRules($selection['level_rules']);

display_component('game-group-charts/form', [
  'selection' => $selection,
]);
