<?php

Authorization::authorize('GameMod');

// TODO: automatic "Separate charts" must go.
// We need to explicitly create a group_id for them.
if ($_POST['group_id'] == 0) {
  $game = GamesRepository::get($_POST['game_id']);
  $group = ['group_id' => 0, 'game_id' => $game['game_id']];
} else {
  $group = GroupsRepository::get($_POST['group_id']);
  $game = GamesRepository::get($group['game_id']);
}

$params = filter_post_params(array_merge(
  [
    Params::str('level_names'),
    Params::number('players'),
    Params::number('chart_type'),
    Params::number('num_decimals'),
    Params::str('name_pri'),
    Params::str('score_prefix'),
    Params::str('score_suffix'),

    Params::number('chart_type2'),
    Params::number('num_decimals2'),
    Params::str('name_sec'),
    Params::str('score_prefix2'),
    Params::str('score_suffix2'),

    Params::number('conversion_factor'),
    Params::str('level_rules'),
    Params::str('name1'),
    Params::str('name2'),
    Params::str('name3'),
  ],
  array_map(fn ($chart_flag) => Params::checkbox("flag_{$chart_flag[0]}", true, false), Modifiers::$chart_flags),
  array_map(fn ($csp_modifier) => Params::checkbox("csp_modifier_{$csp_modifier[0]}", true, false), Modifiers::$csp_modifiers),
));

$level_pos = database_single_value("SELECT 1 + MAX(level_pos) FROM levels WHERE game_id = ? AND group_id = ?", 'ss', [$group['game_id'], $group['group_id']]);

if ($params['chart_type2'] == 0) {
  $params['num_decimals2'] = -1;
}

if (empty($params['conversion_factor']) || $params['chart_type2'] == 0) {
  $params['conversion_factor'] = 0;
}

$level_names = array_filter(array_map(fn($name) => trim($name), explode("\n", trim($params['level_names']))), fn($name) => $name != '');
foreach ($level_names as $i => $level_name) {
  $level_id = database_insert('levels', [
    'game_id' => $group['game_id'],
    'group_id' => $group['group_id'],
    'level_name' => $level_name,
    'level_rules' => $params['level_rules'],
    'chart_type' => $params['chart_type'],
    'num_decimals' => $params['num_decimals'],
    'score_prefix' => $params['score_prefix'],
    'score_suffix' => $params['score_suffix'],
    'chart_type2' => $params['chart_type2'],
    'num_decimals2' => $params['num_decimals2'],
    'score_prefix2' => $params['score_prefix2'],
    'score_suffix2' => $params['score_suffix2'],
    'conversion_factor' => $params['conversion_factor'],
    'entity_lock' => '',
    'players' => $params['players'],
    'name_pri' => $params['name_pri'],
    'name_sec' => $params['name_sec'],
    'name1' => $params['name1'],
    'name2' => $params['name2'],
    'name3' => $params['name3'],
    'level_pos' => $level_pos + $i,
    'ranked' => 1,
    'dlc' => 0,
  ]);

  $chart_modifier = ['level_id' => $level_id];

  foreach (Modifiers::$chart_flags as [$chart_flag,]) {
    $chart_modifier[$chart_flag] = $params["flag_{$chart_flag}"] ?? false;
  }

  foreach (Modifiers::$csp_modifiers as [$csp_modifier,]) {
    $chart_modifier[$csp_modifier] = $params["csp_modifier_{$csp_modifier}"] ?? false;
  }

  // TODO: enforce validation to avoid javascript shenanigans
  database_insert('chart_modifiers2', $chart_modifier);
}

Modifiers::UpdateChartFlooder($group['game_id']);
ChartCacheRebuilder::QueueGameChartsForRebuild($group['game_id']);
GameCacheRebuilder::QueueGameForRebuild($group['game_id']);

$cs->WriteNote(true, count($level_names) . " chart(s) added");
redirect_to("/mod_scripts/edit_game_groups.php?game_id={$group['game_id']}");
