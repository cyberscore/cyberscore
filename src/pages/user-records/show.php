<?php

Authorization::authorize('UserMod');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

$filters = ['records.user_id = ?'];
if (isset($_GET['game_ids'])) {
  $game_ids = implode(", ", array_map('intval', $_GET['game_ids']));

  $filters []= "games.game_id IN ($game_ids) ";
}

if (isset($_GET['unproven'])) {
  $filters []= 'rec_status = 0';
}

switch ($_GET['sort'] ?? 'default') {
case 'newest':
  $sort = "last_update DESC, rec_status DESC, game_name ASC, group_pos ASC, level_pos ASC";
  break;
case 'oldest':
  $sort = "last_update ASC, rec_status DESC, game_name ASC, group_pos ASC, level_pos ASC";
  break;
default:
  $sort = "rec_status DESC, game_name ASC, group_pos ASC, level_pos ASC";
  break;
}

$filters = implode(' AND ', $filters);
$records = database_get_all(database_select("
  SELECT
    records.*,
    levels.*,
    level_groups.group_name,
    moderators.username AS moderator_username
  FROM records
  JOIN games USING (game_id)
  JOIN levels USING (level_id)
  LEFT JOIN level_groups USING(group_id)
  LEFT JOIN staff_tasks ON tasks_type = 'record_investigated' AND task_id = records.record_id
  LEFT JOIN users AS moderators ON moderators.user_id = staff_tasks.user_id
  WHERE $filters
  ORDER BY $sort
", 's', [$user['user_id']]));

foreach ($records as &$record) {
  switch ($record['rec_status']) {
  case 1: $record['status'] = "Awaiting action"; break;
  case 5: $record['status'] = "Awaiting action, proof submitted"; break;
  case 2: $record['status'] = "Under investigation"; break;
  case 6: $record['status'] = "Under investigation, proof submitted"; break;
  case 3: $record['status'] = "Approved!"; break;
  case 4: $record['status'] = "Pending approval"; break;
  case 0: $record['status'] = "Live on site"; break;
  }
}
unset($record);

$t->CacheGameNames();

render_with('user-records/show', [
  'page_title' => 'Edit Records',
  'records' => $records,
  'user' => $user,
]);
