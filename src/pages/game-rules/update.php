<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);

$new_rule_ids = $_POST['rule_ids'];

$rules_per_level = [];

foreach ($_POST['rules'] as $level_id => $checkboxes) {
  $rules_per_level[intval($level_id)] = [];

  foreach ($checkboxes as $rule_id => $value) {
    $rules_per_level[intval($level_id)] []= $rule_id;
  }
}

if (isset($_POST['new_rules'])) {
  foreach ($_POST['new_rules'] as $level_id => $checkboxes) {
    if (array_key_exists($level_id, $rules_per_level) == false) {
      $rules_per_level[intval($level_id)] = [];
    }

    foreach ($checkboxes as $new_rule_index => $value) {
      $rules_per_level[intval($level_id)] []= intval($new_rule_ids[$new_rule_index]);
    }
  }
}

$levels = database_get_all(database_select('SELECT level_id FROM levels WHERE game_id = ?', 'i', [$game_id]));

foreach ($levels as $level) {
  $rule_ids = implode(',', $rules_per_level[$level['level_id']] ?? []);

  database_update_by(
    'levels',
    ['level_rules' => $rule_ids],
    ['level_id' => $level['level_id']]
  );
}
if (isset($_POST['new_rules'])) {
  $cs->WriteNote(TRUE, "Rules successfully updated");
}

$cs->RedirectToPreviousPage();
