<?php

Authorization::authorize('UserMod');

$record_history_entry = database_find_by('record_history', ['history_id' => $_GET['id']]);

if (!$record_history_entry) {
  $cs->PageNotFound();
}

database_delete_by('record_history', ['history_id' => $record_history_entry['history_id']]);

redirect_to("/record/{$record_history_entry['record_id']}");
