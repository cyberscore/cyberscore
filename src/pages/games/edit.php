<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);
$game = GamesRepository::get($game_id);

if (!$game) {
  $cs->LeavePage('/games.php', 'The requested game does not exist.');
}

$game_name = $t->GetGameName($game_id);

$related_games = database_fetch_all("
  SELECT
    games.game_id,
    games.game_name,
    COALESCE(r1.super_related, r2.super_related) = FALSE AS related,
    COALESCE(r1.super_related, r2.super_related) = TRUE  AS super_related
  FROM games
  LEFT JOIN games_related r1 ON games.game_id = r1.game_id AND r1.related_game_id = ?
  LEFT JOIN games_related r2 ON games.game_id = r2.related_game_id AND r2.game_id = ?
  WHERE games.game_id != ?
  ORDER BY games.game_name ASC
", [$game_id, $game_id, $game_id]);

$patch_list = $t->GetGamePatches($game['game_patches']);

$platform_list = database_get_all(database_select("
  SELECT
    platforms.platform_id,
    platforms.platform_name,
    SUM(game_platforms.original = 1) AS platform_check1,
    SUM(game_platforms.original = 0) AS platform_check2
  FROM platforms
  LEFT JOIN game_platforms ON platforms.platform_id = game_platforms.platform_id
        AND game_platforms.game_id = ?
  GROUP BY platforms.platform_id
  ORDER BY platforms.platform_name ASC
", 's', [$game_id]));

render_with("games/edit", [
  'page_title' => "Edit Game",
  'game' => $game,
  'related_games' => $related_games,
  'patch_list' => $patch_list,
  'platform_list' => $platform_list,
]);
