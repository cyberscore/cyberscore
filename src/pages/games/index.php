<?php

Authorization::authorize('Guest');

global $t;
$t->CacheGameNames();

// Search defaults
$site_id     = $_GET['site_id'] ?? 'all';
$platform_id = $_GET['platform_id'] ?? 0;
$sort        = $_GET['sort'] ?? 'game_names';
$charttype   = $_GET['charttype'] ?? 'ALL';

$letter = $_GET['letter'] ?? '';
$sort_direction = $_GET['sort_direction'] ?? 'ASC';

$where = [];
$games_bind = [];
if ($site_id == 'mine') {
  $where[] = "EXISTS(SELECT * FROM records WHERE records.game_id = games.game_id AND records.user_id = ?)";
  $games_bind []= $current_user['user_id'];
} else {
  $where[] = "games.site_id != 4";
}

if ($platform_id > 0) {
  // TODO(wiki): this is a join in disguise
  $where[] = "? IN (
    SELECT game_platforms.platform_id
    FROM game_platforms
    WHERE game_platforms.game_id = games.game_id
  )";
  $games_bind []= $platform_id;
}

switch ($charttype) {
  case 1: $where []= "games.standard_charts > 0"; break;
  case 2: $where []= "games.arcade_charts > 0"; break;
  case 3: $where []= "games.speedrun_charts > 0"; break;
  case 6: $where []= "games.solution_charts > 0"; break;
  case 7: $where []= "games.challenge_charts > 0"; break;
  case 8: $where []= "games.collectible_charts > 0"; break;
  case 9: $where []= "games.incremental_charts > 0"; break;
  case "ALL": break;
}

$limit = "";
if ($sort == "recent" && $sort_direction == "DESC" && $site_id == "all") {
  // The hardcoded game id check is for proper display and handling of the
  // date of game publication (feature implemented on October 15 2018)

  // No games with a game ID lower than 2359 will show up on the latest games
  // applet (useful if we manually give older published games accurate
  // publication dates in the database)

  // Hardcoded game ID checks in $older_games_with_date_published are used to
  // exclude older, legitimate games in the workshop that may be published in
  // the future (and thus receive a date), from the above parameter

  // TODO(wiki): Most date_published entries are 0000-00-00 instead of NULL.
  // This makes things a bit ackward to use, and relies on strict mode.
  // We should remove the NOT NULL constraint and update the zeros to NULL.
  $older_games_with_date_published = implode(",", [
    1735, 1784, 1785, 1814, 1822, 1923,
    1924, 2052, 2102, 2171, 2346
  ]);

  $where []= "(games.game_id >= 2359 OR games.game_id IN ($older_games_with_date_published))";
}

switch ($sort) {
  case 'recent':
    if ($sort_direction == "DESC" && $site_id == "all") {
      $sorting = "games.date_published $sort_direction, game_id $sort_direction";
    } else {
      $sorting = "game_id $sort_direction";
    }
    break;
  case 'standard_charts':    $sorting = "standard_charts $sort_direction"; break;
  case 'standard_records':   $sorting = "standard_subs $sort_direction"; break;
  case 'standard_rpc':       $sorting = "standard_rpc $sort_direction"; break;
  case 'standard_players':   $sorting = "standard_players $sort_direction, standard_rpc $sort_direction"; break;
  case 'arcade_charts':    $sorting = "arcade_charts $sort_direction"; break;
  case 'arcade_records':   $sorting = "arcade_subs $sort_direction"; break;
  case 'arcade_rpc':       $sorting = "arcade_rpc $sort_direction"; break;
  case 'arcade_players':   $sorting = "arcade_players $sort_direction, arcade_rpc $sort_direction"; break;
  case 'speedrun_charts':  $sorting = "speedrun_charts $sort_direction"; break;
  case 'speedrun_records': $sorting = "speedrun_subs $sort_direction"; break;
  case 'speedrun_rpc':     $sorting = "speedrun_rpc $sort_direction"; break;
  case 'speedrun_players': $sorting = "speedrun_players $sort_direction, speedrun_rpc $sort_direction"; break;
  case 'solution_charts':  $sorting = "solution_charts $sort_direction"; break;
  case 'solution_records': $sorting = "solution_subs $sort_direction"; break;
  case 'solution_rpc':     $sorting = "solution_rpc $sort_direction"; break;
  case 'solution_players': $sorting = "solution_players $sort_direction, solution_rpc $sort_direction"; break;
  case 'challenge_players': $sorting = "challenge_players $sort_direction, challenge_rpc $sort_direction"; break;
  case 'challenge_charts':  $sorting = "challenge_charts $sort_direction"; break;
  case 'challenge_records': $sorting = "challenge_subs $sort_direction"; break;
  case 'challenge_rpc':     $sorting = "challenge_rpc $sort_direction"; break;
  case 'collectible_players': $sorting = "collectible_players $sort_direction, collectible_rpc $sort_direction"; break;
  case 'collectible_charts':  $sorting = "collectible_charts $sort_direction"; break;
  case 'collectible_records': $sorting = "collectible_subs $sort_direction"; break;
  case 'collectible_rpc':     $sorting = "collectible_rpc $sort_direction"; break;
  case 'incremental_charts':  $sorting = "incremental_charts $sort_direction"; break;
  case 'incremental_records': $sorting = "incremental_subs $sort_direction"; break;
  case 'incremental_rpc':     $sorting = "incremental_rpc $sort_direction"; break;
  case 'incremental_players': $sorting = "incremental_players $sort_direction, incremental_rpc $sort_direction"; break;
  case 'all_records':    $sorting = "all_subs $sort_direction"; break;
  case 'all_charts':     $sorting = "all_charts $sort_direction"; break;
  case 'all_rpc':        $sorting = "all_rpc $sort_direction"; break;
  case 'all_players':    $sorting = "all_players $sort_direction, all_rpc $sort_direction"; break;
  case 'csp_records':    $sorting = "csp_subs $sort_direction"; break;
  case 'csp_charts':     $sorting = "csp_charts $sort_direction"; break;
  case 'csp_rpc':        $sorting = "csp_rpc $sort_direction"; break;
  case 'csp_players':    $sorting = "csp_players $sort_direction, csp_rpc $sort_direction"; break;
  case 'unranked_records':    $sorting = "unranked_subs $sort_direction"; break;
  case 'unranked_charts':     $sorting = "unranked_charts $sort_direction"; break;
  case 'unranked_rpc':        $sorting = "unranked_rpc $sort_direction"; break;
  case 'unranked_players':    $sorting = "unranked_players $sort_direction, unranked_rpc $sort_direction"; break;
  case 'game_name':        $sorting = "game_name $sort_direction"; break;
  default:                 $sorting = "game_name $sort_direction"; break;
}

if ($platform_id != 0) {
  $platform_name = database_single_value("SELECT platform_name FROM platforms WHERE platform_id = ?", 's', [$platform_id]);
  $header = "Games on " . $platform_name;
} else {
  if ($site_id == 'mine') $header = "Games that you submitted to";
  else                      $header = "The complete list";
}

$link_extra = "&amp;site_id=$site_id&amp;platform_id=$platform_id&amp;letter=$letter";

$workshop_games = database_get_all(database_select("
  SELECT game_id, all_charts
  FROM games
  WHERE site_id = 4
  ORDER BY game_id DESC
", '', []));

$platforms = database_get_all(database_select("
  SELECT platforms.platform_id, platforms.platform_name
  FROM platforms
  JOIN game_platforms USING(platform_id)
  GROUP BY platforms.platform_id
  ORDER BY platforms.platform_name ASC
", '', []));

$where = implode(" AND ", $where);
$games = database_get_all(database_select("
  SELECT
    games.game_id,
    games.site_id,
    games.all_charts,
    games.all_subs,
    games.all_players,
    IF(games.all_charts = 0, 0, games.all_subs / games.all_charts) AS all_rpc,
    games.standard_charts,
    games.standard_subs,
    games.standard_players,
    IF(games.standard_charts = 0, 0, games.standard_subs / games.standard_charts) AS standard_rpc,
    games.csp_charts,
    games.csp_subs,
    games.csp_players,
    IF(games.csp_charts = 0, 0, games.csp_subs / games.csp_charts) AS csp_rpc,
    games.unranked_charts,
    games.unranked_subs,
    games.unranked_players,
    IF(games.unranked_charts = 0, 0, games.unranked_subs / games.unranked_charts) AS unranked_rpc,
    games.arcade_charts,
    games.arcade_subs,
    games.arcade_players,
    IF(games.arcade_charts = 0, 0, games.arcade_subs / games.arcade_charts) AS arcade_rpc,
    games.speedrun_charts,
    games.speedrun_subs,
    games.speedrun_players,
    IF(games.speedrun_charts = 0, 0, games.speedrun_subs / games.speedrun_charts) AS speedrun_rpc,
    games.solution_charts,
    games.solution_subs,
    games.solution_players,
    IF(games.solution_charts = 0, 0, games.solution_subs / games.solution_charts) AS solution_rpc,
    games.challenge_charts,
    games.challenge_subs,
    games.challenge_players,
    IF(games.challenge_charts = 0, 0, games.challenge_subs / games.challenge_charts) AS challenge_rpc,
    games.collectible_charts,
    games.collectible_subs,
    games.collectible_players,
    IF(games.collectible_charts = 0, 0, games.collectible_subs / games.collectible_charts) AS collectible_rpc,
    games.incremental_charts,
    games.incremental_subs,
    games.incremental_players,
    IF(games.incremental_charts = 0, 0, games.incremental_subs / games.incremental_charts) AS incremental_rpc,
    GROUP_CONCAT(platforms.platform_id, '_', platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ',') AS platforms
  FROM games
  JOIN game_platforms USING (game_id)
  JOIN platforms USING (platform_id)
  WHERE $where
  GROUP BY games.game_id
  ORDER BY $sorting
  $limit
", str_repeat("s", count($games_bind)), $games_bind));

function first_letter($name) {
  $first_letter = ucfirst(mb_substr($name, 0, 1));

  if ($first_letter == 'Ō') return 'O';
  if ($first_letter == 'フ') return 'F';
  if ($first_letter < 'A' || $first_letter > 'Z') return '0';
  return $first_letter;
}

if ($letter != "") {
  $games = array_filter($games, fn ($game) => first_letter($t->GetGameName($game['game_id'])) == $letter);
}

foreach ($games as &$game) {
  $game['platforms'] = array_map(
    fn($p) => array_combine(['platform_id', 'platform_name'], explode('_', $p)),
    explode(',', $game['platforms'])
  );
}
unset($game);

$selected_game_platforms = [];
foreach ($games as $game) {
  foreach ($game['platforms'] as $platform) {
    $selected_game_platforms []= $platform['platform_id'];
  }
}

$num_levels = array_sum(pluck($games, 'all_charts'));
$num_records = array_sum(pluck($games, 'all_subs'));
$num_games = count($games);

$columns = [
  ['name' => 'all',         'title' => 'Total', 'visible' => $charttype == 'ALL'],
  ['name' => 'csp',         'title' => 'CSP-Awarding', 'visible' => $charttype == 'ALL'],
  ['name' => 'standard',    'title' => 'Standard', 'visible' => $charttype == 'ALL' || $charttype == 1],
  ['name' => 'speedrun',    'title' => 'Speedrun', 'visible' => $charttype == 'ALL' || $charttype == 3],
  ['name' => 'arcade',      'title' => 'Arcade', 'visible' => $charttype == 'ALL' || $charttype == 2],
  ['name' => 'solution',    'title' => 'Solution', 'visible' => $charttype == 'ALL' || $charttype == 6],
  ['name' => 'challenge',   'title' => 'User Challenge', 'visible' => $charttype == 'ALL' || $charttype == 7],
  ['name' => 'collectible', 'title' => 'Collectible', 'visible' => $charttype == 'ALL' || $charttype == 8],
  ['name' => 'incremental', 'title' => 'Incremental', 'visible' => $charttype == 'ALL' || $charttype == 9],
  ['name' => 'unranked',    'title' => 'Zero-CSP', 'visible' => $charttype == 'ALL'],
];

render_with('games/index', [
  'page_title' => t('general_games'),
  'platforms' => $platforms,
  'games' => $games,
  'selected_game_platforms' => array_unique($selected_game_platforms),
  'workshop_games' => $workshop_games,
  'num_levels' => $num_levels,
  'num_records' => $num_records,
  'num_games' => $num_games,
  'filters' => [
    'platform_id' => $platform_id,
    'letter' => $letter,
    'chart_type' => $charttype,
    'sort_direction' => $sort_direction,
    'sort' => $sort,
    'site_id' => $site_id,
  ],
  'columns' => $columns,
  'header' => $header,
]);
