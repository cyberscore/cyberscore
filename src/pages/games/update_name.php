<?php

Authorization::authorize('GameMod');

$game_id = intval($_GET['id']);

$params = filter_post_params([
  Params::number('lang_id'),
  Params::str('game_name'),
]);

  $t->ChangeGameName($params['lang_id'], $game_id, $params['game_name']);
  $cs->WriteNote(true, $t->GetChanges());

$cs->RedirectToPreviousPage();
