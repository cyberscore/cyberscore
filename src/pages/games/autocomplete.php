<?php

if ($_GET['q'] != '') {
  $games = search($_GET['q'])[1];
} else {
  $games = [];
}

display_partial('games/autocomplete', [
  'games' => $games,
]);
