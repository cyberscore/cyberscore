<?php

Authorization::authorize('GameMod');

$params = Params::ParsePost([
  'game_name' => Params::string(['min-length' => 1]),
]);

$game_id = database_insert('games', [
  'site_id' => 4,
  'game_name' => $params['game_name'],
  'notes' => '',
  'date_published' => NULL,
  'solution_charts' => 0,
  'solution_subs' => 0,
  'solution_players' => 0,
  'mod_notes' => '',
  'game_patches' => '',
]);

database_insert('staff_tasks', [
  'user_id' => $current_user['user_id'],
  'tasks_type' => 'game_added',
  'task_id' => $game_id,
  'result' => 'Game added',
]);

Notification::NewGame($game_id, $current_user['user_id'])->DeliverToGroups(
  ['GameMod', 'GlobalMod', 'Admin', 'Designer'],
  ['except' => [$current_user['user_id']]]);

redirect_to("/games/$game_id/edit");
