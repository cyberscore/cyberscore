<?php

Authorization::authorize('UserMod');

$params = filter_post_params([
  Params::array_of_numbers('confirm_ids'),
  Params::array_of_numbers('remove_ids'),
  Params::array_of_numbers('unconfirm_ids'),
]);

if (empty($params['confirm_ids']) && empty($params['remove_ids']) && empty($params['unconfirm_ids'])) {
  $cs->WriteNote(false, 'No referrals selected');
} else if (count(array_intersect($params['confirm_ids'], $params['remove_ids'])) > 0) {
  $cs->WriteNote(false, 'Invalid selection. Please select only one option per entry.');
} else if (count(array_intersect($params['unconfirm_ids'], $params['remove_ids'])) > 0) {
  $cs->WriteNote(false, 'Invalid selection. Please select only one option per entry.');
} else {
  foreach ($params['confirm_ids'] as $referral_id) {
    referral_confirm($referral_id, $user_id);
  }

  if (count($params['confirm_ids']) > 0) {
    $cs->WriteNote(true, count($params['confirm_ids']) . " referral(s) confirmed");
  }

  foreach ($params['unconfirm_ids'] as $referral_id) {
    referral_unconfirm($referral_id, $user_id);
  }

  if (count($params['unconfirm_ids']) > 0) {
    $cs->WriteNote(true, count($params['unconfirm_ids']) . " referral(s) reset");
  }

  foreach ($params['remove_ids'] as $referral_id) {
    referral_reject($referral_id, $user_id);
  }

  if (count($params['remove_ids']) > 0) {
    $cs->WriteNote(true, count($params['remove_ids']) . " referral(s) removed");
  }
}

$cs->RedirectToPreviousPage();
