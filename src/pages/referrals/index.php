<?php

Authorization::authorize('UserMod');

$pending_referrals = database_get_all(database_select("
  SELECT
    referrals.referral_id,
    referrals.referrer_id,
    referrers.username AS referrer_username,
    referrals.user_id,
    users.username AS user_username
  FROM referrals
  LEFT JOIN users ON users.user_id = referrals.user_id
  LEFT JOIN users referrers ON referrers.user_id = referrals.referrer_id
  WHERE referrer_id > 0
  AND confirmed = 0
  AND deleted_at IS NULL
", '', []));

$confirmed_referrals = database_get_all(database_select("
  SELECT
    referrals.referral_id,
    referrals.referrer_id,
    referrers.username AS referrer_username,
    referrals.user_id,
    users.username AS user_username
  FROM referrals
  LEFT JOIN users ON users.user_id = referrals.user_id
  LEFT JOIN users referrers ON referrers.user_id = referrals.referrer_id
  WHERE referrer_id > 0
  AND confirmed = 1
  AND deleted_at IS NULL
", '', []));

render_with('referrals/index', [
  'page_title' => 'Referral management',
  'pending_referrals' => $pending_referrals,
  'confirmed_referrals' => $confirmed_referrals,
]);
