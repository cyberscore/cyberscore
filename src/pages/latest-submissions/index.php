<?php

if (isset($_GET['game_id'])) {
  $game = GamesRepository::get($_GET['game_id']);

  if (!$game) {
    $cs->LeavePage('/', "Game not found");
  }
} else {
  $game = null;
}

if (isset($_GET['offset'])) {
  $offset = intval($_GET['offset']);
} else {
  $offset = 0;
}

$joins = [];
$wheres = ["1=1"];
$binds = [];

if (isset($game)) {
  $wheres []= "records.game_id = ?";
  $binds []= $game['game_id'];
}

if (isset($_GET['cust'])) {
  $joins []= "JOIN records AS r2 ON r2.level_id = records.level_id";
  $wheres []= "r2.user_id = ?";
  $binds []= $current_user['user_id'];
}

$joins = implode(" ", $joins);
$wheres = implode(" AND ", $wheres);

//Game ID 2000 is the development test-game
$latest_subs = database_get_all(database_select("
  SELECT
    records.*,
    users.username, users.country_code, users.country_id, users.user_groups, levels.game_id, levels.group_id, levels.level_id,
    levels.chart_type, levels.num_decimals, levels.score_prefix, levels.score_suffix,
    levels.chart_type2, levels.num_decimals2, levels.score_prefix2, levels.score_suffix2,
    levels.conversion_factor, levels.decimals, levels.ranked, POW((records.csp) / 100,4) AS csr
  FROM records
  LEFT JOIN users USING(user_id)
  LEFT JOIN levels USING(level_id)
  $joins
  WHERE $wheres AND records.game_id != 2000
  ORDER BY records.last_update DESC
  LIMIT 100
  OFFSET $offset
", str_repeat('s', count($binds)), $binds));

if (isset($game)) {
  $t->CacheGame($game['game_id']);
  $page_title = str_replace('[game]', $t->GetGameName($game['game_id']), $t['general_latest_subs_game']);
} else {
  $page_title = $t['general_latest_subs'];
}

$t->CacheGameNames();

foreach ($latest_subs as &$latest) {
  $latest['game_name'] = $t->GetGameName($latest['game_id']);
  $latest['awards'] = fetch_chart_awards($latest['level_id']);
  $latest['colour'] = Modifiers::FetchChartFlagColouration($latest['level_id']);
  $latest['group_colour'] = Modifiers::GroupColouration(database_single_value("SELECT ranked FROM level_groups WHERE group_id = ?", 's', [$latest['group_id']]));
  $latest['group_name'] = $t->GetGroupName($latest['group_id']);
  $latest['chart_name'] = $t->GetChartName($latest['level_id']);
}
unset($latest);

render_with('latest-submissions/index', [
  'page_title' => $page_title,
  'game' => $game,
  'latest_subs' => $latest_subs,
]);
