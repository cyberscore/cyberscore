<?php

Authorization::authorize('UserMod');

$user = UsersRepository::get($_GET['id']);

if (!$user) {
  $cs->PageNotFound();
}

Uploads::remove('user-banner', $user['user_id']);

$cs->WriteNote(true, "{$user['username']}'s banner has been successfully removed.");
redirect_to_back();
