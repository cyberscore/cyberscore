<?php

Authorization::authorize('Guest');

$scoreboards = [
  [
    ['/scoreboards/starboard',  '/images/CSStar.png',                  'standard',    $t['general_mainboard']],
    ['/scoreboards/trophy',     '/images/icon_trophy.png',             'standard',    $t['general_trophy_table']],
    ['/scoreboards/medal',      '/images/icon_ranked.png',             'standard',    $t['general_medal_table']],
    ['/scoreboards/rainbow',    '/images/rainbow_star.png',            'standard',    $t['scoreboards_ultimate']],
  ],
  [
    ['/scoreboards/speedrun',   '/images/scoreboards/speedrun.svg',    'speedrun',    $t['general_speedrun_awards']],
    ['/scoreboards/arcade',     '/images/scoreboards/arcade.svg',      'arcade',      $t['general_arcade_table']],
    ['/scoreboards/solution',   '/images/scoreboards/solution.svg',    'solution',    $t['general_solution_hub']],
    ['/scoreboards/challenge',  '/images/scoreboards/challenge.svg',   'challenge',   $t['general_challenge_table']],
    ['/scoreboards/collectible','/images/scoreboards/collectible.svg', 'collectible', $t['general_collectors_cache']],
    ['/scoreboards/incremental','/images/scoreboards/incremental.svg', 'incremental', $t['general_experience_table']],
  ],
  [
    ['/scoreboards/submissions', '/images/icon_submissions.png',                      'default', $t['general_total_subs']],
    ['/scoreboards/proof',       '/images/icon_proof.png',                            'default', $t['general_proof_board']],
    ['/scoreboards/vproof',      '/images/icon_vproof.png',                           'default', $t['general_vproof_board']],
  ],
  [
    ['/scoreboards/csp',         '/images/icon_submissions.png',                      'default', "CSP scoreboard"],
    ['/scoreboards/positional',  '/images/scoreboards/default.svg',                   'default', 'Positional scoreboard'],
    ['/scoreboards/ambassador',  '/skins4/default/images/scoreboards/ambassador.svg', 'default', $t['general_ambassador_table']],
    ['/scoreboards/staff',       '/images/scoreboards/default.svg',                   'default', $t['general_staff_scoreboard']],
    ['/scoreboards/movers',      '/images/scoreboards/default.svg',                   'default', $t['general_movers_losers']],
  ],
  [
    ['/scoreboards/monthly-challenge',              '/images/icon_submissions.png', 'default', "Monthly chart challenge board"],
    ['/scoreboards/country?board=country_csr',      '/images/icon_global.png',      'default', $t['general_country'] . ' ' . $t['general_mainboard_lower']],
    ['/scoreboards/country?board=country_medals',   '/images/icon_global.png',      'default', $t['general_country'] . ' ' . $t['general_medal_table_lower']],
    ['/scoreboards/country?board=country_csp',      '/images/icon_global.png',      'default', $t['general_country'] . ' (CSP)'],
    ['/scoreboards/country?board=continent_csr',    '/images/icon_global.png',      'default', $t['general_continent'] . ' ' . $t['general_mainboard_lower']],
    ['/scoreboards/country?board=continent_medals', '/images/icon_global.png',      'default', $t['general_continent'] . ' ' . $t['general_medal_table_lower']],
    ['/scoreboards/country?board=continent_csp',    '/images/icon_global.png',      'default', $t['general_continent'] . ' (CSP)'],
  ],
];

render_with('scoreboards/index', [
  'page_title' => t('general_scoreboards'),
  'scoreboards' => $scoreboards,
]);
