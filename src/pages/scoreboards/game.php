<?php

Authorization::authorize('Guest');

$game = GamesRepository::get($_GET['game_id']);

if (!$game) {
  $cs->PageNotFound();
}

$game['platforms'] = database_get_all(database_select("
  SELECT *
  FROM platforms
  JOIN game_platforms USING (platform_id)
  WHERE game_id = ? AND game_platforms.original = 1
  ORDER BY platform_name
", 's', [$game['game_id']]));

$sort = $_GET['sort'] ?? null;
$board = $_GET['board'] ?? 'csp';

$game_name = $t->GetGameName($game['game_id']);

$t->CacheCountryNames();

$scoreboards = [
  ["scoreboard" => "csp",                     "label" => "general_scoreboard",        "icon" => "CSStar24x25White",         "entries" => $cs->GetGameScoreboard($game['game_id'])],
  ["scoreboard" => "medal",                   "label" => "general_medal_table",       "icon" => "icon_ranked20x25White",    "entries" => $cs->GetGameMedalTable($game['game_id'], $sort)],
  ["scoreboard" => "arcade",                  "label" => "general_arcade_table",      "icon" => "icon_arcadeWhite17x25",    "entries" => $cs->GetGameArcadeboard($game['game_id'])],
  ["scoreboard" => "speedrun",                "label" => "general_speedrun_awards",   "icon" => "icon_speedrun20x25White",  "entries" => $cs->GetGameSpeedrunTable($game['game_id'], $sort)],
  ["scoreboard" => "solution",                "label" => "general_solution_hub",      "icon" => "SolutionWhite25x17",       "entries" => $cs->GetGameSolutionboard($game['game_id'])],
  ["scoreboard" => "challenge",               "label" => "general_challenge_table",   "icon" => "icon_challenge25x25White", "entries" => $cs->GetGameChallengeBoard($game['game_id'])],
  ["scoreboard" => "collectible",             "label" => "general_collectors_cache",  "icon" => "icon_collectible_white",   "entries" => $cs->GetGameCollectorsCache($game['game_id'])],
  ["scoreboard" => "incremental",             "label" => "general_incremental",       "icon" => "icon_incremental_white",   "entries" => $cs->GetGameScoreboardIncremental($game['game_id'])],
  ["scoreboard" => "trophy",                  "label" => "profilemodules_trophy_case","icon" => "icon_trophy_white",        "entries" => $cs->GetGameTrophyCase($game['game_id'])], 
  ["scoreboard" => "tsc",                     "label" => "groupstats_tsc",            "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedScoreboardTSC($game['game_id'])],
  ["scoreboard" => "mkpp",                    "label" => "groupstats_mkpp",           "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedScoreboardMKPP($game['game_id'])], 
  ["scoreboard" => "theelite",                "label" => "groupstats_theelite",       "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedScoreboardTE($game['game_id'])], 
  ["scoreboard" => "vgr",                     "label" => "groupstats_vgr",            "icon" => "CSStar24x25White",         "entries" => $cs->GetGameUnrankedMedalTableVGR($game['game_id'])], 
];

$selected_scoreboard = NULL;
foreach ($scoreboards as $scoreboard) {
  if ($scoreboard['scoreboard'] == $board) {
    $selected_scoreboard = $scoreboard;
  }
}

if ($board == 'starboard') {
  $board = 'csp';
}

if ($selected_scoreboard == NULL) {
  $board = "csp";
  $selected_scoreboard = $scoreboards[0];
}

render_with('scoreboards/game', [
  'page_title' => $game_name,
  'board' => $board,
  'scoreboards' => $scoreboards,
  'selected_scoreboard' => $selected_scoreboard,
  'game' => $game,
  'time_to_next' => Rebuilders::game()->time_to_next(),
]);
