<?php

require_once("src/scoreboards/starboard.php");
require_once("src/scoreboards/medal.php");
require_once("src/scoreboards/arcade.php");
require_once("src/scoreboards/speedrun.php");
require_once("src/scoreboards/solution.php");
require_once("src/scoreboards/trophy.php");
require_once("src/scoreboards/challenge.php");
require_once("src/scoreboards/collectible.php");
require_once("src/scoreboards/incremental.php");
require_once("src/scoreboards/rainbow.php");
require_once("src/scoreboards/ambassador.php");
require_once("src/scoreboards/csp.php");
require_once("src/scoreboards/proof.php");
require_once("src/scoreboards/vproof.php");
require_once("src/scoreboards/submissions.php");
require_once("src/scoreboards/staff.php");
require_once("src/scoreboards/positional.php");
require_once("src/scoreboards/movers.php");

switch ($_GET['board'] ?? $_GET['id']) {
case 1:  case 'starboard':   $board = 'starboard'; break;
case 2:  case 'medal':       $board = 'medal'; break;
case 3:  case 'trophy':      $board = 'trophy'; break;
case 4:  case 'submissions': $board = 'submissions'; break;
case 5:  case 'proof':       $board = 'proof'; break;
case 6:  case 'vproof':      $board = 'vproof'; break;
case 7:  case 'csp':         $board = 'csp'; break;
case 8:  case 'arcade':      $board = 'arcade'; break;
case 9:  case 'speedrun':    $board = 'speedrun'; break;
case 10: case 'challenge':   $board = 'challenge'; break;
case 11: case 'ambassador':  $board = 'ambassador'; break;
case 12: case 'rainbow':     $board = 'rainbow'; break;
case 13: case 'solution':    $board = 'solution'; break;
case 14: case 'staff':       $board = 'staff'; break;
case 14: case 'positional':  $board = 'positional'; break;
case 15: case 'movers':      $board = 'movers'; break;
case 16: case 'collectible': $board = 'collectible'; break;
case 17: case 'incremental': $board = 'incremental'; break;
default: $board = 'starboard'; break;
}

require_once("../includes/cron_update_calculator.php");

// filters
$offset = intval($_GET['offset'] ?? 0);
$sort = $_GET['manual_sort'] ?? NULL;
$platform_id = $_GET['platform_id'] ?? NULL;
$series_id = $_GET['series'] ?? NULL;
$country_id = $_GET['country'] ?? NULL;
$continent_id = $_GET['continent'] ?? NULL;

// game filters based on platform or series
$game_filters = ["games.site_id < 3"];
$game_filters_values = [];

if ($platform_id) {
  $game_filters []= 'EXISTS(SELECT 1 FROM game_platforms WHERE original = 1 AND platform_id = ? AND game_platforms.game_id = games.game_id)';
  $game_filters_values []= intval($platform_id);
}

if ($series_id) {
  $game_filters []= 'EXISTS(SELECT 1 FROM series WHERE series_id = ? AND series.game_id = games.game_id)';
  $game_filters_values []= intval($series_id);
}

$game_filters_sql = implode(" AND ", $game_filters);
$game_ids = pluck(database_fetch_all("
  SELECT games.game_id
  FROM games
  WHERE $game_filters_sql
", $game_filters_values), 'game_id');


// user filters based on country and continent
$country_filters = ["1=1"];
$country_filters_values = [];

if ($country_id) {
  $country_filters []= "country_id = ?";
  $country_filters_values []= intval($country_id);
}

if ($continent_id) {
  $country_filters []= "continent = ?";
  $country_filters_values []= intval($continent_id);
}

$country_filters_sql = implode(" AND ", $country_filters);
$country_ids = pluck(database_fetch_all("
  SELECT country_id
  FROM countries
  WHERE $country_filters_sql
", $country_filters_values), 'country_id');

//Fetch the scoreboard
switch ($board) {
case 'starboard':
  $text = $t['general_mainboard'];
  $scoreboard = new StarScoreboard($game_ids, $country_ids, $sort);
  break;
case 'medal':
  $text = $t['general_medal_table'];
  $scoreboard = new MedalScoreboard($game_ids, $country_ids, $sort);
  break;
case 'trophy':
  $text = $t['general_trophy_table'];
  $scoreboard = new TrophyScoreboard($game_ids, $country_ids, $sort);
  break;
case 'csp':
  $text = "CSP Scoreboard";
  $scoreboard = new CSPScoreboard($game_ids, $country_ids, $sort);
  break;
case 'arcade':
  $text = $t['general_arcade_table'];
  $scoreboard = new ArcadeScoreboard($game_ids, $country_ids, $sort);
  break;
case 'speedrun':
  $text = $t['general_speedrun_awards'];
  $scoreboard = new SpeedrunScoreboard($game_ids, $country_ids, $sort);
  break;
case 'challenge':
  $text = $t['general_challenge_table'];
  $scoreboard = new ChallengeScoreboard($game_ids, $country_ids, $sort);
  break;
case 'collectible':
  $text = $t['general_collectors_cache'];
  $scoreboard = new CollectibleScoreboard($game_ids, $country_ids, $sort);
  break;
case 'incremental':
  $text = $t['general_incremental'];
  $scoreboard = new IncrementalScoreboard($game_ids, $country_ids, $sort);
  break;
case 'ambassador':
  $text = $t['general_ambassador_table'];
  $scoreboard = new AmbassadorScoreboard($game_ids, $country_ids, $sort);
  break;
case 'rainbow':
  $text = $t['scoreboards_ultimate'];
  $scoreboard = new RainbowScoreboard($game_ids, $country_ids, $sort);
  break;
case 'solution':
  $text = $t['general_solution_hub'];
  $scoreboard = new SolutionScoreboard($game_ids, $country_ids, $sort);
  break;
case 'submissions':
  $text = $t['general_total_subs'];
  $scoreboard = new SubmissionsScoreboard($game_ids, $country_ids, $sort);
  break;
case 'proof':
  $text = "Approved submissions";
  $scoreboard = new ProofScoreboard($game_ids, $country_ids, $sort);
  break;
case 'vproof':
  $text = 'Approved video submissions';
  $scoreboard = new VideoProofScoreboard($game_ids, $country_ids, $sort);
  break;
case 'staff':
  $text = $t['general_staff_scoreboard'];
  $scoreboard = new StaffScoreboard($game_ids, $country_ids, $sort);
  break;
case 'positional':
  $text = "Positional Scoreboard";
  $scoreboard = new PositionalScoreboard($game_ids, $country_ids, $sort);
  break;
case 'movers':
  $text = $t['general_movers_losers'];
  $scoreboard = new MoversAndLosersScoreboard($game_ids, $country_ids, $sort);
  break;
}

$supported_filters = $scoreboard->filters();
$num_users = $scoreboard->count();
$entries = $cs->FormatRanking($scoreboard->fetch(100, $offset), $offset);
$platforms = database_fetch_all("SELECT * FROM platforms WHERE platform_id != 33 ORDER BY platform_name ASC", []);
$serieses = database_fetch_all("SELECT * FROM series_info ORDER BY series_name ASC", []);

$t->CacheCountryNames();

$continents = [1,2,3,4,5,6];

$continents = array_map(fn($id) => ['id' => $id, 'name' => $t->GetContinentName($id)], $continents);

render_with('scoreboards/show', [
  'page_title' => $text,
  'game_filters' => $game_filters,
  'board' => $board,
  'sort' => $sort,
  'supported_filters' => $supported_filters,
  'platforms' => $platforms,
  'serieses' => $serieses,
  'num_users' => $num_users,
  'game_ids' => $game_ids,
  'continents' => $continents,
  'countries' => $t->GetCountryNames(),
  'country_ids' => $country_ids,

  'series_id' => $series_id,
  'continent_id' => $continent_id,
  'platform_id' => $platform_id,
  'country_id' => $country_id,
  'offset' => $offset,
  'pages' => intval($num_users/100),

  'scoreboard' => $scoreboard,
  'entries' => $entries,

  'time_to_next' => Rebuilders::global()->time_to_next(),
]);
