## Cyberscore API development guide

To make it easier for other developers to interact with cyberscore, we're
starting to expose some API endpoints.

To be extra careful, we've made the API very limited and not flexible at all.
Feel free to suggest any improvements by creating a new issue.


### For anyone wanting to use applications that use our API

Some third-party applications may want to access your information on
cyberscore, or even act on your behalf by submitting records, for example. To
grant them access, you need to create an **Personal Access Token** by going to
[Settings > Password security](https://cyberscore.me.uk/settings/password) and
creating one.

The application developer should tell you which permissions they require. Do
not select any more permissions that necessary. You can't change these after
creation, so if you make a mistake just delete the token and create a new one.

Be aware that you are responsible for any actions that those applications make
using your personal access tokens. **Do not share these tokens with people you
do not trust**.

Usage of our APIs with these tokens are logged, including the requester's IP
address.


### For anyone interested in developing things that use our API

Our endpoint responses are JSON based. You can always scrape the HTML pages,
but we'd prefer if you use the JSON endpoints (they're not as heavy on our
servers).

API requests can have an `X-Authorization` header when you want to act on
behalf of a player (authenticated requests). Some endpoints accept requests
without that header (anonymous requests).

Each authenticated request requires the token to have certain permissions
enabled. Token permissions can't be changed, so if you need a new permission
you'll have to ask for a new token.

We have two styles of API endpoints, "old style" and "new style". Old style
endpoints share the same url as regular HTML pages, and will eventually be
removed. New style endpoints are all under the `/api/` namespace and have fewer
restrictions.

Here are the new style endpoints:

- `GET /api/charts/:id`:
  - Description: returns information about a chart, including all its scores.
  - parameters: `:id` is the chart identifier
  - No token or scopes required
- `GET /api/games/:id`:
  - Description: returns information about a game, including its groups and
    charts.
  - parameters: `:id` is the game identifier
  - Optional scope `records:read`: if this scope is present, it will
    include every score of the authenticated player for this game.
- `GET /api/notifications`:
  - Description: returns all of the authenticated player notifications
  - Requires scope `notifications:read`
- `POST /api/records`:
  - Description: submits multiple records for a single game
  - Requires scope `records:create`
  - Body (json):
    ```json
      {
        "game_id": 3279,
        "records": [
          { "chart_id": 690377, submission: 42 },
          { "chart_id": 690378, submission: 31 }
        ]
      }
    ```

These are the old style API endpoints, which are destined to be removed at some
point:

- `GET /search.json`:
  - query parameters: `q` contains the text to search for
  - Anonymous: returns a list of games and users whose name matches the given text
  - Authenticated: not supported
- `GET /profile-api/:id`:
  - parameters: `:id` is the user identifier or their username
  - Anonymous: returns information about a player, including its scoreboard positions
  - Authenticated: not supported
- `GET /chart-submission-history/:id`
  - parameters: `:id` is the chart identifier
  - Anonymous: returns the full submission history for a chart, including records that were later updated
  - Authenticated: not supported
- `GET /game/:id.json`:
  - parameters: `:id` is the game identifier
  - Anonymous: returns information about a game, including its groups and
    charts
  - Authenticated: not supported. Use new style instead

Additional limitations:

- We don't currently support CORS, so you won't be able to use our API from
  frontend only applications;
- In the old style endpoints, the `.json` extension must be present in
  authenticated API endpoints;
- In the old style endpoints, even if they could return partial responses when
  some scopes are omitted (see `GET /game/:id` for an example), they won't and
  will fail;


### For Cyberscore developers wanting to expand our API

Read the previous sections first, they describe some of the restrictions of
this system.

We don't have any web frameworks in place, so things are a bit adhoc. To try to
reduce the probability of having security flaws, things are a bit more weird
than you'd expect.

If you want to create a new API, you should create specific API endpoints
under `/api/`, using the same pattern as the other API endpoints.

Our old style API endpoints piggyback on existing HTML endpoints (`/game/3279`
returns HTML while `/game/3279.json` returns JSON), which helps us avoid
duplicating complex queries, but it has a bunch of downsides (discussed below).
We won't cover implementing this style of API endpoints.

Say that you want to add an API that returns the contents of a CSMail message.
The existing endpoint is `GET /csmail-messages/:id`. This is defined in
`src/pages/csmail-messages/show.php` and it looks roughly like this:

~~~~php
<?php

Authorization::authorize('User');

$message = CSMailRepository::get($_GET['id']);

if (!$message) {
  $cs->LeavePage('/csmails', $t['csmail_error_faulty_id']);
}

// You can only read your own messages
if (!in_array($current_user['user_id'], [$message['from_id'], $message['to_id']]) {
  $cs->LeavePage('/csmails', $t['csmail_error_not_yours']);
}

// Mark it as read
if ($message['to_status'] == 0 && $message['to_id'] == $current_user['user_id']) {
  database_update_by('csmail', ['to_status' => 1], ['csmail_id' => $csmail_id]);
}

render_with("csmail-messages/show", ['message' => $message]);
~~~~

To create an equivalent API endpoint, you'd need to do the following steps:

1. Add a `resources('csmail-messages', ['only' => 'show'])` line to `src/router.php`;
1. Create a file `src/pages/api/csmail-messages/show.php` that:
  1. Is similar to the HTML version
  1. Specifies which scopes are necessary
  1. Fetches the message from the database
  1. Ignore any behavior that should be html only (marking messages as read, for example)
  1. Renders json in both success and error cases

In the end, it might look something like this:

~~~~php
<?php

Authorization::require_all_api_permissions('csmail:read');
Authorization::authorize('User');

$message = CSMailRepository::get($_GET['id']);

if (!$message) {
  http_response_code(404);
  render_json(['error' => 'CSMail Message not found']);
  exit;
}

// You can only read your own messages
if (!in_array($current_user['user_id'], [$message['from_id'], $message['to_id']]) {
  http_response_code(404);
  render_json(['error' => 'CSMail Message not found']);
  exit;
}

// be specific about which fields to render to avoid leaking internal/sensitive information
render_json([
  'from_id' => $message['from_id'],
  'to_id' => $message['to_id'],
  'title' => $message['title'],
  'body' => $message['body'],
]);
~~~~

In this case, you need to duplicate the authorization checks, so there's some
risk of behavior drift. Message loading isn't as trivial as this example shows,
and sometimes there's a ginormous query that would have to be duplicated. This
common behavior should eventually be moved to a common behavior layer.

If you want to create a new permission string, you'll need to add it to two files:

- `src/templates/components/settings/password.html.twig`
- `src/pages/api-tokens/create.php`

There isn't any strict standard on what these strings should look like, but
try to follow the pattern of `<objects>:<action>`:

- `records:create` allows you to create records on behalf of the player
- `records:read` allows you to read records of the player
- `profile:read` allows you to read basic profile information of the player

Note that read permissions don't necessarily stop developers from getting your
public information. For example, if they have a token without the
`records:read` permission, fetching `GET /games/:id` won't return your scores,
but if they know your user id, they can get your scores by fetching `GET
/charts/:id` and searching for your id in the scores list.


#### Improvements that need to be made

We don't have a good way of running code in HTML endpoints only, so every
endpoint right now loads `$current_user` from cookies. Since we don't want
cookie processing in API endpoints, in these endpoints we're resetting
`$current_user` and loading it from the authorization token instead. This is a
small hack that may cause issues when we do CSRF protection.

The only protection we have against CSRF is setting the session cookie to
`SameSite: Lax`, which may not be supported by every browser. If we ever add
proper CSRF protections, these should be disabled for API endpoints, which must
not process session cookies, or they would circunvent our protection.

We'll need to implement CORS, which means adding some headers to API endpoint
responses and processing `OPTIONS` requests. Adding headers would probably be
simple, but processing `OPTIONS` requires some router work.

All old style API endpoints work with either cookies or tokens. We take
advantage of this in some javascript-heavy pages (like the auto prover tool).
To deprecate old style API endpoints, we'd need to figure out a way to make
these javascript-heavy pages work, preferably without having to create tokens
just for this. We could build internal APIs that work with cookies only, but
this would be duplicating code once again.
