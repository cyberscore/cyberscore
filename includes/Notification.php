<?php
class Notification {
  // GUIDE TO ADDING NEW NOTIFICATION TYPES:
  //
  // In order to add a new type of notification, the following steps need to be carried out:
  //
  // 1. Pick a notification type ('record_added', 'referral_rejected', etc)
  // 2. Decide if you want to enable this notification by default ('yes', 'no')
  // 3. Run bin/add-notification-type yourtype yes-or-no
  // 4. Run the migrations: bin/migrations migrate
  // 5. Add a generating function to this file
  // 6. Decide which URL the user will be taken when they click on the notification
  // 7. Alter TargetURL by adding the new type and the URL
  // 8. Add a new template in src/components/notifications/
  // 9. Code the script to call our new notification type.
  // 10. Add the ability for the user to alter their notification preference
  //
  // HAPPY ADDING!!
  //
  // | notification_id | int(10) unsigned  | NO   | PRI | NULL    | auto_increment |
  // | type            | enum              | NO   |     | NULL    |                |
  // | note_time       | datetime          | NO   |     | NULL    |                |
  // | note_seen       | enum('y','n')     | NO   |     | n       |                |
  // | note_details    | text              | NO   |     | NULL    |                |
  // | user_id         | int(10) unsigned  | NO   | MUL | NULL    |                |
  // |-----------------+-------------------+------+-----+---------+----------------|
  // | sender_user_id  | int(11)           | YES  |     | NULL    |                |
  // |-----------------+-------------------+------+-----+---------+----------------|
  // | chart_id        | int(11)           | YES  |     | NULL    |                |
  // | game_id         | int(11)           | YES  |     | NULL    |                |
  // | game_request_id | int(11)           | YES  |     | NULL    |                |
  // | record_id       | int(11)           | YES  |     | NULL    |                |
  // | support_id      | int(11)           | YES  |     | NULL    |                |
  // | news_id         | int(11)           | YES  |     | NULL    |                |
  // | referral_id     | int(11)           | YES  |     | NULL    |                |
  // | target_user_id  | int(11)           | YES  |     | NULL    |                |
  private $type = null;
  private $params = null;

  function __construct($type = '', $params = []) {
    $this->type = $type;
    $this->params = $params;
  }

  function DeliverToGroups($group_names, $options = []) {
    $except = $options['except'] ?? [];
    $group_mask = Authorization::UserGroupMask($group_names);

    $users = database_fetch_all(
      "SELECT user_id FROM users WHERE (user_groups & ?) != 0",
      [$group_mask]
    );

    $user_ids = array_diff(pluck($users, 'user_id'), $except);

    return $this->DeliverToUsers($user_ids);
  }

  function DeliverToTarget() {
    return $this->DeliverToUser($this->params['target_user_id']);
  }

  function DeliverToUsers($user_ids) {
    $notification_ids = [];

    foreach ($user_ids as $user_id) {
      $notification_ids []= $this->DeliverToUser($user_id);
    }

    return $notification_ids;
  }

  function DeliverToUser($user_id) {
    $params = [
      'type' => $this->type,
      'user_id' => $user_id,
      'note_time' => database_now(),
      'note_details' => '',
      'note_seen' => 'n',
    ];

    if ($this->UserWantsNotification($user_id, $this->type)) {
      return database_insert('notifications', array_merge($params, $this->params));
    }
  }

  public static function YGB($game_id, $chart_id, $new_leader_id) {
    return new Notification('ygb', [
      'game_id' => $game_id,
      'chart_id' => $chart_id,
      'sender_user_id' => $new_leader_id,
    ]);
  }

  public static function ProofRefused($record_id, $reviewer_id) {
    $record = database_fetch_one("
      SELECT game_id, level_id, user_id FROM records WHERE record_id = ?
    ", [$record_id]);

    return new Notification('proof_refused', [
      'game_id' => $record['game_id'],
      'chart_id' => $record['level_id'],
      'record_id' => $record_id,
      'target_user_id' => $record['user_id'],
      'sender_user_id' => $reviewer_id,
    ]);
  }

  public static function NewGameRequest($game_request_id, $requester_id) {
    return new Notification('new_grequest', [
      'game_request_id' => $game_request_id,
      'sender_user_id' => $requester_id,
    ]);
  }

  public static function RecordApproval($record_id, $reviewer_id) {
    $record = database_fetch_one("
      SELECT game_id, level_id, user_id FROM records WHERE record_id = ?
    ", [$record_id]);

    return new Notification('rec_approved', [
      'game_id' => $record['game_id'],
      'chart_id' => $record['level_id'],
      'record_id' => $record_id,
      'target_user_id' => $record['user_id'],
      'sender_user_id' => $reviewer_id,
    ]);
  }

  public static function RecordProofRequired($record_id, $reviewer_id) {
    $record = database_fetch_one("
      SELECT game_id, level_id, user_id FROM records WHERE record_id = ?
    ", [$record_id]);

    return new Notification('rec_reported', [
      'game_id' => $record['game_id'],
      'chart_id' => $record['level_id'],
      'record_id' => $record_id,
      'target_user_id' => $record['user_id'],
      'sender_user_id' => $reviewer_id,
    ]);
  }

  public static function RecordDeleted($record_id, $reviewer_id) {
    $record = database_fetch_one("
      SELECT game_id, level_id, user_id FROM records_del WHERE record_id = ?
    ", [$record_id]);

    return new Notification('rec_deleted', [
      'game_id' => $record['game_id'],
      'chart_id' => $record['level_id'],
      'record_id' => $record_id,
      'target_user_id' => $record['user_id'],
      'sender_user_id' => $reviewer_id,
    ]);
  }

  public static function RecordObliterated($chart_id, $record_id, $user_id, $reviewer_id) {
    $record = database_fetch_one("
      SELECT game_id FROM levels WHERE level_id = ?
    ", [$chart_id]);

    return new Notification('rec_deleted', [
      'game_id' => $record['game_id'],
      'chart_id' => $chart_id,
      'record_id' => $record_id,
      'target_user_id' => $user_id,
      'sender_user_id' => $reviewer_id,
    ]);
  }

  public static function ArticleComment($comment_id) {
    $comment = database_fetch_one("
      SELECT
        news_comments.user_id AS commenter_id,
        news.user_id AS writer_id,
        news.news_id
      FROM news_comments
      JOIN news USING (news_id)
      WHERE news_comments_id = ?
    ", [$comment_id]);

    return new Notification('article_comment', [
      'target_user_id' => $comment['writer_id'],
      'sender_user_id' => $comment['commenter_id'],
      'news_id' => $comment['news_id'],
      'note_details' => $comment_id,
    ]);
  }

  public static function ArticleCommentReply($comment_id) {
    $comment = database_fetch_one("
      SELECT
        news_comments.user_id AS commenter_id,
        parent.user_id AS parent_user_id,
        news_comments.news_id
      FROM news_comments
      JOIN news_comments parent ON news_comments.reply_id = parent.news_comments_id
      WHERE news_comments.news_comments_id = ?
    ", [$comment_id]);

    return new Notification('reply', [
      'target_user_id' => $comment['parent_user_id'],
      'sender_user_id' => $comment['commenter_id'],
      'news_id' => $comment['news_id'],
      'note_details' => 'news',
    ]);
  }

  public static function GameRequestComment($comment_id) {
    $comment = database_fetch_one("
      SELECT
        request_comments.user_id AS commenter_id,
        game_requests.submitter_id AS submitter_id,
        game_requests.gamereq_id AS game_request_id
      FROM request_comments
      JOIN game_requests USING (gamereq_id)
      WHERE comment_id = ?
    ", [$comment_id]);

    return new Notification('greq_comment', [
      'target_user_id' => $comment['submitter_id'],
      'sender_user_id' => $comment['commenter_id'],
      'game_request_id' => $comment['game_request_id'],
      'note_details' => $comment_id,
    ]);
  }

  public static function GameRequestCommentReply($comment_id) {
    $comment = database_fetch_one("
      SELECT
        request_comments.user_id AS commenter_id,
        parent.user_id AS parent_user_id,
        request_comments.gamereq_id AS game_request_id
      FROM request_comments
      JOIN request_comments parent ON request_comments.reply_id = parent.comment_id
      WHERE request_comments.comment_id = ?
    ", [$comment_id]);

    return new Notification('reply', [
      'target_user_id' => $comment['parent_user_id'],
      'sender_user_id' => $comment['commenter_id'],
      'game_request_id' => $comment['game_request_id'],
      'note_details' => 'game_requests',
    ]);
  }

  public static function UserPageComment($comment_id) {
    $comment = database_fetch_one("
      SELECT
        user_comments.user_id AS commenter_id,
        user_comments.userpage_id AS target_user_id,
        user_comments.userpage_id
      FROM user_comments
      WHERE comment_id = ?
    ", [$comment_id]);

    return new Notification('userpage_comments', [
      'target_user_id' => $comment['target_user_id'],
      'sender_user_id' => $comment['commenter_id'],
      'userpage_id' => $comment['userpage_id'],
      'note_details' => $comment_id,
    ]);
  }

  public static function UserPageCommentReply($comment_id) {
    $comment = database_fetch_one("
      SELECT
        user_comments.user_id AS commenter_id,
        parent.user_id AS parent_user_id,
        user_comments.userpage_id
      FROM user_comments
      JOIN user_comments parent ON user_comments.reply_id = parent.comment_id
      WHERE user_comments.comment_id = ?
    ", [$comment_id]);

    return new Notification('reply', [
      'target_user_id' => $comment['parent_user_id'],
      'sender_user_id' => $comment['commenter_id'],
      'userpage_id' => $comment['userpage_id'],
      'note_details' => 'userpage',
    ]);
  }

  public static function PageStringAdded($translation_key, $editor_id) {
    return new Notification('pstring_added', [
      'sender_user_id' => $editor_id,
      'note_details' => $translation_key,
    ]);
  }

  public static function PageStringEdited($translation_key, $editor_id) {
    return new Notification('pstring_edited', [
      'sender_user_id' => $editor_id,
      'note_details' => $translation_key,
    ]);
  }

  public static function RecordReported($record_id, $reporter_id) {
    $record = database_fetch_one("
      SELECT game_id, level_id, user_id FROM records WHERE record_id = ?
    ", [$record_id]);

    return new Notification('reported_rec', [
      'game_id' => $record['game_id'],
      'chart_id' => $record['level_id'],
      'record_id' => $record_id,
      'target_user_id' => $record['user_id'],
      'sender_user_id' => $reporter_id,
    ]);
  }

  public static function SupportRequest($support_id) {
    $support = database_fetch_one("
      SELECT user_id FROM support WHERE support_id = ?
    ", [$support_id]);

    return new Notification('supp', [
      'sender_user_id' => $support['user_id'],
      'support_id' => $support_id,
    ]);
  }

  public static function BlogFollowed($followee_id, $follower_id) {
    return new Notification('blog_follow', [
      'sender_user_id' => $follower_id,
      'target_user_id' => $followee_id,
    ]);
  }

  public static function BlogUnfollowed($followee_id, $follower_id) {
    return new Notification('blog_unfollow', [
      'sender_user_id' => $follower_id,
      'target_user_id' => $followee_id,
    ]);
  }

  public static function BlogPosted($article_id) {
    $article = database_fetch_one("
      SELECT user_id FROM news WHERE news_id = ?
    ", [$article_id]);

    return new Notification('followed_posted', [
      'sender_user_id' => $article['user_id'],
      'news_id' => $article_id,
    ]);
  }

  public static function NewGame($game_id, $creator_id) {
    return new Notification('new_game', [
      'game_id' => $game_id,
      'sender_user_id' => $creator_id,
    ]);
  }

  public static function ReferralConfirmed($referral_id, $reviewer_id) {
    return new Notification('referral_confirmed', [
      'referral_id' => $referral_id,
      'sender_user_id' => $reviewer_id,
    ]);
  }

  public static function ReferralRejected($referral_id, $reviewer_id) {
    return new Notification('referral_rejected', [
      'referral_id' => $referral_id,
      'sender_user_id' => $reviewer_id,
    ]);
  }

  public static function ReferralCreated($referral_id) {
    return new Notification('referral_created', [
      'referral_id' => $referral_id,
    ]);
  }

  public static function TrophyLost($game_id, $scoreboard_position, $new_leader_id, $scoreboard_type) {
    return new Notification('trophy_lost', [
      'game_id' => $game_id,
      'position' => $scoreboard_position,
      'sender_user_id' => $new_leader_id,
      'note_details' => $scoreboard_type,
    ]);
  }

  public static function TrophyGained($game_id, $scoreboard_position, $scoreboard_type) {
    return new Notification('trophy_gained', [
      'game_id' => $game_id,
      'position' => $scoreboard_position,
      'note_details' => $scoreboard_type,
    ]);
  }

  public function UserWantsNotification($user_id, $type) {
    $wants_notification = database_find_by('notification_prefs', ['user_id' => $user_id]);

    return $wants_notification[$type] ?? false;
  }

  public static function TargetURL($notification) {
    switch ($notification['type']) {
    case 'article_comment':
      return "/articles/{$notification['news_id']}";
    case 'blog_follow':
      return "/user/{$notification['sender_user_id']}";
    case 'blog_unfollow':
      return "/user/{$notification['sender_user_id']}";
    case 'followed_posted':
      return "/articles/{$notification['news_id']}";
    case 'greq_comment':
      return "/game_requests/{$notification['game_request_id']}";
    case 'new_game':
      return "/game/{$notification['game_id']}";
    case 'new_grequest':
      return "/game_requests/{$notification['game_request_id']}";
    case 'proof_refused':
      return "/record/{$notification['record_id']}";
    case 'pstring_added':
      return "/translation/page_view_key.php?key={$notification['note_details']}";
    case 'pstring_edited':
      return "/translation/page_view_key.php?key={$notification['note_details']}";
    case 'rec_approved':
      return "/record/{$notification['record_id']}";
    case 'rec_deleted':
      return "/chart/{$notification['chart_id']}";
    case 'rec_reported':
      return "/record/{$notification['record_id']}";
    case 'referral_created':
      return "/referrals";
    case 'referral_confirmed':
      return "/settings/referrals";
    case 'referral_rejected':
      return "/settings/referrals";
    case 'reply':
      switch ($notification['note_details']) {
      case 'userpage':
        return "/user/{$notification['userpage_id']}/comments";
      case 'news':
        return "/articles/{$notification['news_id']}";
      case 'game_requests':
        return "/game_requests/{$notification['game_request_id']}";
      default:
        return $notification['type'];
      }
    case 'reported_rec':
      return "/record/{$notification['record_id']}";
    case 'supp':
      return "/support_requests.php";
    case 'userpage_comments':
      return "/user/{$notification['user_id']}/comments";
    case 'ygb':
      return "/chart/{$notification['chart_id']}";
    case 'trophy_lost':
      return "/game_scoreboard.php?game_id={$notification['game_id']}&index=0&board={$notification['note_details']}";
    case 'trophy_gained':
      return "/game_scoreboard.php?game_id={$notification['game_id']}&index=0&board={$notification['note_details']}";
    default:
      return $notification['type'];
    }
  }
}
