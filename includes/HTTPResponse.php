<?php

class HTTPResponse {
  public static function PageNotFound($title = "Page not found") {
    http_response_code(404);
    render_with("home/404", ['page_title' => $title]);
    self::Exit();
  }

  public static function LeavePage($redirect, $note_head = '', $note_extra_text = '') {
    if ($note_head != '') {
      Flash::AddError($note_head, $note_extra_text);
    }

    if (wants_json()) {
      http_response_code(400);
      render_json([]);
    } else {
      // Newline injection is prevented by `header`.
      header("Location: $redirect");
    }
    self::Exit();
  }

  public static function RedirectToPreviousPage() {
    global $config;

    $from = $_SERVER['HTTP_REFERER'] ?? '';
    if (!empty($from)) {
      $from_url = parse_url($from);
      $base_url = parse_url($config['app']['base_url']);

      unset($from_url['query']);
      unset($from_url['path']);

      if ($from_url === $base_url) {
        self::LeavePage($from);
      }
    }

    self::LeavePage("/");
  }

  // This is where we can place hooks to always run after the page request.
  // It's not great, but we were calling exit directly from a bunch of places,
  // so this was the easiest solution. Ideally we'd stop calling exit from
  // random places, but that would mean implementing exceptions or something
  // like that to stop page processing and bubble up.
  //
  // Update: this code is now throwing an exception. I needed to remove the
  // exit because it was screwing up the unit tests runner. There might be some
  // work that can now be done to cleanup this flow, but not today.
  public static function Exit() {
    global $timer;

    $timer->finish();
    $target = strval(Router::instance()->matched_target);
    log_event('page_request', ['execution_time' => $timer->delta(), 'target' => $target]);

    Flash::Persist();

    throw new EarlyExitException();
  }
}
