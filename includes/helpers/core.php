<?php

function group_by($array, $function) {
  $grouped = [];

  foreach ($array as $element) {
    $key = $function($element);

    if (array_key_exists($key, $grouped)) {
      $grouped[$key] []= $element;
    } else {
      $grouped[$key] = [$element];
    }
  }

  return $grouped;
}

function index_by($array, $key, $value = null) {
  $grouped = [];

  foreach ($array as $element) {
    if ($value == null) {
      $grouped[$element[$key]] = $element;
    } else {
      $grouped[$element[$key]] = $element[$value];
    }
  }

  return $grouped;
}

function tally($array) {
  $tally = [];

  foreach ($array as $element) {
    if (isset($tally[$element])) {
      $tally[$element] += 1;
    } else {
      $tally[$element] = 1;
    }
  }

  return $tally;
}

function pluck($array, $key) {
  return array_map(
    function ($element) use ($key) {
      return $element[$key];
    },
    $array
  );
}

// Returns the array without any NULL elements
function array_compact($array) {
  return array_filter($array, fn($e) => $e !== NULL);
}

// Returns the number of elements in the array for which the predicate returns true.
// array_filter([1,2,3,4,5], fn($e) => $e % 2 == 0) == 2
function array_count($array, $fn) {
  $n = 0;
  foreach ($array as $e) {
    if ($fn($e)) {
      $n++;
    }
  }
  return $n;
}

function tree_sort($comments, $id_field, $parent_field) {
  $sorted = [];

  $by_parent_id = group_by(
    $comments,
    function($c) use ($parent_field) { return $c[$parent_field] ?? 0; }
  );

  tree_sort_aux(0, $by_parent_id, $sorted, $id_field);

  return $sorted;
}

function tree_sort_aux($comment_id, &$by_parent_id, &$sorted, $id_field) {
  foreach ($by_parent_id[$comment_id] ?? [] as $c) {
    $sorted []= $c;
    tree_sort_aux($c[$id_field], $by_parent_id, $sorted, $id_field);
  }
};
