<?php

function classnames($classes) {
  $classnames = [];

  foreach ($classes as $k => $v) {
    if ($v) {
      $classnames []= $k;
    }
  }

  return implode(' ', $classnames);
}

function h($contents) {
  return htmlspecialchars($contents, ENT_QUOTES);
}

function first_line($text) {
  return explode("\n", $text)[0];
}

function build_html_options($options) {
  return implode(
    ' ',
    array_map(
      function($key, $value) {
        $value = h($value);
        return "$key='$value'";
      },
      array_keys($options),
      $options
    )
  );
}

function option_selected($option, $selected) {
  if (is_array($selected)) {
    if (in_array($option, $selected)) {
      return 'selected="selected"';
    }
  } else if ($option == $selected) {
    return 'selected="selected"';
  }
}

function text_field($field, $object, $options = []) {
  $options['type'] = 'text';
  $options['name'] = $field;
  $options['value'] = $object[$field] ?? "";

  $html_options = build_html_options($options);

  return "<input $html_options />";
}

function hidden_field($field, $object, $options = []) {
  $options['type'] = 'hidden';
  $options['name'] = $field;
  $options['value'] = $object[$field] ?? "";

  $html_options = build_html_options($options);

  return "<input $html_options />";
}

function number_field($field, $object, $options, $nested = null) {
  $options['type'] = 'number';
  $options['name'] = $nested ? "{$nested}[$field]" : $field;
  $options['value'] = $object[$field] ?? "";

  $html_options = build_html_options($options);

  return "<input $html_options />";
}

function email_field($field, $object, $options = []) {
  $options['type'] = 'email';
  $options['name'] = $field;
  $options['value'] = $object[$field] ?? "";

  $html_options = build_html_options($options);

  return "<input $html_options />";
}

function password_field($field, $object, $options) {
  $options['type'] = 'password';
  $options['name'] = $field;
  $options['value'] = $object[$field] ?? "";

  $html_options = build_html_options($options);

  return "<input $html_options />";
}

function radio_field($field, $object, $html_options) {
  $html_options['type'] = 'radio';
  $html_options['name'] = $field;
  if ($object[$field] == $html_options['value']) {
    $html_options['checked'] = 'checked';
  }

  $serialized_html_options = build_html_options($html_options);

  return "<input $serialized_html_options />";
}

function select_field($field, $object, $options, $html_options = []) {
  $current_id = $object[$field];
  $html_options['name'] = $field;
  $html_options = build_html_options($html_options);

  $option_tags = implode("", array_map(
    function($id, $label) use(&$current_id) {
      $option_html_options = ['value' => $id];
      if ($current_id == $id) {
        $option_html_options['selected'] = 'selected';
      }
      $html_value = h($label);
      $serialized_option_html_options = build_html_options($option_html_options);
      return "<option $serialized_option_html_options>$html_value</option>";
    },
    array_keys($options),
    $options
  ));

  return "<select $html_options>$option_tags</select>";
}

function timezone_select_field($field, $object) {
  $options = [];

  foreach (timezone_identifiers_list() as $timezone) {
    $options[$timezone] = $timezone;
  }

  return select_field($field, $object, $options);
}

function checkbox_field($field, $object, $html_options) {
  $html_options['type'] = 'checkbox';
  $html_options['name'] = $field;
  $html_options['value'] = '1';
  if ($object[$field]) {
    $html_options['checked'] = 'checked';
  }

  $serialized_html_options = build_html_options($html_options);
  return "<input $serialized_html_options />";
}

function slider_field($field, $object) {
  $hidden = hidden_field($field, [$field => '0']);
  $checkbox = checkbox_field($field, $object, []);

  return "<label class='slider_switch'>$hidden$checkbox<span class='slider round'></span></label>";
}

function country_flag_image_tag($country, $options = []) {
  global $t;

  $options = array_merge($options, [
    'src' => "/flags/{$country['country_code']}.png",
    'title' => $t->GetCountryName($country['country_id']),
    'alt' => $t->GetCountryName($country['country_id']),
  ]);

  if (isset($options['class'])) {
    $options['class'] = "{$options['class']} country-flag";
  } else {
    $options['class'] = "country-flag";
  }

  $html_options = build_html_options($options);
  return "<img $html_options />";
}

function user_avatar_image_tag($user, $options = []) {
  global $cs;
  $basename = $cs->GetUserPic($user);

  $options = array_merge($options, [
    'src' => $basename,
  ]);

  if (isset($options['class'])) {
    $options['class'] = "{$options['class']} user-avatar";
  } else {
    $options['class'] = "user-avatar";
  }

  $html_options = build_html_options($options);

  if ($options['link'] ?? false) {
    return "<a href='/users/" . h($user['user_id']) . "'><img $html_options /></a>";
  } else {
    return "<img $html_options />";
  }
}

function user_banner_image_tag($user, $options = []) {
  global $cs;

  $basename = $cs->GetBannerURL($user);

  $options = array_merge($options, [
    'src' => $basename,
  ]);

  $html_options = build_html_options($options);
  return "<img $html_options />";
}

function highlight_text($text, $highlight) {
  if ($highlight === null || $text === null) {
    return $text;
  }

  $highlight = preg_quote($highlight);
  $b = preg_split("/($highlight)/ui", $text, -1, PREG_SPLIT_DELIM_CAPTURE);

  $r = [];
  foreach ($b as $index => $part) {
    if ($index % 2 == 0) {
      $r []= h($part);
    } else {
      $r []= "<span style='background-color:#FFB74C;'>" . h($part) . "</span>";
    }
  }

  return implode('', $r);
}
