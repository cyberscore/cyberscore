<?php

use Ramsey\Uuid\Uuid;

function wikilog($message) {
  global $config;

  if (!$config['app']['verbose']) return;

  // every query goes through a database function that
  // tracks total number of queries done so far.
  static $previous = 0;
  static $previous_db_queries = 0;

  if ($previous == 0) $previous = hrtime(true);
  if ($previous_db_queries == 0) $previous_db_queries = database_query_count();
  $now = hrtime(true);
  $delta = $now - $previous;
  $previous = $now;

  $delta_queries = database_query_count() - $previous_db_queries;
  $previous_db_queries = database_query_count();

  $secs = intdiv($delta, 1000000000);
  $msecs = intdiv($delta, 1000000) % 1000;

  printf("[%3d sql][%ds %3dms] %s\n", $delta_queries, $secs, $msecs, $message);

  flush();
  ob_flush();
}

class Timer {
  private $start = 0;
  private $finish = 0;

  function start() {
    $this->start = hrtime(true);
  }

  function finish() {
    $this->finish = hrtime(true);
  }

  function delta() {
    return $this->finish - $this->start;
  }
}

function execution_time() {
  static $previous = 0;
  $now = hrtime(true);
  $delta = $now - $previous;
  $previous = $now;

  return $delta / 1_000_000_000;
}

function request_uuid() {
  static $id = null;

  if ($id == null) {
    $id = Uuid::uuid4()->toString();
  }

  return $id;
}

function log_event($name, $properties = NULL) {
  global $_SERVER;
  global $current_user;
  global $config;

  if (($config['app']['log_events'] ?? false) == false) {
    return;
  }

  if ($properties === NULL) {
    $properties = new ArrayObject();
  }

  database_insert('internal_events', [
    'user_id' => $current_user['user_id'] ?? NULL,
    'user_ip' => get_ip(),
    'request_uuid' => request_uuid(),
    'request_method' => $_SERVER['REQUEST_METHOD'],
    'request_uri' => $_SERVER['REQUEST_URI'],
    'name' => $name,
    'properties' => json_encode($properties),
    'created_at' => database_now(),
  ]);
}
