<?php

class Expression {
  public $rpn = null;
  public $functions = null;

  function __construct($expression) {
    $this->rpn = explode(' ', $expression);

    $this->functions = [
      '+' => [2, fn($a, $b) => $a + $b],
      '-' => [2, fn($a, $b) => $a - $b],
      '*' => [2, fn($a, $b) => $a * $b],
      '/' => [2, fn($a, $b) => $a / $b],
      'minus' => [1, fn($a) => -$a],
      'round' => [2, fn($a, $b) => round($a, $b)],
      'floor' => [2, fn($a, $b) => floor($a * (10 ** $b)) / (10 ** $b)],
      'ceil' => [2, fn($a, $b) => ceil($a * (10 ** $b)) / (10 ** $b)],
    ];
  }

  function apply($bindings) {
    $stack = [];

    foreach ($this->rpn as $token) {
      if (is_string($token) && array_key_exists($token, $bindings)) {
        $stack []= $bindings[$token];
      } else if (is_string($token) && array_key_exists($token, $this->functions)) {
        [$n, $fn] = $this->functions[$token];
        if (count($stack) < $n) { return 'error'; }

        $args = array_splice($stack, count($stack) - $n);

        $stack []= call_user_func_array($fn, $args);
      } else {
        $stack []= $token;
      }
    }

    return $stack[0];
  }
}
