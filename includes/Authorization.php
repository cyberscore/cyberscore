<?php

// This class handles two types of authorization:
// - Role based authorization, in which $current_user must have certain roles,
// - and Token based authorization, in which the current_user_token must have all of the specified permissions.
//
// There is a difference between:
// - a user having a role or not;
// - a user having access to the functions granted to those roles.
//
// For example, users with the GlobalMod role can access everything that's
// granted to Designers, Translators, Newswriters, ProofMods, etc.
//
// This difference is reflected in this api by having two functions:
// - user_has_role($user, $role): Checks if the user has the role or not
// - user_has_access($user, $role): Checks if the user can access spaces restricted to that role
//
// user_has_access is used in authorization checks. user_has_role isn't really authorization related, but
// it lives in this class because it's close enough. Maybe it should live in the User class instead.
//
// AddToUserGroup and RemoveFromUserGroup also don't really belong here, they're helper functions
// to add/remove roles to users. They could also live in the User class (User::add_role/remove_role).
// UserGroupMask is an optimized WHERE filter, should also be moved.
//
class Authorization {
  // Role based authorization

  // Checks if the current user has one of the given roles or
  // if their id matches the given user id. If authorization fails,
  // render an "unauthorized" page.
  public static function authorize($role_or_roles, $expected_user_id = -1) {
    if (!self::has_access($role_or_roles, $expected_user_id)) {
      $needs_login = ($role_or_roles == 'User');

      http_response_code(401);
      render_with('home/unauthorized', [
        'needs_login' => $needs_login,
        'referrer' => $_SERVER['REQUEST_URI'],
      ]);
      HTTPResponse::Exit();
    }
  }

  // Checks if the current user has access to functions restricted to these roles
  public static function has_access($role_or_roles, $expected_user_id = -1) {
    global $current_user;
    return self::user_has_access($current_user, $role_or_roles, $expected_user_id);
  }

  // Checks if the given user has access to functions restricted to these roles
  public static function user_has_access($user, $role_or_roles, $expected_user_id = -1) {
    if (is_string($role_or_roles)) {
      $role_or_roles = [$role_or_roles];
    }

    if ($user && $user['user_id'] == $expected_user_id) {
      return true;
    }

    // Roles have a hierarchy. Admins can do everything that GlobalMods do,
    // GlobalMods can do everything that GameMods do, etc. This defines the
    // hierarchy.
    //
    // Each role line indicates which roles can do the actions specified by
    // that role. For example, the GlobalMod line has 1s in the Admin,
    // GlobalMod, and Dev columns. This means that a user with one of those
    // three roles can act as a GlobalMod.
    //
    // Curator role and Dev role have specific handling here, Developer gets
    // access to everything for development reasons, and curator role does
    // not get widespread access to permissions in all contexts.
    static $roles  = [ // OAGDCUPMNDT
      'Translator' => 0b0001110000001,
      'Designer'   => 0b0001110001010,
      'Newswriter' => 0b0001110000100,
      'GameMod'    => 0b0001110001000,
      'ProofMod'   => 0b0001110010000,
      'UserMod'    => 0b0001110100000,
      'Curator'    => 0b0001111000000,
      'Dev'        => 0b0000010000000,
      'GlobalMod'  => 0b0001110000000,
      'Admin'      => 0b0001010000000,
      'Owner'      => 0b0010010000000,
    ];

    foreach ($role_or_roles as $role) {
      // We have a few virtual roles:
      // - Guest: anonymous (not logged in) users.
      // - UnverifiedUser: authenticated users that need to verify their email.
      // - User: authenticated users that have verified their email.

      if ($role == 'Guest') return true;
      if ($role == 'UnverifiedUser' && $user !== NULL) return true;
      if ($role == 'User' && $user !== NULL && User::IsVerified($user)) return true;
      if ($user && ($user['user_groups'] & $roles[$role])) return true;
    }

    return false;
  }

  // Structurally, usergroups are stored using a binary system. Each usergroup adds the
  // value of `2^n` with each value of `n` being unique to a specific usergroup
  // according to the following table. This system allows us to store any combination
  // of usergroups into a single integer. This function transforms a string into the required
  // binary value.
  public static function ReadUserGroup($user_group_string) {
    switch ($user_group_string) {
    case 'Owner':      return 1024;
    case 'Admin':      return 512;
    case 'GlobalMod':  return 256;
    case 'Dev':        return 128;
    case 'Curator':    return 64;
    case 'UserMod':    return 32;
    case 'ProofMod':   return 16;
    case 'GameMod':    return 8;
    case 'Newswriter': return 4;
    case 'Designer':   return 2;
    case 'Translator': return 1;
    }
  }

  // Token based authorization

  public static function require_all_api_permissions($permission_or_permissions) {
    if (!self::has_all_permissions($permission_or_permissions)) {
      http_response_code(401);
      render_json(['error' => "Request unauthorized", 'required' => $permission_or_permissions]);

      global $cs;
      $cs->Exit();
    }
  }

  // Checks if the current user token has **ALL** of the given permissions.
  public static function has_all_permissions($permission_or_permissions) {
    global $current_user_token;

    if (is_string($permission_or_permissions)) {
      $permission_or_permissions = [$permission_or_permissions];
    }

    $current_permissions = explode(",", $current_user_token['permissions'] ?? "");

    foreach ($permission_or_permissions as $permission) {
      if (!in_array($permission, $current_permissions)) {
        return false;
      }
    }

    return true;
  }

  // The following functions could probably be moved to the User class. They're
  // not strictly related to authorization.

  // Checks if the current user has one of these roles
  public static function has_role($role_or_roles) {
    global $current_user;
    return self::user_has_role($current_user, $role_or_roles);
  }

  // Checks if the current user has one of the given roles. Returns a boolean
  // describing authorization success for use in conditionals.
  public static function user_has_role($user, $role_or_roles) {
    if (is_string($role_or_roles)) {
      $role_or_roles = [$role_or_roles];
    }

    foreach ($role_or_roles as $role) {
      if (self::ReadUserGroup($role) & $user['user_groups']) {
        return true;
      }
    }

    return false;
  }

  // helper functions to add/remove roles to users. Maybe move to User class (User::add_role/remove_role).
  public static function AddToUserGroup($user, $role) {
    if (!self::user_has_role($user, $role)) {
      $new_user_groups = $user['user_groups'] + self::ReadUserGroup($role);
      database_update_by('users', ['user_groups' => $new_user_groups], ['user_id' => $user['user_id']]);
      return true;
    }
    return false;
  }

  // helper functions to add/remove roles to users. Maybe move to User class (User::add_role/remove_role).
  public static function RemoveFromUserGroup($user, $role) {
    if (self::user_has_role($user, $role)) {
      $new_user_groups = $user['user_groups'] - self::ReadUserGroup($role);
      database_update_by('users', ['user_groups' => $new_user_groups], ['user_id' => $user['user_id']]);
      return true;
    }
    return false;
  }

  // This function prepares a minor query optimization for staff notification handling
  public static function UserGroupMask($group_names) {
    $group_mask = 0;

    foreach ($group_names as $group_name) {
      $group_mask |= self::ReadUserGroup($group_name);
    }

    return $group_mask;
  }
}
