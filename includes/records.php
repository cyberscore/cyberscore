<?php

/* * **************************************************************************
  THIS FUNCTION REBUILDS THE CHART ACCORDING TO THE COLOURTHIEF 2005 ALGORITHM
 * ************************************************************************** */

function chart_build($chart_id) {
    $scores_array = [];

    $chart = database_fetch_one("SELECT * FROM levels JOIN chart_modifiers2 USING(level_id) WHERE level_id = ?", [$chart_id]);
    $chart_data = barebones_chart_load($chart);

    if ($chart['quantification_factor'] != NULL) {
      $quantifier = new Expression($chart['quantification_factor']);
    } else {
      $quantifier = NULL;
    }

    $num_subs = count($chart_data);
    if ($num_subs > 0) {
        // TODO: move query to join on $chart
        [$final_modifier, $_modifier_list, $modifier_set] = Modifiers::ChartModifiers($chart);
        $positions = [];
        $num_tied = 0;
        $rank = new Aggregates\Rank();
        $oldest_submission_date = new Aggregates\Minimum();
        foreach ($chart_data as $record) {
            $chart_pos = $rank->update([$record['submission'], $record['submission2']]);
            $oldest_submission_date->update($record['original_date']);

            $positions []= $chart_pos;
            if ($chart_pos == 1) {
              $num_tied += 1;
            }
        }

        $last_place = $positions[count($positions)-1];
        foreach ($chart_data as $i => $record) {
            $chart_pos = $positions[$i];

            if ($chart['chart_type2'] != 0 && $chart['conversion_factor'] == 0) {
              if ($quantifier) {
                $scores_array[] = $quantifier->apply(['x' => $record['submission'], 'y' => $record['submission2']]);
              } else {
                $scores_array[] = $last_place - $chart_pos + 1;
              }
            } else {
              $scores_array[] = $record['submission'];
            }
        }

        // determine if we award platinums & speedrun bonus for top positions
        $required_subs = 5 * pow($num_tied, 2);
        if ($modifier_set['challenge']) {
          $required_subs = $required_subs * 2;
        }
        $award_platinum = ($num_subs >= $required_subs && !$modifier_set['unranked']);
        $speedrun_bonus = $modifier_set['speedrun'];

        // Determine secondary award modifier based on part-game and/or full-game modifiers. Body of Work modifiers are not currently a secondary award multiplier.
        if($modifier_set['full_game']) { $secondary_modifier_value = 1.25; }
        else if($modifier_set['part_game']) { $secondary_modifier_value = 1.10; }
        else $secondary_modifier_value = 1.00;

        // Time to actually fetch all your rewards
        $points = get_cs_points($scores_array, $chart_id, $final_modifier, $speedrun_bonus, $award_platinum, $secondary_modifier_value);

        $keys = [];
        $fields = [];
        for ($i = 0; $i < $num_subs; $i++) {
          $keys []= intval($chart_data[$i]['record_id']);
          $fields []= array_merge($points[$i], [
            'chart_pos' => $positions[$i],
            'platinum' => ($award_platinum && $positions[$i] == 1 ? 1 : 0),
            'chart_subs' => $num_subs,
            'first_submitted' => $oldest_submission_date->min != null && $chart_data[$i]['original_date'] == $oldest_submission_date->min,
            'ranked' => $chart['ranked'],
          ]);
        }

        $decimals = get_max_decimals($chart_data);
        $last_update = max(pluck($chart_data, 'last_update'));

        database_update_all('records', 'record_id', $keys, $fields);
        database_update_by('levels', [
          'decimals' => $decimals,
          'num_subs' => $num_subs,
          'last_sub_date' => $last_update,
        ], ['level_id' => $chart_id]);
    } else {
        database_update_by('levels', [
          'decimals' => 0,
          'num_subs' => 0,
          'last_sub_date' => null
        ], ['level_id' => $chart_id]);
    }

    GameCacheRebuilder::QueueGameForRebuild($chart['game_id']);
}

function chart_rebuild_vs_cxp($chart_id) {
  $beats = database_fetch_all("
    SELECT
      records.chart_pos,
      records.record_id,
      records.cxp,
      SUM(sb_cache_incremental.base_level) OVER(ORDER BY records.chart_pos DESC) AS user_levels_below_or_tied,
      SUM(sb_cache_incremental.base_level) OVER(PARTITION BY records.chart_pos) AS user_levels_tied,
      sb_cache_incremental.base_level AS own_user_level
    FROM records
    LEFT JOIN sb_cache_incremental USING (user_id)
    WHERE records.level_id = ?
    ORDER BY records.chart_pos DESC
  ", [$chart_id]);

  $keys = [];
  $fields = [];
  foreach ($beats as $beat) {
    $keys  []= $beat['record_id'];
    $fields []= ['vs_cxp' => Award::VersusExperience(
      $beat['user_levels_below_or_tied'] - $beat['user_levels_tied'],
      $beat['user_levels_tied'],
      $beat['own_user_level'],
      $beat['cxp'],
    )];
  }

  database_update_all('records', 'record_id', $keys, $fields);
}

function get_max_decimals($chart_data) {
  $decimals = 0;

  foreach ($chart_data as $record) {
    if ($record['real_sub'] !== null) {
      $decimals = max($decimals, GetNumOfDecimals($record['real_sub']));
    }
  }

  return $decimals;
}

/****************************************************************************
  THIS IS THE FUNCTION WHICH RETURNS THE CS POINTS. SERIOUS PLAYTIME.
 ****************************************************************************/

function get_cs_points($scores, $chart_id, $chart_modifier, $speedrun_flag = FALSE, $platinum_check = FALSE, $secondary_modifier_value = 1.00) {
    global $r;

    $points = [];

    $num_subs = count($scores);

    $median = Aggregates\Median::Calculate($scores);

    for ($i = 0; $i < $num_subs; $i += $numtied) {
        $numtied = 1;
        while ($i + $numtied < $num_subs && $scores[$i] === $scores[$i + $numtied]) {
            $numtied++;
        }

        // WHEN A PLACE IS TIED, THE AVERAGE POSITION IS TAKEN, AS IN 3 PLAYERS IN 2ND ARE CONSIDERED 3RD PLACE
        $this_place = $i + 1 + ($numtied - 1) / 2;

        // HANDLE INTERMEDIATE ENTRIES
        if (($i != 0) && ($i + $numtied < $num_subs)) {
            // DIFF RATIO IS BETWEEN 0 and 1
            // CLOSE TO 0 INDICATES THAT THE RECORD IS VERY CLOSE TO THE RECORD ABOVE
            // CLOSE TO 1 INDICATES THAT THE RECORD IS VERY CLOSE TO THE RECORD BELOW
            // THE FINAL - 0.5 MAPS THAT ONTO WINNING OR LOSING HALF A PLACE MAX FOR THE RATIO
            $this_place -= ($scores[$i] - $scores[$i + $numtied]) / ($scores[$i - 1] - $scores[$i + $numtied]) - 0.5;
        }

        // Generate points values
        $ucsp          = Award::UCSP($this_place, $num_subs);
        $csp           = Award::ModifyCSP($ucsp, $chart_modifier, $speedrun_flag, $platinum_check, $i);
        $arcade        = Award::Tokens($this_place, $num_subs, $secondary_modifier_value);
        $solution      = Award::BrainPower($i + 1, $num_subs, $numtied, $secondary_modifier_value);
        $challenge     = Award::StylePoints($this_place, $num_subs, $secondary_modifier_value);
        $medal_points  = Award::MedalPoints($i + 1, $num_subs, $numtied, $secondary_modifier_value);
        $collectibles  = Award::Collectibles($i + 1, $chart_id, $scores[$i], $scores[0], $secondary_modifier_value);
        $experience    = Award::Experience($i + 1, $median, $scores[$i], $secondary_modifier_value);

        for ($j = $i; $j < ($i + $numtied); $j++) {
          $points []= [
            'csp'           => $csp,
            'ucsp'          => $ucsp,
            'arcade_points' => $arcade,
            'brain_power'   => $solution,
            'style_points'  => $challenge,
            'medal_points'  => $medal_points,
            'cyberstars'    => $collectibles,
            'cxp'           => $experience,
            'vs_cxp'        => 0,
          ];
        }
    }

    return $points;
}

/* * *****************************************************************
  LOAD BAREBONES CHART FOR REBUILDING
 * ***************************************************************** */

function barebones_chart_load($chart) {
  $chart_id = $chart['level_id'];
  $chart_type = $chart['chart_type'];
  $chart_type2 = $chart['chart_type2'];
  $game_id = $chart['game_id'];
  $conversion_factor = $chart['conversion_factor'];
  $num_decimals = $chart['num_decimals'];

  /* chart_type enum:
   * - 0: null
   * - 1: Time shorter
   * - 2: Time longer
   * - 3: Score lower
   * - 4: Score higher
   * - 5: Rank
   */

  if ($conversion_factor == 0) {
    // submission1 and submission2 are independent
    if ($chart_type == 1 || $chart_type == 3) {
      $sort_order = "submission IS NULL ASC, submission ASC";
    } else {
      $sort_order = "submission IS NULL ASC, submission DESC";
    }

    // if there is a submission2:
    if ($chart_type2 != 0) {
      if ($chart_type2 == 1 || $chart_type2 == 3) {
        $sort_order .= ", submission2 IS NULL ASC, submission2 ASC";
      } else {
        $sort_order .= ", submission2 IS NULL ASC, submission2 DESC";
      }
    }

    return database_fetch_all("
      SELECT
        record_id,
        user_id,
        submission,
        submission2,
        submission AS real_sub,
        submission2 AS real_sub2,
        extra1,
        extra2,
        extra3,
        original_date,
        last_update
      FROM records
      WHERE level_id = ?
      ORDER BY $sort_order, last_update ASC
    ", [$chart_id]);
  } else {
    // submission1 and submission2 are the same value in different units
    // so we only sort by submission, which is overloaded below to mean
    // submission2 if submission is null.
    $submission_expr = "ROUND(COALESCE(submission, submission2 / $conversion_factor), $num_decimals)";

    if ($chart_type2 != 0) {
      if ($chart_type2 == 1 || $chart_type2 == 3) {
        $sort_order = "$submission_expr IS NULL ASC, $submission_expr ASC";
      } else {
        $sort_order = "$submission_expr IS NULL ASC, $submission_expr DESC";
      }
    }

    return database_fetch_all("
      SELECT
        record_id,
        user_id,
        $submission_expr AS submission,
        NULL AS submission2,
        submission AS real_sub,
        submission2 AS real_sub2,
        extra1,
        extra2,
        extra3,
        original_date,
        last_update
      FROM records
      WHERE level_id = ?
      ORDER BY $sort_order, last_update ASC
    ", [$chart_id]);
  }
}
