<?php

require_once("records.php");

class ChartCacheRebuilder {
  // Rebuild a single chart immediately (skipping the queue)
  public static function RebuildChart($chart_id) {
    chart_build($chart_id);
    database_delete_by('chartrebuilder', ['chart_id' => $chart_id]);
  }

  public static function QueueChartForRebuild($chart_id) {
    db_query("INSERT IGNORE INTO chartrebuilder (chart_id) VALUES (?)", [$chart_id]);
  }

  public static function QueueMultipleChartsForRebuild($chart_ids) {
    foreach ($chart_ids as $chart_id) {
      self::QueueChartForRebuild($chart_id);
    }
  }

  public static function QueueAllChartsForRebuild() {
    $chart_ids = pluck(database_fetch_all("SELECT level_id FROM levels", []), 'level_id');
    self::QueueMultipleChartsForRebuild($chart_ids);
  }

  public static function QueueGameChartsForRebuild($game_id) {
    $chart_ids = pluck(database_fetch_all("SELECT level_id FROM levels WHERE game_id = ?", [$game_id]), 'level_id');
    self::QueueMultipleChartsForRebuild($chart_ids);
  }

  public static function QueueChartRangeForRebuild($lowermost_chart, $uppermost_chart) {
    $chart_ids = pluck(database_fetch_all("SELECT level_id FROM levels WHERE level_id >= ? AND level_id <= ?", [$lowermost_chart, $uppermost_chart]), 'level_id');
    self::QueueMultipleChartsForRebuild($chart_ids);
  }
}
