<?php

// This file has a bunch of stuff that would probably be happier in other
// helper files.

/********************************************************************
 * FUNCTION:  get_ip
 * PURPOSE:   Works out a user's IP number
 * ARGUMENTS: None
 * RETURNS:   $ip (string) - IP number
 ********************************************************************/
function get_ip()
{
  // If we ever start being behind a proxy, we'd need
  // a configuration setting to let us know that we should
  // be looking at HTTP_X_FORWARDED_FOR instead (and in that
  // case, REMOTE_ADDR should be ignored)
  return $_SERVER['REMOTE_ADDR'];
}

//-----------------------------------------------------------------------
function GetNumOfDecimals($number) {
  if (floor($number) == $number) {
    return 0;
  }

  $decimal = substr($number, strrpos($number, '.') + 1, 3);
  return strlen($decimal);
}

/********************************************************************
 * FUNCTION:  nth
 * PURPOSE:   Returns the ordinal suffix
 * ARGUMENTS: $in (int) - number to be suffixed
 * RETURNS:   suffixed number
 ********************************************************************/
function nth(int|string $in): string {
  if (is_string($in)) {
    $in = intval($in);
  }

  if ($in == 0)				 										return "Not registered";
  else if($in % 100 == 11 || $in % 100 == 12 || $in % 100 == 13)		return $in . "th";
  else if($in % 10 == 1)												return $in . "st";
  else if($in % 10 == 2)												return $in . "nd";
  else if($in % 10 == 3)												return $in . "rd";
  else																return $in . "th";
}

function plural_word($count, $word)
{
	if($count == 1)		{ return $word;}
	else				{ return $word . "s";}
}

/************************************
 CONVERTS THE NUMBER TO A H:M.S VALUE
 ************************************/
function number_to_time($number, $purpose, $decimals = 0)
{
	if($purpose == 1)
	{
		// Negative times
		if($number < 0)		return '- '.number_to_time($number * -1, 1, $decimals);
		
		// FOR DISPLAY IN THE CHART
		$hours = floor($number / 3600);
		$mins  = floor(($number - ($hours * 3600)) / 60);
		$secs = round($number - (($hours * 3600) + ($mins * 60)),3);
		$secs = ($secs < 10 ? "0".$secs : $secs);
		
		if($decimals && strpos($secs, '.') === false)
			$secs .= '.';
		while(strlen($secs) - (strpos($secs, '.') + 1) < $decimals)
			$secs .= '0';
		
		if ($hours > 0)
		{
			$mins = ($mins < 10 ? "0".$mins : $mins);
			return $hours . ":" . $mins . ":" . $secs;
		}
		else
		{
			return $mins . ":" . $secs;
		}
	}
	
	else
	{
		$mins = floor($number / 60);
		$secs = floor($number - ($mins * 60));
		$secs = ($secs < 10 ? "0".$secs : $secs);
		$this_decimals = floor( round($number - floor($number), 3) * 1000 );
		if($this_decimals < 10)				{ $this_decimals = '00'.$this_decimals;}
		else if($this_decimals < 100)		{ $this_decimals = '0'.$this_decimals;}
		
		// FOR DISPLAY IN THE SUBMISSION FORM
		return array($mins, $secs, $this_decimals);
	}
}


//------------------------------------- BBCode ------------------------------------
function prettify($message, $quotestring = null) {
  $message = htmlspecialchars($message, ENT_NOQUOTES);

  $message = nl2br($message);
  $initialquotecount = min(substr_count($message,"[quote]"),substr_count($message,"[/quote]"));
  $i = 1;

  while($i <= $initialquotecount) {
  	$i++;
  	$message = substr_replace($message,"<div class=\"csmailquote\"><strong><small>".$quotestring."</small></strong><br />",stripos($message,"[quote]"),7);
  	$message = substr_replace($message,"</div>",stripos($message,"[/quote]"),8);
  }

  //Define BB-code
  $bb = array(
    '@\[u\](.*?)\[\/u\]@is',
    '@\[i\](.*?)\[\/i\]@is',
    '@\[b\](.*?)\[\/b\]@is',
    '@\[s\](.*?)\[\/s\]@is',
    '@\[blue\](.*?)\[\/blue\]@is',
    '@\[orange\](.*?)\[\/orange\]@is',
    '@\[green\](.*?)\[\/green\]@is',
    '@\[purple\](.*?)\[\/purple\]@is',
    '@\[red\](.*?)\[\/red\]@is',
    '@\[bold_blue\](.*?)\[\/bold_blue\]@is',
    '@\[bold_orange\](.*?)\[\/bold_orange\]@is',
    '@\[bold_green\](.*?)\[\/bold_green\]@is',
    '@\[bold_purple\](.*?)\[\/bold_purple\]@is',
    '@\[bold_red\](.*?)\[\/bold_red\]@is',
    '@\[url\](.*?)\[\/url\]@is',
    '@\[url=(.*?)\](.*?)\[\/url\]@is',
    '@\[color=(.*?)\](.*?)\[\/color\]@is',
    '@\[colour=(.*?)\](.*?)\[\/colour\]@is',
    '@\[size=(.*?)\](.*?)\[\/size\]@is',
    '@\[img\](.*?)\[\/img\]@is',
    '@\[code\](.*?)\[\/code\]@is',
    '@\[c\](.*?)\[\/c\]@is',
    '@\[quote="(.*?)"\](.*?)\[\/quote\]@is',
    '@\[ol\](.*?)\[\/ol\]@is',
    '@\[ul\](.*?)\[\/ul\]@is',
    '@\[li\](.*?)\[\/li\]@is',
    '@\[youtube\](.*?)\[\/youtube\]@is',
  );

  //Define allowed HTML
  $html = array(
    '<u>$1</u>',
    '<em>$1</em>',
    '<strong>$1</strong>',
    '<s>$1</s>',
    '<span style="color: #23528a;">$1</span>',
    '<span style="color: #ff8100;">$1</span>',
    '<span style="color: #199600;">$1</span>',
    '<span style="color: #9700db;">$1</span>',
    '<span style="color: #ff0000;">$1</span>',
    '<span style="color: #23528a; font-weight: bold">$1</span>',
    '<span style="color: #ff8100; font-weight: bold">$1</span>',
    '<span style="color: #199600; font-weight: bold">$1</span>',
    '<span style="color: #9700db; font-weight: bold">$1</span>',
    '<span style="color: #ff0000; font-weight: bold">$1</span>',
    '<a href="$1">$1</a>',
    '<a href="$1">$2</a>',
    '<span style="color: $1;">$2</span>',
    '<span style="color: $1;">$2</span>',
    '<span style="font-size: $1px;">$2</span>',
    '<img src="$1"/>',
    '<div class="bbcodecode"><h2 class="inset_heading_code">CODE</h1><pre><code>$1</code></pre></div>',
    '<span style="background-color: #e6e6ff;"><code>$1</code></span>',
    '<div class="bbcodecode"><h2 class="inset_heading_quote">QUOTE <small>($1)</small></h1>"$2"</div>',
    '<ol>$1</ol>',
    '<ul>$1</ul>',
    '<li>$1</li>',
    '<iframe width="420" height="315" src="https://www.youtube.com/embed/$1"></iframe>'
  );

  $message = preg_replace($bb, $html, $message);

  $message = preg_replace_callback_array(
    [
      '/\[user=(\d+?)\]/is' => function($m) { return Article::FormatNews_User($m); },
      '/\[game=(\d+?)\]/' => function($m) { return Article::FormatNews_Game($m); },
      '/\[gameinfo=(\d+?)\]/' => function($m) { return Article::FormatNews_GameInfo($m); },
      '/\[gameinfo_rb=(\d+?)\]/' => function($m) { return Article::FormatNews_GameInfoRankbutton($m); },
      '/\[gameinfo_boxart=(\d+?)\]/' => function($m) { return Article::FormatNews_GameInfoBoxart($m); }
    ],
    $message
  );

  return $message;
}

//---------------------------------------------------------------------------------
function imagettftextc($image, $size, $angle, $x, $y, $color, $fontfile, $text) {
  $bbox = imagettfbbox($size, $angle, $fontfile, $text);
  imagettftext($image, $size, $angle, round($x - ($bbox[4] / 2), 0), round($y - ($bbox[5] / 2), 0), $color, $fontfile, $text);
}

if (!function_exists('str_starts_with')) {
  function str_starts_with($str, $prefix) {
    return strpos($str, $prefix) === 0;
  }
}

// based on original work from the PHP Laravel framework
if (!function_exists('str_contains')) {
  function str_contains($haystack, $needle) {
    return $needle !== '' && mb_strpos($haystack, $needle) !== false;
  }
}

if (!function_exists('str_ends_with')) {
  function str_ends_with($str, $suffix) {
    return strpos($str, $suffix) === strlen($str) - strlen($suffix);
  }
}
