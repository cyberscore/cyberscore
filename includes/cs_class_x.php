<?php

class CSClass {
  // User info
  public $user_id = 0;

  // User prefs
  private $show_full_names = true;
  private $show_entity_icons = true;

  // This is a global translator cache, not scoped to current_user
  private $translator_infos = null;

  private $friends_cache = null;

  public function ResetUser() {
    $this->user_id = 0;
    $this->show_full_names = true;
    $this->show_entity_icons = true;
  }

  public function LoadUser($current_user) {
    $this->user_id = $current_user['user_id'];
    $this->show_full_names = $current_user['show_full_names'];
    $this->show_entity_icons = $current_user['show_entity_icons'];
  }

  //TODO - Migrate this to translation class?
  public function LoadTranslatorInfos() {
    $translators = database_fetch_all("
      SELECT translator_info.user_id, GROUP_CONCAT(translator_info.language_code ORDER BY translator_info.language_code ASC SEPARATOR ',') AS language_codes
      FROM translator_info
      JOIN languages USING(language_code)
      WHERE languages.available = 'y'
      GROUP BY translator_info.user_id
    ", []);

    foreach ($translators as $translator) {
      $this->translator_infos[$translator['user_id']] = explode(',', $translator['language_codes']);
    }
  }

  /********************************************************************************************
  COMMON FUNCTIONS
  ********************************************************************************************/
  public function GetStarIcons($user) {
    $pink = '/images/Star_Pink16.png';
    $red = '/images/Star_Red16.png';
    $blue = '/images/Star_Blue16.png';
    $violet = '/images/Star_Violet16.png';
    $magenta = '/images/Star_Magenta16.png';
    $lightblue = '/images/Star_LightBlue16.png';
    $teal = '/images/Star_LightGreen16.png';
    $green = '/images/Star_Green16.png';
    $black = '/images/Star_Black16.png';
    $yellow = '/images/Star_Yellow16.png';
    $orange = '/images/Star_Orange16.png';
    $white = '/images/Star_White16.png'; // The White star is currently not in use for a user-role.

    $stars = [];

    if (Authorization::user_has_role($user, 'Owner'))      array_push($stars, array('src' => $pink, 'title' => 'Owner', 'alt' => 'Owner'));
    if (Authorization::user_has_role($user, 'Admin'))      array_push($stars, array('src' => $red, 'title' => 'Administrator', 'alt' => 'Administrator'));
    if (Authorization::user_has_role($user, 'GlobalMod'))  array_push($stars, array('src' => $blue, 'title' => 'Global Moderator', 'alt' => 'Global Moderator'));
    if (Authorization::user_has_role($user, 'Dev'))        array_push($stars, array('src' => $violet, 'title' => 'Developer', 'alt' => 'Developer'));
    if (Authorization::user_has_role($user, 'Curator'))    array_push($stars, array('src' => $magenta, 'title' => 'Curator', 'alt' => 'Curator'));
    if (Authorization::user_has_role($user, 'UserMod'))    array_push($stars, array('src' => $lightblue, 'title' => 'User Moderator', 'alt' => 'User Moderator'));
    if (Authorization::user_has_role($user, 'ProofMod'))   array_push($stars, array('src' => $teal, 'title' => 'Proof Moderator', 'alt' => 'Proof Moderator'));
    if (Authorization::user_has_role($user, 'GameMod'))    array_push($stars, array('src' => $green, 'title' => 'Game Moderator', 'alt' => 'Game Moderator'));
    if (Authorization::user_has_role($user, 'Newswriter')) array_push($stars, array('src' => $black, 'title' => 'Newswriter', 'alt' => 'Newswriter'));
    if (Authorization::user_has_role($user, 'Designer'))   array_push($stars, array('src' => $yellow, 'title' => 'Designer', 'alt' => 'Designer'));

    if ($this->translator_infos == null) $this->LoadTranslatorInfos();
    if (isset($this->translator_infos[$user['user_id']])) {
      $languages = $this->translator_infos[$user['user_id']];
      array_push($stars, array('src' => $orange, 'title' => 'Translator', 'alt' => 'Translator', 'languages' => $languages));
    }

    return $stars;
  }

  public function GetChartIcon($user) {
    return ChartIcon::get($user);
  }

  public function GetUserPic($user_info = null) {
    global $config;

    if ($user_info == null) {
      $user_info = ['user_id' => $this->user_id, 'updated_at' => null];
    }

    if (file_exists("{$config['app']['root']}/public/userpics/{$user_info['user_id']}.jpg")) {
      $timestamp = (new DateTime($user_info['updated_at'] ?? "now"))->format('U');

      return "/userpics/{$user_info['user_id']}.jpg?{$timestamp}";
    } else {
      return "/userpics/nopic-male.png";
    }
  }

  public function GetBannerURL($user_info = null) {
    global $config;

    if ($user_info == null) {
      $user_info = ['user_id' => $this->user_id, 'updated_at' => null];
    }

    if (file_exists("{$config['app']['root']}/public/userpics/banner/{$user_info['user_id']}.jpg")) {
      $timestamp = (new DateTime($user_info['updated_at'] ?? "now"))->format('U');

      return "/userpics/banner/{$user_info['user_id']}.jpg?{$timestamp}";
    } else {
      return "/userpics/nopic-male.png";
    }
  }

  public function GetUserFriends($user_id) {
    if ($this->friends_cache !== NULL) {
      return $this->friends_cache;
    }

    $friends = database_fetch_all("SELECT friend_id FROM user_friends WHERE user_id = ?", [$user_id]);
    $this->friends_cache = pluck($friends, 'friend_id');
    return $this->friends_cache;
  }

  public function GetBoxArts($game_id) {
    $boxarts = database_fetch_all("SELECT * FROM boxarts WHERE game_id = ?", [$game_id]);

    foreach ($boxarts as &$boxart) {
      if ($boxart['remote']) {
        // TODO
        // "https://s3.eu-west-2.amazonaws.com/cyberscoreproofs/backup/boxarts";
      } else {
        $boxart['url'] = "/uploads/boxarts/{$boxart['sha256']}";
      }
    }
    unset($boxart);

    shuffle($boxarts);
    return $boxarts;
  }

  public function GetRankbuttons($game_id) {
    global $config;
    $root = $config['app']['root'];

    $rankbuttons = [];

    for ($i = 1; $i <= 3; $i++) {
      $rankbutton = "rankbuttons/$i/$game_id.gif";
      $path = "$root/public/$rankbutton";

      if (file_exists($path)) {
        $rankbuttons []= [
          'url' => "/$rankbutton",
          'local_path' => $path,
        ];
      }
    }

    return $rankbuttons;
  }

  public static function GetHomepics($small = true) {
    global $config;
    $root = $config['app']['root'];

    if ($small) {
      $files = array_merge(glob("$root/public/uploads/homepics/small/*"));
    } else {
      $files = array_merge(glob("$root/public/uploads/homepics/big/*"));
    }
    natsort($files);

    return array_map(fn($path) => [
      'local_path' => $path,
      'url' => str_replace("$root/public", "", $path),
      'mtime' => filemtime($path),
    ], $files);
  }

  /********************************************************************************************
    FORMATING
    ********************************************************************************************/
  public function FormatRanking($data, $offset = 0) {
    $formated_ranking = [];
    $viewer_friends = $this->GetUserFriends($this->user_id);

    $last_score = $last_pos = -1;
    $pos = 1 + $offset;
    foreach ($data as $this_data) {
      if (isset($this_data['scoreboard_pos'])) {
        $this_pos = $this_data['scoreboard_pos'];
      } else if (isset($this_data['score'])) { // Look out for ties, if you have a score
        $this_pos = $last_score == $this_data['score'] ? $last_pos : $pos;
        $last_score = $this_data['score']; $last_pos = $this_pos;
      } else if (isset($this_data['gold'])) { // Look for medal ties
        if (!isset($this_data['platinum'])) $this_data['platinum'] = 0;
        if (!isset($this_data['fourth']))   $this_data['fourth'] = 0;
        if (!isset($this_data['fifth']))    $this_data['fifth'] = 0;

        $this_score = array($this_data['platinum'], $this_data['gold'], $this_data['silver'], $this_data['bronze'], $this_data['fourth'], $this_data['fifth']);
        $this_pos = $last_score == $this_score ? $last_pos : $pos;
        $last_score = $this_score;
        $last_pos = $this_pos;
      } else {
        $this_pos = $pos;
      }

      if (($this_data['display_trophies'] ?? NULL) !== NULL) {
        if ($this_data['trophy_type'] == 'platinum') {
          $this_data['award'] = ['type' => 'trophy', 'place' => 'platinum'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("trophy_1.png") . '" width="40" height="40" alt="platinum medal" />';
        } else if ($this_data['trophy_type'] == 'gold') {
          $this_data['award'] = ['type' => 'trophy', 'place' => 'gold'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("trophy_2.png") . '" width="40" height="40" alt="gold medal" />';
        } else if ($this_data['trophy_type'] == 'silver') {
          $this_data['award'] = ['type' => 'trophy', 'place' => 'silver'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("trophy_3.png") . '" width="40" height="40" alt="silver medal" />';
        } else if ($this_data['trophy_type'] == 'bronze') {
          $this_data['award'] = ['type' => 'trophy', 'place' => 'bronze'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("trophy_4.png") . '" width="40" height="40" alt="bronze medal" />';
        } else if ($this_data['trophy_type'] == 'fourth') {
          $this_data['award'] = ['type' => 'trophy', 'place' => 'fourth'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("trophy_5.png") . '" width="40" height="40" alt="fourth place" />';
        } else if ($this_data['trophy_type'] == 'fifth') {
          $this_data['award'] = ['type' => 'trophy', 'place' => 'fifth'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("trophy_6.png") . '" width="40" height="40" alt="fifth place" />';
        } else {
          $this_data['pos_content'] = $this_pos;
        }
      } else { // No trophies available, displaying medals instead
        if ($this_pos == 1) {
          $this_data['award'] = ['type' => 'medal', 'place' => 'gold'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("goldmedal.png") . '" width="40" height="40" alt="gold medal" />';
        } else if ($this_pos == 2) {
          $this_data['award'] = ['type' => 'medal', 'place' => 'silver'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("silvermedal.png") . '" width="40" height="40" alt="silver medal" />';
        } else if ($this_pos == 3) {
          $this_data['award'] = ['type' => 'medal', 'place' => 'bronze'];
          $this_data['pos_content'] = '<img src="' . skin_image_url("bronzemedal.png") . '" width="40" height="40" alt="bronze medal" />';
        } else {
          $this_data['pos_content'] = $this_pos;
        }
      }

      $this_data['tr_class'] = 'entry';
      if ($this_pos == 1) {
        $this_data['tr_class'] .= ' gold';
      } else if ($this_pos == 2) {
        $this_data['tr_class'] .= ' silver';
      } else if ($this_pos == 3) {
        $this_data['tr_class'] .= ' bronze';
      } else if (isset($this_data['user_id'])) { // If this is a user scoreboard, hightlight yourself and your friends
        if ($this_data['user_id'] == $this->user_id) {
          $this_data['tr_class'] .= ' you';
        } else if (array_search($this_data['user_id'], $viewer_friends) !== false) {
          $this_data['tr_class'] .= ' friend';
        }
      }
      $this_data['tr_class'] = " class='{$this_data['tr_class']}'";

      if (isset($this_data['user_id'])) {
        if ($this->show_full_names == 'yes') {
          $this_data['display_name'] = trim($this_data['forename'] . " &ldquo;" . $this_data['username'] . "&rdquo; " . $this_data['surname']);
        } else {
          $this_data['display_name'] = "<code>" . h($this_data['username']) . "</code>";
        }
      }

      $formated_ranking []= $this_data;
      $pos++;
    }

    return $formated_ranking;
  }

  //-------------------------------------------------------------------------------------------
  private function FormatChart($data) {
    $formated_ranking = [];
    $viewer_friends = $this->GetUserFriends($this->user_id);

    if (count($data) > 0) {
      $awards = chart_awards(...Modifiers::ChartModifiers($data[0]));
    }

    foreach ($data as $this_data) {
      // Data wrangling
      if ($this_data['platinum'] && $awards['medals']) {
        $this_data['award'] = ['type' => 'medal', 'value' => 'platinum'];
      } else if ($this_data['chart_pos'] == 1 && $awards['medals']) {
        $this_data['award'] = ['type' => 'medal', 'value' => 'gold'];
      }  else if ($this_data['chart_pos'] == 2 && $awards['medals']) {
        $this_data['award'] = ['type' => 'medal', 'value' => 'silver'];
      }  else if ($this_data['chart_pos'] == 3 && $awards['medals']) {
        $this_data['award'] = ['type' => 'medal', 'value' => 'bronze'];
      } else {
        $this_data['award'] = null;
      }

      $this_data['proof_details'] = ProofManager::ProofDetails($this_data);

      // Display stuff

      if ($this_data['proof_details'] != null) {
        $this_data['linked_proof_display'] = '<a href="/proofs/' . $this_data['record_id'] . '" target="_blank"><img src="' . $this_data['proof_details']['icon'] . '" width="16" height="16" alt="Proof" title="View proof" /></a>';
      } else {
        $this_data['linked_proof_display'] = '';
      }

      // Username
      if ($this->show_full_names) {
        $this_data['display_name'] = trim(h($this_data['forename'] ?? "") . " &ldquo;" . h($this_data['username']) . "&rdquo; " . h($this_data['surname'] ?? ""));
      } else {
        $this_data['display_name'] = "<code>" . h($this_data['username']) . "</code>";
      }

      // Multiplayer formatting
      if ($this_data['players'] == 1) {
        $this_data['user_display'] = '<a href="/user/' . $this_data['user_id'] . '"><b>' . $this_data['display_name'] . '</b></a>';
      } else {
        $this_data['user_display'] = '<a href="/user/' . $this_data['user_id'] . '">' . $this_data['username'] . '</a>';

        if ($this_data['other_user'] != '') {
          $this_data['user_display'] .= ' <small>(with ' . $this_data['other_user'] . ')</small>';
        }
      }

      // Rec status icon
      switch($this_data['rec_status']) {
      case 1: $this_data['rec_status_display'] = '<img src="' . skin_image_url("error.png") . '" width="16" height="16" alt="Pending investigation" title="Pending investigation" /> '; break;
      case 2: $this_data['rec_status_display'] = '<img src="' . skin_image_url("exclamation.png") . '" width="16" height="16" alt="Under investigation" title="Under investigation" /> '; break;
      case 3: $this_data['rec_status_display'] = '<img src="' . skin_image_url("tick.png") . '" width="16" height="16" alt="Record approved" title="' . $this->ReadProofType($this_data['proof_type']) . '" /> '; break;
      default: $this_data['rec_status_display'] = ''; break;
      }

      $formated_ranking []= $this_data;
    }
    return $formated_ranking;
  }

  //-------------------------------------------------------------------------------------------
  public static function GetConvertedSubmission($submission, $other_submission, $conversion_factor, $goal, $num_decimals) {
    if ($submission !== null || $conversion_factor == 0) {
      return $submission;
    }

    // Convert if submission is null and conversion factor is defined
    $converted_submission = $goal == 1 ? $other_submission / $conversion_factor : $other_submission * $conversion_factor;
    return round($converted_submission, $num_decimals);
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatSubmission($submission, $chart_type, $decimals = null, $game_id = null, $defined_decimals = null) {
    if ($submission === null) { return NULL; }

    // Overwrite decimals...
    if ($decimals !== null && $decimals != -1) {
      $num_decimals = max(GetNumOfDecimals($submission), $decimals);
    } else if ($defined_decimals != null && $defined_decimals != -1) {
      $num_decimals = max(GetNumOfDecimals($submission), $defined_decimals);
    } else {
      $num_decimals = GetNumOfDecimals($submission);
    }

    /*
      0 null
      1 Time shorter
      2 Time longer
      3 Score lower
      4 Score higher
      5 Rank
     */

    switch($chart_type) {
    case 1:
    case 2:  return number_to_time($submission, 1, $num_decimals);
    case 3:
    case 4:  return number_format($submission, $num_decimals);
    case 5:  return is_numeric($submission) ? self::xxxGetRank($submission, $game_id) : $submission;
    default: return number_format($submission, $num_decimals);
    }
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatSubmissions($info, $prefix = '') {
    $chart = $info['chart'] ?? $info;
    $text = self::FormatSubmissionPart($info, 'primary', $prefix);
    if ($chart['chart_type2'] != 0) {
      $text .= ' / ' . self::FormatSubmissionPart($info, 'secondary', $prefix);
    }

    return $text;
  }

  public static function FormatSubmissionPart($info, $part, $prefix = '') {
    $chart = $info['chart'] ?? $info;
    if ($part == 1 || $part == 'primary') {
      $submission1 = self::GetConvertedSubmission($info["{$prefix}submission"] ?? $info["{$prefix}submission1"] ?? null, $info["{$prefix}submission2"], $chart['conversion_factor'] ?? 0, 1, $chart['num_decimals']);
      return $chart['score_prefix'] . self::FormatSubmission($submission1, $chart['chart_type']) . $chart['score_suffix'];
    } elseif ($part == 2 || $part == 'secondary') {
      $submission2 = self::GetConvertedSubmission($info["{$prefix}submission2"], $info["{$prefix}submission"] ?? $info["{$prefix}submission1"] ?? null, $chart['conversion_factor'] ?? 0, 2, $chart['num_decimals2']);
      return $chart['score_prefix2'] . self::FormatSubmission($submission2, $chart['chart_type2']) . $chart['score_suffix2'];
    } else {
      throw "Oh no";
    }
  }

  public static function SubmissionDistance($submission, $target, $chart_type) {
    if ($chart_type == 5) { return NULL; }

    return ($submission > $target ? '+' : '-') . self::FormatSubmission(abs($target - $submission), $chart_type);
  }

  //-------------------------------------------------------------------------------------------
  private static function xxxGetRank($submission, $game_id) {
    if ($game_id === null) {
      return '-';
    }

    return database_value("SELECT rank_name FROM game_ranks WHERE game_id = ? AND submission = ?", [$game_id, $submission]);
  }

  //-------------------------------------------------------------------------------------------
  public function ReadInput($chart_type, $input1, $input2 = '', $input3 = '', $input4 = '') {
    if ($chart_type == 1 || $chart_type == 2) {
      if ("$input1$input2$input3$input4" === "") { return NULL; }
      return round((intval($input1) * 3600 + intval($input2) * 60 + intval($input3)) . '.' . $input4, 3);
    } else if ($chart_type == 3 || $chart_type == 4 || true) {
      if(!is_numeric($input1))	{ return NULL; }
      return number_format($input1, 3, '.', '');
    }
  }

	//-------------------------------------------------------------------------------------------
	public static function GetSubmissionInputs($chart_type, $submission)
	{
		if($chart_type == 1 || $chart_type == 2)
		{
			if($submission === null)	{ return array('hours' => '', 'minutes' => '', 'seconds' => '', 'decimals' => '');}
			
			$input['hours'] = (int)($submission / 3600); $submission -= $input['hours'] * 3600;
			$input['minutes'] = (int)($submission / 60); $submission -= $input['minutes'] * 60;
			$input['seconds'] = (int)($submission); $submission -= $input['seconds'];
			$input['decimals'] = round($submission * 1000);
			
			if($input['decimals'] >= 0)
			{
				if($input['decimals'] < 10)				{ $input['decimals'] = '00' . $input['decimals'];}
				else if($input['decimals'] < 100)		{ $input['decimals'] = '0' . $input['decimals'];}
			}
			else
			{
				if($input['decimals'] > -10)			{ $input['decimals'] = '-00' . abs($input['decimals']);}
				else if($input['decimals'] > -100)		{ $input['decimals'] = '-0' . abs($input['decimals']);}
			}
			
			return $input;
		}
		else if($chart_type == 3 || $chart_type == 4 || true)
		{
			if($submission === null)	{ return '';}
			return round($submission, 3);
		}
	}
	
	//-------------------------------------------------------------------------------------------
	public function ReadProofType($proof_type)
	{
		switch($proof_type)
		{
			case 'p':	return 'Image proof';
			case 'v':	return 'Video proof';
			case 'e':	return 'Eye witness';
			case 'm':	return 'Moderator record';
			case 'w':	return 'From another website';
			case 't':	return 'Trusted User';
			case 's':	return 'Live stream';
      case 'r': return 'Replay';
			default:	return 'Proof type unknown';
		}
	}

    //-------------------------------------------------------------------------------------------
    public function GetChart($chart_id, $sort = null) {
        if(!isset($sort)) {
            $sort = "
                records.chart_pos ASC,
                records.rec_status = 3 AND records.proof_type = 'v' DESC,
                records.rec_status = 3 DESC,
                records.last_update ASC
            ";
        }

        // TODO: do we need chart_modifiers2 here? the caller probably has it
        $get_chart = database_fetch_all("
            SELECT
              records.*,
              records.entity_id AS entity_id1,
              users.*,
              levels.players,
              user_prefs.preferred_chart_icon,
              platforms.platform_name,
              sb_cache_incremental.base_level,
              chart_modifiers2.*
            FROM records
            JOIN levels USING (level_id)
            JOIN chart_modifiers2 USING (level_id)
            JOIN users USING (user_id)
            LEFT JOIN user_prefs USING (user_id)
            JOIN platforms USING (platform_id)
            LEFT JOIN sb_cache_incremental USING (user_id)
            WHERE records.level_id = ?
            ORDER BY $sort
        ", [$chart_id]);

        return $this->FormatChart($get_chart);
    }

    //-------------------------------------------------------------------------------------------
    public function BoardDisplaySettings($table, $sort) {
      // Board display settings

      //DEFAULTS
      if ($sort == 0) {
        $trophy_display = "trophypoints";
        $medal_display = "plats";
        $speedrun_display = "plats";
      } else if ($sort < -6) {
        //MANUAL SORTING
        $trophy_display = "fifths";
        $medal_display = "fifths";
        $speedrun_display = "fifths";
      } else if ($sort < -5) {
        $trophy_display = "fourths";
        $medal_display = "fourths";
        $speedrun_display = "fourths";
      } else if ($sort < -4) {
        $trophy_display = "bronzes";
        $medal_display = "bronzes";
        $speedrun_display = "bronzes";
      } else if ($sort < -3) {
        $trophy_display = "silvers";
        $medal_display = "silvers";
        $speedrun_display = "silvers";
      } else if ($sort < -2) {
        $trophy_display = "golds";
        $medal_display = "golds";
        $speedrun_display = "golds";
      } else if ($sort < -1) {
        $trophy_display = "plats";
        $medal_display = "plats";
        $speedrun_display = "plats";
      } else if ($sort < 0) {
        $trophy_display = "trophypoints";
        $medal_display = "medalpoints";
        $speedrun_display = "speedrunpoints";
      } else {
        //USER PREFERENCES
        $trophy_display   = database_value("SELECT trophy_display FROM user_prefs WHERE user_id = ?", [$sort]);
        $medal_display    = database_value("SELECT medal_display FROM user_prefs WHERE user_id = ?", [$sort]);
        $speedrun_display = database_value("SELECT speedrun_display FROM user_prefs WHERE user_id = ?", [$sort]);
      }

      //CASE HANDLING
      switch ($trophy_display) {
        case "trophypoints": $trophysort = "trophy_points DESC, platinum DESC, gold DESC, silver DESC, bronze DESC, fourth DESC, fifth DESC, COUNT(*) DESC"; break;
        case "plats":        $trophysort = "platinum DESC, gold DESC, silver DESC, bronze DESC, fourth DESC, fifth DESC, COUNT(*) DESC"; break;
        case "golds":        $trophysort = "gold DESC, silver DESC, bronze DESC, fourth DESC, fifth DESC, COUNT(*) DESC"; break;
        case "silvers":      $trophysort = "silver DESC, gold DESC, bronze DESC, fourth DESC, fifth DESC, COUNT(*) DESC"; break;
        case "bronzes":      $trophysort = "bronze DESC, gold DESC, silver DESC, fourth DESC, fifth DESC, COUNT(*) DESC"; break;
        case "fourths":      $trophysort = "fourth DESC, gold DESC, silver DESC, bronze DESC, fifth DESC, COUNT(*) DESC"; break;
        case "fifths":       $trophysort = "fifth DESC, gold DESC, silver DESC, bronze DESC, fourth DESC, COUNT(*) DESC"; break;
      }

      switch($speedrun_display) {
        case "speedrunpoints":  $speedrunsort = "medal_points DESC, platinum DESC, gold DESC, silver DESC, bronze DESC, COUNT(*) DESC"; break;
        case "plats":           $speedrunsort = "platinum DESC, gold DESC, silver DESC, bronze DESC, COUNT(*) DESC"; break;
        case "golds":           $speedrunsort = "gold DESC, silver DESC, bronze DESC, COUNT(*) DESC"; break;
        case "silvers":         $speedrunsort = "silver DESC, gold DESC, bronze DESC, COUNT(*) DESC"; break;
        case "bronzes":         $speedrunsort = "bronze DESC, gold DESC, silver DESC, COUNT(*) DESC"; break;
      }

      switch ($medal_display) {
        case "medalpoints": $medalsort = "medal_points DESC, platinum DESC, gold DESC, silver DESC, bronze DESC"; break;
        case "plats":       $medalsort = "platinum DESC, gold DESC, silver DESC, bronze DESC"; break;
        case "golds":       $medalsort = "gold DESC, silver DESC, bronze DESC"; break;
        case "silvers":     $medalsort = "silver DESC, gold DESC, bronze DESC, COUNT(*) DESC"; break;
        case "bronzes":     $medalsort = "bronze DESC, gold DESC, silver DESC, COUNT(*) DESC"; break;
      }

      switch($table) {
        case "trophy":      return $trophysort;
        case "medal":       return $medalsort;
        case "speedrun":    return $speedrunsort;
      }
    }

  /********************************************************************************************
   * GROUP SCOREBOARDS
   ********************************************************************************************/
  public function GetGroupMedalTable($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT
        users.user_id,
        users.forename,
        users.username,
        users.surname,
        users.country_code,
        users.country_id,
        users.user_groups,
        SUM(IF(records.platinum = 1,1,0)) AS platinum,
        SUM(IF(records.chart_pos = 1,1,0)) AS gold,
        SUM(IF(records.chart_pos = 2,1,0)) AS silver,
        SUM(IF(records.chart_pos = 3,1,0)) AS bronze,
        COUNT(records.record_id) AS num_subs
      FROM records
      JOIN users USING (user_id)
      JOIN levels USING (level_id)
      LEFT JOIN chart_modifiers2 USING(level_id)
      WHERE levels.group_id IN($group_filter) AND records.game_id = ? AND chart_modifiers2.standard
      GROUP BY records.user_id
      HAVING platinum + gold + silver + bronze > 0
      ORDER BY platinum DESC, gold DESC, silver DESC, bronze DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupSpeedrunTable($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
      SUM(records.medal_points) AS speedrun_points, COUNT(records.record_id) AS num_subs
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      JOIN chart_modifiers2 USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ? AND chart_modifiers2.speedrun
      GROUP BY records.user_id HAVING speedrun_points > 0
      ORDER BY speedrun_points DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupScoreboard($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(records.csp) AS score, SUM(POW((records.csp) / 100,4)) AS total_csr, COUNT(records.user_id) AS num_subs
      FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ?
      GROUP BY records.user_id ORDER BY score DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupChallengeboard($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(records.style_points) AS score, COUNT(records.user_id) AS num_subs
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      INNER JOIN chart_modifiers2 USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ? AND chart_modifiers2.challenge
      GROUP BY records.user_id ORDER BY score DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupExperienceBoard($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(records.cxp) AS cxp, SUM(records.vs_cxp) AS vs_cxp, COUNT(records.user_id) AS num_subs
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      INNER JOIN chart_modifiers2 USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ? AND chart_modifiers2.incremental
      GROUP BY records.user_id ORDER BY cxp DESC, vs_cxp DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupArcadeboard($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
      SUM(records.arcade_points) AS score, COUNT(records.user_id) AS num_subs
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      JOIN chart_modifiers2 USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ? AND chart_modifiers2.arcade
      GROUP BY records.user_id ORDER BY score DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupCollectorsCache($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
      SUM(records.cyberstars) AS score, COUNT(records.user_id) AS num_subs
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      JOIN chart_modifiers2 USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ? AND chart_modifiers2.collectible
      GROUP BY records.user_id ORDER BY score DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupSolutionboard($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
      SUM(records.brain_power) AS score, COUNT(records.user_id) AS num_subs
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      JOIN chart_modifiers2 USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ? AND chart_modifiers2.solution
      GROUP BY records.user_id ORDER BY score DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGroupLeadersTable($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT
        levels.game_id, levels.group_id, levels.level_id, levels.chart_type, levels.players, levels.num_decimals, levels.score_prefix, levels.score_suffix,
        levels.chart_type2, levels.num_decimals2, levels.score_prefix2, levels.score_suffix2, levels.conversion_factor, levels.decimals,
        MIN(records.submission) AS submission,
        MIN(records.submission2) AS submission2,
        MIN(records.chart_subs) AS chart_subs,
        MIN(records.platinum) AS platinum,
        GROUP_CONCAT(users.user_id ORDER BY records.last_update ASC SEPARATOR ',') AS user_ids,
        COUNT(*) AS num_first
      FROM levels
      LEFT JOIN level_groups USING(group_id)
      JOIN records USING(level_id)
      JOIN users USING(user_id)
      WHERE levels.game_id = ? AND group_id IN ($group_filter) AND records.chart_pos = 1
      GROUP BY levels.level_id
      ORDER BY level_groups.group_pos ASC, levels.level_pos ASC
    ", [$game_id]);

    return $get_sb;
  }


  public function GetGroupTotalizer($game_id, $group_ids, $chart_type) {
    $group_filter = implode(",", $group_ids);

    if ($chart_type == 1 || $chart_type == 3) {
      $subs_req = database_value("SELECT COUNT(level_id) FROM levels WHERE group_id IN ($group_filter) AND chart_type = ?", [$chart_type]);
      $sort = 'ASC';
    } else {
      $subs_req = 1;
      $sort = 'DESC';
    }

    $get_sb = database_fetch_all("
      SELECT
        users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        COUNT(record_id) AS num_subs,
        SUM(submission) AS score,
        MIN(chart_type) AS chart_type
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      WHERE levels.game_id = ? AND levels.group_id IN($group_filter) AND levels.chart_type = ?
      GROUP BY users.user_id
      HAVING num_subs >= ?
      ORDER BY score $sort
    ", [$game_id, $chart_type, $subs_req]);

    return $this->FormatRanking($get_sb);
  }

  // Unranked group scoreboard emulating The Sonic Center's ranking system
  public function GetGroupUnrankedScoreboardTSC($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $max_points = database_value("SELECT SUM(levels.num_subs) FROM levels WHERE levels.group_id IN ($group_filter) AND levels.game_id = ?", [$game_id]) ?? 0;
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(records.chart_pos) - COUNT(records.chart_pos) + $max_points - SUM(records.chart_subs) AS score,
        (SUM(records.chart_subs) + COUNT(records.chart_pos) - SUM(records.chart_pos)) / $max_points * 100 AS percentage
      FROM records
      JOIN users USING(user_id)
      JOIN levels USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ?
      GROUP BY records.user_id ORDER BY score ASC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

    // Unranked group scoreboard emulating VGR's medal ranking system
    public function GetGroupUnrankedMedalTableVGR($game_id, $group_ids) {
      $group_filter = implode(",", $group_ids);
      $get_sb = database_fetch_all("
        SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(CASE WHEN records.chart_pos = 3 THEN 1 END) AS bronze,
        SUM(CASE WHEN records.chart_pos = 2 THEN 1 END) AS silver,
        SUM(CASE WHEN records.chart_pos = 1 THEN 1 END) AS gold,
        COUNT(DISTINCT CASE WHEN records.chart_pos = 1 AND records.chart_subs > 1 THEN records.level_id END) AS platinum
        FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
        WHERE levels.group_id IN ($group_filter) AND records.game_id = ?
        GROUP BY records.user_id ORDER BY platinum DESC, gold DESC, silver DESC, bronze DESC
      ", [$game_id]);

      return $this->FormatRanking($get_sb);
    }

  // Unranked group scoreboard emulating an AF ranking system
  public function GetGroupUnrankedScoreboardMKPP($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $max_points = database_value("SELECT SUM(levels.num_subs) FROM levels WHERE levels.group_id IN ($group_filter) AND levels.game_id = ?", [$game_id]);
    $chart_count = database_value("SELECT COUNT(*) FROM levels WHERE levels.group_id IN ($group_filter) AND levels.game_id = ?", [$game_id]);
    $empty_chart_count = database_value("SELECT COUNT(*) FROM levels WHERE levels.group_id IN ($group_filter) AND levels.num_subs = 0 AND levels.game_id = ?", [$game_id]);
    $chart_count -= $empty_chart_count;
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        (SUM(records.chart_pos) + $max_points - SUM(records.chart_subs) + $chart_count - COUNT(records.chart_pos)) / $chart_count AS score
      FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ?
      GROUP BY records.user_id ORDER BY score ASC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  // Unranked group scoreboard emulating The-Elite's ranking system
  public function GetGroupUnrankedScoreboardTE($game_id, $group_ids) {
    $group_filter = implode(",", $group_ids);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(CASE WHEN records.chart_pos = 1 THEN 100
                 WHEN records.chart_pos = 2 THEN 97
                 WHEN records.chart_pos > 98 THEN 0
                 ELSE (98 - records.chart_pos)
                 END) AS score
      FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
      WHERE levels.group_id IN ($group_filter) AND records.game_id = ?
      GROUP BY records.user_id ORDER BY score DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  /********************************************************************************************
   * GAME SCOREBOARDS
   ********************************************************************************************/
  public function GetGameScoreboard($game_id) {
    $get_sb = database_fetch_all("
        SELECT
            users.user_id,
            users.username,
            users.forename,
            users.surname,
            users.country_code,
            users.country_id,
            users.user_groups,
            true AS display_trophies,
            gsb_cache_trophy.csp_trophy AS trophy_type,
            gsb_cache_trophy.csp_trophy_points,
            gsb_cache_csp.*
        FROM gsb_cache_csp
        JOIN users USING(user_id)
        LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
        WHERE gsb_cache_csp.game_id = ?
        ORDER BY scoreboard_pos ASC
    ", [$game_id]);

    return $this->FormatRanking($get_sb, 0);
  }

  public function GetGameArcadeboard($game_id) {
    $get_sb = database_fetch_all("
      SELECT
        users.user_id, users.username, users.forename, users.surname, users.country_code, users.country_id, users.user_groups,
        gsb_cache_arcade.*,
        true AS display_trophies,
        gsb_cache_trophy.arcade_trophy AS trophy_type,
        gsb_cache_trophy.arcade_trophy_points
      FROM gsb_cache_arcade JOIN users USING(user_id)
      LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
      WHERE gsb_cache_arcade.game_id = ?
      ORDER BY gsb_cache_arcade.tokens DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGameCollectorsCache($game_id) {
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.username, users.forename, users.surname, users.country_code, users.country_id, users.user_groups,
      true AS display_trophies,
                 gsb_cache_trophy.collectible_trophy AS trophy_type,
                 gsb_cache_trophy.collectible_trophy_points,
      gsb_cache_collectible.*
      FROM gsb_cache_collectible JOIN users USING(user_id) LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
      WHERE gsb_cache_collectible.game_id = ?
      ORDER BY gsb_cache_collectible.cyberstars DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGameScoreboardIncremental($game_id) {
    $get_sb = database_fetch_all("
      SELECT
        users.user_id,
        users.username,
        users.forename,
        users.surname,
        users.country_code,
        users.country_id,
        users.user_groups,
        true AS display_trophies,
                   gsb_cache_trophy.incremental_trophy AS trophy_type,
                   gsb_cache_trophy.incremental_trophy_points,
        gsb_cache_incremental.*
      FROM gsb_cache_incremental JOIN users USING(user_id) LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
      WHERE gsb_cache_incremental.game_id = ?
      ORDER BY gsb_cache_incremental.cxp DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGameSolutionboard($game_id) {
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.username, users.forename, users.surname, users.country_code, users.country_id, users.user_groups,
      true AS display_trophies,
                 gsb_cache_trophy.solution_trophy AS trophy_type,
                 gsb_cache_trophy.solution_trophy_points,
        gsb_cache_solution.*
      FROM gsb_cache_solution
      JOIN users USING(user_id)
      LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
      WHERE gsb_cache_solution.game_id = ?
      ORDER BY gsb_cache_solution.brain_power DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGameChallengeBoard($game_id) {
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.username, users.forename, users.surname, users.country_code, users.country_id, users.user_groups,
      true AS display_trophies,
                 gsb_cache_trophy.challenge_trophy AS trophy_type,
                 gsb_cache_trophy.challenge_trophy_points,
        gsb_cache_userchallenge.*
      FROM gsb_cache_userchallenge JOIN users USING(user_id) LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
      WHERE gsb_cache_userchallenge.game_id = ?
      ORDER BY gsb_cache_userchallenge.style_points DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  //-------------------------------------------------------------------------------------------
  public function GetGameMedalTable($game_id, $sort = null) {
    global $current_user;

    if ($sort === null && $current_user) {
      $sort = $current_user['medal_display'];
    }

    switch ($sort) {
    case 'gold':
    case 'golds':
      $sort = "gold DESC, silver DESC, bronze DESC";
      break;
    case 'plats':
    case 'platinum':
    default:
      $sort = "platinum DESC, gold DESC, silver DESC, bronze DESC";
      break;
    }

    $get_sb = database_fetch_all("
      SELECT
        users.user_id,
        users.forename,
        users.username,
        users.surname,
        users.country_code,
        users.country_id,
        users.user_groups,
        true AS display_trophies,
        gsb_cache_trophy.standard_trophy AS trophy_type,
        gsb_cache_trophy.standard_trophy_points,
        gsb_cache_medals.*
      FROM gsb_cache_medals
      JOIN users USING(user_id)
      LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
      WHERE gsb_cache_medals.game_id = ? AND (gsb_cache_medals.platinum > 0 OR gsb_cache_medals.gold > 0 OR gsb_cache_medals.silver > 0 OR gsb_cache_medals.bronze > 0)
      ORDER BY $sort
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGameSpeedrunTable($game_id, $sort = null) {
    global $current_user;

    if ($sort === null && $current_user) {
      $sort = $current_user['speedrun_display'];
    }

    switch ($sort) {
    case "speedrunpoints":
      $sort = "medal_points DESC, platinum DESC, gold DESC, silver DESC, bronze DESC";
      break;
    case 'gold':
    case 'golds':
      $sort = "gold DESC, silver DESC, bronze DESC";
      break;
    case 'plats':
    case 'platinum':
    default:
      $sort = "platinum DESC, gold DESC, silver DESC, bronze DESC";
      break;
    }

    $get_sb = database_fetch_all("
      SELECT
        users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        true AS display_trophies,
                   gsb_cache_trophy.speedrun_trophy AS trophy_type,
                   gsb_cache_trophy.speedrun_trophy_points,
        gsb_cache_speedrun.*
      FROM gsb_cache_speedrun
      JOIN users USING (user_id) LEFT JOIN gsb_cache_trophy USING (game_id, user_id)
      WHERE gsb_cache_speedrun.game_id = ?
      AND gsb_cache_speedrun.medal_points > 0
      ORDER BY $sort
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  public function GetGameTrophyCase($game_id) {
    $get_sb = database_fetch_all("
      SELECT users.*, gsb_cache_trophy.*, gsb_cache_submissions.num_subs
      FROM gsb_cache_trophy
      JOIN gsb_cache_submissions USING (user_id, game_id)
      JOIN users USING (user_id)
      WHERE gsb_cache_trophy.game_id = ?
      ORDER BY gsb_cache_trophy.platinum DESC, gsb_cache_trophy.trophy_points DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  // Unranked game scoreboard emulating The Sonic Center's ranking system
  public function GetGameUnrankedScoreboardTSC($game_id) {
    $max_points = database_value("SELECT SUM(levels.num_subs) FROM levels WHERE levels.game_id = ?", [$game_id]);
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(records.chart_pos) - COUNT(records.chart_pos) + $max_points - SUM(records.chart_subs) AS score,
        (SUM(records.chart_subs) + COUNT(records.chart_pos) - SUM(records.chart_pos)) / $max_points * 100 AS percentage
      FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
      WHERE records.game_id = ?
      GROUP BY records.user_id ORDER BY score ASC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  // Unranked game scoreboard emulating VGR's medal ranking system
  public function GetGameUnrankedMedalTableVGR($game_id) {
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
      SUM(CASE WHEN records.chart_pos = 3 THEN 1 END) AS bronze,
      SUM(CASE WHEN records.chart_pos = 2 THEN 1 END) AS silver,
      SUM(CASE WHEN records.chart_pos = 1 THEN 1 END) AS gold,
      COUNT(DISTINCT CASE WHEN records.chart_pos = 1 AND records.chart_subs > 1 THEN records.level_id END) AS platinum
      FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
      WHERE records.game_id = ?
      GROUP BY records.user_id ORDER BY platinum DESC, gold DESC, silver DESC, bronze DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  // Unranked game scoreboard emulating an AF ranking system
  public function GetGameUnrankedScoreboardMKPP($game_id) {
    $max_points = database_value("SELECT SUM(levels.num_subs) FROM levels WHERE levels.game_id = ?", [$game_id]);
    $chart_count = database_value("SELECT COUNT(*) FROM levels WHERE levels.game_id = ?", [$game_id]);
    $empty_chart_count = database_value("SELECT COUNT(*) FROM levels WHERE levels.num_subs = 0 AND levels.game_id = ?", [$game_id]);
    $chart_count -= $empty_chart_count;
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        (SUM(records.chart_pos) + $max_points - SUM(records.chart_subs) + $chart_count - COUNT(records.chart_pos)) / $chart_count AS score
      FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
      WHERE records.game_id = ?
      GROUP BY records.user_id ORDER BY score ASC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  // Unranked game scoreboard emulating The-Elite's ranking system
  public function GetGameUnrankedScoreboardTE($game_id) {
    $get_sb = database_fetch_all("
      SELECT users.user_id, users.forename, users.username, users.surname, users.country_code, users.country_id, users.user_groups,
        SUM(CASE WHEN records.chart_pos = 1 THEN 100
                 WHEN records.chart_pos = 2 THEN 97
                 WHEN records.chart_pos > 98 THEN 0
                 ELSE (98 - records.chart_pos)
                 END) AS score
      FROM records JOIN users USING(user_id) JOIN levels USING(level_id)
      WHERE records.game_id = ?
      GROUP BY records.user_id ORDER BY score DESC
    ", [$game_id]);

    return $this->FormatRanking($get_sb);
  }

  /**
   * Global scoreboards
   *
   * These functions should use RANK() AS scoreboard_pos, but this is not
   * supported in mysql. We'd need to upgrade to mariadb 10.2 first, which is
   * only available on ubuntu 20.04. Instead, we'll rely on an row number like
   * incremental position.
   **/
  public function GetCountryCSRBoard() {
    $get_sb = database_fetch_all("
      SELECT
        MIN(users.country_code) AS country_code,
        users.country_id,
        SUM(total_csr + bonus_csr) AS score,
        SUM(num_subs) AS num_subs
      FROM gsb_cache_csp
      JOIN users USING(user_id)
      WHERE users.country_id != 1
      GROUP BY users.country_id
      ORDER BY score DESC, num_subs ASC
    ", []);

    return $this->FormatRanking($get_sb);
  }

  public function GetCountryMedalTable() {
    $get_sb = database_fetch_all("
      SELECT
        MIN(users.country_code) AS country_code,
        users.country_id,
        SUM(platinum) as platinum,
        SUM(gold) as gold,
        SUM(silver) as silver,
        SUM(bronze) as bronze
      FROM sb_cache_standard
      JOIN users USING (user_id)
      WHERE users.country_id != 1
      GROUP BY users.country_id
      ORDER BY platinum DESC, gold DESC, silver DESC, bronze DESC
    ", []);

    return $this->FormatRanking($get_sb);
  }

  public function GetCountryCSPBoard() {
    $get_sb = database_fetch_all("
      SELECT
        users.country_code,
        users.country_id,
        SUM(total_csp) AS score,
        SUM(num_subs) AS num_subs
      FROM gsb_cache_csp
      JOIN users USING (user_id)
      WHERE users.country_id != 1
      GROUP BY users.country_id, users.country_code
      ORDER BY score DESC, num_subs ASC
    ", []);

    return $this->FormatRanking($get_sb);
  }

  public function GetContinentCSPBoard() {
    $get_sb = database_fetch_all("
      SELECT
        countries.continent,
        SUM(total_csp) AS score,
        SUM(num_subs) AS num_subs
      FROM gsb_cache_csp
      JOIN users USING (user_id)
    JOIN countries USING (country_id)
    WHERE countries.continent != 0
    GROUP BY countries.continent
    ORDER BY score DESC, num_subs ASC
    ", []);

    return $this->FormatRanking($get_sb);
  }

  public function GetContinentCSRBoard() {
    $get_sb = database_fetch_all("
      SELECT
        countries.continent,
        SUM(sb_cache.total_csr) AS score,
        SUM(sb_cache.num_subs) AS num_subs
      FROM sb_cache
      JOIN users USING (user_id)
      JOIN countries USING (country_id)
      WHERE countries.continent != 0
      GROUP BY countries.continent
      ORDER BY score DESC, num_subs ASC
    ", []);

    return $this->FormatRanking($get_sb);
  }

  public function GetContinentMedalTable() {
    $get_sb = database_fetch_all("
      SELECT
        countries.continent,
        SUM(platinum) as platinum,
        SUM(gold) as gold,
        SUM(silver) as silver,
        SUM(bronze) as bronze
      FROM sb_cache_standard
      JOIN users USING(user_id)
      JOIN countries USING(country_id)
      WHERE countries.continent != 0
      GROUP BY countries.continent
      ORDER BY platinum DESC, gold DESC, silver DESC, bronze DESC
    ", []);

    return $this->FormatRanking($get_sb);
  }

  /********************************************************************************************
  LOAD CHART INFO
  ********************************************************************************************/
  public static function GetRecordInputs($chart_id, $user_id, $chart_type, $chart_type2, $game_id = 0) {
    $record = database_fetch_one("
      SELECT
        record_id,
        entity_id, entity_id2, entity_id3, entity_id4, entity_id5, entity_id6,
        platform_id, submission, submission2, comment, extra1, extra2, extra3,
        other_user, rec_status, game_patch
      FROM records
      WHERE user_id = ? AND level_id = ?
    ", [$user_id, $chart_id]);

    if ($record == NULL) {
      $record = [];

      if ($game_id != 0) {
        $def_platform = self::FetchDefaultPlatform($user_id, $game_id);
      } else {
        $def_platform = null;
      }
    }

    return [
      [
        1 => $record['entity_id'] ?? -1,
        2 => $record['entity_id2'] ?? -1,
        3 => $record['entity_id3'] ?? -1,
        4 => $record['entity_id4'] ?? -1,
        5 => $record['entity_id5'] ?? -1,
        6 => $record['entity_id6'] ?? -1,
      ],
      $record['platform_id'] ?? $def_platform,
      $record['game_patch'] ?? '',
      self::GetSubmissionInputs($chart_type, $record['submission'] ?? null),
      self::GetSubmissionInputs($chart_type2, $record['submission2'] ?? null),
      $record['comment'] ?? '',
      $record['rec_status'] ?? 0,
      $record['extra1'] ?? '',
      $record['extra2'] ?? '',
      $record['extra3'] ?? '',
      $record['other_user'] ?? '',
      false,
    ];
  }

  public static function FetchDefaultPlatform($user_id, $game_id) {
    return database_value("
      SELECT platform_id
      FROM records
      WHERE user_id = ? AND game_id = ?
      GROUP BY platform_id
      ORDER BY COUNT(1) DESC
      LIMIT 1
    ", [$user_id, $game_id]);
  }

  //-------------------------------------------------------------------------------------------
  public function GetChartInfo($chart_id) {
    $chart_info = LevelsRepository::get($chart_id);

    $game_id = $chart_info['game_id'];
    $group_id = $chart_info['group_id'];

    if ($chart_info['name_pri'] == '') {
      $chart_info['name_pri'] = $chart_info['chart_type'] >= 3 ? 'Score' : 'Time';
    }

    $entities = self::FetchChartEntities($chart_info);
    $platforms = self::GetGamePlatforms($game_id);

    return [$game_id, $group_id, $chart_info, $entities, $platforms];
  }

  public static function FetchChartEntities($chart) {
    $entities = [];

    if ($chart['entity_lock'] != '') {
      $lock = "game_entity_id IN ({$chart['entity_lock']})";
    } else {
      $lock = "entity_group = 1";
    }

    $entities[1] = database_fetch_all("
      SELECT *
      FROM game_entities
      WHERE game_id = ? AND $lock
      ORDER BY entity_name ASC, game_entity_id ASC, inherited_entity_id ASC
    ", [$chart['game_id']]);

    for ($i = 2; $i <= $chart['max_entities']; $i++) {
      $entities[$i] = database_fetch_all("
        SELECT *
        FROM game_entities
        WHERE game_id = ? AND entity_group = ?
        ORDER BY entity_name ASC
      ", [$chart['game_id'], $i]);

      if (count($entities[$i]) == 0) {
        if ($chart['entity_override_group'] != 1) {
          $lock = "entity_group = {$chart['entity_override_group']}";
        } else if ($chart['entity_lock'] != '') {
          $lock = "game_entity_id IN ({$chart['entity_lock']})";
        } else {
          $lock = "entity_group = 1";
        }

        $entities[$i] = database_fetch_all("
          SELECT *
          FROM game_entities
          WHERE game_id = ? AND $lock
          ORDER BY entity_name ASC, game_entity_id ASC, inherited_entity_id ASC
        ", [$chart['game_id']]);
      }
    }

    return $entities;
  }

  //-------------------------------------------------------------------------------------------
  public static function GetGamePlatforms($game_id) {
    return database_fetch_all("
      SELECT platforms.*
      FROM platforms
      JOIN game_platforms USING(platform_id)
      WHERE game_platforms.game_id = ?
      ORDER BY original DESC, platforms.platform_name ASC
    ", [$game_id]);
  }

  //-------------------------------------------------------------------------------------------
  public function GameExists($game_id) {
    return database_value("SELECT 1 FROM games WHERE game_id = ?", [$game_id]) == 1;
  }

  // HTTP Response and Flash related functions.
  // These were extracted but are still called from everywhere.

  public function LeavePage($redirect, $note_head = '', $note_extra_text = '') {
    HTTPResponse::LeavePage($redirect, $note_head, $note_extra_text);
  }

  public static function Exit() {
    HTTPResponse::Exit();
  }

  public static function PageNotFound($title = "Page not found") {
    HTTPResponse::PageNotFound($title);
  }

  public static function FlashSuccess($title, $body = '') {
    Flash::AddSuccess($title, $body);
  }

  public static function FlashError($title, $body = '') {
    Flash::AddError($title, $body);
  }

  public static function RedirectToPreviousPage() {
    HTTPResponse::RedirectToPreviousPage();
  }

  public static function WriteNote($WasSuccess, $head, $extra_text = '') {
    $type = $WasSuccess ? 'success' : 'error';
    Flash::AddMessage($type, $head, $extra_text);
  }
}
