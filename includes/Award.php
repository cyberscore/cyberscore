<?php

class Award {
  const DP_CSP = 3;
  const DP_CSR = 3;
  const DP_ARCADE = 0;
  const DP_CHALLENGE = 1;
  const DP_COLLECTIBLE = 0;
  const DP_INCREMENTAL = 1;
  const DP_SOLUTION = 0;

  static function UCSP($weighted_position, $num_subs) {
    if ($num_subs == 1) {
      return 50;
    }

    $mean = 60;
    $stdev = 15 / 0.1975;
    $pop = 1.5;

    // CENTERED CSP
    $p = ($num_subs - $weighted_position) / max($num_subs - 1, 1);

    // CALCULATE z SCORE, WE MAP $p BETWEEN -1 AND +1 THEN MULTIPLY BY $STDEV
    if ($p >= 0.5) {
      $z = (pow($p, 0.135) - pow(1 - $p, 0.135)) * $stdev;
    } else {
      $z = (pow($p, 0.405) - pow(1 - $p, 0.405)) * $stdev;
    }

    $limit = max(40, log($num_subs, 2) * 6);

    // LIMIT CSP BETWEEN 20 AND 100, CONSIDER $p SO THAT NON FIRST PLACES CAN'T GET THE IDENTICAL CSP
    $z = min($z, $limit * $p * (max($num_subs - $pop, 0)) / $num_subs);
    $z = max($z, -1 * $limit * (1 - $p));

    // ADJUST THE Z SCORE FOR POPULARITY
    $z *= max($num_subs - $pop, 0) / $num_subs;

    // Prepare unrounded UCSP
    $ucsp_unrounded = $z + $mean;

    return round($ucsp_unrounded, self::DP_CSP);
  }

  static function ModifyCSP($ucsp, $modifier, $speedrun_flag, $platinum_flag, $i) {
    // Calculate the speedrun bonus CSP modifier for 1st/2nd/3rd
    $speedrun_bonus_modifier = 0;
    if ($speedrun_flag == TRUE && $i < 3) {
      $speedrun_bonus_modifier += Modifiers::SpeedrunBonus($i+1, $platinum_flag);
    }

    // Add the modifiers to UCSP to make regular CSP
    $csp_unrounded = $ucsp * ($modifier + $speedrun_bonus_modifier);

    return round($csp_unrounded, self::DP_CSP);
  }

  static function CSR($csp) {
    return round(pow($csp / 100, 4), self::DP_CSR);
  }

  public static function BonusCSR($csr, $rpc, $scoreboard_position, $user_num_subs, $num_charts, $num_proofs, $num_video_proofs) {
    $pos_factor = 0.1 * (log(4 + $rpc, 2) / sqrt($scoreboard_position));
    $sub_factor = ($user_num_subs / $num_charts);
    $proof_factor = ($num_proofs / $user_num_subs / 3);
    $video_proof_factor = ($num_video_proofs / $user_num_subs / 6);

    $bonus_csr = round($csr * $pos_factor * ($sub_factor + $proof_factor + $video_proof_factor), self::DP_CSR);

    return [$bonus_csr, $pos_factor, $sub_factor, $proof_factor, $video_proof_factor];
  }

  static function StylePoints($level_pos, $num_records, $secondary_modifier) {
    $style_points = ($num_records - $level_pos + 1) * (($num_records - $level_pos + 1) / $num_records) * $secondary_modifier;

    if ($style_points < 0.1) {
      $style_points = 0.1;
    }

    return round($style_points, self::DP_CHALLENGE);
  }

  static function Tokens($level_pos, $num_records, $secondary_modifier) {
    //constants
    $average_token_count = 500; // Everything will scale around this number
    $power_scaler = 0.39; // Bigger number = bigger token rewards

    //prepare calculation variables
    $mid_pos = ($num_records + 1) / 2;
    $pos_multiplier = 1 / pow($level_pos, $power_scaler);
    $mid_pos_multiplier = 1 / pow($mid_pos, $power_scaler);

    $tokens = $pos_multiplier / $mid_pos_multiplier * $average_token_count * $secondary_modifier;
    return round($tokens, self::DP_ARCADE);
  }

  static function BrainPower($user_pos, $number_of_subs, $num_ties, $secondary_modifier) {
    $exponent_adjust = 10;
    $minimum_bp = 10;
    $bp_multiplier = 100; #formula is written so base score is 1, but we want a base score of 100

    #if ties occur on what would otherwise be positions 5, 6, 7, 8, and 9, we want to give out the
    #BP that would be awarded in the middle of that pack (position 7). So we fake the user_pos accordingly.
    #to disincentivise intentional ties, we penalise ties slightly more than a true average, so in the example
    #above, instead of giving the BP for position 7, we'd actually be giving the BP for ~7.5th
    $user_pos += ($num_ties - 1) / 1.6;

    $chart_strength = pow($number_of_subs + $exponent_adjust, 1.45);
    $user_strength = $chart_strength / pow($user_pos + $exponent_adjust, 1.45);

    #the number/percentage of scores better than this user's score
    $num_better = $user_pos - 1;
    #perc_better is expressed as a number between 0 and 1
    $perc_better = 1 / $number_of_subs * $num_better;

    #bp_penalty reduces your score from the base (100) to the minimum (10) proportionally
    #so if 50% of the people who have submitted to the chart have beaten you, you get a
    #penalty of 50%. Since the base->minimum drop is 90BP, this is a drop of 45BP.
    #this penalty applies to every single user, although in the top half of a chart it will
    #be fairly negligible compared to overall BP, and for a first place it comes out as 0.
    $bp_penalty = ($bp_multiplier - $minimum_bp) * ($perc_better);

    #for untied/platinum first places, we add a multiplicative bonus to the final Brain Power
    $untied_bonus = 1.00;
    if($user_pos == 1) {
      if($num_ties == 1 || Award::ValidatePlatinum($number_of_subs, $num_ties)) {
        #we add a 0.25x bonus for every additional submission, up to a maximum of 2.00x
        $untied_bonus = 1.00 + min(1.00, ($number_of_subs-1) * 0.25);
      }
    }

    $final_bp = $untied_bonus * (($bp_multiplier * $user_strength) - $bp_penalty) * $secondary_modifier;

    return round($final_bp, self::DP_SOLUTION);
  }

  static function Collectibles($scoreboard_position, $level_id, $score, $max, $secondary_modifier) {

    //Custom-handling of secondary modifiers for fragments
    switch($secondary_modifier) {
      case 1.25: $secondary_multiplier = 5; break;
      case 1.10: $secondary_multiplier = 2; break;
      case 1.00: $secondary_multiplier = 1; break;
      default: $secondary_multiplier = 1; break;
    }

    // We give 5 base fragments for 1st position always, regardless of ties
    if ($scoreboard_position == 1) { $fragments = 5; }
    // If a score is over 75% of the max score but not in first place, award 3 base fragments
    else if (($score * 4 / 3) >= $max) { $fragments = 3; }
    // If a score is over 50% of the max score but not in first place, award 2 base fragments
    else if (($score * 2) >= $max) { $fragments = 2; }
    // Else we award 1 default base fragment for submitting
    else { $fragments = 1; }


    // All other fragments are handled at game scoreboard generation (We can't award proof fragments yet because record status isn't organised on chart build)

    return round($fragments * $secondary_multiplier, self::DP_COLLECTIBLE);
  }

  static function MedalPoints($scoreboard_position, $number_of_subs, $num_ties, $secondary_modifier) {
    $medal_points = 0;

    //Custom-handling of secondary modifiers for speedrun points
    switch($secondary_modifier) {
      case 1.25: $secondary_multiplier = 1.00; break;
      case 1.10: $secondary_multiplier = 0.40; break;
      case 1.00: $secondary_multiplier = 0; break;
      default: $secondary_multiplier = 0; break;
    }

    //Start by assigning significant bonuses for the top 3, scaling based on ties.
    switch ($scoreboard_position) {
      case 1:
      //Golds
      $medal_points += Award::GoldMedalPoints($number_of_subs, $num_ties);

      //Platinums get additional bonuses on top of qualifying as a Gold.
      if(Award::ValidatePlatinum($number_of_subs, $num_ties)) {
        $medal_points += Award::PlatinumMedalPoints($number_of_subs, $num_ties);
      }
      break;

    case 2:
      //Silvers
      $medal_points += Award::SilverMedalPoints($number_of_subs, $num_ties);
      break;

    case 3:
      //Bronzes
      $medal_points += Award::BronzeMedalPoints($number_of_subs, $num_ties);
      break;

    default:
      $medal_points = 0;
      break;

    }

    //We award additional 1 point for each person beaten on the chart, and 1 additional point so even last place gets something. 
    $medal_points += $number_of_subs + 1 - $scoreboard_position;

    return round($medal_points * $secondary_multiplier, 0);
  }

  static function PlatinumMedalPoints($total_subs, $numties) {
    $platinum_multiplier = 10;

    return (($total_subs - (($numties - 1) * $platinum_multiplier)) * $platinum_multiplier + 1) / 2;
  }

  static function GoldMedalPoints($total_subs, $numties) {
    if ($numties == 1) $numties = 0;

    return max(10, (($total_subs * 3) - ($numties * 1.75) * 3) + 5);
  }

  static function SilverMedalPoints($total_subs, $numties) {
    if ($numties == 1) $numties = 0;

    return max(7.5, (($total_subs - $numties * 1.75) * 2) + 3);
  }

  static function BronzeMedalPoints($total_subs, $numties) {
    if ($numties == 1) $numties = 0;

    return max(5, (($total_subs - $numties * 1.75) * 1.5) + 1);
  }

  // This function checks whether or not to award a platinum
  static function ValidatePlatinum($number_of_subs, $num_ties) {
    //Initialise constants
    $base_requirement = 5;
    $additional_platinum_exponent = 2;

    //Calculate # of subs required for a platinum
    $required_subs = $base_requirement * pow($num_ties, $additional_platinum_exponent);

    //Return a boolean if the record should earn a platinum or not.
    if ($number_of_subs >= $required_subs) {
      return TRUE;
    }
    else return FALSE;
  }

  // This function converts a # of speedrun points into a time format, logarithmically approaching 0. Needs to be public.
  public static function SpeedrunTime($points)
  {
    // Turns the # of points into a float representing # of hours in base 10.
    $time_float = 216 / pow(6, log10($points));
    $speedrun_time = [];

    // Redefines $time_float into H M S variables.
    $speedrun_time['hours'] = floor($time_float);
    $remaining_minutes = ($time_float - $speedrun_time['hours']) * 60;
    $speedrun_time['minutes'] = floor($remaining_minutes);
    $remaining_seconds = ($remaining_minutes - $speedrun_time['minutes']) * 60;
    $speedrun_time['seconds'] = floor($remaining_seconds);

    // Format the time for MySQL
    return sprintf("%02d:%02d:%02d", $speedrun_time['hours'], $speedrun_time['minutes'], $speedrun_time['seconds']);
  }



  // Incremental chart awards

  // Base experience you win by submitting to an incremental chart
  // These are always collectibles, so $median should be >= 1.
  static function Experience($scoreboard_position, $median, $submission, $secondary_modifier) {
    if ($median == 0) { return 0; }

    return round(10 / (1 + exp(-0.2*($submission/$median - 1))) * $secondary_modifier, self::DP_INCREMENTAL);
  }

  // The amount of bonus XP you win by beating other players.
  //
  // $levels_beaten: sum of the base level of every player below you;
  // $levels_tied:   sum of the base level of every player tied with you;
  // $own_level:     your own base level; and
  // $cxp:           your base experience for this chart.
  static function VersusExperience($levels_beaten, $levels_tied, $own_level, $cxp) {
    return round(($levels_beaten + $own_level) * $cxp, self::DP_INCREMENTAL);
  }

  // The level of a user with a given xp amount.
  static function LevelFromExperience($xp) {
    // Level(1) = 0
    // Level(n) = round(25 * 2.5**(n-2))

    for ($i = 2;; $i++) {
      if ($xp < round(25 * pow(2.5, $i-2))) {
        return $i - 1;
      }
    }
  }

  // The xp required for a given level.
  public static function ExperienceFromLevel($level) {
    // Level(1) = 0
    // Level(n) = round(25 * 2.5**(n-2))
    if ($level <= 1) {
      return 0;
    } else {
      return round(25 * pow(2.5, $level-2));
    }
  }
}
