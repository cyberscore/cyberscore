<?php

function current_url_with_query($params) {
  $url = parse_url($_SERVER['REQUEST_URI']);
  parse_str($url['query'] ?? '', $query);

  $query = array_merge($query, $params);
  $query = http_build_query($query);

  $new_url = $url['path'];
  if (!empty($query)) { $new_url .= "?$query"; }
  if (!empty($url['fragment'])) { $new_url .= "#{$url['fragment']}"; }

  return $new_url;
}

function build_url_query($params) {
  $query = "";
  foreach ($params as $key => $value) {
    $query .= ($query == '' ? "?" : "&") . $key . "=" . urlencode($value);
  }
  return $query;
}

function url_for($attrs) {
  global $config;
  $base = $config['app']['base_url'];

  foreach ($attrs as $key => $model) {
    $id = $model["{$key}_id"];
    $resource = pluralize($key);
    return "$base/$resource/$id";
  }
}

function edit_url_for($attrs) {
  $base = url_for($attrs);
  return "$base/edit";
}

function delete_url_for($attrs) {
  $base = url_for($attrs);
  return "$base/delete";
}

function url_for_group($group) {
  global $config;

  $game_id = $group['game_id'];
  $group_id = $group['group_id'];
  $base = $config['app']['base_url'];
  return "$base/group/$game_id/$group_id";
}

function url_for_user_comparison($user_id1, $user_id2) {
  return "/compare.php?user_id1=$user_id1&user_id2=$user_id2";
}

function link_to_user($user, $fullname = false, $options = []) {
  $url = url_for(['user' => $user]);

  if ($fullname === false) {
    $display = h($user['username']);
  } else {
    $username = h($user['username']);
    $forename = h($user['forename'] ?? "");
    $surname = h($user['surname'] ?? "");

    $display = "$forename &ldquo;$username&rdquo; $surname";
  }

  $options = array_merge($options, ['href' => $url]);
  $html_options = build_html_options($options);

  return "<a $html_options>$display</a>";
}

function link_to_news_article($news) {
  $news_id = $news['news_id'];
  $headline = htmlspecialchars($news['headline'], ENT_QUOTES);

  return "<a title='$headline' href='/articles/$news_id'>$headline</a>";
}

function link_to_game($game) {
  global $t;

  if (is_array($game)) {
    $game_id = $game['game_id'];
  } else {
    $game_id = $game;
  }

  $name = h($t->GetGameName($game_id));

  return "<a href='/games/$game_id'>$name</a>";
}

function link_to_game_group($game_group) {
  global $t;

  if (is_array($game_group)) {
    $game_id = $game_group['game_id'];
    $group_id = $game_group['group_id'];
  } else {
    $group_id = $game_group;
  }

  $name = h($t->GetGroupName($group_id));

  return "<a href='/group/$game_id/$group_id'>$name</a>";
}

function link_to_game_chart($game_chart, $options = []) {
  return link_to_chart($game_chart, $options);
}

function link_to_chart($game_chart, $options = []) {
  global $t;

  if (is_array($game_chart)) {
    $chart_id = $game_chart['level_id'];
  } else {
    $chart_id = $game_chart;
  }

  $name = h($t->GetChartName($chart_id));

  $options = array_merge($options, ['href' => "/chart/$chart_id"]);
  $html_options = build_html_options($options);
  return "<a $html_options>$name</a>";
}

function link_to_record($record) {
  global $cs;
  $record_id = $record['record_id'];
  $name = h(CSClass::FormatSubmissions($record));

  return "<a href='/record/$record_id'>$name</a>";
}

// To-do: Deprecate and remove this.
function rank_to_category($rank) {
  switch ($rank) {
    case 1: return 'standard';
    case 2: return 'arcade';
    case 3: return 'speedrun';
    case 6: return 'solution';
  }
}

function skin_stylesheet_url($filename) {
  global $skin_folder;
  $css_version = '4.6.1';

  return htmlspecialchars("/skins4/$skin_folder/$filename?v=$css_version", ENT_QUOTES);
}

function skin_stylesheet($filename) {
  $url = skin_stylesheet_url($filename);

  return "<link rel='stylesheet' type='text/css' href='$url' />";
}

function skin_image_url($filename) {
  global $skin_folder;

  return htmlspecialchars("/skins4/$skin_folder/images/$filename", ENT_QUOTES);
}

function javascript_tag($filename) {
  $url = h("/js/$filename");

  return "<script type='text/javascript' src='$url'></script>";
}

// TODO: check Accepts header?
function wants_json() {
  return ($_GET['format'] ?? NULL) == 'json';
}

function redirect_to($url) {
  HTTPResponse::LeavePage($url);
}

function redirect_to_back() {
  HTTPResponse::RedirectToPreviousPage();
}

function not_found() {
  HTTPResponse::PageNotFound();
}

function csp2csr($csp) {
  return round(pow($csp / 100, 4), 3);
}

function award_image_tag($award) {
  switch ($award['type'] ?? 'none') {
  case 'medal':
    return "<img src='/images/icon-{$award['value']}star-v2.png' alt='{$award['value']}' />";
  }
}

function chart_icon_image_tag($user) {
  global $cs;
  $proof_icon = $cs->GetChartIcon($user);

  if ($proof_icon) {
    return "<img class='user-badge' src='" . h($proof_icon['image']) . "' title='" . h($proof_icon['title']) . "' alt='" . h($proof_icon['title']) . "' />";
  } else {
    return '<div class="user-badge user-badge--none"></div>';
  }
}

function user_star_image_tags($user) {
  global $cs;
  global $t;

  $stars = $cs->GetStarIcons($user);
  $tags = [];

  foreach ($stars as $star) {
    $star = $t->FormatTranslatorStar($star, $user['username']);
    $tags []= "<img src='{$star['src']}' title='{$star['title']}' alt='{$star['alt']}' />";
  }

  return implode(' ', $tags);
}

function chart_submit_submission_tags($chart, $is_main, $value, $options, $nested = false) {
  global $t;

  $s = $is_main ? "" : "2";
  $type   = $chart["chart_type$s"];
  $prefix = h($chart["score_prefix$s"]);
  $suffix = h($chart["score_suffix$s"]);

  $nested = $nested ? "records[{$chart['level_id']}]" : null;

  $b1 = $is_main ? 1 : 5;
  $b2 = $b1 + 1;
  $b3 = $b1 + 2;
  $b4 = $b1 + 3;

  if ($type == 1 || $type == 2) {
    $h_field = number_field("input$b1", ["input$b1" => $value['hours'] ?? null],    ['id' => "input$b1", "autocomplete" => "off"], $nested);
    $m_field = number_field("input$b2", ["input$b2" => $value['minutes'] ?? null],  ['id' => "input$b2", "autocomplete" => "off"], $nested);
    $s_field = number_field("input$b3", ["input$b3" => $value['seconds'] ?? null],  ['id' => "input$b3", "autocomplete" => "off"], $nested);
    $d_field = number_field("input$b4", ["input$b4" => $value['decimals'] ?? null], ['id' => "input$b4", "autocomplete" => "off"], $nested);

    return "
      <div class='chart-submission-fields'>
        <span class='chart-submission-field'>$h_field</span>
        <span class='chart-submission-suffix'>{$t['chart_submit_hours']}</span>

        <span class='chart-submission-field'>$m_field</span>
        <span class='chart-submission-suffix'>{$t['chart_submit_minutes']}</span>

        <span class='chart-submission-field'>$s_field</span>
        <span class='chart-submission-suffix'>{$t['chart_submit_seconds']}</span>

        <span class='chart-submission-field'>$d_field</span>
        <span class='chart-submission-suffix'>{$t['chart_submit_decimals']}</span>
      </div>
    ";
  } else if ($type == 3 || $type == 4) {
    $field = number_field("input$b1", ["input$b1" => $value], ['id' => "input$b1", "step" => "any", "autocomplete" => "off"], $nested);

    $body = "";
    if ($prefix != "") $body .= "<span class='chart-submission-prefix'>$prefix</span>";
    if ($field != "") $body  .= "<span class='chart-submission-field'>$field</span>";
    if ($suffix != "") $body .= "<span class='chart-submission-suffix'>$suffix</span>";

    return "
      <div class='chart-submission-fields'>
        $body
      </div>
    ";
  } else if ($type == 5) {
    $field = select_field("input$b1", ["input$b1" => $value], $options);

    $body = "";
    if ($prefix != "") $body .= "<span class='chart-submission-prefix'>$prefix</span>";
    if ($field != "") $body  .= "<span class='chart-submission-field'>$field</span>";
    if ($suffix != "") $body .= "<span class='chart-submission-suffix'>$suffix</span>";

    return "
      <div class='chart-submission-fields'>
        $body
      </div>
    ";
  }
}

// div_tag(['class' => 'chart-submission-form-field chart-submission-form-field--single'], [
//   span_tag([], $chart_info['score_prefix']),
//   span_tag([], number_field('input1', ['input1' => $input1], ['id' => 'input1'])),
//   span_tag([], $chart_info['score_suffix']),
// ]);

function youtube_embedded_url($youtube_url) {
  if (strpos($youtube_url, 'youtube') > 0) {
    return str_replace("watch?v=", "/embed/", $youtube_url);
  } else if (strpos($youtube_url, 'youtu.be') > 0) {
    return str_replace("youtu.be/", "www.youtube.com/embed/", $youtube_url);
  }
}

function twitch_embedded_url($twitch_url) {
  if (preg_match("/^https:\/\/www\.twitch\.tv\/videos\/(\d+)$/", $twitch_url, $matches)) {
    $domain = config_domain();
    return "https://player.twitch.tv/?video={$matches[1]}&parent={$domain}&autoplay=false";
  }

  return $twitch_url;
}

function game_rankbutton($game_id, $position, $type = 'game') {
  $path = h(RankbuttonGenerator::game_template_url($game_id, $type));
  $position = h($position);
  $game_id = h($game_id);

  return "
    <div class='rankbutton'>
      <a href='/game_scoreboard.php?board=0&game_id=$game_id&index=0'>
        <img src='$path' title='View scoreboard' alt='Game scoreboard position: $position' />
      </a>
      <h5 style='left:57px;width:28px;text-align:center;'>$position</h5>
    </div>
  ";
}

function global_rankbutton($position, $type = 'game') {
  $path = h(RankbuttonGenerator::global_template_url($type));
  $position = h($position);

  return "
    <div class='rankbutton'>
      <a href='/scoreboards/$type'>
        <img src='$path' title='View scoreboard' alt='Scoreboard position: $position' />
      </a>
      <h5 style='left:57px;width:28px;text-align:center;'>$position</h5>
    </div>
  ";
}

function boxart_image_tag($boxart) {
  $url = h($boxart['url']);

  $html_options = build_html_options(['class' => 'boxart']);

  return "<img $html_options src='$url' />";
}

function record_proof($record) {
  $details = ProofManager::ProofDetails($record);

  $id = $record['record_id'];

  $url = $details['url'];

  if ($details['type'] == 'youtube') {
    $url = h(youtube_embedded_url($url));
    return "<iframe width='496' height='375' src='$url'></iframe>";
  } else if ($details['type'] == 'twitch') {
    $url = h(twitch_embedded_url($url));
    return "<iframe width='496' height='375' src='$url'></iframe>";
  } else if ($details['type'] == 'picture') {
    $url = h($url);
    return "<a href='/proofs/$id' target='_blank'><img src='/proofs/$id' style='max-width: 496px; max-height: 375px; height: auto; width: auto; display: block; margin-left: auto;
    margin-right: auto;' /></a>";
  } else {
    $url = h($url);
    return "<a href='/proofs/$id' target='_blank' class='proof-preview--not-available'>Preview not available</a>";
  }
}

// Returns the plural form of the given word.
// Doesn't cover all cases
function pluralize($word) {
  if (str_ends_with($word, "s")) {
    return "$word";
  } else {
    return "{$word}s";
  }
}

function singularize($word) {
  if (str_ends_with($word, "s")) {
    return trim($word, 's');
  } else {
    return "$word";
  }
}

function t($string, $parameters = []) {
  global $t;

  $keys = array_map(fn($p) => "[$p]", array_keys($parameters));
  $values = array_values($parameters);

  return str_replace($keys, $values, $t[$string]);
}

require_once("includes/helpers/core.php");
require_once("includes/helpers/debug.php");
require_once("includes/helpers/render.php");
require_once("includes/helpers/html.php");
require_once("includes/helpers/database.php");
require_once("includes/helpers/distance_of_time_in_words.php");
require_once("includes/helpers/expression.php");
