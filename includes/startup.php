<?php

set_include_path(__DIR__ . '/..');
require_once __DIR__ . '/../vendor/autoload.php';

require_once("includes/config.php");
$config = load_config(__DIR__ . '/../config.ini');

$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/../src/templates');
$twig = new \Twig\Environment($loader, [
  'cache' => __DIR__ . '/../cache',
  'debug' => true,
  'strict_variables' => $config['app']['debug'],
]);

// FUNCTIONS USED IN PRACTICALLY EVERY FILE ON THE SITE
// These should be refactored and renamed so that they are autoloaded.
require_once("common.php");
require_once("uploads.php");
require_once("email.php");
require_once("helpers.php");

require_once("includes/recaptcha.php");
require_once("includes/search.php");
require_once("includes/cs_class_x.php");
require_once("includes/proof_class.php");
require_once("includes/cron_update_calculator.php");

require_once("comment.php");
require_once("modifiers.php");
require_once("charts.php");
require_once("referral.php");
require_once("user.php");
require_once("scoreboard_class.php");
require_once("chart_icons.php");
require_once("trophies.php");

require_once("src/repositories/api_tokens.php");
require_once("src/repositories/api_token_activities.php");
require_once("src/repositories/csmail_messages.php");
require_once("src/repositories/discord_servers.php");
require_once("src/repositories/email_bans.php");
require_once("src/repositories/game_entities.php");
require_once("src/repositories/games.php");
require_once("src/repositories/groups.php");
require_once("src/repositories/proof_rules.php");
require_once("src/repositories/rankbuttons.php");
require_once("src/repositories/records.php");
require_once("src/repositories/record_histories.php");
require_once("src/repositories/levels.php");
require_once("src/repositories/DeletedLevelsRepository.php");
require_once("src/repositories/StaffTasksRepository.php");
require_once("src/repositories/notifications.php");
require_once("src/repositories/referrals.php");
require_once("src/repositories/rules.php");
require_once("src/repositories/users.php");

require_once("src/lib/rankbutton_generator.php");
require_once("src/lib/entity.php");
require_once("src/lib/tabbed_modules.php");
require_once("src/lib/twig_functions.php");
require_once("src/lib/scoreboard_snapshot.php");
require_once("src/lib/dashboard.php");

// GET SCRIPT START TIME
$timer = new Timer();
$timer->start();

execution_time();

// OPEN DATABASE CONNECTION
database_open();

// UNICODE COMPLIANCE
mb_http_output('UTF-8');
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_language('uni');
ob_start('mb_output_handler');
define("CHARSET", "UTF-8");

// CREATE CLASSES
// TODO: Most of these should probably be static classes.
// In some cases some methods are already static, so we
// just need to update their callers.
$t = new TranslationManager();
$cs = new CSClass();

Flash::Load();

new Params;

// We're loading cookie sessions even on API endpoints.
// It's not great, but we unset current_user things on API endpoints in a middleware.
CurrentUser::load_from_cookie();
