<?php

class GameCacheRebuilder {
  //Rebuild a single game immediately (skipping the queue)
  public static function RebuildGame($game_id) {
    self::RebuildGameBoard($game_id);
    database_delete_by("cacherebuilder", ['game_id' => $game_id]);
  }

  // Add one game to rebuild queue
  public static function QueueGameForRebuild($game_id) {
    db_query("INSERT IGNORE INTO cacherebuilder (game_id) VALUES (?)", [$game_id]);
  }

  // Add multiple games to rebuild queue
  public static function QueueMultipleGamesForRebuild($game_ids) {
    foreach ($game_ids as $game_id) {
      self::QueueGameForRebuild($game_id);
    }
  }

  // Add all games to rebuild queue
  public static function QueueAllGamesForRebuild() {
    $game_ids = pluck(database_fetch_all("SELECT game_id FROM games", []), 'game_id');
    self::QueueMultipleGamesForRebuild($game_ids);
  }

  // Add multiple games from a specified range of game IDs to rebuild queue
  public static function QueueGameRangeForRebuild($lowermost_game, $uppermost_game) {
    $game_ids = pluck(database_fetch_all("SELECT game_id FROM games WHERE ? <= game_id AND game_id <= ?", [$lowermost_game, $uppermost_game]), 'game_id');
    self::QueueMultipleGamesForRebuild($game_ids);
  }

  public static function BuildGameNumSubs($game_id) {
    // TODO: make this a single query to records
    $all         = database_fetch_one("SELECT COUNT(*) AS subs, COUNT(DISTINCT user_id) AS players FROM records WHERE game_id = ?", [$game_id]);
    $unranked    = database_fetch_one("SELECT COUNT(*) AS subs, COUNT(DISTINCT user_id) AS players FROM records WHERE game_id = ? AND csp = 0", [$game_id]);
    $csp         = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_csp WHERE game_id = ?", [$game_id]);
    $medal       = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_medals WHERE game_id = ?", [$game_id]);
    $arcade      = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_arcade WHERE game_id = ?", [$game_id]);
    $speedrun    = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_speedrun WHERE game_id = ?", [$game_id]);
    $solution    = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_solution WHERE game_id = ?", [$game_id]);
    $challenge   = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_userchallenge WHERE game_id = ?", [$game_id]);
    $collectible = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_collectible WHERE game_id = ?", [$game_id]);
    $incremental = database_fetch_one("SELECT SUM(num_subs) AS subs, COUNT(DISTINCT user_id) AS players FROM gsb_cache_incremental WHERE game_id = ?", [$game_id]);

    if (!isset($all['subs'])) { $all['subs'] = 0;}
    if (!isset($unranked['subs'])) { $unranked['subs'] = 0;}
    if (!isset($csp['subs'])) { $csp['subs'] = 0;}
    if (!isset($medal['subs'])) { $medal['subs'] = 0;}
    if (!isset($arcade['subs'])) { $arcade['subs'] = 0;}
    if (!isset($speedrun['subs'])) { $speedrun['subs'] = 0;}
    if (!isset($solution['subs'])) { $solution['subs'] = 0;}
    if (!isset($challenge['subs'])) { $challenge['subs'] = 0;}
    if (!isset($collectible['subs'])) { $collectible['subs'] = 0;}
    if (!isset($incremental['subs'])) { $incremental['subs'] = 0;}

    database_update_by('games', [
      'all_subs'            => $all["subs"],
      'standard_subs'       => $medal["subs"],
      'arcade_subs'         => $arcade["subs"],
      'speedrun_subs'       => $speedrun["subs"],
      'solution_subs'       => $solution["subs"],
      'challenge_subs'      => $challenge["subs"],
      'collectible_subs'    => $collectible["subs"],
      'incremental_subs'    => $incremental["subs"],
      'csp_subs'            => $csp["subs"],
      'unranked_subs'       => $unranked["subs"],
      'all_players'         => $all["players"],
      'standard_players'    => $medal["players"],
      'arcade_players'      => $arcade["players"],
      'speedrun_players'    => $speedrun["players"],
      'solution_players'    => $solution["players"],
      'challenge_players'   => $challenge["players"],
      'collectible_players' => $collectible["players"],
      'incremental_players' => $incremental["players"],
      'csp_players'         => $csp["players"],
      'unranked_players'    => $unranked["players"],
    ], ['game_id' => $game_id]);
  }

  public static function BuildGameNumCharts($game_id) {
    $counts = database_fetch_one("
      SELECT
        COUNT(1) AS total,
        COALESCE(SUM(standard), 0) AS standard,
        COALESCE(SUM(speedrun), 0) AS speedrun,
        COALESCE(SUM(solution), 0) AS solution,
        COALESCE(SUM(unranked), 0) AS unranked,
        COALESCE(SUM(challenge), 0) AS challenge,
        COALESCE(SUM(collectible), 0) AS collectible,
        COALESCE(SUM(incremental), 0) AS incremental,
        COALESCE(SUM(arcade), 0) AS arcade
      FROM levels
      LEFT JOIN chart_modifiers2 USING (level_id)
      WHERE levels.game_id = ?
    ", [$game_id]);

    database_update_by('games', [
      'all_charts'         => $counts["total"],
      'standard_charts'    => $counts["standard"],
      'arcade_charts'      => $counts["arcade"],
      'speedrun_charts'    => $counts["speedrun"],
      'challenge_charts'   => $counts["challenge"],
      'solution_charts'    => $counts["solution"],
      'collectible_charts' => $counts["collectible"],
      'incremental_charts' => $counts["incremental"],
      'csp_charts'         => $counts["total"] - $counts["unranked"],
      'unranked_charts'    => $counts["unranked"],
    ], ['game_id' => $game_id]);
  }

  /** Game cache rebuilding functions.
   *
   * Below are a bunch of functions to recalculate gsb_cache_* tables. This
   * includes some helper methods to calculate things like medal points,
   * challenge points, etc.
   *
   * If a function name starts with Build, it will calculate the information
   * that should go into a cache table, without storing it. StoreCache saves
   * the data to the database. If it starts with Fetch, it will make a database
   * query. When creating functions that have complex formulas, it is
   * recommended to put them in a queryless function and create another
   * function called Fetch* that makes any queries required. This allows us to
   * mass compute these values, and potentially call them from other places in
   * the codebase without automatically taking the performance penalty of extra
   * queries.
   *
   * These should be mostly a roll-up of the chart caches, stored in the
   * records table. There are some exceptions, for example: the records table
   * doesn't have bonus_csr, since that's a game-wide calculation, not chart by
   * chart.
   **/
  public static function RebuildGameBoard($game_id) {
    // Rebuild game information (chart counts, submission counts etc.)
    Modifiers::UpdateChartFlooder($game_id);
    self::BuildGameNumCharts($game_id);
    self::RebuildGameCaches($game_id);
    self::BuildGameNumSubs($game_id);
  }

  public static function RebuildGameCaches($game_id) {
    $game = database_find_by('games', ['game_id' => $game_id]);
    $records = self::FetchRecordsWithModifiers($game_id);

    $caches = [];
    $caches['proof']       = self::BuildGameProofCache($game_id, $records);
    $caches['vproof']      = self::BuildGameVideoProofCache($game_id, $records);
    $caches['subs']        = self::BuildGameSubmissionsCache($game_id, $records);
    $caches['first']       = self::BuildGameFirstCache($game, $records);
    $caches['csp']         = self::BuildGameCSPCache($game, $records);
    $caches['standard']    = self::BuildGameMedalsCache($game, $records);
    $caches['arcade']      = self::BuildGameArcadeCache($game_id, $records);
    $caches['incremental'] = self::BuildGameIncrementalCache($game_id, $records);
    $caches['collectible'] = self::BuildGameCollectibleCache($game_id, $records);
    $caches['speedrun']    = self::BuildGameSpeedrunCache($game_id, $records);
    $caches['solution']    = self::BuildGameSolutionCache($game_id, $records);
    $caches['challenge']   = self::BuildGameUserChallengeCache($game, $records);

    $trophies = self::BuildGameTrophyCache($game, $caches);

    database_transaction(function() use ($game, $game_id, &$caches, &$trophies) {
      self::StoreCache("proof", $game_id, $caches['proof']);
      self::StoreCache("vproof", $game_id, $caches['vproof']);
      self::StoreCache("submissions", $game_id, $caches['subs']);
      self::StoreCache("first", $game_id, $caches['first']);
      self::StoreCache("csp", $game_id, $caches['csp']);
      self::StoreCache("medals", $game_id, $caches['standard']);
      self::StoreCache("arcade", $game_id, $caches['arcade']);
      self::StoreCache("incremental", $game_id, $caches['incremental']);
      self::StoreCache("collectible", $game_id, $caches['collectible']);
      self::StoreCache("speedrun", $game_id, $caches['speedrun']);
      self::StoreCache("solution", $game_id, $caches['solution']);
      self::StoreCache("userchallenge", $game_id, $caches['challenge']);

      self::StoreCache("trophy", $game_id, $trophies);
    });
  }

  public static function StoreCache($cache, $game_id, $records) {
    database_delete_by("gsb_cache_$cache", ['game_id' => $game_id]);
    database_insert_all("gsb_cache_$cache", $records);
  }

  public static function FetchRecordsWithModifiers($game_id) {
    $records = database_filter_by('records', ['game_id' => $game_id]);

    // $levels information is not loaded in $records using a join on purpose
    // to reduce memory usage.
    $levels = index_by(database_fetch_all("
      SELECT chart_modifiers2.*
      FROM levels
      JOIN chart_modifiers2 USING (level_id)
      WHERE game_id = ?
    ", [$game_id]), 'level_id');

    foreach ($records as &$record) {
      $record['level'] = $levels[$record['level_id']];
    }
    unset($record);

    return $records;
  }

  public static function BuildGameProofCache($game_id, $submissions) {
    $users = array_values(group_by($submissions, fn($r) => $r['user_id']));

    $users = array_map(function($rs) {
      $approved = array_filter($rs, fn($r) => RecordManager::IsApproved($r));
      $counts = [
        'user_id' => $rs[0]['user_id'],
        'num_subs' => count($rs),
        'total'       => count($approved),
        'standard' => 0,
        'speedrun' => 0,
        'solution' => 0,
        'unranked' => 0,
        'challenge' => 0,
        'incremental' => 0,
        'collectible' => 0,
        'arcade' => 0,
      ];
      foreach ($approved as $r) {
        $level = $r['level'];
        if ($level['standard']) $counts['standard'] += 1;
        if ($level['speedrun']) $counts['speedrun'] += 1;
        if ($level['solution']) $counts['solution'] += 1;
        if ($level['unranked']) $counts['unranked'] += 1;
        if ($level['challenge']) $counts['challenge'] += 1;
        if ($level['collectible']) $counts['collectible'] += 1;
        if ($level['incremental']) $counts['incremental'] += 1;
        if ($level['arcade']) $counts['arcade'] += 1;
      }

      return $counts;
    }, $users);

    usort($users, function($a, $b) {
      return [-$a['total'], $a['user_id']] <=> [-$b['total'], $b['user_id']];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['total'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['total']);
      $percentage = $top_score == 0 ? 0.0 : $user['total'] / $top_score;

      $record = [
        'user_id'     => $user['user_id'],
        'game_id'     => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'total'          => $user['total'],
        'percentage'     => round($percentage, 6),
        'num_subs'       => $user['num_subs'],

        'medal'       => $user['standard'],
        'speedrun'    => $user['speedrun'],
        'solution'    => $user['solution'],
        'unranked'    => $user['unranked'],
        'challenge'   => $user['challenge'],
        'collectible' => $user['collectible'],
        'incremental' => $user['incremental'],
        'arcade'      => $user['arcade'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameVideoProofCache($game_id, $submissions) {
    $users = array_values(group_by($submissions, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      // TODO: optimize all these array_counts into a single loop. It's uglier but faster.
      'total'       => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
      'standard'    => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['standard']),
      'speedrun'    => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['speedrun']),
      'solution'    => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['solution']),
      'unranked'    => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['unranked']),
      'challenge'   => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['challenge']),
      'collectible' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['collectible']),
      'incremental' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['incremental']),
      'arcade'      => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r) && $r['level']['arcade']),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total'] > $b['total']) return -1;
      if ($a['total'] < $b['total']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['total'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['total']);

      $percentage = $top_score == 0 ? 0 : $user['total'] / $top_score;

      $record = [
        'user_id'     => $user['user_id'],
        'game_id'     => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'total'          => $user['total'],
        'percentage'     => $percentage,
        'num_subs'       => $user['num_subs'],

        'medal'       => $user['standard'],
        'speedrun'    => $user['speedrun'],
        'solution'    => $user['solution'],
        'unranked'    => $user['unranked'],
        'challenge'   => $user['challenge'],
        'collectible' => $user['collectible'],
        'incremental' => $user['incremental'],
        'arcade'      => $user['arcade'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameSubmissionsCache($game_id, $submissions) {
    $users = array_values(group_by($submissions, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'total'    => count($rs),
      'standard'    => array_count($rs, fn($r) => $r['level']['standard']),
      'speedrun'    => array_count($rs, fn($r) => $r['level']['speedrun']),
      'solution'    => array_count($rs, fn($r) => $r['level']['solution']),
      'unranked'    => array_count($rs, fn($r) => $r['level']['unranked']),
      'challenge'   => array_count($rs, fn($r) => $r['level']['challenge']),
      'collectible' => array_count($rs, fn($r) => $r['level']['collectible']),
      'incremental' => array_count($rs, fn($r) => $r['level']['incremental']),
      'arcade'      => array_count($rs, fn($r) => $r['level']['arcade']),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total'] > $b['total']) return -1;
      if ($a['total'] < $b['total']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['total'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['total']);

      $percentage = $top_score == 0 ? 0 : $user['total'] / $top_score;

      $record = [
        'user_id'     => $user['user_id'],
        'game_id'     => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'total'          => $user['total'],
        'percentage'     => round($percentage, 6),
        'num_subs'       => $user['num_subs'],

        'medal'       => $user['standard'],
        'speedrun'    => $user['speedrun'],
        'solution'    => $user['solution'],
        'unranked'    => $user['unranked'],
        'challenge'   => $user['challenge'],
        'collectible' => $user['collectible'],
        'incremental' => $user['incremental'],
        'arcade'      => $user['arcade'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameFirstCache($game, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['first_submitted']);
    $users = array_values(group_by($users, fn($r) => "{$r['user_id']}-" . substr($r['original_date'], 0, 4+1+2)));

    $users = array_map(fn($rs) => [
      'game_id' => $game['game_id'],
      'user_id' => $rs[0]['user_id'],
      'year' => substr($rs[0]['original_date'], 0, 4),
      'month' => substr($rs[0]['original_date'], 5, 2),
      'points' => count($rs),
    ], $users);

    return $users;
  }

  public static function BuildGameCSPCache($game, $submissions) {
    $users = array_filter($submissions, fn($r) => !$r['level']['unranked']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'total_csp' => round(array_sum(array_map(fn($r) => $r['csp'], $rs)), Award::DP_CSP),
      'total_csr' => round(array_sum(array_map(fn($r) => Award::CSR($r['csp']), $rs)), Award::DP_CSR),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total_csp'] > $b['total_csp']) return -1;
      if ($a['total_csp'] < $b['total_csp']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    if (empty($users)) {
      return [];
    }

    $num_subs = array_sum(pluck($users, 'num_subs'));
    $num_players = count($users);
    $num_charts = $game['csp_charts'];
    $rpc = $num_subs / $num_charts;
    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    $first_place_count = 0;

    foreach ($users as &$user) {
      $user['scoreboard_pos'] = $rank->update($user['total_csp']);

      if ($first == 1) {
        $top_score = $user['total_csp'];
        $first = 0;
      }

      if ($top_score == $user['total_csp']) {
        $first_place_count++;
      }
    }
    unset($user);

    $platinum = Trophies::ValidatePlatinumTrophy($first_place_count, $rpc, 'csp');
    foreach ($users as $user) {
      // This series of declarations builds data to fetch trophy information for this scoreboard.
      $user['platinum']      = $platinum && $user['scoreboard_pos'] == 1 ? 1 : 0;
      $user['gold']          = $user['scoreboard_pos'] == 1 ? 1 : 0;
      $user['trophy_points'] = Trophies::TrophyPoints($platinum, $user, $num_players, $rpc, $first_place_count, 0.5);

      $percentage = $top_score == 0 ? 0 : $user['total_csp'] / $top_score;

      $user['bonus_csr'] = Award::BonusCSR($user['total_csr'], $rpc, $user['scoreboard_pos'], $user['num_subs'], $num_charts, $user['num_approved'], $user['num_approved_v'])[0];

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game['game_id'],

        'scoreboard_pos' => $user['scoreboard_pos'],
        'percentage' => round($percentage, 6),
        'rpc' => $rpc,
        'bonus_csr' => $user['bonus_csr'],
        'total_csr' => $user['total_csr'],
        'total_csp' => $user['total_csp'],
        'trophy_points' => $user['trophy_points'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameMedalsCache($game, $submissions) {
    $cmp = function($user) {
      return [
        $user['platinum'],
        $user['gold'],
        $user['silver'],
        $user['bronze'],
      ];
    };

    $users = array_filter($submissions, fn($r) => $r['level']['standard']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'platinum' => array_count($rs, fn($r) => $r['platinum']),
      'gold' => array_count($rs, fn($r) => $r['chart_pos'] == 1),
      'silver' => array_count($rs, fn($r) => $r['chart_pos'] == 2),
      'bronze' => array_count($rs, fn($r) => $r['chart_pos'] == 3),
      'medal_points' => round(array_sum(array_map(fn($r) => Award::CSR($r['csp']), $rs)), Award::DP_CSR),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) use ($cmp) { return $cmp($b) <=> $cmp($a); });

    if (empty($users)) {
      return [];
    }

    $maximum = new Aggregates\Maximum();
    foreach ($users as $user) {
      $maximum->update($user['medal_points']);
    }

    $rpc = $game['standard_subs'] / $game['standard_charts'];
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
      $scoreboard_position = $rank->update($cmp($user));

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game['game_id'],

        'scoreboard_pos' => $scoreboard_position,
        'medal_points' => $user['medal_points'],
        'percentage' => round($maximum->percentage($user['medal_points']), 6),
        'platinum' => $user['platinum'],
        'gold' => $user['gold'],
        'silver' => $user['silver'],
        'bronze' => $user['bronze'],
        'max' => $maximum->max,
        'rpc' => $rpc,
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameSpeedrunCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['speedrun']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'medal_points' => array_sum(pluck($rs, 'medal_points')),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['medal_points'] > $b['medal_points']) return -1;
      if ($a['medal_points'] < $b['medal_points']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['medal_points'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['medal_points']);

      $percentage = $top_score == 0 ? 0 : $user['medal_points'] / $top_score;

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'medal_points' => round($user['medal_points'], 1),
        'percentage' => round($percentage, 6),
        'platinum' => 0,
        'gold' => 0,
        'silver' => 0,
        'bronze' => 0,
        'max' => 0.0,
        'rpc' => 0.0,
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameArcadeCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['arcade']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'tokens' => round(array_sum(pluck($rs, 'arcade_points')), Award::DP_ARCADE),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['tokens'] > $b['tokens']) return -1;
      if ($a['tokens'] < $b['tokens']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['tokens'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['tokens']);

      $percentage = $top_score == 0 ? 0 : $user['tokens'] / $top_score;

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game_id,

        'scoreboard_pos' => $scoreboard_position,
        'tokens' => $user['tokens'],
        'percentage' => round($percentage, 6),
        'num_subs' => $user['num_subs'],
        'num_approved' => intval($user['num_approved']),
        'num_approved_v' => intval($user['num_approved_v']),
      ];

      $records []= $record;
    }

    return $records;
  }

  public static function BuildGameIncrementalCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['incremental']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));

    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'cxp' => round(array_sum(pluck($rs, 'cxp')), Award::DP_INCREMENTAL),
      'vs_cxp' => round(array_sum(pluck($rs, 'vs_cxp')), Award::DP_INCREMENTAL),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['cxp'] > $b['cxp']) return -1;
      if ($a['cxp'] < $b['cxp']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
        if ($first == 1) {
            $top_score = $user['cxp'];
            $first = 0;
        }

        $scoreboard_position = $rank->update($user['cxp']);

        $percentage = $top_score == 0 ? 0 : $user['cxp'] / $top_score;

        $record = [
          'user_id' => $user['user_id'],
          'game_id' => $game_id,

          'scoreboard_pos' => $scoreboard_position,
          'cxp' => $user['cxp'],
          'vs_cxp' => $user['vs_cxp'],
          'percentage' => round($percentage, 6),
          'num_subs' => $user['num_subs'],
          'num_approved' => $user['num_approved'],
          'num_approved_v' => $user['num_approved_v'],
        ];

        $records []= $record;
    }

    return $records;
  }

  public static function BuildGameCollectibleCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['collectible']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));

    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'total_stars' => round(array_sum(array_map(fn($r) => $r['cyberstars'] + RecordManager::IsApproved($r) + RecordManager::IsApprovedVideo($r), $rs)), Award::DP_COLLECTIBLE),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['total_stars'] > $b['total_stars']) return -1;
      if ($a['total_stars'] < $b['total_stars']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
        if ($first == 1) {
          $top_score = $user['total_stars'];
          $first = 0;
        }

        $scoreboard_position = $rank->update($user['total_stars']);

        $percentage = $top_score == 0 ? 0 : $user['total_stars'] / $top_score;

        $record = [
          'user_id' => $user['user_id'],
          'game_id' => $game_id,

          'scoreboard_pos' => $scoreboard_position,
          'cyberstars' => $user['total_stars'],
          'percentage' => round($percentage, 6),
          'num_subs' => $user['num_subs'],
          'num_approved' => $user['num_approved'],
          'num_approved_v' => $user['num_approved_v'],
        ];

        $records []= $record;
    }

    return $records;
  }

  public static function BuildGameSolutionCache($game_id, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['solution']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));

    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'brain_power' => round(array_sum(pluck($rs, 'brain_power')), Award::DP_SOLUTION),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['brain_power'] > $b['brain_power']) return -1;
      if ($a['brain_power'] < $b['brain_power']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
        if ($first == 1) {
            $top_score = $user['brain_power'];
            $first = 0;
        }

        $scoreboard_position = $rank->update($user['brain_power']);

        $percentage = $top_score == 0 ? 0 : $user['brain_power'] / $top_score;

        $record = [
          'user_id' => $user['user_id'],
          'game_id' => $game_id,

          'scoreboard_pos' => $scoreboard_position,
          'brain_power' => $user['brain_power'],
          'percentage' => round($percentage, 6),
          'num_subs' => $user['num_subs'],
          'num_approved' => $user['num_approved'],
          'num_approved_v' => $user['num_approved_v'],
        ];

        $records []= $record;
    }

    return $records;
  }

  public static function BuildGameTrophyCache($game, $scoreboards) {
    $game_id = $game['game_id'];

    $boards = ['csp', 'standard', 'speedrun', 'challenge', 'solution', 'arcade', 'incremental', 'collectible'];

    // This is not the same as csp_charts or all_charts.
    // Some charts belong to more than one chart flag, so they should be counted twice.
    $total_charts = 0;
    foreach ($boards as $board) {
      if ($board != 'csp') {
        $total_charts += $game["{$board}_charts"];
      }
    }

    $users = [];
    $trophies = [];
    foreach ($boards as $board) {
      $trophies[$board] = [];
      if ($game["{$board}_charts"] > 0) {
        $num_players = count($scoreboards[$board]);
        $first_place_count = array_count($scoreboards[$board], fn($u) => $u['scoreboard_pos'] == 1);
        $rpc = $game["{$board}_subs"] / $game["{$board}_charts"];
        $platinum = Trophies::ValidatePlatinumTrophy($first_place_count, $rpc, $board);

        foreach ($scoreboards[$board] as $user) {
          switch ($user['scoreboard_pos']) {
            case 1: $trophy = $platinum ? 'platinum' : 'gold'; break;
            case 2: $trophy = 'silver'; break;
            case 3: $trophy = 'bronze'; break;
            case 4: $trophy = 'fourth'; break;
            case 5: $trophy = 'fifth'; break;
            default: $trophy = $user['scoreboard_pos']; break;
          }

          $users []= $user['user_id'];

          // If this board is the medal table and the user has no medal score, omit them.
          if ($board == 'standard' &&
             $user['platinum'] == 0 &&
             $user['gold'] == 0 &&
             $user['silver'] == 0 &&
             $user['bronze'] == 0) {
            $trophies[$board][$user['user_id']] = [
              'trophy' => "N/A",
              'points' => 0,
            ];
          } else {
            $trophies[$board][$user['user_id']] = [
              'trophy' => $trophy,
              'points' => Trophies::TrophyPoints($platinum, $user, $num_players, $rpc, $first_place_count, max(0.01, $board == 'csp' ? 0.5 : ($game["{$board}_charts"] / $total_charts * 0.5))),
            ];
          }
        }
      }
    }

    $users = array_unique($users);

    $records = [];
    foreach ($users as $user_id) {
      $record = [
        'game_id' => $game_id,
        'user_id' => $user_id,
        'platinum' => 0,
        'gold' => 0,
        'silver' => 0,
        'bronze' => 0,
        'fourth' => 0,
        'fifth' => 0,
        'trophy_points' => 0,
      ];

      foreach ($boards as $board) {
        if (array_key_exists($user_id, $trophies[$board])) {
          $record["{$board}_trophy"]        = $trophies[$board][$user_id]['trophy'];
          $record["{$board}_trophy_points"] = $trophies[$board][$user_id]['points'];

          $record['trophy_points'] += $trophies[$board][$user_id]['points'];
          if ($trophies[$board][$user_id]['trophy'] != NULL && $trophies[$board][$user_id]['trophy'] != "N/A" && !is_numeric($trophies[$board][$user_id]['trophy'])) {
            $record[$trophies[$board][$user_id]['trophy']] += 1;
            if ($trophies[$board][$user_id]['trophy'] == 'platinum') {
              $record['gold'] += 1;
            }
          }
        } else if(count($trophies[$board]) > 0) {
          $record["{$board}_trophy"]        = "N/A";
          $record["{$board}_trophy_points"] = 0.0;
        } else {
          $record["{$board}_trophy"]        = NULL;
          $record["{$board}_trophy_points"] = 0.0;
        }
      }

      $records []= $record;
    }

    usort($records, fn($a, $b) => -$a['trophy_points'] <=> -$b['trophy_points']);
    $rank = new Aggregates\Rank();
    foreach ($records as &$record) {
      $scoreboard_position = $rank->update($record['trophy_points']);
      $record['scoreboard_pos'] = $scoreboard_position;
    }
    unset($record);

    return $records;
  }

  public static function BuildGameUserChallengeCache($game, $submissions) {
    $users = array_filter($submissions, fn($r) => $r['level']['challenge']);
    $users = array_values(group_by($users, fn($r) => $r['user_id']));
    $users = array_map(fn($rs) => [
      'user_id' => $rs[0]['user_id'],
      'num_subs' => count($rs),
      'style_points' => round(array_sum(pluck($rs, 'style_points')), Award::DP_CHALLENGE),
      'num_approved' => array_count($rs, fn($r) => RecordManager::IsApproved($r)),
      'num_approved_v' => array_count($rs, fn($r) => RecordManager::IsApprovedVideo($r)),
    ], $users);

    usort($users, function($a, $b) {
      if ($a['style_points'] > $b['style_points']) return -1;
      if ($a['style_points'] < $b['style_points']) return 1;
      return $a['user_id'] - $b['user_id'];
    });

    if (empty($users)) {
      return [];
    }

    $first = 1;
    $rank = new Aggregates\Rank();
    $records = [];
    foreach ($users as $user) {
      if ($first == 1) {
        $top_score = $user['style_points'];
        $first = 0;
      }

      $scoreboard_position = $rank->update($user['style_points']);

      $percentage = $top_score == 0 ? 0 : $user['style_points'] / $top_score;

      $record = [
        'user_id' => $user['user_id'],
        'game_id' => $game['game_id'],

        'scoreboard_pos' => $scoreboard_position,
        'style_points' => $user['style_points'],
        'percentage' => round($percentage, 6),
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $records []= $record;
    }

    return $records;
  }

  // This function is the same as RebuildGameBoard but limited to rebuilding
  // the incremental caches. It is used in `bin/rebuild` to recalculate the
  // vs_cxp after someone levels up.
  public static function RebuildGameVersusExperience($game_id) {
    $records = self::FetchRecordsWithModifiers($game_id);
    $incremental = self::BuildGameIncrementalCache($game_id, $records);
    self::StoreCache("incremental", $game_id, $incremental);
  }
}
