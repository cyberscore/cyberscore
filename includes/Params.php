<?php

function filter_post_params($params) {
  global $_POST;

  $input = $_POST;

  $results = [];
  array_walk(
    $params,
    function($param) use(&$results, $input) {
      list($key, $value) = $param($input);

      if ($key) {
        $results[$key] = $value;
      }
    }
  );

  return $results;
}

class Params {
  use ParamsV1;
  use ParamsV3;
}

// deprecated
trait ParamsV1 {
  public static function str($key) {
    return function($params) use($key) {
      if (array_search($key, array_keys($params)) !== false) {
        return [$key, $params[$key]];
      }
    };
  }

  public static function array_of_strs($key) {
    return function($params) use ($key) {
      if (array_search($key, array_keys($params)) !== false) {
        if (is_array($params[$key])) {
          return [$key, $params[$key]];
        } else {
          return [$key, []];
        }
      } else {
        return [$key, []];
      }
    };
  }

  public static function array_of_numbers($key) {
    return function($params) use ($key) {
      if (array_search($key, array_keys($params)) !== false) {
        if (is_array($params[$key])) {
          return [$key, array_map(function($x){ return self::numval($x); }, $params[$key])];
        } else {
          return [$key, []];
        }
      } else {
        return [$key, []];
      }
    };
  }

  public static function number($key) {
    return function($params) use($key) {
      if (array_search($key, array_keys($params)) !== false) {
        if ($params[$key] === "") {
          return [$key, NULL];
        } else {
          return [$key, self::numval($params[$key])];
        }
      }
    };
  }

  public static function enum($key, $values = null) {
    if ($values == null) {
      // this belongs to v3, but it's here due to name clashing.
      return new ParamsEnumParser($key);
    }

    return function($params) use($key, $values) {
      if (array_search($key, array_keys($params)) !== false) {
        $value = $params[$key];
        return [$key, in_array($value, $values) ? $value : NULL];
      }
    };
  }

  public static function checkbox($key, $yes, $no) {
    return function($params) use($key, $yes, $no) {
      if (array_search($key, array_keys($params)) !== false) {
        return [$key, $params[$key] == "1" ? $yes : $no];
      }
    };
  }

  private static function numval($str) {
    if (mb_strpos($str, ".") !== false) {
      return floatval($str);
    } else {
      return intval($str);
    }
  }
}

trait ParamsV3 {
  public static function ParsePost($schema) {
    $schema = self::object($schema);

    [$params, $errors] = self::Parse($schema, $_POST);

    if ($errors !== []) {
      var_dump($errors);
      HTTPResponse::Exit();
    }

    return $params;
  }

  public static function ParseGet($schema) {
    $schema = self::object($schema);

    [$params, $errors] = self::Parse($schema, $_GET);

    if ($errors !== []) {
      var_dump($errors);
      HTTPResponse::Exit();
    }

    return $params;
  }

  public static function Parse($schema, $source) {
    return $schema->Parse($source);
  }

  public static function object($schema, $options = []) {
    return new ParamsObjectParser($schema, $options);
  }

  public static function array($schema, $options = []) {
    return new ParamsArrayParser($schema, $options);
  }

  public static function float($options = []) {
    return new ParamsFloatParser($options);
  }

  public static function integer($options = []) {
    return new ParamsIntegerParser($options);
  }

  public static function string($options = []) {
    return new ParamsStringParser($options);
  }

  public static function password($options = []) {
    // TODO: figure out if there's anything, security wise,
    // that would make sense to implement here.
    return new ParamsStringParser($options);
  }

  public static function boolean($options = []) {
    return new ParamsBooleanParser($options);
  }

  public static function date($options = []) {
    return new ParamsDateParser($options);
  }

  public static function datetime($options = []) {
    return new ParamsDatetimeParser($options);
  }
}

class ParamsParser {
  const MISSING_PARAM = "MISSING_PARAM_9012346890746";

  public $default = self::MISSING_PARAM;
  public $required = true;
  public $not_null = true;
  public $empty_string_is_null = true;

  public function default($default) {
    $this->default = $default;
    return $this;
  }

  public function optional() {
    $this->required = false;
    return $this;
  }

  public function nullable() {
    $this->not_null = false;
    return $this;
  }

  public function empty_string_is_null() {
    $this->empty_string_is_null = true;
    return $this;
  }

  public function Parse($source) {
    if ($source == self::MISSING_PARAM) { // || $source === null?
      $source = $this->default;
    }

    if ($this->empty_string_is_null && $source === '') {
      $source = null;
    }

    if ($this->required && $source == ParamsParser::MISSING_PARAM) {
      return [null, ["value must be specified"]];
    }

    if ($this->not_null && $source === null) {
      return [null, ["value can't be null"]];
    }

    if ($source === null) {
      return [null, []];
    }

    if ($source === ParamsParser::MISSING_PARAM) {
      return [$source, []];
    }

    return $this->ParseValue($source);
  }
}

class ParamsObjectParser extends ParamsParser {
  private $schema = null;
  private $options = null;
  public function __construct($schema, $options) {
    $this->schema = $schema;
    $this->options = $options;
  }

  public function ParseValue($source) {
    $params = [];
    $errors = [];
    foreach ($this->schema as $key => $subschema) {
      [$subparam, $suberrors] = $subschema->Parse($source[$key] ?? ParamsParser::MISSING_PARAM);

      if ($suberrors !== []) {
        $errors += array_map(fn($err) => "$key.$err", $suberrors);
      } else if ($subparam !== ParamsParser::MISSING_PARAM) {
        $params[$key] = $subparam;
      }
    }

    return [$params, $errors];
  }
}

class ParamsArrayParser extends ParamsParser {
  private $schema = null;
  private $options = null;
  public function __construct($schema, $options) {
    $this->schema = $schema;
    $this->options = $options;
  }

  public function separator($separator) {
    $this->options['separator'] = $separator;
    $this->empty_string_is_null = false;
    return $this;
  }

  public function ParseValue($source) {
    if (isset($this->options['separator']) && is_string($source)) {
      if ($source === "") {
        $source = [];
      } else {
        $source = explode($this->options['separator'], $source);
        $source = array_map(fn($e) => trim($e), $source);
      }
    }

    if (!is_array($source)) {
      return [null, ["value must be an array"]];
    }

    $params = [];
    $errors = [];
    foreach ($source as $i => $subsource) {
      [$subparam, $suberrors] = $this->schema->Parse($subsource);

      if ($suberrors !== []) {
        $errors += array_map(fn($err) => "#{$i}.$err", $suberrors);
      } else {
        $params []= $subparam;
      }
    }

    return [$params, $errors];
  }
}

class ParamsFloatParser extends ParamsParser {
  private $options = null;
  public function __construct($options) {
    $this->options = $options;
  }

  public function ParseValue($source) {
    if (filter_var($source, FILTER_VALIDATE_FLOAT) === false) {
      return [null, ["value must be a float"]];
    } else {
      return [floatval($source), []];
    }
  }
}

class ParamsIntegerParser extends ParamsParser {
  private $options = null;
  public function __construct($options) {
    $this->options = $options;
  }

  public function ParseValue($source) {
    if (filter_var($source, FILTER_VALIDATE_INT) === false) {
      return [null, ["value must be an integer"]];
    } else {
      return [intval($source), []];
    }
  }
}

class ParamsBooleanParser extends ParamsParser {
  private $options = null;
  public function __construct($options) {
    $this->options = $options;
  }

  public function ParseValue($source) {
    if ($source === true || $source == 'yes' || $source == '1') {
      return [true, []];
    }

    if ($source === false || $source == 'no' || $source == '0') {
      return [false, []];
    }

    return [null, ["value must be a boolean (or yes/no/1/0)"]];
  }
}

class ParamsStringParser extends ParamsParser {
  private $options = null;
  public function __construct($options) {
    $this->options = $options;
    $this->empty_string_is_null = false;
  }

  public function ParseValue($source) {
    if (!is_string($source)) {
      return [null, ["value must be a string"]];
    }

    if (isset($this->options['min-length']) && strlen($source) < $this->options['min-length']) {
      return [null, ["value must be at least {$this->options['min-length']} characters"]];
    }

    return [strval($source), []];
  }
}

class ParamsEnumParser extends ParamsParser {
  private $options = null;
  public function __construct($options) {
    $this->options = $options;
  }

  public function ParseValue($source) {
    if (in_array($source, $this->options)) {
      return [$source, []];
    }

    $msg = implode(", ", $this->options);
    return [null, ["value must be one of the following options: {$msg}"]];
  }
}

class ParamsDateParser extends ParamsParser {
  private $options = null;
  public function __construct($options) {
    $this->options = $options;
  }

  public function ParseValue($source) {
    $date = DateTime::createFromFormat("Y-m-d", $source);

    if ($date === false) {
      return [null, ["value must be a date in the format YYYY-MM-DD"]];
    }

    return [strval($source), []];
  }
}

class ParamsDatetimeParser extends ParamsParser {
  private $options = null;
  public function __construct($options) {
    $this->options = $options;
  }

  public function ParseValue($source) {
    $date = DateTime::createFromFormat("Y-m-d\TH:i:s", $source);
    if ($date === false) {
      $date = DateTime::createFromFormat("Y-m-d\TH:i", $source);
    }

    if ($date === false) {
      return [null, ["value must be a datetime in the format yyyy-mm-ddThh:ii:ss"]];
    }

    return [strval($source), []];
  }
}
