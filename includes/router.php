<?php

function get($path, $file = NULL, $params = []) {
  Router::instance()->get($path, $file, $params);
}

function post($path, $file = NULL) {
  Router::instance()->post($path, $file, []);
}

function redirect($path, $target) {
  Router::instance()->redirect($path, $target);
}
function before($middleware) {
  Router::instance()->before($middleware);
}

function scope($prefix, $definitions) {
  Router::instance()->scope($prefix, $definitions);
}

function resources($resource, $options = []) {
  Router::instance()->resources($resource, $options);
}


class RouterStack {
  private $items = [];

  public function __construct($default) {
    $this->items []= $default;
  }

  public function push() {
    array_unshift($this->items, $this->items[0]);
  }

  public function pop() {
    array_shift($this->items);
  }

  public function &get() {
    return $this->items[0];
  }

  public function set($what) {
    $this->items[0] = $what;
  }
}

class Router {
  private static $singleton = null;

  private $middlewares = null;
  private $url_prefix = null;
  private $controller_prefix = null;
  public $routes = [
    'GET' => [],
    'POST' => [],
  ];
  public $matched_target = null;

  public function __construct() {
    $this->middlewares = new RouterStack([]);
    $this->url_prefix = new RouterStack("");
    $this->controller_prefix = new RouterStack("");
  }

  static function instance() {
    if (self::$singleton === null) {
      self::$singleton = new Router();
    }

    return self::$singleton;
  }

  public function get($path, $file = null, $params = []) {
    $this->serve('GET', $path, $file, $params);
  }
  public function post($path, $file = null, $params = []) {
    $this->serve('POST', $path, $file, $params);
  }
  public function resources($resource, $options = []) {
    $actions = $options['only'] ?? ['index', 'new', 'show', 'edit', 'create', 'update', 'delete'];

    if (in_array('index', $actions)) {
      $this->get("$resource", "$resource/index");
      $this->get("$resource/index", "$resource/index");
    }
    if (in_array('new', $actions)) {
      $this->get("$resource/new", "$resource/new");
    }
    if (in_array('show', $actions)) {
      $this->get("$resource/{id}", "$resource/show");
    }
    if (in_array('edit', $actions)) {
      $this->get("$resource/{id}/edit", "$resource/edit");
    }
    if (in_array('create', $actions)) {
      $this->post("$resource", "$resource/create");
    }
    if (in_array('update', $actions)) {
      $this->post("$resource/{id}", "$resource/update");
    }
    if (in_array('delete', $actions)) {
      $this->post("$resource/{id}/delete", "$resource/delete");
    }
  }
  public function redirect($path, $target) {
    $this->serve('GET', $path, new RouteRedirect($target), []);
  }
  public function before($middleware) {
    $this->middlewares->get() []= $middleware;
  }
  public function scope($prefix, $definitions) {
    $this->middlewares->push();
    $this->url_prefix->push();
    $this->controller_prefix->push();

    $this->url_prefix->set("{$this->url_prefix->get()}/$prefix");
    $this->controller_prefix->set("{$this->controller_prefix->get()}$prefix/");

    $definitions();

    $this->middlewares->pop();
    $this->url_prefix->pop();
    $this->controller_prefix->pop();
  }

  public function serve($method, $path, $target, $params) {
    if ($target === null) {
      $target = $path;
    }

    if (is_string($target)) {
      $target = ltrim($target, "/");
      $target = "{$this->controller_prefix->get()}$target";
    }

    $path = "/" . ltrim($path, "/");
    $path = "{$this->url_prefix->get()}$path";

    $regexp = preg_replace(
      '/\\\{([^\/]*)\\\}/',
      '(?P<\1>[^\/]*?)',
      preg_quote($path, '/')
    );

    if (is_string($target)) {
      $target = new RouteController($target);
    }

    $this->routes[$method] []= [$regexp, $target, $params, $this->middlewares->get()];
  }

  public function match($method, $uri) {
    $url = parse_url($uri);

    foreach ($this->routes[strtoupper($method)] as $match) {
      [$regexp, $target, $params, $middlewares] = $match;

      if (preg_match("/^$regexp(\.(?P<format>.+))?$/", $url['path'], $matches)) {
        $matches = array_filter(
          $matches,
          function ($k) { return is_string($k); },
          ARRAY_FILTER_USE_KEY
        );

        $this->matched_target = $target;
        return [
          $target,
          array_merge($matches, $params),
          $middlewares,
        ];
      }
    }

    return NULL;
  }
}

class RouteRedirect {
  private $target = null;

  public function __construct($target) {
    $this->target = $target;
  }

  public function target($params) {
    return preg_replace_callback(
      '/\{([^\/]*)\}/',
      function($m) use ($params) { return $params[$m[1]]; },
      $this->target
    );
  }

  public function __toString() {
    return "redirect({$this->target})";
  }

  public function run($params) {
    try {
      redirect_to($this->target($params));
    } catch (EarlyExitException $e) {
    }
  }
}

class RouteController {
  private $target = null;

  public function __construct($target) {
    $this->target = $target;
  }

  public function controller() {
    $controller = path_to_controller_class($this->target);

    if (class_exists($controller)) {
      return new $controller();
    } else {
      return null;
    }
  }

  public function page() {
    $page = __DIR__ . "/../src/pages/{$this->target}.php";

    if (file_exists($page)) {
      return $page;
    } else {
      return null;
    }
  }

  public function __toString() {
    return "controller({$this->target})";
  }

  public function run($params) {
    $__controller = $this->controller();
    $__page = $this->page();

    try {
      if ($__controller !== null) {
        $__controller->run();
      } else if ($__page !== null) {
        global $current_user;
        global $t;
        global $cs;
        require($__page);
      } else {
        http_response_code(500);
        render_with("home/404", ['page_title' => "Internal server error"]);
      }
      HTTPResponse::Exit();
    } catch (EarlyExitException $e) {
    }
  }
}

function path_to_controller_class($path) {
  $parts = explode("/", $path);
  $parts = array_map(fn($p) => kebab_to_camel($p), $parts);
  array_unshift($parts, "\\Controllers");
  return implode("\\", $parts);
}

function extract_request_params($params) {
  global $_GET;
  global $_POST;
  global $_SERVER;

  $_GET = array_merge($_GET, $params);
  if (($_SERVER["CONTENT_TYPE"] ?? "") == "application/json") {
    $_POST = json_decode(file_get_contents('php://input'), true);
  }
}
