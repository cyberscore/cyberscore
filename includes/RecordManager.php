<?php

class RecordManager {
  private static function CheckForExistingRecord($chart_id, $user_id) {
    $record = database_find_by('records', ['user_id' => $user_id, 'level_id' => $chart_id]);
    if ($record) {
      return $record;
    }

    return false;
  }

  public static function IsUnderInvestigation($record) {
    $rec_status = $record['rec_status'] ?? 0;
    return ($rec_status == 1 || $rec_status == 2 || $rec_status == 5 || $rec_status == 6);
  }

  public static function IsApproved($record) {
    return $record['rec_status'] == 3 || $record['rec_status'] == 4;
  }

  public static function IsApprovedVideo($record) {
    return $record['rec_status'] == 3 && ($record['proof_type'] == 'v' || $record['proof_type'] == 's' || $record['proof_type'] == 'r');
  }

  public static function IsValidScore($chart, $record) {
    if ($chart['conversion_factor'] != 0) {
      return isset($record['submission']) != isset($record['submission2']);
    } else {
      return isset($record['submission']) || isset($record['submission2']);
    }
  }

  public static function SubmitQueuedRecord($queued_record) {
    $chart = LevelsRepository::get($queued_record['chart_id']);
    $record = RecordsRepository::find_by(['level_id' => $queued_record['chart_id'], 'user_id' => $queued_record['user_id']]);

    return self::SubmitRecord(
      $chart,
      $record,
      $queued_record['entity_id'],
      $queued_record['entity_id2'],
      $queued_record['entity_id3'],
      $queued_record['entity_id4'],
      $queued_record['entity_id5'],
      $queued_record['entity_id6'],
      $queued_record['platform_id'],
      $queued_record['submission'],
      $queued_record['submission2'],
      $queued_record['comment'],
      $queued_record['user_id'],
      '',
      $queued_record['extra1'],
      $queued_record['extra2'],
      $queued_record['extra3'],
      $queued_record['game_patch']
    );
  }

  public static function SubmitRecord(
    $chart,
    $existing_record,
    $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6,
    $platform_id, $submission, $submission2, $comment, $user_id, $other_user,
    $extra1, $extra2, $extra3, $game_patch
  ) {
    $valid_entities = pluck(database_fetch_all("SELECT game_entity_id FROM game_entities WHERE game_id = ?", [$chart['game_id']]), 'game_entity_id');
    $available_platform_ids = pluck(database_fetch_all("SELECT platform_id FROM game_platforms WHERE game_id = ?", [$chart['game_id']]), 'platform_id');

    // Just make sure everything is okay
    $min_entities = $chart['min_entities'];
    $max_entities = $chart['max_entities'];
    $entity_lock  = $chart['entity_lock'];

    if (count($valid_entities) == 0) {
        $min_entities = 0;
    }

    if ($entity_lock != '' && $min_entities != 0) {
      $allowed_entities = explode(',', $entity_lock);
      if (!in_array($entity_id, $allowed_entities)) {
        return ['error_entity', NULL, NULL];
      }
    }

    // Truncate comments that are too long. We remove trailing and leading whitespace first to retain the highest amount of actual content possible.
    $comment = trim($comment);
    if (strlen($comment) > 300) {
      $comment = substr($comment, 0, 300);
    }

    if (($entity_id  !== NULL && !in_array($entity_id,  $valid_entities)) ||
      ($entity_id2 !== NULL && !in_array($entity_id2, $valid_entities)) ||
      ($entity_id3 !== NULL && !in_array($entity_id3, $valid_entities)) ||
      ($entity_id4 !== NULL && !in_array($entity_id4, $valid_entities)) ||
      ($entity_id5 !== NULL && !in_array($entity_id5, $valid_entities)) ||
      ($entity_id6 !== NULL && !in_array($entity_id6, $valid_entities))) {
      // invalid entities
      return ['error_entity', NULL, NULL];
    }

    if (($entity_id  === NULL && $min_entities >= 1) ||
      ($entity_id2 === NULL && $min_entities >= 2) ||
      ($entity_id3 === NULL && $min_entities >= 3) ||
      ($entity_id4 === NULL && $min_entities >= 4) ||
      ($entity_id5 === NULL && $min_entities >= 5) ||
      ($entity_id6 === NULL && $min_entities >= 6)) {
      // needs more entities
      return ['error_entity', NULL, NULL];
    }

    if ($platform_id === NULL && count($available_platform_ids) == 1) {
      $platform_id = $available_platform_ids[0];
    }

    if (!in_array($platform_id, $available_platform_ids)) {
      return ['error_platform', NULL, NULL];
    }

    // We have a few options here:
    // - no previous record
    // - previous record had different score
    // - previous record had same score but different metadata
    // - everything is the same, what are you doing?

    if ($existing_record == null) {
      $record_id = self::AddRecord($chart, $user_id, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch);

      ChartCacheRebuilder::RebuildChart($chart['level_id']);
      self::HandleYGB($chart['game_id'], $chart['level_id'], $record_id);
      self::AddRecordHistoryEntry($record_id, 'f');
      return ['success', 'add', $record_id];
    } else if ($submission != $existing_record['submission'] || ($submission2 != NULL && $submission2 != $existing_record['submission2'])) {
      $old_leaders = database_value("SELECT COUNT(record_id) FROM records WHERE chart_pos = 1 AND level_id = ?", [$chart['level_id']]);

      self::UpdateRecord($existing_record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch);

      ChartCacheRebuilder::RebuildChart($chart['level_id']);
      if ($existing_record['chart_pos'] != 1 || $old_leaders > 1) {
        self::HandleYGB($chart['game_id'], $chart['level_id'], $existing_record['record_id']);
      }
      // Store the old comment in the history and insert a new entry
      self::StoreCommentInHistory($existing_record['record_id'], $existing_record['comment']);
      self::AddRecordHistoryEntry($existing_record['record_id'], 'u');
      return ['success', 'update', $existing_record['record_id']];
    } else if ($entity_id != $existing_record['entity_id'] || $entity_id2 != $existing_record['entity_id2'] ||
      $entity_id3 != $existing_record['entity_id3'] || $entity_id4 != $existing_record['entity_id4'] ||
      $entity_id5 != $existing_record['entity_id5'] || $entity_id6 != $existing_record['entity_id6'] ||
      $platform_id != $existing_record['platform_id'] || $comment != $existing_record['comment'] ||
      (($extra1 != NULL || $existing_record['extra1']) && $extra1 != $existing_record['extra1']) ||
      (($extra2 != NULL || $existing_record['extra2']) && $extra2 != $existing_record['extra2']) ||
      (($extra3 != NULL || $existing_record['extra3']) && $extra3 != $existing_record['extra3']) ||
      $other_user != $existing_record['other_user'] || $game_patch != $existing_record['game_patch']) {
      self::ChangeRecord($existing_record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch);
      return ['success', 'change', $existing_record['record_id']];
    } else {
      return ['success', 'no change', $existing_record['record_id']];
    }
  }

  public static function ModEditRecord($chart_id, $record_id, $entity_ids, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $linked_proof, $game_patch) {
    [$entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6] = $entity_ids;

    $record = database_fetch_one("SELECT submission, submission2 FROM records WHERE record_id = ?", [$record_id]);

    $old_submission = $record['submission'];
    $old_submission2 = $record['submission2'];

    self::EditUpdateRecord(
      $record_id,
      $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6,
      $platform_id,
      $submission,
      $submission2,
      $comment,
      $extra1, $extra2, $extra3,
      $linked_proof,
      $game_patch
    );

    if ($old_submission != $submission || ($submission2 != NULL && $old_submission2 != $submission2)) {
      ChartCacheRebuilder::RebuildChart($chart_id);
      self::AddRecordHistoryEntry($record_id, 'e');
      return 'update';
    } else {
      return 'change';
    }
  }

    //---------------------------------------------------------------------------
    private static function AddRecord($chart, $user_id, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch) {
      return RecordsRepository::create([
        'user_id' => $user_id,
        'other_user' => $other_user,
        'game_id' => $chart['game_id'],
        'level_id' => $chart['level_id'],
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'entity_id_original' => 0,
        'report_reason' => '',
        'platform_id' => $platform_id,
        'submission' => $submission,
        'submission2' => $submission2,
        'original_date' => database_now(),
        'last_update' => database_now(),
        'rec_status' => 0,
        'proof_type' => '',
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'ranked' => $chart['ranked'],
        'game_patch' => $game_patch,
      ]);
    }

    //---------------------------------------------------------------------------
    private static function UpdateRecord($record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch) {
      RecordsRepository::update($record['record_id'], [
        'other_user' => $other_user,
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'platform_id' => $platform_id,
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'submission' => $submission,
        'submission2' => $submission2,
        'last_update' => database_now(),
        'linked_proof' => '',
        'proof_type' => '',
        'rec_status' => 0,
        'game_patch' => $game_patch
      ]);
    }

    //---------------------------------------------------------------------------
    private static function ChangeRecord($record, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $comment, $extra1, $extra2, $extra3, $other_user, $game_patch) {
      RecordsRepository::update($record['record_id'], [
        'other_user' => $other_user,
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'platform_id' => $platform_id,
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'game_patch' => $game_patch,
      ]);
    }

    //---------------------------------------------------------------------------
    private static function EditUpdateRecord($record_id, $entity_id, $entity_id2, $entity_id3, $entity_id4, $entity_id5, $entity_id6, $platform_id, $submission, $submission2, $comment, $extra1, $extra2, $extra3, $linked_proof, $game_patch) {
      RecordsRepository::update($record_id, [
        'entity_id' => $entity_id,
        'entity_id2' => $entity_id2,
        'entity_id3' => $entity_id3,
        'entity_id4' => $entity_id4,
        'entity_id5' => $entity_id5,
        'entity_id6' => $entity_id6,
        'platform_id' => $platform_id,
        'comment' => $comment,
        'extra1' => $extra1,
        'extra2' => $extra2,
        'extra3' => $extra3,
        'submission' => $submission,
        'submission2' => $submission2,
        'linked_proof' => $linked_proof,
        'game_patch' => $game_patch,
      ]);
    }


    //----------------------------------------------------------------------------
    public static function MoveRecord($record_id, $new_chart_id, $mod_id) {
      $old_chart_id = database_value("SELECT level_id FROM records WHERE record_id = ?", [$record_id]);
      $new_game_id = database_value("SELECT game_id FROM levels WHERE level_id = ?", [$new_chart_id]);
      $ranked = database_value("SELECT ranked FROM levels WHERE level_id = ?", [$new_chart_id]);

      RecordsRepository::update($record_id, [
        'level_id' => $new_chart_id,
        'game_id' => $new_game_id,
        'ranked' => $ranked,
      ]);

      database_insert('staff_tasks', [
        'user_id' => $mod_id,
        'tasks_type' => 'record_moved',
        'task_id' => $record_id,
        'result' => 'Record moved',
      ]);

      self::AddRecordHistoryEntry($record_id, 'm');
      ChartCacheRebuilder::RebuildChart($old_chart_id);
      ChartCacheRebuilder::RebuildChart($new_chart_id);
    }

    //----------------------------------------------------------------------------
    public static function MoveDeletedRecord($record_id, $new_chart_id, $mod_id) {
      $new_game_id = database_value("SELECT game_id FROM levels WHERE level_id = ?", [$new_chart_id]);
      $ranked = database_value("SELECT ranked FROM levels WHERE level_id = ?", [$new_chart_id]);

      DeletedRecordsRepository::update($record_id, [
        'level_id' => $new_chart_id,
        'game_id' => $new_game_id,
        'ranked' => $ranked,
      ]);

      database_insert('staff_tasks', [
        'user_id' => $mod_id,
        'tasks_type' => 'record_moved',
        'task_id' => $record_id,
        'result' => 'Record moved',
      ]);
    }

    public static function HandleYGB($game_id, $chart_id, $record_id) {
      // We only send YGB notifications if:
      // - this record is the sole leader
      // - this record is a *new* leader (this is checked before calling this function, not here)

      $leader_user_id = database_value("SELECT user_id FROM records WHERE record_id = ?", [$record_id]);
      $chart_pos = database_value("SELECT chart_pos FROM records WHERE record_id = ?", [$record_id]);
      $sole_leader = database_value("SELECT COUNT(record_id) = 1 FROM records WHERE chart_pos = 1 AND level_id = ?", [$chart_id]);

      if ($chart_pos == 1 && $sole_leader) {
        // Get the user ids of the folks that were downgraded to second place
        $beaten_user_ids = array_map(
          fn($user) => $user['user_id'],
          database_fetch_all("SELECT user_id FROM records WHERE chart_pos = 2 AND level_id = ?", [$chart_id])
        );

        Notification::YGB($game_id, $chart_id, $leader_user_id)->DeliverToUsers($beaten_user_ids);
      }
    }

    //---------------------------------------------------------------------------
    private static function StoreCommentInHistory($record_id, $comment) {
        $history_id = database_value("SELECT MAX(history_id) FROM record_history WHERE record_id = ? AND update_type IN('f', 'u')", [$record_id]);

        RecordHistoriesRepository::update($history_id, ['comment' => $comment]);
    }

    //---------------------------------------------------------------------------
    public static function AddRecordHistoryEntry($record_id, $history_mode) {
      $record = RecordsRepository::get($record_id);

      return RecordHistoriesRepository::create([
        'record_id' => $record_id,
        'update_type' => $history_mode,
        'update_date' => database_now(),
        'submission' => $record['submission'],
        'submission2' => $record['submission2'],
        'chart_pos' => $record['chart_pos'],
        'game_id' => $record['game_id'],
        'user_id' => $record['user_id'],
      ]);
    }

    public static function ResetRecordStatus($record_id) {
      $record = RecordsRepository::get($record_id);
      
      // Reset Record Status needs to respect whether or not the record has a linked proof or not. 
      // Otherwise we can orphan records into unlinked/unhandled rec_status & proof_status values.
      // ... Therefore, if it has linked proof, back to pending proofs with you.
      if(ProofManager::ProofDetails($record_id) == NULL) { $record_status = 0; }
      else { $record_status = 4; }

      database_update_by('records', ['rec_status' => $record_status, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => NULL, 'proof_mod' => ''], ['record_id' => $record_id]);
      database_update_by('staff_tasks', ['result' => 'Record status reset'], ['tasks_type' => 'record_investigated', 'task_id' => $record_id]);

      self::AddRecordHistoryEntry($record_id, 'r');
    }

    //---------------------------------------------------------------------------
    public static function TrackApproval($record_id, $mod_id) {
      database_insert('record_approvals', ['record_id' => $record_id, 'approval_date' => database_now(), 'user_id' => $mod_id]);
    }

    //---------------------------------------------------------------------------
    public static function ApproveRecord($record_id, $proof_type) {
      database_update_by('records', ['rec_status' => 3, 'proof_type' => $proof_type, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => null, 'proof_mod' => ''], ['record_id' => $record_id]);
    }

    public static function DeleteRecord($record_id, $user_id, $chart_id) {
      database_select("INSERT INTO records_del(SELECT * FROM records WHERE record_id = ?)", 's', [$record_id]);
      database_insert('staff_tasks', ['user_id' => $user_id, 'tasks_type' => 'record_deleted', 'task_id' => $record_id, 'result' => 'Record deleted']);
      database_update_by('staff_tasks', ['result' => 'Record deleted'], ['tasks_type' => 'record_investigated', 'task_id' => $record_id]);

      $deleted_count = database_value("SELECT COUNT(*) FROM records_del WHERE record_id = ?", [$record_id]);

      // We only really delete records if we managed to store a copy in records_del
      if ($deleted_count == 1) {
        // Who deleted it and when?
        database_insert('records_del_info', ['record_id' => $record_id, 'deleter_id' => $user_id, 'delete_date' => database_now()]);
        database_delete_by('records', ['record_id' => $record_id]);

        ChartCacheRebuilder::RebuildChart($chart_id);
      }
    }

    public static function ReinstateRecord($record_id, $mod_id, $user_id, $chart_id) {
      $existing_record = self::CheckForExistingRecord($chart_id, $user_id);

      if ($existing_record == false) {
        // Move the record from the deleted table to the live table
        database_transaction(function() use ($record_id, $chart_id, $mod_id) {
          database_select("INSERT INTO records(SELECT * FROM records_del WHERE record_id = ?)", 's', [$record_id]);

          // Reset the record status
          database_update_by('records', ['rec_status' => 0, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => null, 'proof_mod' => '', 'linked_proof' => ''], ['record_id' => $record_id]);

          database_insert('staff_tasks', ['user_id' => $mod_id, 'tasks_type' => 'record_reinstated', 'task_id' => $record_id, 'result' => 'Record reinstated']);
          database_update_by('staff_tasks', ['result' => 'Record reinstated'], ['tasks_type' => 'record_deleted', 'task_id' => $record_id]);
          database_delete_by('records_del', ['record_id' => $record_id]);
          database_delete_by('records_del_info', ['record_id' => $record_id]);

          $comment = database_value("SELECT comment FROM records WHERE record_id = ?", [$record_id]);

          ChartCacheRebuilder::RebuildChart($chart_id);
          self::StoreCommentInHistory($record_id, $comment);
          self::AddRecordHistoryEntry($record_id, 's');
        });

        return "reinstated";
      } else {
        return "failed";
      }
    }

    public static function TerminateRecord($record_id, $chart_id, $user_id) {
      database_insert('staff_tasks', [
        'user_id' => $user_id,
        'tasks_type' => 'record_terminated',
        'task_id' => $record_id,
        'result' => 'Record terminated',
      ]);

      database_delete_by('records', ['record_id' => $record_id]);
      database_delete_by('record_history', ['record_id' => $record_id]);

      ChartCacheRebuilder::RebuildChart($chart_id);
    }

    //---Reverts to a prior record value of choice---
    public static function RevertRecord($record_id, $history_id, $mod_id) {
      $reverted_submission = database_value("SELECT submission FROM record_history WHERE history_id = ?", [$history_id]);
      $reverted_submission2 = database_value("SELECT submission2 FROM record_history WHERE history_id = ?", [$history_id]);
      $reverted_submissions_date = database_value("SELECT update_date FROM record_history WHERE history_id = ?", [$history_id]);

      if ($reverted_submission2 == null) {
        database_update_by('records', ['submission' => $reverted_submission, 'last_update' => $reverted_submissions_date, 'rec_status' => 0, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => NULL, 'proof_mod' => '', 'linked_proof' => ''], ['record_id' => $record_id]);
      } else {
        database_update_by('records', ['submission' => $reverted_submission, 'submission2' => $reverted_submission2, 'last_update' => $reverted_submissions_date, 'rec_status' => 0, 'rec_reporter' => 0, 'report_reason' => '', 'proof_deadline' => NULL, 'proof_mod' => '', 'linked_proof' => ''], ['record_id' => $record_id]);
      }

      database_insert('staff_tasks', ['user_id' => $mod_id, 'tasks_type' => 'record_reverted', 'task_id' => $record_id, 'result' => 'Record reverted']);

      $chart_id = database_value("SELECT level_id FROM records WHERE record_id = ?", [$record_id]);
      $game_id  = database_value("SELECT game_id  FROM records WHERE record_id = ?", [$record_id]);
      $comment  = database_value("SELECT comment  FROM records WHERE record_id = ?", [$record_id]);

      ChartCacheRebuilder::RebuildChart($chart_id);
      self::StoreCommentInHistory($record_id, $comment);
      self::AddRecordHistoryEntry($record_id, 'v');
    }

    public static function SortChartsAlphabetically($game_id, $group_id) {
      $charts = database_fetch_all("
        SELECT level_id FROM levels WHERE game_id = ? AND group_id = ? ORDER BY level_name ASC
      ", [$game_id, $group_id]);

      foreach ($charts as $i => $chart) {
        database_update_by('levels', ['level_pos' => $i + 1], ['level_id' => $chart['level_id']]);
      }
    }
}
