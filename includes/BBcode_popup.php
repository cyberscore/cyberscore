<script type="text/javascript" src="../js/popup.js"></script>
<style>
#popUpBBC {
  padding-right: 40px;
  padding-bottom: 20px;
  padding-left: 40px;
  overflow-y: auto;
  text-align: left;
  position:relative;
  font-family: 'Open Sans', serif;
  width:723px;
  height:73%;
  z-index: 9003;
}

#popUpBB {
  position:absolute;
  width:800px;
  padding-right: 10px;
  text-align: right;
  height:40%;
  border-radius: 22px 22px 22px 22px;
  -moz-border-radius: 22px 22px 22px 22px;
  -webkit-border-radius: 22px 22px 22px 22px;
  border: 5px solid #23538a;
  -webkit-box-shadow: 10px 10px 29px 0px rgba(0,0,0,0.69);
  -moz-box-shadow: 10px 10px 29px 0px rgba(0,0,0,0.69);
  box-shadow: 10px 10px 29px 0px rgba(0,0,0,0.69);
  background: #ffffff;
  z-index: 9002;

}


#popUpBB a { position:relative; top:20px; left:20px}

#popUpBB ::-webkit-scrollbar {
    width: 18px;
    height: 18px;
}
#popUpBB ::-webkit-scrollbar-thumb {
    height: 6px;
    width: 4px;
    border: 4px solid rgba(0, 0, 0, 0);
    background-clip: padding-box;
    -webkit-border-radius: 7px;
    background-color: rgba(0, 0, 0, 0.15);
    -webkit-box-shadow: inset -1px -1px 0px rgba(0, 0, 0, 0.05), inset 1px 1px 0px rgba(0, 0, 0, 0.05);
}
#popUpBB ::-webkit-scrollbar-button {
    width: 0;
    height: 0;
    display: none;
}
#popUpBB ::-webkit-scrollbar-corner {
    background-color: transparent;
    }
</style>

<div id="popUpBB" style="display:none;">
  <h3><a href="#" onclick="popup('popUpBB')" ><img src="/images/x.png" height="12" width="12" alt="<?php echo $t['tools_close'];?>"?></a></h3>

  <div id="popUpBBC">
    <h1>Available BB-Code</h1>
    Don't input the { and }, they are there to show you where your input would go.
    <table>
      <tr><td colspan="2"><b>Text decoration</b></td></tr>
      <tr><td>[u]{Text}[/u]</td><td><u>Underlined text</u></td></tr>
      <tr><td>[b]{Text}[/b]</td><td><b>Bold text</b></td></tr>
      <tr><td>[i]{Text}[/i]</td><td><i>Italic text</i></td></tr>
      <tr><td>[s]{Text}[/s]</td><td><s>Struckthrough text</s></td></tr>
      <tr><td>[c]{Text}[/c]</td><td><span style="background-color: #e6e6ff; color: #000;"><code>In-line code fragment</code></span></td></tr>
      <tr><td>[color={color}]{Text}[/c]</td><td rowspan="2"><span style="color: red;"><code>Text colour</code></span></td></tr>
      <tr><td>[colour={colour}]{Text}[/c]</td></tr>
      <tr><td>[size={size}]{Text}[/c]</td><td><span style="font-size: 20px;"><code>Text size</code></span></td></tr>
    </table>

    <br /><br />
    <table>
      <tr><td colspan="2"><b>Media</b></td></tr>
      <tr><td>[url]{link}[/url]</td><td>Hyperlinked URL<br /><a href="https://cyberscore.me.uk">https://cyberscore.me.uk</a><br /></td></tr>
      <tr><td>[url={link}]{Text}[/url]</td><td>Hyperlinked text<br /><a href="https://cyberscore.me.uk">Cyberscore index page</a><br /></td></tr>
      <tr><td>[img]{Link}[/img]</td><td>Embedded image</td></tr>
      <tr><td>[youtube]{ID}[/youtube]</td><td>Embedded youtube video</td></tr>
    </table>

    <br /><br />
    <table>
      <tr><td colspan="2"><b>Organisational</b></td></tr>
      <tr><td>[ol]</td><td>Ordered list</td></tr>
      <tr><td style="padding-left: 25px;">[li]{Text}[/li]<br />[li]{Text}[/li]</td><td><ol><li>Ordered list</li><li>Ordered list</li></ol></td></tr>
      <tr><td>[/ol]</td></tr>
      <tr><td>[ul]</td><td>Unordered list</td></tr>
      <tr><td style="padding-left: 25px;">[li]{Text}[/li]<br />[li]{Text}[/li]</td><td><ul><li>Unordered list</li><li>Unordered list</li></ul></td></tr>
      <tr><td>[/ul]</td></tr>
      <tr><td>[code]{Text}[/code]</td><td><b>Code fragment</b></td></tr>
      <tr><td>[quote="{Author}"]{Text}[/quote]</td><td>Embedded quote</td></tr>
    </table>

    <br /><br />
    <table>
      <tr><td colspan="2"><b>Cyberscore</b></td></tr>
      <tr><td>[user={User ID}]</td><td><?php echo Article::FormatText("[user=10]");?><br /><br /></td></tr>
      <tr><td>[game={Game ID}]</td><td><?php echo Article::FormatText("[game=1501]");?><br /><br /></td></tr>
      <tr><td>[gameinfo={Game ID}]</td><td><?php echo Article::FormatText("[gameinfo=1501]");?><br /><br /></td></tr>
      <tr><td>[gameinfo_rb={Game ID}]</td><td><?php echo Article::FormatText("[gameinfo_rb=1501]");?><br /><br /></td></tr>
      <tr><td>[gameinfo_boxart={Game ID}]</td><td><?php echo Article::FormatText("[gameinfo_boxart=1501]");?><br /><br /></td></tr>
    </table>
  </div>
</div>
