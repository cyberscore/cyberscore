<?php
class TranslationManager implements arrayaccess {
  // USER SETTINGS
  private $user_id = 0;
  private $site_lang = 1;
  private $game_lang = 1;
  private $game_lang_super = 0;
  private $translate_lang = 1;
  private $date_formatter;

  // CACHED TRANSLATIONS
  private $country_cache = null;
  private $entity_cache = null;
  private $rule_cache = null;
  private $proof_rule_cache = null;

  // new cache system
  private $new_chart_name_cache = null;
  private $new_group_name_cache = null;
  private $new_game_name_cache = null;

  // other caches
  private $page_cache = null;
  public $missing_translations = [];

  // COUNTER FOR MODIFYING TRANSLATIONS
  private $count_added = 0;
  private $count_edited = 0;
  private $count_activated = 0;
  private $count_deleted = 0;

  public function LoadUser($current_user) {
    $this->user_id        = $current_user['user_id'];
    $this->site_lang      = $current_user['site_lang'] ?? 1;
    $this->game_lang      = $current_user['game_lang'] ?? 1;
    $this->translate_lang = $current_user['translate_lang'];
    $this->date_formatter = new DateFormatter($current_user['date_format'], $current_user['timezone']);

    // Additional game language?
    if ($this->game_lang != 1) {
      $this->game_lang_super = database_value("SELECT super_language_id FROM languages WHERE language_id = ?", [$this->game_lang]);
    }

    $this->SetLocale();
    $this->InitializeCaches();
  }

  public function ResetUser() {
    $this->user_id        = 0;
    $this->site_lang      = 1;
    $this->game_lang      = 1;
    $this->translate_lang = 1;
    $this->date_formatter = new DateFormatter(null, 'UTC');

    $this->game_lang_super = 0;

    $site_lang = Cookies::Fetch('site_lang');
    if ($site_lang !== null) {
      $this->site_lang = $site_lang;
    }

    $game_lang = Cookies::Fetch('game_lang');
    if ($game_lang !== null) {
      $this->game_lang = $game_lang;
    }

    $this->SetLocale();
    $this->InitializeCaches();
  }

  private function InitializeCaches() {
    $this->new_chart_name_cache = new TranslationCache('levels', 'level_name', 'level_id', $this->game_lang, $this->game_lang_super);
    $this->new_group_name_cache = new TranslationCache('level_groups', 'group_name', 'group_id', $this->game_lang, $this->game_lang_super);
    $this->new_game_name_cache  = new TranslationCache('games', 'game_name', 'game_id', $this->game_lang, $this->game_lang_super);
  }

  //-------------------------------------------------------------------------------------------
  public function GetSiteLang() { return $this->site_lang; }
  public function GetGameLang() { return $this->game_lang; }
  public function GetTranslateLang() { return $this->translate_lang == 1 ? 2 : $this->translate_lang; }

  public function SetLocale() {
    $locale = database_value("SELECT locale FROM languages WHERE language_id = ?", [$this->site_lang]);
    setlocale(LC_TIME, $locale . '.utf8');
  }

  public function FormatDateTime(int|string $timestamp, string $format, bool $apply_timezone = true): string {
    return $this->date_formatter->Format($timestamp, $format, $apply_timezone);
  }

  //-------------------------------------------------------------------------------------------
  public function GetLangName($lang_id)	{
    return database_value("
      SELECT language_name FROM languages
      WHERE language_id = ?
    ", [$lang_id]);
  }

  //-------------------------------------------------------------------------------------------
  private function GetTableInfo($table_field) {
    switch ($table_field) {
      case 'country_name':    return ['countries', 'country_id'];
      case 'headline':        return ['news', 'news_id'];
      case 'news_text':       return ['news', 'news_id'];
      case 'rule_text':       return ['rules', 'rule_id'];
      case 'proof_rule_text': return ['proof_rules', 'rule_id'];
      default:                die('Unknown table field');
    }
  }

  //-------------------------------------------------------------------------------------------
  public function GetContinentName($id) {
    switch ($id) {
      case 1:		return $this['continents_europe'];
      case 2:		return $this['continents_north_america'];
      case 3:		return $this['continents_south_america'];
      case 4:		return $this['continents_asia'];
      case 5:		return $this['continents_africa'];
      case 6:		return $this['continents_oceania'];
      default:	return $this['continents_none'];
    }
  }

  //-------------------------------------------------------------------------------------------
  public function ReadUpdateType($update_type) {
    switch ($update_type) {
      case 'f':	return $this['record_history_type_first'];
      case 'u':	return $this['record_history_type_updated'];
      case 'a':	return $this['record_history_type_approved'];
      case 'e':	return $this['record_history_type_edited'];
      case 'r':	return $this['record_history_type_reset'];
      case 'v':	return $this['record_history_type_reverted'];
      case 's':	return $this['record_history_type_reinstated'];
      case 'm':	return $this['record_history_type_moved'];
      case 'p':	return $this['record_history_type_proof_submit'];
      default:	return $this['record_history_type_unknown'];
    }
  }

  //-------------------------------------------------------------------------------------------
  public function ReadNotificationType($type) {
    switch($type) {
    case 'ygb':					          return $this['notifications_type_ygb'];
    case 'rec_approved':	        return $this['notifications_type_approved'];
    case 'rec_reported':	        return $this['notifications_type_reported'];
    case 'rec_deleted':			      return $this['notifications_type_deleted'];
    case 'proof_refused':	  	    return $this['notifications_type_refused'];
    case 'supp':                  return $this['notifications_type_support'];
    case 'pstring_added':         return $this['notifications_type_pstring_added'];
    case 'pstring_edited':        return $this['notifications_type_pstring_edited'];
    case 'new_grequest':          return $this['notifications_type_new_grequest'];
    case 'greq_comment':          return $this['notifications_type_greq_comment'];
    case 'new_game':              return $this['notifications_type_new_game'];
    case 'reported_rec':          return $this['notifications_type_reported_mod'];
    case 'article_comment':       return $this['notifications_type_article_comment'];
    case 'blog_follow':           return $this['notifications_type_blog_follow'];
    case 'blog_unfollow':         return $this['notifications_type_blog_unfollow'];
    case 'followed_posted':       return $this['notifications_type_followed_posted'];
    case 'referral_confirmed_1':  return $this['notifications_type_referral_confirmed_1'];
    case 'referral_confirmed_2':  return $this['notifications_type_referral_confirmed_2'];
    case 'referral_rejected_1':   return $this['notifications_type_referral_rejected_1'];
    case 'referral_rejected_2':   return $this['notifications_type_referral_rejected_2'];
    case 'userpage_comments':     return $this['notifications_type_userpage_comment'];
    case 'reply_1':               return $this['notifications_type_reply_a'];
    case 'reply_2':               return $this['notifications_type_reply_gr'];
    case 'reply_3':               return $this['notifications_type_reply_u'];
    default:					            return '';
    }
  }

  //-------------------------------------------------------------------------------------------
  public function ReadChartType($i) {
    switch($i) {
      case 0:						return 'None';
      case 1:						return 'Time, faster better';
      case 2:						return 'Time, slower better';
      case 3:						return 'Score, lower better';
      case 4:						return 'Score, higher better';
      case 5:						return 'Rank';
      default:					return '';
    }
  }

  //-------------------------------------------------------------------------------------------
  public function FormatTranslatorStar($star, $username) {
    if (isset($star['languages'])) {
      foreach ($star['languages'] as &$lang_code) {
        $lang_code = $this['language_' . $lang_code];
      }
      $star['title'] = str_replace(['[username]', '[languages]'], [$username, implode(str_replace(['[language1]', '[language2]'], '', $this['language_credits_seperator']), $star['languages'])], $this['language_credits']);
    }
    return $star;
  }

  //-------------------------------------------------------------------------------------------
  //--------------------------------------- COUNTRIES -----------------------------------------
  //-------------------------------------------------------------------------------------------
  public function CacheCountryNames() {
    if ($this->country_cache !== NULL) { return; }

    // FIRST CACHE THE ENGLISH NAMES
    $eng_names = database_fetch_all("SELECT country_id, country_name FROM countries", []);
    foreach ($eng_names as $eng_name) {
      $this->country_cache[$eng_name['country_id']] = $eng_name['country_name'];
    }

    // OVERWRITE WITH THE TRANSLATED NAMES, IF AVAILABLE
    if ($this->site_lang != 1) {
      $names = database_fetch_all("
        SELECT table_id, translation
        FROM translation_db
        JOIN countries ON table_id = country_id
        WHERE table_field = 'country_name' AND is_active = 'y' AND language_id = ?
      ", [$this->site_lang]);

      foreach ($names as $name) {
        $this->country_cache[$name['table_id']] = $name['translation'];
      }
    }
  }

  public function GetCountryName($country_id) {
    $this->CacheCountryNames();
    return $this->country_cache[$country_id];
  }

  //-------------------------------------------------------------------------------------------
  public function GetCountryNames() {
    $this->CacheCountryNames();

    // copy the array
    $return_array = $this->country_cache;

    // remove "no country selected", sort it and return it
    unset($return_array[1]);
    natsort($return_array);
    return $return_array;
  }

  //-------------------------------------------------------------------------------------------
  public function RetrieveCountryNames($lang_id) {
    $translations = [];

    if ($lang_id == 1) {
      $eng_names = database_fetch_all("SELECT country_id, country_name FROM countries", []);
      foreach ($eng_names as $eng_name) {
        $translations[$eng_name['country_id']] = $eng_name['country_name'];
      }
    } else {
      $names = database_fetch_all("SELECT table_id, translation FROM translation_db WHERE table_field = 'country_name' AND is_active = 'y' AND language_id = ?", [$lang_id]);
      foreach ($names as $name) {
        $translations[$name['table_id']] = $name['translation'];
      }
    }

    return $translations;
  }

  //-------------------------------------------------------------------------------------------
  //----------------------------------------- NEWS --------------------------------------------
  //-------------------------------------------------------------------------------------------
  public function GetNewsHeadline($table_id) {
    // TRY TO GET THE TRANSLATED TEXT
    $headline = database_value("SELECT translation FROM translation_db WHERE table_field = 'headline' AND table_id = ? AND is_active = 'y' AND language_id = ?", [$table_id, $this->site_lang]);
    if ($headline !== null) { return $headline; }
    // IF NOT AVAILABLE, GET THE ENGLISH TEXT
    return database_value("SELECT headline FROM news WHERE news_id = ?", [$table_id]);
  }

  public function GetNewsText($table_id) {
    // TRY TO GET THE TRANSLATED TEXT
    $news_text = database_value("SELECT translation FROM translation_db WHERE table_field = 'news_text' AND table_id = ? AND is_active = 'y' AND language_id = ?", [$table_id, $this->site_lang]);
    if ($news_text !== null) { return $news_text;}
    // IF NOT AVAILABLE, GET THE ENGLISH TEXT
    return database_value("SELECT news_text FROM news WHERE news_id = ?", [$table_id]);
  }

  public function RetrieveNewsHeadline($lang_id, $table_id) {
    if ($lang_id == 1) {
      return database_value("SELECT headline FROM news WHERE news_id = ?", [$table_id]);
    } else {
      return database_value("SELECT translation FROM translation_db WHERE table_field = 'headline' AND table_id = ? AND is_active = 'y' AND language_id = ?", [$table_id, $lang_id]);
    }
  }

  public function RetrieveNewsText($lang_id, $table_id) {
    if ($lang_id == 1) {
      return database_value("SELECT news_text FROM news WHERE news_id = ?", [$table_id]);
    } else {
      return database_value("SELECT translation FROM translation_db WHERE table_field = 'news_text' AND table_id = ? AND is_active = 'y' AND language_id = ?", [$table_id, $lang_id]);
    }
  }

  public function FormatNewsText($text) {
    $text = preg_replace(
      ['/\[user=(.+?)\]/e', '/\[game=(.+?)\]/e',
        '/\[gameinfo=(.+?)\]/e', '/\[gameinfo_rb=(.+?)\]/e',
        '/\[gameinfo_boxart=(.+?)\]/e'],
      ["\$this->FormatNews_User($1)", "\$this->FormatNews_Game($1)",
        "\$this->FormatNews_GameInfo($1)", "\$this->FormatNews_GameInfoRankbutton($1)",
        "\$this->FormatNews_GameInfoBoxart($1)"],
      $text);

    return prettify($text);
  }

  private function FormatNews_User($user_id) {
    $user = database_find_by('users', ['user_id' => $user_id]);
    $username = $user['username'];
    $country_code = $user['country_code'];

    return '<img src="/flags/' . $country_code . '.png" style="width:16px; height:16px; vertical-align:-20%;" /> <a href="/user/' . $user_id . '">' . $username . '</a>';
  }

  private function FormatNews_Game($game_id) {
    return '<a href="/game/' . $game_id . '">' . h($this->GetGameName($game_id)) . '</a>';
  }

  private function FormatNews_GameInfo($game_id) {
    $game = database_fetch_one("
      SELECT COUNT(*) AS num_platforms, GROUP_CONCAT(platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ', ') AS platform_names
      FROM games JOIN game_platforms USING(game_id) JOIN platforms USING(platform_id)
      WHERE games.game_id = ? AND game_platforms.original = 1
    ", [$game_id]);

    $num_platforms  = $game['num_platforms'];
    $platform_names = $game['platform_names'];

    return '<a href="/game/' . $game_id . '">' . h($this->GetGameName($game_id)) . '</a>
      ' . ($num_platforms == 1 ? 'Platform' : 'Platforms') . ': ' . $platform_names;
  }

  private function FormatNews_GameInfoRankbutton($game_id) {
    $game = database_fetch_one("
      SELECT COUNT(*) AS num_platforms, GROUP_CONCAT(platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ', ') AS platform_names
      FROM games JOIN game_platforms USING(game_id) JOIN platforms USING(platform_id)
      WHERE games.game_id = ? AND game_platforms.original = 1
    ", [$game_id]);

    $num_platforms  = $game['num_platforms'];
    $platform_names = $game['platform_names'];

    return '<img src="/rankbuttons/1/' . $game_id . '.gif" alt="Rankbutton" />
      <a href="/game/' . $game_id . '">' . h($this->GetGameName($game_id)) . '</a>
      ' . ($num_platforms == 1 ? 'Platform' : 'Platforms') . ': ' . $platform_names;
  }

  //-------------------------------------------------------------------------------------------
  private function FormatNews_GameInfoBoxart($game_id) {
    $boxart = $cs->GetBoxArts($game_id)[0];
    return boxart_image_tag($boxart) . link_to_game(['game_id' => $game_id]);
  }

  //-------------------------------------------------------------------------------------------
  //---------------------------------------- RULES --------------------------------------------
  //-------------------------------------------------------------------------------------------
  public function CacheRules() {
    if ($this->rule_cache !== NULL) { return; }

    // FIRST CACHE THE ENGLISH NAMES
    $eng_names = database_fetch_all("SELECT rule_id, rule_text FROM rules", []);
    foreach ($eng_names as $eng_name) {
      $this->rule_cache[$eng_name['rule_id']] = $eng_name['rule_text'];
    }

    // OVERWRITE WITH THE TRANSLATED NAMES, IF AVAILABLE
    if ($this->site_lang != 1) {
      $names = database_fetch_all("
        SELECT table_id, translation
        FROM translation_db
        WHERE table_field = 'rule_text' AND is_active = 'y' AND language_id = ?
      ", [$this->site_lang]);

      foreach ($names as $name) {
        $this->rule_cache[$name['table_id']] = $name['translation'];
      }
    }
  }

  public function GetRuleText($rule_ids) {
    $this->CacheRules();

    $rule_ids = array_map('intval', explode(',', $rule_ids));

    $complete_rule_texts = [];
    foreach ($rule_ids as $rule_id) {
      $text = $this->rule_cache[$rule_id] ?? null;
      if ($text != null) {
        $complete_rule_texts []= $text;
      }
    }

    return $complete_rule_texts;
  }

  public function GetRules($rule_ids) {
    $rule_ids = array_unique(explode(',', $rule_ids));
    $rule_ids = array_filter($rule_ids, fn ($e) => $e != '' && $e != 1);
    $rule_ids = array_map('intval', $rule_ids);

    $rules = [];
    foreach ($rule_ids as $rule_id) {
      $translated_rule_text = $this->GetRuleTextLang($rule_id, $this->site_lang);
      $english_rule_text = database_value("SELECT rule_text FROM rules WHERE rule_id = ?", [$rule_id]);

      if (!empty($translated_rule_text)) {
        $rule_text = $translated_rule_text;
      } else {
        $rule_text = $english_rule_text;
      }

      $rules []= [
        'rule_id' => $rule_id,
        'rule_text' => $rule_text,
      ];
    }

    return $rules;
  }

  public function GetRuleTextLang($table_id, $language_id) {
    return database_value("
      SELECT translation
      FROM translation_db
      WHERE table_field = 'rule_text' AND table_id = ? AND is_active = 'y' AND language_id = ?
    ", [$table_id, $language_id]);
  }

  public function GetSuggestedRuleTextLang($table_id, $language_id) {
    return database_value("
      SELECT translation
      FROM translation_db
      WHERE table_field = 'rule_text' AND table_id = ? AND is_active = 'n' AND language_id = ?
    ", [$table_id, $language_id]);
  }

  public function GetPreferredTranslation($translation_options) {
    return $translation_options[$this->site_lang] ?? $translation_options['1'];
  }

  public function RetrieveRuleText($lang_id, $table_id) {
    if ($lang_id == 1) {
      return database_value("SELECT rule_text FROM rules WHERE rule_id = ?", [$table_id]);
    } else {
      return database_value("SELECT translation FROM translation_db WHERE table_field = 'rule_text' AND table_id = ? AND is_active = 'y' AND language_id = ?", [$table_id, $lang_id]);
    }
  }

  //-------------------------------------------------------------------------------------------
  //---------------------------------------- PROOF RULES --------------------------------------
  //-------------------------------------------------------------------------------------------
  public function CacheProofRules() {
    if ($this->proof_rule_cache !== NULL) { return; }

    // FIRST CACHE THE ENGLISH NAMES
    $eng_names = database_fetch_all("SELECT rule_id, rule_text FROM proof_rules", []);
    foreach ($eng_names as $eng_name) {
      $this->proof_rule_cache[$eng_name['rule_id']] = $eng_name['rule_text'];
    }

    // OVERWRITE WITH THE TRANSLATED NAMES, IF AVAILABLE
    if ($this->site_lang != 1) {
      $names = database_fetch_all("
        SELECT table_id, translation
        FROM translation_db
        WHERE table_field = 'proof_rule_text' AND is_active = 'y' AND language_id = ?
      ", [$this->site_lang]);

      foreach ($names as $name) {
        $this->proof_rule_cache[$name['table_id']] = $name['translation'];
      }
    }
  }

  public function GetProofRuleText($rule_ids) {
    $this->CacheProofRules();

    $rule_ids = array_map('intval', explode(',', $rule_ids ?? ""));

    $complete_rule_texts = [];
    foreach ($rule_ids as $rule_id) {
      $text = $this->proof_rule_cache[$rule_id] ?? null;
      if ($text != null) {
        $complete_rule_texts []= $text;
      }
    }

    return $complete_rule_texts;
  }

  public function GetProofRuleTextLang($table_id, $language_id) {
    return database_value("
      SELECT translation
      FROM translation_db
      WHERE table_field = 'proof_rule_text' AND table_id = ? AND is_active = 'y' AND language_id = ?
    ", [$table_id, $language_id]);
  }

  public function GetSuggestedProofRuleTextLang($table_id, $language_id) {
    return database_value("
      SELECT translation
      FROM translation_db
      WHERE table_field = 'proof_rule_text' AND table_id = ? AND is_active = 'n' AND language_id = ?
    ", [$table_id, $language_id]);
  }

  public function RetrieveProofRuleText($lang_id, $table_id) {
    if ($lang_id == 1) {
      return database_value("SELECT rule_text FROM proof_rules WHERE rule_id = ?", [$table_id]);
    } else {
      return database_value("SELECT translation FROM translation_db WHERE table_field = 'proof_rule_text' AND table_id = ? AND is_active = 'y' AND language_id = ?", [$table_id, $lang_id]);
    }
  }

  //-------------------------------------------------------------------------------------------
  //----------------------------- GAMES, GROUPS, CHARTS, RECORDS ------------------------------
  //-------------------------------------------------------------------------------------------

  public function GetGameLanguages($game_id) {
    return database_fetch_all("
      SELECT DISTINCT translation_games.language_id, languages.language_name, languages.sub_language
      FROM translation_games
      JOIN languages USING(language_id)
      WHERE translation_games.game_id = ?
      ORDER BY languages.language_name LIKE 'English%' DESC, languages.language_name ASC
    ", [$game_id]);
  }

  public function CacheGame($game_id) {
    $this->new_game_name_cache->Cache(['game_id' => $game_id]);
    $this->new_chart_name_cache->Cache(['game_id' => $game_id]);
    $this->new_group_name_cache->Cache(['game_id' => $game_id]);
  }

  public function CacheRecords($records) {
    $game_ids = pluck($records, 'game_id');
    $chart_ids = pluck($records, 'level_id');
    $group_ids = pluck($records, 'group_id');

    $this->new_chart_name_cache->Cache(['level_id' => $chart_ids]);
    $this->new_group_name_cache->Cache(['group_id' => $group_ids]);
    $this->new_game_name_cache->Cache(['game_id' => $game_ids]);
  }

  public function CacheGroups($groups) {
    $group_ids = pluck($groups, 'group_id');

    $this->new_group_name_cache->Cache(['group_id' => $group_ids]);
  }

  public function CacheGameNames() {
    $this->new_game_name_cache->Cache([]);
  }

  public function CacheChartNames($game_id, $lang_id) {
    $this->new_chart_name_cache->CacheLang($lang_id, ['game_id' => $game_id]);
  }

  public function GetGameName($game_id) {
    return $this->new_game_name_cache->GetName($game_id);
  }

  public function RetrieveGameName($lang_id, $game_id) {
    return $this->new_game_name_cache->GetNameInLanguage($game_id, $lang_id);
  }

  public function GetGroupName($table_id) {
    if ($table_id == 0) {
      return $this['general_group0'];
    }

    return $this->new_group_name_cache->GetName($table_id);
  }

  public function RetrieveGroupName($lang_id, $group_id) {
    return $this->new_group_name_cache->GetNameInLanguage($group_id, $lang_id);
  }

  public function GetChartName($table_id) {
    return $this->new_chart_name_cache->GetName($table_id);
  }

  public function GetChartNameLang($level_id, $lang_id) {
    return $this->new_chart_name_cache->GetNameInLanguage($level_id, $lang_id);
  }

  public function CacheEntityNames($game_id, $lang_id = 0) {
    // FIRST CACHE THE ENGLISH NAMES
    $eng_names = database_fetch_all("SELECT game_entity_id, entity_name, inherited_entity_id FROM game_entities WHERE game_id = ?", [$game_id]);
    foreach ($eng_names as $eng_name) {
      if ($eng_name['inherited_entity_id']) {
        $eng_name['entity_name'] = database_value("SELECT entity_name FROM game_entities WHERE game_entity_id = ?", [$eng_name['inherited_entity_id']]);
      }
      $this->entity_cache[0][$eng_name['game_entity_id']] = $eng_name['entity_name'];
      $this->entity_cache[1][$eng_name['game_entity_id']] = $eng_name['entity_name'];
      if($lang_id > 1)	{ $this->entity_cache[$lang_id][$eng_name['game_entity_id']] = '';}
    }

    if ($lang_id == 0) {
      // OVERWRITE WITH THE TRANSLATED NAMES, IF AVAILABLE
      if ($this->game_lang != 1) {
        if ($this->game_lang_super > 1) {
          $translated = database_fetch_all("SELECT table_id, translation FROM translation_games WHERE table_field = 'entity_name' AND game_id = ? AND language_id = ?", [$game_id, $this->game_lang_super]);
          foreach ($translated as $tr) {
            $this->entity_cache[0][$tr['table_id']] = $tr['translation'];
          }
        }
        $translated = database_fetch_all("SELECT table_id, translation FROM translation_games WHERE table_field = 'entity_name' AND game_id = ? AND language_id = ?", [$game_id, $this->game_lang]);
        foreach ($translated as $tr) {
          $this->entity_cache[0][$tr['table_id']] = $tr['translation'];
        }
      }
    } else if ($lang_id > 1) {
      $translated = database_fetch_all("SELECT table_id, translation FROM translation_games WHERE table_field = 'entity_name' AND game_id = ? AND language_id = ?", [$game_id, $lang_id]);
      foreach ($translated as $tr) {
        $this->entity_cache[$lang_id][$tr['table_id']] = $tr['translation'];
      }
    }
  }

  //-------------------------------------------------------------------------------------------
  public function GetEntityName($table_id, $lang_id = 0, $format = false) {
    if ($table_id == 0) {
      return '';
    }

    // LOOK IN THE CACHE
    if (isset($this->entity_cache[$lang_id][$table_id])) {
      $entity_name = $this->entity_cache[$lang_id][$table_id];
    } else if ($lang_id == 0) {
      // TRY TO GET THE TRANSLATED NAME
      $entity_name = database_value("SELECT translation FROM translation_games WHERE table_field = 'entity_name' AND table_id = ? AND language_id = ?", [$table_id, $this->game_lang]);
      if ($entity_name == null) {
        if ($this->game_lang_super > 1) {
          $entity_name = database_value("SELECT translation FROM translation_games WHERE table_field = 'entity_name' AND table_id = ? AND language_id = ?", [$table_id, $this->game_lang_super]);
        }

        if ($entity_name == null) {
          // IF NOT AVAILABLE, GET THE ENGLISH NAME
          $entity_name = database_value("SELECT entity_name FROM game_entities WHERE game_entity_id = ?", [$table_id]);
        }
      }
    } else if ($lang_id == 1) {
      $entity_name = database_value("SELECT entity_name FROM game_entities WHERE game_entity_id = ?", [$table_id]);
    } else {
      $entity_name = database_value("SELECT translation FROM translation_games WHERE table_field = 'entity_name' AND table_id = ? AND language_id = ?", [$table_id, $lang_id]);
    }

    // format=true means we want to get a filename, so we need to do some escaping
    if ($lang_id == 1 && $format) {
      return str_replace(
        [' ', '/', '&rsquo;', 'é', '♂', '♀'],
        ['_', '_', '_', 'e', '(m)', '(f)'],
        html_entity_decode($entity_name)
      );
    } else {
      return $entity_name;
    }
  }

  public function GetEntityNames($table_ids) {
    $entity_names = [];
    foreach ($table_ids as $table_id) {
      $entity_name = $this->GetEntityName($table_id);
      if($entity_name != '')	$entity_names []= $entity_name;
    }
    return implode(', ', $entity_names);
  }

  public function GetGamePatches($unformatted_patch_list) {
    $formatted_patch_list = [];

    if (isset($unformatted_patch_list) && $unformatted_patch_list !== '') {
      $patch_list = explode("\r\n", $unformatted_patch_list);

      foreach ($patch_list as $patch_name) {
        $patch_name = trim($patch_name);

        $formatted_patch_list []= $patch_name;
      }
    }
    return $formatted_patch_list;
  }

  //-------------------------------------------------------------------------------------------
  //---------------------------------- COUNTING TRANSLATIONS ----------------------------------
  //-------------------------------------------------------------------------------------------
  private function FormatCount($num_active, $num_pending, $format_string) {
    if ($format_string == 'a') {
      return $num_active;
    } else if ($format_string == 'p') {
      return $num_pending;
    }

    $p = $num_pending > 0 ? ' (' . $num_pending . ')' : '';
    return $num_active . $p;
  }

  public function CountCountryNames($lang_id, $format_string = '') {
    if (database_value("SELECT sub_language FROM languages WHERE language_id = ?", [$lang_id]) == 'y') {
      return '-';
    }

    if ($lang_id == 1) {
      $num_active = database_value("SELECT COUNT(*) FROM countries", []);
      $num_pending = 0;
    } else {
      $num_active = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'country_name' AND is_active = 'y' AND language_id = ?", [$lang_id]);
      $num_pending = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'country_name' AND is_active = 'n' AND language_id = ?", [$lang_id]);
    }

    return $this->FormatCount($num_active, $num_pending, $format_string);
  }

  public function CountNewsHeadlines($lang_id, $format_string = '') {
    if (database_value("SELECT sub_language FROM languages WHERE language_id = ?", [$lang_id]) == 'y') {
      return '-';
    }

    if ($lang_id == 1) {
      $num_active = database_value("SELECT COUNT(*) FROM news", []);
      $num_pending = 0;
    } else {
      $num_active = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'headline' AND is_active = 'y' AND language_id = ?", [$lang_id]);
      $num_pending = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'headline' AND is_active = 'n' AND language_id = ?", [$lang_id]);
    }

    return $this->FormatCount($num_active, $num_pending, $format_string);
  }

  public function CountNewsTexts($lang_id, $format_string = '') {
    if (database_value("SELECT sub_language FROM languages WHERE language_id = ?", [$lang_id]) == 'y') {
      return '-';
    }

    if ($lang_id == 1) {
      $num_active = database_value("SELECT COUNT(*) FROM news", []);
      $num_pending = 0;
    } else {
      $num_active = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'news_text' AND is_active = 'y' AND language_id = ?", [$lang_id]);
      $num_pending = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'news_text' AND is_active = 'n' AND language_id = ?", [$lang_id]);
    }

    return $this->FormatCount($num_active, $num_pending, $format_string);
  }

  public function CountProofRuleTexts($lang_id, $format_string = '') {
    if (database_value("SELECT sub_language FROM languages WHERE language_id = ?", [$lang_id]) == 'y') {
      return '-';
    }

    if ($lang_id == 1) {
      $num_active = database_value("SELECT COUNT(*) FROM proof_rules", []);
      $num_pending = 0;
    } else {
      $num_active = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'proof_rule_text' AND is_active = 'y' AND language_id = ?", [$lang_id]);
      $num_pending = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'proof_rule_text' AND is_active = 'n' AND language_id = ?", [$lang_id]);
    }

    return $this->FormatCount($num_active, $num_pending, $format_string);
  }

  public function CountRuleTexts($lang_id, $format_string = '') {
    if (database_value("SELECT sub_language FROM languages WHERE language_id = ?", [$lang_id]) == 'y') {
      return '-';
    }

    if ($lang_id == 1) {
      $num_active = database_value("SELECT COUNT(*) FROM rules", []);
      $num_pending = 0;
    } else {
      $num_active = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'rule_text' AND is_active = 'y' AND language_id = ?", [$lang_id]);
      $num_pending = database_value("SELECT COUNT(*) FROM translation_db WHERE table_field = 'rule_text' AND is_active = 'n' AND language_id = ?", [$lang_id]);
    }

    return $this->FormatCount($num_active, $num_pending, $format_string);
  }

  public function CountGameProgress($lang_id, $format_string = '') {
    if ($lang_id == 1) {
      $num_active = database_value("SELECT COUNT(*) FROM games WHERE site_id < 4", []);
      $num_pending = 0;
    } else {
      $num_active = database_value("SELECT COUNT(*) FROM translation_games_progress WHERE language_id = ? AND status = 'complete'", [$lang_id]);
      $num_pending = database_value("SELECT COUNT(*) FROM translation_games_progress WHERE language_id = ? AND status != 'complete'", [$lang_id]);
    }

    return $this->FormatCount($num_active, $num_pending, $format_string);
  }

  public function CountTranslationPage($lang_id, $format_string = '') {
    if (database_value("SELECT sub_language FROM languages WHERE language_id = ?", [$lang_id]) == 'y') {
      return '-';
    }

    $num_active = database_value("SELECT COUNT(*) FROM translation_pages WHERE is_active = 'y' AND language_id = ?", [$lang_id]);
    $num_pending = database_value("SELECT COUNT(*) FROM translation_pages WHERE is_active = 'n' AND language_id = ?", [$lang_id]);

    return $this->FormatCount($num_active, $num_pending, $format_string);
  }

  //-------------------------------------------------------------------------------------------
  //------------------------------------ PAGE TRANSLATIONS ------------------------------------
  //-------------------------------------------------------------------------------------------
  function CachePageStrings() {
    // First cache the english names.
    // Then overwrite with the translated names, if available.

    $eng_names = database_fetch_all("SELECT `key`, translation FROM translation_pages WHERE language_id = 1", []);
    foreach ($eng_names as $eng_name) {
      $this->page_cache[$eng_name['key']] = $eng_name['translation'];
    }

    if ($this->site_lang != 1) {
      $names = database_fetch_all("SELECT `key`, translation FROM translation_pages WHERE language_id = ? AND is_active = 'y'", [$this->site_lang]);
      foreach ($names as $name) {
        $this->page_cache[$name['key']] = $name['translation'];
      }
    }
  }

  public function GetPageString($key) {
    if ($this->page_cache === null) {
      $this->CachePageStrings();
    }

    // Non-existing key?
    if(!isset($this->page_cache[$key])) {
      $this->missing_translations []= $key;
      return "ERROR_T<$key>";
    }

    return $this->FormatForHighlighting($this->page_cache[$key], $key);
  }

  function FormatForHighlighting($string, $key) {
    if (isset($_GET['ht'])) {
      return "[t={$key}]{$string}[/t]";
    } else {
      return $string;
    }
  }

  /********************************************************************************************
    A BIT HACKING FOR ARRAY ACCESS
    ********************************************************************************************/
  public function offsetGet($offset): mixed {
    return $this->GetPageString($offset);
  }

  public function offsetSet($offset, $value): void {
    die('Godmiljaar, this is not really an array');
  }
  public function offsetExists($offset): bool {
    if ($this->page_cache === null) {
      $this->CachePageStrings();
    }

    return isset($this->page_cache[$offset]);
  }
  public function offsetUnset($offset): void {
    die('Godmiljaar, this is not really an array');
  }

  /* LANGUAGE SPECIFIC */
  public function RetrievePageTranslation($lang_id, $key) {
    return database_value("
      SELECT translation
      FROM translation_pages
      WHERE language_id = ? AND `key` = ? AND is_active = 'y'
    ", [$lang_id, $key]);
  }

  public function RetrievePageSuggestions($lang_id, $key) {
    return database_fetch_all("
      SELECT id, translation, comment, suggestion_date, users.username
      FROM translation_pages
      LEFT JOIN users ON users.user_id = translation_pages.suggester_id
      WHERE language_id = ? AND `key` = ? AND is_active = 'n'
      ORDER BY suggestion_date ASC
    ", [$lang_id, $key]);
  }

  //-------------------------------------------------------------------------------------------
  public function RetrievePageVotes($id) {
    $upvoter = [];
    $downvoter = [];

    $votes = database_fetch_all("
      SELECT translation_pages_votes.vote, users.username
      FROM translation_pages_votes
      JOIN users USING(user_id)
      WHERE translation_pages_votes.translation_pages_id = ?
    ", [$id]);

    foreach ($votes as $vote) {
      if ($vote['vote'] == 1) {
        $upvoter []= $vote['username'];
      } else {
        $downvoter []= $vote['username'];
      }
    }

    return [$upvoter, $downvoter];
  }

  public function RetrieveDBSuggestions($lang_id, $table_field, $table_id) {
    return database_fetch_all("
      SELECT id, translation, comment, suggestion_date, users.username
      FROM translation_db LEFT JOIN users ON users.user_id = translation_db.suggester_id
      WHERE table_field = ? AND table_id = ? AND is_active = 'n' AND language_id = ?
      ORDER BY suggestion_date ASC
    ", [$table_field, $table_id, $lang_id]);
  }
  /* LANGUAGE SPECIFIC END */

  // Get all page groups
  public function RetrievePageGroups() {
    $groups = [];
    $all_groups = database_get_all(database_select("SELECT DISTINCT `group` FROM translation_pages ORDER by `group`", '', []));
    foreach ($all_groups as $group) {
      $groups []= $group['group'];
    }
    return $groups;
  }

  //-------------------------------------------------------------------------------------------
  //--------------------------------- MODIFYING TRANSLATIONS ----------------------------------
  //-------------------------------------------------------------------------------------------
  public function AllowHarmlessHTML($string) {
    return str_replace(
      ['&lt;p&gt;', '&lt;/p&gt;', '&lt;u&gt;', '&lt;/u&gt;', '&lt;b&gt;', '&lt;/b&gt;', '&lt;strong&gt;', '&lt;/strong&gt;', '&lt;em&gt;', '&lt;/em&gt;', '&lt;i&gt;', '&lt;/i&gt;'],
      ['<p>', '</p>', '<u>', '</u>', '<b>', '</b>', '<b>', '</b>', '<i>', '</i>', '<i>', '</i>'],
      $string
    );
  }

  //-------------------------------------------------------------------------------------------
  public function UpdateGameStatusEntry($lang_id, $game_id) {
    if (database_value("SELECT COUNT(*) FROM translation_games_progress WHERE game_id = ? AND language_id = ?", [$game_id, $lang_id]) == 0) {
      database_insert('translation_games_progress', [
        'game_id' => $game_id,
        'language_id' => $lang_id,
      ]);
    }
  }

  public function ChangeGameTranslatableName($language_id, $table, $id, $translation) {
    switch ($table) {
      case 'games': $pkey = 'game_id'; $field = 'game_name'; break;
      case 'level_groups': $pkey = 'group_id'; $field = 'group_name'; break;
      case 'levels': $pkey = 'level_id'; $field = 'level_name'; break;
    }
    $object = database_find_by($table, [$pkey => $id]);

    if ($language_id == 1) {
      if ($object[$field] != $translation) {
        database_update_by($table, [$field => $translation], [$pkey => $id]);
        $this->count_edited++;
      }
    } else {
      $translation_object = database_find_by('translation_games', [
        'table_field' => $field == 'level_name' ? 'chart_name' : $field,
        'table_id' => $object[$pkey],
        'language_id' => $language_id,
      ]);

      if ($translation_object === null && $translation !== '') {
        database_insert('translation_games', [
          'table_field' => $field == 'level_name' ? 'chart_name' : $field,
          'table_id' => $id,
          'language_id' => $language_id,
          'translation' => $translation,
          'suggester_id' => $this->user_id,
          'suggestion_date' => database_now(),
          'game_id' => $object['game_id'],
        ]);
        $this->count_added++;
        $this->UpdateGameStatusEntry($language_id, $object['game_id']);
      } else if ($translation_object && $translation !== '') {
        database_update_by('translation_games', [
          'translation' => $translation,
          'suggester_id' => $this->user_id,
          'suggestion_date' => database_now()
        ], ['id' => $translation_object['id']]);
        $this->count_edited++;
      } else if ($translation_object && $translation == '') {
        database_delete_by('translation_games', ['id' => $translation_object['id']]);
        $this->count_deleted++;
      }
    }
  }

  public function ChangeGameName($lang_id, $game_id, $translation) {
    $this->ChangeGameTranslatableName($lang_id, 'games', $game_id, $translation);
  }

  public function ChangeGroupName($lang_id, $group_id, $translation) {
    $this->ChangeGameTranslatableName($lang_id, 'level_groups', $group_id, $translation);
  }

  public function ChangeChartName($lang_id, $chart_id, $translation, $game_id) {
    $this->ChangeGameTranslatableName($lang_id, 'levels', $chart_id, $translation);
  }

  //-------------------------------------------------------------------------------------------
  public function ChangeEntityName($lang_id, $game_entity_id, $translation, $game_id) {
    if ($lang_id == 1) {
      $entity_name = database_value("SELECT entity_name FROM game_entities WHERE game_entity_id = ? AND game_id = ?", [$game_entity_id, $game_id]);
      if ($translation != $entity_name) {
        database_update_by('game_entities', ['entity_name' => $translation], ['game_entity_id' => $game_entity_id, 'game_id' => $game_id]);
        $this->count_edited++;
      }
    } else {
      [$id, $existing_translation] = database_value("SELECT id, translation FROM translation_games WHERE table_field = 'entity_name' AND table_id = ? AND language_id = ?", [$game_entity_id, $lang_id]);
      if ($id !== null) {
        if ($translation === '') {
          database_delete_by('translation_games', ['id' => $id]);
          $this->count_deleted++;
        } else if ($translation != $existing_translation) {
          database_update_by('translation_games', ['translation' => $translation, 'suggester_id' => $this->user_id, 'suggestion_date' => database_now()], ['id' => $id]);
          $this->count_edited++;
        }
      } else if ($translation !== '') {
        database_insert('translation_games', [
          'table_field' => 'entity_name',
          'table_id' => $game_entity_id,
          'language_id' => $lang_id,
          'translation' => $translation,
          'suggester_id' => $this->user_id,
          'suggestion_date' => database_now(),
          'game_id' => $game_id,
        ]);
        $this->count_added++;
        $this->UpdateGameStatusEntry($lang_id, $game_id);
      }
    }
  }

  public function AddTranslationPage($language_id, $group, $key, $translation, $suggester_id, $comment = '', $set_active = 'n') {
    // check that the translation text isn't empty
    if ($translation == '')	{
      return;
    }

    $translation = $this->AllowHarmlessHTML($translation);

    // The english string must always be active
    if ($language_id == 1) {
      $set_active = 'y';
    }

    // Delete old suggestions/translation if needed
    if ($set_active == 'y') {
      database_delete_by('translation_pages', ['group' => $group, 'key' => $key, 'language_id' => $language_id]);
      database_delete('translation_pages_votes', 'NOT EXISTS(SELECT * FROM translation_pages WHERE translation_pages.id = translation_pages_votes.translation_pages_id)', []);
      $this->count_activated++;
    }

    // INSERT IT
    database_insert('translation_pages', [
      'group' => $group,
      'key' => $key,
      'language_id' => $language_id,
      'is_active' => $set_active,
      'suggester_id' => $suggester_id,
      'suggestion_date' => database_now(),
      'comment' => $comment,
      'translation' => $translation,
    ]);

    $this->count_added++;
  }

  public function EditTranslationPage($id, $translation, $comment = '') {
    // CHECK THAT THE TRANSLATION TEXT ISN'T EMPTY
    if ($translation == '') {
      return;
    }

    $translation = $this->AllowHarmlessHTML($translation);

    database_update_by('translation_pages', ['translation' => $translation, 'comment' => $comment, 'suggestion_date' => database_now()], ['id' => $id]);
    database_delete_by('translation_pages_votes', ['translation_pages_id' => $id]);

    $this->count_edited++;
  }

  public function DeactivateTranslationPageByKey($key) {
    // WE CHANGED THE ORGINIAL, MIGHT WANT TO DEACTIVATE THE TRANSLATIONS
    database_update_by('translation_pages', ['is_active' => 'n', 'suggester_id' => 0, 'comment' => 'NOTE: The English string has been updated.'], ['key' => $key, 'language_id' => database_not(1)]);
  }

  //-------------------------------------------------------------------------------------------
  public function ActivateTranslationPage($id) {
    // DEACTIVATE AN EXISTING TRANSLATION IF ONE EXISTS
    $translation_page = database_get(database_select("SELECT * FROM translation_pages WHERE id = ?", 's', [$id]));

    $group   = $translation_page['group'];
    $key     = $translation_page['key'];
    $lang_id = $translation_page['language_id'];

    database_update_by('translation_pages', ['is_active' => 'n'], ['group' => $group, 'key' => $key, 'language_id' => $lang_id, 'id' => database_not($id)]);

    // SET IS_ACTIVE TO Y FOR THE SPECIFIED ID
    database_update_by('translation_pages', ['is_active' => 'y'], ['id' => $id]);
    $this->count_activated++;
  }

  //-------------------------------------------------------------------------------------------
  public function DeleteTranslationPage($id) {
    $translation_page = database_get(database_select("SELECT * FROM translation_pages WHERE id = ?", 's', [$id]));

    $group   = $translation_page['group'];
    $key     = $translation_page['key'];
    $lang_id = $translation_page['language_id'];

    // ENGLISH MEANS DELETING THE OTHERS ASWELL...
    if ($lang_id == 1) {
      $num_deleted = database_value("SELECT COUNT(*) FROM translation_pages WHERE `group` = ? AND `key` = ?", [$group, $key]);

      database_delete_by('translation_pages', ['group' => $group, 'key' => $key]);
      database_delete('translation_pages_votes', 'NOT EXISTS(SELECT * FROM translation_pages WHERE translation_pages.id = translation_pages_votes.translation_pages_id)', []);

      $this->count_deleted += $num_deleted;
    } else {
      database_delete_by('translation_pages', ['id' => $id]);
      database_delete_by('translation_pages_votes', ['translation_pages_id' => $id]);
      $this->count_deleted++;
    }
  }

  //-------------------------------------------------------------------------------------------
  public function AddTranslationDB($lang_id, $table_field, $table_id, $translation, $suggester_id, $comment = '', $set_active = 'n') {
    if ($lang_id == 1) {
      list($table_name, $table_id_name) = $this->GetTableInfo($table_field);
      database_update_by($table_name, [$table_field => $translation], [$table_id_name => $table_id]);
      $this->count_edited++;
    } else {
      $active_translation = database_value("
        SELECT translation
        FROM translation_db
        WHERE table_field = ? AND table_id = ? AND language_id = ? AND is_active = 'y'
      ", [$table_field, $table_id, $lang_id]);

      // Check that the translation text isn't empty or identical to the active one
      if ($translation == '' || $translation == $active_translation) {
        return;
      }

      // Delete old suggestions/translation if needed
      if ($set_active == 'y') {
        database_delete_by('translation_db', ['table_field' => $table_field, 'table_id' => $table_id, 'language_id' => $lang_id]);
        $this->count_activated++;
      }

      database_insert('translation_db', [
        'table_field' => $table_field,
        'table_id' => $table_id,
        'language_id' => $lang_id,
        'translation' => $translation,
        'is_active' => $set_active,
        'suggester_id' => $suggester_id,
        'suggestion_date' => database_now(),
        'comment' => $comment,
      ]);

      $this->count_added++;
    }
  }

  public function SuggestTranslationDB($language_id, $table_field, $table_id, $translation, $suggester_id) {
    // For the same table_field/table_id/language tuple, translation_db can
    // have at most one row with is_active = 'y' and at most one row with
    // is_active = 'n'.

    // Check that the translation text isn't empty or identical to the latest suggestion
    $latest_suggestion = database_value("
      SELECT translation
      FROM translation_db
      WHERE table_field = ? AND table_id = ? AND language_id = ?
      ORDER BY is_active = 'n' DESC
      LIMIT 1
    ", [$table_field, $table_id, $language_id]);

    if ($translation == '' || $translation === $latest_suggestion) {
      return;
    }

    // Delete old suggestion if needed
    database_delete_by(
      'translation_db',
      [
        'table_field' => $table_field,
        'table_id' => $table_id,
        'language_id' => $language_id,
        'is_active' => 'n',
      ]
    );

    database_insert('translation_db', [
      'table_field' => $table_field,
      'table_id' => $table_id,
      'language_id' => $language_id,
      'translation' => $translation,
      'suggester_id' => $suggester_id,
      'suggestion_date' => database_now(),
      'is_active' => 'n',
      'comment' => '',
    ]);
  }

  public function ReviewTranslationDB($id, $action) {
    switch ($action) {
      case 'approve':
        $row = database_get(database_select("SELECT table_field, table_id, language_id FROM translation_db where id = ?", 's', [$id]));
        database_delete_by('translation_db', ['table_field' => $row['table_field'], 'table_id' => $row['table_id'], 'language_id' => $row['language_id'], 'is_active' => 'y']);
        database_update_by('translation_db', ['is_active' => 'y'], ['id' => $id]);
        break;
      case 'reject':
        database_delete_by('translation_db', ['id' => $id]);
        break;
    }
  }

  public function EditTranslationDB($id, $translation, $suggester_id) {
    database_update_by('translation_db', ['translation' => $translation, 'suggestion_date' => database_now()], ['id' => $id]);
    $this->count_edited++;
  }

  public function ActivateTranslationDB($id) {
    // DELETE THE FORMER TRANSLATION IF THERE IS
    $translation_db = database_get(database_select("SELECT * FROM translation_db WHERE id = ?", 's', [$id]));

    $table_field = $translation_db['table_field'];
    $table_id    = $translation_db['table_id'];
    $lang_id     = $translation_db['lang_id'];

    database_delete_by('translation_db', ['table_field' => $table_field, 'table_id' => $table_id, 'language_id' => $lang_id, 'id' => database_not($id)]);
    // SET IS_ACTIVE TO Y FOR THE SPECIFIED ID
    database_update_by('translation_db', ['is_active' => 'y'], ['id' => $id]);
    $this->count_activated++;
  }

  public function DeleteTranslationDB($id) {
    database_delete_by('translation_db', ['id' => $id]);
    $this->count_deleted++;
  }

  public function GetChanges() {
    return $this->count_added . ' added, ' . $this->count_edited . ' edited, ' . $this->count_activated . ' activated, ' . $this->count_deleted . ' deleted';
  }
}

class DateFormatter {
  public $date_format;
  public $timezone;

  public function __construct(?string $date_format, ?string $timezone) {
    if ($date_format !== null && $date_format != '') {
      $this->date_format = $date_format;
    } else {
      $this->date_format = 'Y-m-d';
    }

    $this->timezone = $timezone ?? "UTC";
  }

  public function Format(int|string $timestamp, string $format, bool $apply_timezone = true): string {
    if (is_string($timestamp)) {
      $timestamp = (new DateTime($timestamp))->getTimestamp();
    }

    $datetime = new DateTime();
    $datetime->setTimestamp($timestamp);

    // Some dates are to be treated as absolute, such as dates of birth or monthly statistics.
    // The other dates are converted to the player's timezone.
    if ($apply_timezone) {
      $datetime->setTimezone(new DateTimeZone($this->timezone));
    }

    // We allow players to specify a custom date format in their display settings.
    // The other formats are not customizable.
    switch ($format) {
      case 'month':
        return $datetime->format("F, Y");
      case 'date':
        return $datetime->format($this->date_format);
      case 'datetime':
        return $datetime->format($this->date_format . " H:i");
    }
  }
}

class TranslationCache {
  public string $table;
  public string $field;
  public string $primary_key;
  public int $main_language_id;
  public int $base_language_id;

  public $cache = [];
  public $cache_by_lang = [1 => []];

  public function __construct(string $table, string $field, string $primary_key, int $main_language_id, int $base_language_id) {
    $this->table = $table;
    $this->field = $field;
    $this->primary_key = $primary_key;
    $this->main_language_id = $main_language_id;
    $this->base_language_id = $base_language_id;
  }

  public function Cache($filters) {
    $english = database_filter_by($this->table, $filters);
    $ids = pluck($english, $this->primary_key);

    foreach ($english as $object) {
      $this->cache[$object[$this->primary_key]] = $object[$this->field];
      $this->cache_by_lang[1][$object[$this->primary_key]] = $object[$this->field];
    }

    if ($this->base_language_id > 1) {
      $lang = database_filter_by("translation_games", ['table_field' => $this->field == 'level_name' ? 'chart_name' : $this->field, 'table_id' => $ids, 'language_id' => $this->base_language_id]);
      foreach ($lang as $object) {
        $this->cache[$object['table_id']] = $object['translation'];
        $this->cache_by_lang[$this->main_language_id][$object['table_id']] = $object['translation'];
      }
    }

    if ($this->main_language_id > 1) {
      $lang = database_filter_by("translation_games", ['table_field' => $this->field == 'level_name' ? 'chart_name' : $this->field, 'table_id' => $ids, 'language_id' => $this->main_language_id]);
      foreach ($lang as $object) {
        $this->cache[$object['table_id']] = $object['translation'];
        $this->cache_by_lang[$this->main_language_id][$object['table_id']] = $object['translation'];
      }
    }
  }

  public function CacheLang($language_id, $filters) {
    $english = database_filter_by($this->table, $filters);
    $ids = pluck($english, $this->primary_key);

    $this->cache_by_lang[$language_id] ??= [];

    if ($language_id > 1) {
      foreach ($english as $object) {
        $this->cache_by_lang[$language_id][$object[$this->primary_key]] = '';
      }
    } else {
      foreach ($english as $object) {
        $this->cache_by_lang[$language_id][$object[$this->primary_key]] = $object[$this->field];
      }
    }

    if ($language_id > 1) {
      $lang = database_filter_by("translation_games", ['table_field' => $this->field == 'level_name' ? 'chart_name' : $this->field, 'table_id' => $ids, 'language_id' => $language_id]);
      foreach ($lang as $object) {
        $this->cache_by_lang[$language_id][$object['table_id']] = $object['translation'];
      }
    }
  }

  public function GetName($id) {
    if (!isset($this->cache[$id])) {
      $this->Cache([$this->primary_key => $id]);
    }

    return $this->cache[$id];
  }

  public function GetNameInLanguage($id, $language_id) {
    if (!isset($this->cache_by_lang[$language_id][$id])) {
      $this->CacheLang($language_id, [$this->primary_key => $id]);
    }

    return $this->cache_by_lang[$language_id][$id];
  }
}
