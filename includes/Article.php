<?php
class Article {
  public static function RetrieveHeadline($table_id) {
    global $t;
    return $t->RetrieveNewsHeadline($t->GetSiteLang(), $table_id);
  }

  public static function RetrieveText($table_id) {
    global $t;
    return $t->RetrieveNewsText($t->GetSiteLang(), $table_id);
  }

  //-------------------------------------------------------------------------------------------
  public static function AddArticle($poster, $article_type, $headline, $article_text, $usergroup) {
    global $cs;

    $article_id = database_insert('news', [
      'username' => $poster['username'],
      'article_type' => $article_type,
      'user_id' => $poster['user_id'],
      'headline' => $headline,
      'news_text' => $article_text,
      'news_date' => database_now(),
      'edit_date' => database_now(),
      'edit_user_id' => 0,
    ]);

    $cs->WriteNote(true, "Article posted!");

    if ($p_article_type != 2) {
      database_insert('staff_tasks', [
        'user_id' => $poster['user_id'],
        'tasks_type' => 'news_article_written',
        'task_id' => $article_id,
        'result' => 'News article added',
      ]);
    }

    if ($article_type == 2) {
      $followers = database_filter_by('user_blog_follows', ['follow_id' => $poster_id]);
      $followers = pluck($followers, 'user_id');
      Notification::BlogPosted($article_id)->DeliverToUsers($followers);

      if ($followers) {
        $cs->WriteNote(true, "All of your followers have been notified of your article!");
      }
    }

    return $article_id;
  }

  //-------------------------------------------------------------------------------------------
  public static function DeleteArticle($id) {
    database_delete_by('news', ['news_id' => $id]);
  }

  //-------------------------------------------------------------------------------------------
  public static function EditArticle($poster, $headline, $body, $editor_id, $article_id) {
    database_update_by('news', [
      'username' => $poster['username'],
      'user_id' => $poster['user_id'],
      'headline' => $headline,
      'news_text' => $body,
      'edit_date' => database_now(),
      'edit_user_id' => $editor_id,
    ], [
      'news_id' => $article_id,
    ]);
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatText($text) {
    return prettify($text);
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatNews_User($user_id) {
    $user = database_find_by('users', ['user_id' => $user_id[1]]);
    $username = $user['username'];
    $country_code = $user['country_code'];

    return '<img src="/flags/' . $country_code . '.png" style="width:16px; height:16px; vertical-align:-20%;" /> <a href="/user/' . $user_id[1] . '">' . $username . '</a>';
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatNews_Game($game_id) {
    global $t;
    return '<a href="/game/' . $game_id[1] . '">' . h($t->GetGameName($game_id[1])) . '</a>';
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatNews_GameInfo($game_id) {
    global $t;
    $game = database_fetch_one("
      SELECT COUNT(*) AS num_platforms, GROUP_CONCAT(platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ', ') AS platform_names
      FROM games JOIN game_platforms USING(game_id) JOIN platforms USING(platform_id)
      WHERE games.game_id = ? AND game_platforms.original = 1
    ", [$game_id[1]]);

    $num_platforms = $game['num_platforms'];
    $platform_names = $game['platform_names'];

    return '<a href="/game/' . $game_id[1] . '">' . h($t->GetGameName($game_id[1])) . '</a>
      ' . ($num_platforms == 1 ? 'Platform' : 'Platforms') . ': ' . $platform_names;
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatNews_GameInfoRankbutton($game_id) {
    global $t;
    $game = database_fetch_one("
      SELECT COUNT(*) AS num_platforms, GROUP_CONCAT(platforms.platform_name ORDER BY platforms.platform_name ASC SEPARATOR ', ') AS platform_names
      FROM games JOIN game_platforms USING(game_id) JOIN platforms USING(platform_id)
      WHERE games.game_id = ? AND game_platforms.original = 1
    ", [$game_id[1]]);

    $num_platforms = $game['num_platforms'];
    $platform_names = $game['platform_names'];

    return '<img src="/rankbuttons/1/' . $game_id[1] . '.gif" alt="Rankbutton" />
      <a href="/game/' . $game_id[1] . '">' . h($t->GetGameName($game_id[1])) . '</a>
      ' . ($num_platforms == 1 ? 'Platform' : 'Platforms') . ': ' . $platform_names;
  }

  //-------------------------------------------------------------------------------------------
  public static function FormatNews_GameInfoBoxart($game_id) {
    global $cs;
    $boxart = $cs->GetBoxArts($game_id[1])[0] ?? null;

    if ($boxart) {
      return boxart_image_tag($boxart) . link_to_game(['game_id' => $game_id[1]]);
    } else {
      return link_to_game(['game_id' => $game_id[1]]);
    }
  }

  //--------------------------------------------------------------------------------------------
  public static function ArticlePermissions($user) {
    $display = [1];
    if (Authorization::user_has_access($user, 'Admin'))                 { array_push($display, 3); }
    if (Authorization::user_has_access($user, 'GlobalMod'))             { array_push($display, 4); }
    if (Authorization::user_has_access($user, 'Dev'))                   { array_push($display, 8); }
    if (Authorization::user_has_access($user, 'Designer'))              { array_push($display, 9); }
    if (Authorization::user_has_access($user, 'Translator'))            { array_push($display, 10); }
    if (Authorization::user_has_access($user, 'Newswriter'))            { array_push($display, 11); }
    if (Authorization::user_has_access($user, 'GameMod'))               { array_push($display, 5); }
    if (Authorization::user_has_access($user, ['GameMod', 'ProofMod'])) { array_push($display, 6); }
    if (Authorization::user_has_access($user, 'ProofMod'))              { array_push($display, 7); }

    return $display;
  }

  //--------------------------------------------------------------------------------------------
  public static function TargettedArticleStars($star_name) {
    $red = '../images/Star_Red16.png';
    $blue = '../images/Star_Blue16.png';
    $lightblue = '../images/Star_LightBlue16.png';
    $teal = '/images/Star_LightGreen16.png';
    $green = '/images/Star_Green16.png';
    $violet = '/images/Star_Violet16.png';
    $black = '/images/Star_Black16.png';
    $yellow = '/images/Star_Yellow16.png';
    $orange = '/images/Star_Orange16.png';
    $pink = '/images/Star_Pink16.png';
    $ambassador = '/images/Star_Ambassador16.png';

    $stars = array();
    // Admin and Global Moderator stars
    if ($star_name == '3') array_push($stars, array('src' => $red, 'title' => 'Administrator', 'alt' => 'Administrator'));
    if ($star_name == '4') array_push($stars, array('src' => $blue, 'title' => 'Global Moderator', 'alt' => 'Global Moderator'));
    if ($star_name == '5') array_push($stars, array('src' => $green, 'title' => 'Game Moderator', 'alt' => 'Game Moderator'));
    if ($star_name == '6') {
      array_push($stars, array('src' => $lightblue, 'title' => 'Proof &amp; Game Moderator', 'alt' => 'Proof &amp; Game Moderator'));
      array_push($stars, array('src' => $green, 'title' => 'Game Moderator', 'alt' => 'Game Moderator'));
      array_push($stars, array('src' => $teal, 'title' => 'Proof Moderator', 'alt' => 'Proof Moderator'));
    }
    if ($star_name == '7') array_push($stars, array('src' => $teal, 'title' => 'Proof Moderator', 'alt' => 'Proof Moderator'));
    if ($star_name == '8') array_push($stars, array('src' => $violet, 'title' => 'Developer', 'alt' => 'Developer'));
    if ($star_name == '9') array_push($stars, array('src' => $yellow, 'title' => 'Designer', 'alt' => 'Designer'));
    if ($star_name == '10') array_push($stars, array('src' => $orange, 'title' => 'Translator', 'alt' => 'Translator'));
    if ($star_name == '11') array_push($stars, array('src' => $black, 'title' => 'Newswriter', 'alt' => 'Newswriter'));

    return $stars;
  }

  //--------------------------------------------------------------------------------------------
  public static function RetrieveTargetPermissions($usergroup) {
    switch($usergroup) {
    case('1'):
      $article_permissions = 3;
      break;
    case('2'):
      $article_permissions = 4;
      break;
    case('3'):
      $article_permissions = 5;
      break;
    case('4'):
      $article_permissions = 7;
      break;
    case('5'):
      $article_permissions = 6;
      break;
    case('6'):
      $article_permissions = 8;
      break;
    case('7'):
      $article_permissions = 9;
      break;
    case('8'):
      $article_permissions = 10;
      break;
    case('9'):
      $article_permissions = 11;
      break;
    }
    return $article_permissions;
  }

  //--------------------------------------------------------------------------------------------
  public static function BlogName($blogger_id) {
    global $t;

    $blog_name = database_value("SELECT blog_name FROM user_info WHERE user_id = ?", [$blogger_id]);
    $blogger = database_value("SELECT username FROM users WHERE user_id = ?", [$blogger_id]);
    if (empty($blog_name)) {
      $blog_name = str_replace('[username]', $blogger, $t['news_name']);
    }

    return $blog_name;
  }

  public static function AddComment($comment, $commentable_type, $commentable_id, $reply_id, $user_id) {
    $parent = Comment::get($commentable_type, $reply_id) ?? ['comment_depth' => 0];

    $comment_id = Comment::create($commentable_type, [
      'commentable_id' => $commentable_id,
      'user_id' => $user_id,
      'comment' => $comment,
      'comment_depth' => $parent['comment_depth'] + 1,
      'comment_date' => database_now(),
      'reply_id' => $reply_id,
    ]);

    switch ($commentable_type) {
    case 'news':
      Notification::ArticleComment($comment_id)->DeliverToTarget();

      if ($reply_id) {
        Notification::ArticleCommentReply($comment_id)->DeliverToTarget();
      }
      break;

    case 'game_requests':
      Notification::GameRequestComment($comment_id)->DeliverToTarget();

      if ($reply_id) {
        Notification::GameRequestCommentReply($comment_id)->DeliverToTarget();
      }

      break;

    case 'userpage':
      Notification::UserPageComment($comment_id)->DeliverToTarget();

      if ($reply_id) {
        Notification::UserPageCommentReply($comment_id)->DeliverToTarget();
      }

      break;
    }
  }
}
