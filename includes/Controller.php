<?php

class Controller {
  private $rendered = false;

  public function run() {
    $result = $this->action();

    if (!$this->rendered) {
      $this->render($result);
    }
  }

  public function render($parameters) {
    render_with($this->default_view(), $parameters);
    $this->rendered = true;
  }

  public function default_view() {
    $pieces = explode("\\", $this::class);
    array_shift($pieces);
    $pieces = array_map(function($p) { return strtolower($p); }, $pieces);

    return implode("/", $pieces);
  }
}
