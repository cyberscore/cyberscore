<?php

require_once("src/scoreboards/rebuilders/staff.php");

class ScoreboardCacheRebuilder {
  public static function RebuildAll() {
    self::RebuildMainboard();
    wikilog("mainboard");
    self::RebuildMedalTable();
    wikilog("medals");
    self::RebuildArcadeCache();
    wikilog("arcade");
    self::RebuildSpeedrunCache();
    wikilog("speedrun");
    self::RebuildSolutionCache();
    wikilog("solution");
    self::RebuildChallengeCache();
    wikilog("userchallenge");
    self::RebuildCollectibleCache();
    wikilog("collectible");
    self::RebuildIncrementalCache();
    wikilog("incremental");
    self::RebuildTrophyTable();
    wikilog("trophy");
    self::RebuildProofCache();
    wikilog("proof");
    self::RebuildVideoProofCache();
    wikilog("video proof");
    self::RebuildTotalSubmissionsCache();
    wikilog("total submissions");
    StaffScoreboardRebuilder::rebuild();
    wikilog("staff");
    self::RebuildRainbowScoreboard();
    wikilog("rainbow");
    self::RebuildFirstCache();
    wikilog("first");
  }

  //---------------------------------------------------------------------------
  //            MAINBOARD
  //---------------------------------------------------------------------------
  public static function RebuildMainboard() {
    $scoreboard = database_fetch_all("
      SELECT
        user_id,
        SUM(total_csr) AS total_csr,
        SUM(bonus_csr) AS bonus_csr,
        SUM(total_csr + bonus_csr) AS sum_csr,
        SUM(num_subs) AS num_subs,
        SUM(num_approved) AS num_approved,
        SUM(num_approved_v) AS num_approved_v
      FROM gsb_cache_csp
      GROUP BY user_id
      ORDER BY sum_csr DESC
    ", []);

    $values = [];
    $chart_pos = 1;
    foreach ($scoreboard as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'total_csr' => $user['total_csr'],
        'bonus_csr' => $user['bonus_csr'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];
      $chart_pos++;
    }
    database_delete_all('sb_cache');
    database_insert_all('sb_cache', $values);
  }

  //---------------------------------------------------------------------------
  //            MEDAL TABLE
  //---------------------------------------------------------------------------
  public static function RebuildMedalTable() {
    $medal_table = database_fetch_all("
      SELECT
        user_id,
        SUM(medal_points) AS medal_points,
        SUM(platinum) AS platinum,
        SUM(gold) AS gold,
        SUM(silver) AS silver,
        SUM(bronze) AS bronze,
        SUM(num_subs) AS num_subs,
        SUM(num_approved) AS num_approved,
        SUM(num_approved_v) AS num_approved_v
      FROM gsb_cache_medals
      GROUP BY user_id
      ORDER BY platinum DESC, gold DESC, silver DESC, bronze DESC, medal_points DESC
    ", []);

    $values = [];
    $chart_pos = 1;
    foreach ($medal_table as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'medal_points' => $user['medal_points'],
        'platinum' => $user['platinum'],
        'gold' => $user['gold'],
        'silver' => $user['silver'],
        'bronze' => $user['bronze'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];
      $chart_pos++;
    }
    database_delete_all('sb_cache_standard');
    database_insert_all('sb_cache_standard', $values);
  }

  //---------------------------------------------------------------------------
  //            ARCADE BOARD
  //---------------------------------------------------------------------------
  public static function RebuildArcadeCache() {
    $ar_table = database_fetch_all("
      SELECT
        user_id,
        SUM(tokens) AS score,
        SUM(num_subs) AS num_subs,
        SUM(num_approved) AS num_approved,
        SUM(num_approved_v) AS num_approved_v
      FROM gsb_cache_arcade
      GROUP BY user_id
      ORDER BY score DESC
    ", []);

    $values = [];
    $chart_pos = 1;
    foreach ($ar_table as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'arcade_points' => $user['score'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];
      $chart_pos++;
    }
    database_delete_all('sb_cache_arcade');
    database_insert_all('sb_cache_arcade', $values);
  }

  //---------------------------------------------------------------------------
  //            COLLECTORS CACHE
  //---------------------------------------------------------------------------
  public static function RebuildCollectibleCache() {
    $co_table = database_fetch_all("
      SELECT
        user_id,
        SUM(cyberstars) AS score,
        SUM(num_subs) AS num_subs,
        SUM(num_approved) AS num_approved,
        SUM(num_approved_v) AS num_approved_v
      FROM gsb_cache_collectible
      GROUP BY user_id
      ORDER BY score DESC
    ", []);

    $chart_pos = 1;
    $values = [];
    foreach ($co_table as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'cyberstars' => $user['score'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      $chart_pos++;
    }
    database_delete_all('sb_cache_collectible');
    database_insert_all('sb_cache_collectible', $values);
  }

  public static function RebuildIncrementalCache() {
    $users = database_fetch_all("
      SELECT
        user_id,
        sb_cache_incremental.base_level,
        SUM(gsb_cache_incremental.cxp) AS cxp,
        SUM(gsb_cache_incremental.vs_cxp) AS vs_cxp,
        SUM(gsb_cache_incremental.num_subs) AS num_subs,
        SUM(gsb_cache_incremental.num_approved) AS num_approved,
        SUM(gsb_cache_incremental.num_approved_v) AS num_approved_v
      FROM gsb_cache_incremental
      LEFT JOIN sb_cache_incremental USING (user_id)
      GROUP BY user_id
      ORDER BY SUM(gsb_cache_incremental.vs_cxp) DESC
    ", []);

    $chart_pos = 1;
    $values = [];
    $user_level_changes = [];
    foreach ($users as $user) {
      $new_level = Award::LevelFromExperience($user['cxp']);
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'cxp' => $user['cxp'],
        'vs_cxp' => $user['vs_cxp'],
        'base_level' => $new_level,
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];

      if ($new_level != $user['base_level']) {
        $user_level_changes []= ['user_id' => $user['user_id']];
      }

      $chart_pos++;
    }

    database_delete_all('sb_cache_incremental');
    database_insert_all('sb_cache_incremental', $values);
    database_insert_all('user_level_changes', $user_level_changes);
  }

  //---------------------------------------------------------------------------
  //            SPEEDRUN AWARDS TABLE
  //---------------------------------------------------------------------------
  public static function RebuildSpeedrunCache() {
    $sr_table = database_fetch_all("
      SELECT
        user_id,
        SUM(medal_points) AS speedrun_points,
        SUM(platinum) AS platinum,
        SUM(gold) AS gold,
        SUM(silver) AS silver,
        SUM(bronze) AS bronze,
        SUM(num_subs) AS num_subs,
        SUM(num_approved) AS num_approved,
        SUM(num_approved_v) AS num_approved_v
      FROM gsb_cache_speedrun
      GROUP BY user_id
      ORDER BY speedrun_points DESC, user_id ASC
    ", []);

    $values = [];
    $chart_pos = 1;
    foreach ($sr_table as $user) {

      //Fetch the HMS time for the global scoreboard
      $time = Award::SpeedrunTime($user['speedrun_points']);

      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'speedrun_points' => $user['speedrun_points'],
        'speedrun_time' => $time,
        'platinum' => $user['platinum'],
        'gold' => $user['gold'],
        'silver' => $user['silver'],
        'bronze' => $user['bronze'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];
      $chart_pos++;
    }
    database_delete_all('sb_cache_speedrun');
    database_insert_all('sb_cache_speedrun', $values);
  }

  //---------------------------------------------------------------------------
  //            SOLUTION HUB
  //---------------------------------------------------------------------------
  public static function RebuildSolutionCache() {
    $so_table = database_fetch_all("
      SELECT
        user_id,
        SUM(brain_power) AS score,
        SUM(num_subs) AS num_subs,
        SUM(num_approved) AS num_approved,
        SUM(num_approved_v) AS num_approved_v
      FROM gsb_cache_solution
      GROUP BY user_id
      ORDER BY score DESC
    ", []);

    $values = [];
    $chart_pos = 1;
    foreach ($so_table as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'brain_power' => $user['score'],
        'num_subs' => $user['num_subs'],
        'num_approved' => $user['num_approved'],
        'num_approved_v' => $user['num_approved_v'],
      ];
      $chart_pos++;
    }
    database_delete_all('sb_cache_solution');
    database_insert_all('sb_cache_solution', $values);
  }

  //---------------------------------------------------------------------------
  //            TROPHY TABLE
  //---------------------------------------------------------------------------
  public static function RebuildTrophyTable() {
    $trophy_table = database_fetch_all("
      SELECT
        user_id,
        SUM(platinum) as platinum,
        SUM(gold) AS gold,
        SUM(silver) AS silver,
        SUM(bronze) AS bronze,
        SUM(fourth) AS fourth,
        SUM(fifth) AS fifth,
        SUM(trophy_points) AS trophy_points
      FROM gsb_cache_trophy
      GROUP BY user_id
      ORDER BY trophy_points DESC, platinum DESC, gold DESC, silver DESC, bronze DESC, fourth DESC, fifth DESC
    ", []);

    $records = [];
    $rank = new Aggregates\Rank();
    foreach ($trophy_table as $record) {
      $record["scoreboard_pos"] = $rank->update([
        $record['trophy_points'],
        $record['platinum'],
        $record['gold'],
        $record['silver'],
        $record['bronze'],
        $record['fourth'],
        $record['fifth']
      ]);

      $records[]= $record;
    }

    database_delete_all('sb_cache_trophy');
    database_insert_all("sb_cache_trophy", $records);
  }

  //---------------------------------------------------------------------------
  //            USER CHALLENGE BOARD
  //---------------------------------------------------------------------------
  public static function RebuildChallengeCache() {
      $users = database_fetch_all("
        SELECT
          user_id,
          ROUND(SUM(style_points), 1) AS style_points,
          SUM(num_subs) AS num_subs,
          SUM(num_approved) AS num_approved,
          SUM(num_approved_v) AS num_approved_v
        FROM gsb_cache_userchallenge
        GROUP BY user_id
        ORDER BY style_points DESC
      ", []);

      $records = [];
      $rank = new Aggregates\Rank();
      foreach ($users as $user) {
        $records[]= [
          'user_id' => $user['user_id'],
          'scoreboard_pos' => $rank->update($user["style_points"]),
          'cp' => $user['style_points'],
          'num_subs' => $user['num_subs'],
          'num_approved' => $user['num_approved'],
          'num_approved_v' => $user['num_approved_v'],
        ];
      }

      database_delete_all('sb_cache_challenge');
      database_insert_all("sb_cache_challenge", $records);
  }

  //---------------------------------------------------------------------------
  //            PROOF BOARD
  //---------------------------------------------------------------------------
  public static function RebuildProofCache() {
    $p_table = database_fetch_all("
      SELECT
        user_id,
        SUM(total) AS total,
        SUM(medal) AS medal,
        SUM(arcade) as arcade,
        SUM(speedrun) as speedrun,
        SUM(solution) as solution,
        SUM(challenge) as challenge,
        SUM(collectible) as collectible,
        SUM(incremental) as incremental,
        SUM(unranked) as unranked
      FROM gsb_cache_proof
      GROUP BY user_id
      ORDER BY total DESC
    ", []);

    $values = [];
    $chart_pos = 1;
    foreach ($p_table as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'total' => $user['total'],
        'ranked' => $user['medal'],
        'arcade' => $user['arcade'],
        'speedrun' => $user['speedrun'],
        'solution' => $user['solution'],
        'unranked' => $user['unranked'],
        'challenge' => $user['challenge'],
        'ranked_speedrun' => 0,
      ];
      $chart_pos++;
    }

    database_delete_all('sb_cache_proof');
    database_insert_all('sb_cache_proof', $values);
  }

  //---------------------------------------------------------------------------
  //            VIDEO PROOF BOARD
  //---------------------------------------------------------------------------
  public static function RebuildVideoProofCache() {
    $vp_table = database_fetch_all("
      SELECT
        user_id,
        SUM(total) AS total,
        SUM(medal) AS ranked_score,
        SUM(arcade) as arcade_score,
        SUM(speedrun) as speedrun_score,
        SUM(solution) as solution_score,
        SUM(challenge) as challenge_score,
        SUM(incremental) as incremental_score,
        SUM(collectible) as collectible_score,
        SUM(unranked) as unranked_score
      FROM gsb_cache_vproof
      GROUP BY user_id
      ORDER BY total DESC
    ", []);

    $values = [];
    $chart_pos = 1;
    foreach ($vp_table as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'total' => $user['total'],
        'ranked' => $user['ranked_score'],
        'arcade' => $user['arcade_score'],
        'speedrun' => $user['speedrun_score'],
        'solution' => $user['solution_score'],
        'unranked' => $user['unranked_score'],
        'challenge' => $user['challenge_score'],
        'ranked_speedrun' => 0,
      ];
      $chart_pos++;
    }

    database_delete_all('sb_cache_video_proof');
    database_insert_all('sb_cache_video_proof', $values);
  }

  //---------------------------------------------------------------------------
  //            SUBMISSIONS BOARD
  //---------------------------------------------------------------------------
  public static function RebuildTotalSubmissionsCache() {
    $subs_table = database_fetch_all("
      SELECT
        user_id,
        COUNT(user_id) AS score
      FROM records
      GROUP BY user_id
      ORDER BY score DESC
    ", []);

    $values = array();
    $chart_pos = 1;
    foreach ($subs_table as $user) {
      $values []= [
        'user_id' => $user['user_id'],
        'scoreboard_pos' => $chart_pos,
        'total' => $user['score'],
        'ranked' => 0,
        'arcade' => 0,
        'speedrun' => 0,
        'solution' => 0,
        'unranked' => 0,
        'challenge' => 0,
        'ranked_speedrun' => 0,
      ];
      $chart_pos++;
    }

    database_delete_all('sb_cache_total_subs');
    database_insert_all('sb_cache_total_subs', $values);
  }

  //---------------------------------------------------------------------------
  //            RAINBOW SCOREBOARD
  //---------------------------------------------------------------------------
  public static function RebuildRainbowScoreboard() {
    // Initialise constants
    $number_of_scoreboards = 12;

    // Collect rainbow modifiers
    $multipliers = Scoreboard::RainbowModifiers();

    // Query to get the user info
    $get_sb = database_fetch_all("
      SELECT
        users.user_id,
        starboard.scoreboard_pos as starboard_pos,
        medal.scoreboard_pos as medal_pos,
        trophy.scoreboard_pos as trophy_pos,
        arcade.scoreboard_pos as arcade_pos,
        speedrun.scoreboard_pos as speedrun_pos,
        solution.scoreboard_pos as solution_pos,
        challenge.scoreboard_pos as challenge_pos,
        incremental.scoreboard_pos as incremental_pos,
        collectible.scoreboard_pos as collectible_pos,
        proof.scoreboard_pos as proof_pos,
        video_proof.scoreboard_pos as video_proof_pos,
        submissions.scoreboard_pos as submissions_pos
      FROM users
      LEFT JOIN sb_cache             starboard   USING (user_id)
      LEFT JOIN sb_cache_standard    medal       USING (user_id)
      LEFT JOIN sb_cache_trophy      trophy      USING (user_id)
      LEFT JOIN sb_cache_arcade      arcade      USING (user_id)
      LEFT JOIN sb_cache_speedrun    speedrun    USING (user_id)
      LEFT JOIN sb_cache_solution    solution    USING (user_id)
      LEFT JOIN sb_cache_challenge   challenge   USING (user_id)
      LEFT JOIN sb_cache_incremental incremental USING (user_id)
      LEFT JOIN sb_cache_collectible collectible USING (user_id)
      LEFT JOIN sb_cache_proof       proof       USING (user_id)
      LEFT JOIN sb_cache_video_proof video_proof USING (user_id)
      LEFT JOIN sb_cache_total_subs  submissions USING (user_id)
      WHERE starboard.scoreboard_pos >= 1
      GROUP BY users.user_id
    ", []);

    // Time to build the scoreboard
    foreach ($get_sb as $rainbow) {
      // First, we fetch the positions for the user
      $positions = [
        'starboard'   => $rainbow['starboard_pos'],
        'medal'       => $rainbow['medal_pos'],
        'trophy'      => $rainbow['trophy_pos'],
        'arcade'      => $rainbow['arcade_pos'],
        'speedrun'    => $rainbow['speedrun_pos'],
        'solution'    => $rainbow['solution_pos'],
        'challenge'   => $rainbow['challenge_pos'],
        'incremental' => $rainbow['incremental_pos'],
        'collectible' => $rainbow['collectible_pos'],
        'proof'       => $rainbow['proof_pos'],
        'video_proof' => $rainbow['video_proof_pos'],
        'submissions' => $rainbow['submissions_pos'],
      ];

      // Calculate the actual scores
      // Scoreboard scores
      $scores = [
        'starboard' => Scoreboard::FetchUltimateBoardScore($positions['starboard'], $multipliers['starboard']),
        'medal' => Scoreboard::FetchUltimateBoardScore($positions['medal'], $multipliers['medal']),
        'trophy' => Scoreboard::FetchUltimateBoardScore($positions['trophy'], $multipliers['trophy']),
        'arcade' => Scoreboard::FetchUltimateBoardScore($positions['arcade'], $multipliers['arcade']),
        'speedrun' => Scoreboard::FetchUltimateBoardScore($positions['speedrun'], $multipliers['speedrun']),
        'solution' => Scoreboard::FetchUltimateBoardScore($positions['solution'], $multipliers['solution']),
        'challenge' => Scoreboard::FetchUltimateBoardScore($positions['challenge'], $multipliers['challenge']),
        'incremental' => Scoreboard::FetchUltimateBoardScore($positions['incremental'], $multipliers['incremental']),
        'collectible' => Scoreboard::FetchUltimateBoardScore($positions['collectible'], $multipliers['collectible']),
        'proof' => Scoreboard::FetchUltimateBoardScore($positions['proof'], $multipliers['proof']),
        'video_proof' => Scoreboard::FetchUltimateBoardScore($positions['video_proof'], $multipliers['video_proof']),
        'submissions' => Scoreboard::FetchUltimateBoardScore($positions['submissions'], $multipliers['submissions']),
      ];

      // Base is just the sum of all scoreboard scores
      $scores['base'] = array_sum($scores);

      // Bonus rainbow power is calculated from average scoreboard position
      // Default of 0 if there's an unrepresented scoreboard
      // This can probably be refactored to be more efficient~
      if ($positions['starboard'] == NULL ||
         $positions['medal'] == NULL ||
         $positions['trophy'] == NULL ||
         $positions['arcade'] == NULL ||
         $positions['solution'] == NULL ||
         $positions['speedrun'] == NULL ||
         $positions['challenge'] == NULL ||
         $positions['collectible'] == NULL ||
         $positions['incremental'] == NULL ||
         $positions['proof'] == NULL ||
         $positions['video_proof'] == NULL ||
         $positions['submissions'] == NULL) {
         $scores['bonus'] = 0;
      } else {
        // Otherwise we take the average of a user's scoreboard positions and subtract it from the bonus multiplier
        $modified_bonus = $multipliers['bonus'] - (array_sum($positions) / $number_of_scoreboards / 100);

        // If the bonus has gone below 0, reset it to 0
        if($modified_bonus < 0) { $modified_bonus = 0; }

        // Calculate the bonus score (modifier * base score)
        $scores['bonus'] = $modified_bonus * $scores['base'];
      }

      // Calculate the final score
      $scores['final'] = $scores['base'] + $scores['bonus'];

      // Insert values into array
      $scoreboard []= [
        'user_id' => $rainbow['user_id'],

        'base_rainbow_power'  => $scores['base'],
        'bonus_rainbow_power' => $scores['bonus'],
        'rainbow_power'       => $scores['final'],

        'starboard'               => $scores['starboard'],
        'medal_table'             => $scores['medal'],
        'trophy_table'            => $scores['trophy'],
        'arcade_board'            => $scores['arcade'],
        'speedrun_table'          => $scores['speedrun'],
        'solution_hub'            => $scores['solution'],
        'challenge_board'         => $scores['challenge'],
        'proof_board'             => $scores['proof'],
        'video_proof_board'       => $scores['video_proof'],
        'total_submissions_board' => $scores['submissions'],
        'collectors_cache'        => $scores['collectible'],
        'experience_ranking'      => $scores['incremental'],

        'starboard_pos'         => $positions['starboard'],
        'medal_pos'             => $positions['medal'],
        'trophy_pos'            => $positions['trophy'],
        'arcade_pos'            => $positions['arcade'],
        'speedrun_pos'          => $positions['speedrun'],
        'solution_pos'          => $positions['solution'],
        'challenge_pos'         => $positions['challenge'],
        'incremental_pos'       => $positions['incremental'],
        'collectible_pos'       => $positions['collectible'],
        'proof_pos'             => $positions['proof'],
        'video_proof_pos'       => $positions['video_proof'],
        'total_submissions_pos' => $positions['submissions'],
      ];
    }

    // Sort the scoreboard by total rainbow_power DESC
    uasort($scoreboard, function($a, $b) {
      return $b['rainbow_power'] <=> $a['rainbow_power'];
    });

    // Set scoreboard_position
    $this_position = 1;
    foreach($scoreboard as $entry) {
      $entry['scoreboard_pos'] = $this_position;
      $final_board []= $entry;
      $this_position++;
    }

    // Build the cache
    database_delete_all('sb_cache_rainbow');
    database_insert_all('sb_cache_rainbow', $final_board);
  }

  public static function RebuildFirstCache() {
    database_delete_all('sb_cache_first');
    database_insert_all('sb_cache_first', database_fetch_all('
      SELECT user_id, year, month, SUM(points) AS points
      FROM gsb_cache_first
      GROUP BY user_id, year, month
      ORDER BY year ASC, month ASC, points DESC
    ', []));
  }
}
