<?php

class Modifiers {
  /**
   * Check the README section "Chart flags, CSP modifiers, and chart flooder".
   *
   * The awards and CSP a chart yields can be modified by chart flags and CSP
   * modifiers. The CSP can also be modified if the game has a large number of
   * charts for its chart flag. This is called the `chart_flooder` modifier.
   *
   * Most modifiers return a percentage that should be added or removed, and
   * they're additive, not multiplicative. For example, if you have the speedun
   * modifier (+30%) and the premium_dlc modifier (-20%), your final modifier
   * will be +10%. There are a few modifiers that always set the final modifier
   * to zero independently of other modifiers, though. These are implemented by
   * having them return -∞ (-INF).
   */

  static $chart_flags = [
    ['standard'   , +0.00],
    ['speedrun'   , +0.25],
    ['solution'   , -0.10],
    ['arcade'     , -0.00],
    ['unranked'   , -INF],
    ['challenge'  , -0.10],
    ['collectible', +0.00],
    ['incremental', -0.15],
  ];

  // [identifier, modifier, is_arcade_modifier]
  static $csp_modifiers = [
    // arcade modifiers
    ['gameplay_differences_device'                              , -0.10, true],
    ['gameplay_differences_game_version'                        , -0.10, true],
    ['premium_upgrades_and_consumables_earnable'                , -0.05, true],
    ['premium_upgrades_and_consumables_premium_only'            , -0.15, true],
    ['premium_upgrades_and_consumables_inaccessible'            , -0.20, true],
    ['premium_upgrades_and_consumables_conditionally_accessible', -0.05, true],
    ['accessibility_issues_inaccessible'                        , -0.20, true],
    ['accessibility_issues_conditionally_accessible'            , -0.05, true],
    ['accessibility_issues_geographical'                        , -0.10, true],

    // other modifiers
    ['full_game',    +0.25, false],
    ['part_game',    +0.10, false],
    ['body_of_work', +0.00, false],

    // deprecated modifiers
    //['regional_differences'                                     , -0.00],
    //['significant_regional_differences'                         , -0.00],
    //['significant_device_differences'                           , -0.00],
    //['patience'                                                 , -0.00],
    //['computer_generated'                                       , -0.00],
    //['premium_dlc'                                              , -0.00],
  ];

  // Returns three things:
  // - the final modifier (multiplier)
  // - a list of modifiers (with identifier and value keys)
  // - the input $modifiers
  static function ChartModifiers($modifiers) {
    $list = self::ModifierList($modifiers);

    return [
      Modifiers::ChartFinalModifier($list),
      $list,
      $modifiers,
    ];
  }

  static function FetchChartModifiers($chart_id) {
    $modifier = database_find_by("chart_modifiers2", ['level_id' => $chart_id]);

    return self::ChartModifiers($modifier);
  }


  static function ModifierList($modifiers) {
    $list = [];

    if ($modifiers['chart_flooder'] != 0) {
      $list []= ['identifier' => 'chart_flooder', 'value' => $modifiers['chart_flooder']];
    }

    foreach (self::$chart_flags as [$identifier, $multiplier]) {
      if ($modifiers[$identifier]) {
        $list []= ['identifier' => $identifier, 'value' => $multiplier];
      }
    }

    foreach (self::$csp_modifiers as [$identifier, $multiplier]) {
      if ($modifiers[$identifier]) {
        $list []= ['identifier' => $identifier, 'value' => $multiplier];
      }
    }

    return $list;
  }

  // Input: the list of modifiers from ChartModifiers
  static function ChartFinalModifier($modifiers) {
    $total = 1;

    foreach ($modifiers as $modifier) {
      $total += $modifier['value'];
    }

    return max(0, $total);
  }

  //Provide a nice consistent CSP modifier or chart-flag string for front-end
  static function csp_modifier_label($identifier) {
    switch ($identifier) {
    // Chart-flags
    case 'standard':     return "Standard";
    case 'speedrun':     return "Speedrun";
    case 'solution':     return "Solution";
    case 'arcade':       return "Arcade";
    case 'unranked':     return "Unranked";
    case 'challenge':    return "User Challenge";
    case 'collectible':  return "Collectible";
    case 'incremental':  return "Incremental";

    // Chart flooder
    case 'chart_flooder': return "Chart flooder (250+ charts)";

    default:
      $modifier = array_values(array_filter(self::$csp_modifiers, fn($m) => $m[0] == $identifier))[0];

      if ($modifier[2]) {
        $category = self::CSPModifierCategoryLabel($identifier);
        $subcategory = self::CSPModifierSubcategory($identifier);
        return "{$category} ({$subcategory})";
      } else {
        return self::CSPModifierSubcategory($identifier);
      }
    }
  }

  static function CSPModifierCategory($identifier) {
    switch ($identifier) {
    case 'gameplay_differences_device':                               return "gameplay";
    case 'gameplay_differences_game_version':                         return "gameplay";
    case 'premium_upgrades_and_consumables_earnable':                 return "premium";
    case 'premium_upgrades_and_consumables_premium_only':             return "premium";
    case 'premium_upgrades_and_consumables_inaccessible':             return "premium";
    case 'premium_upgrades_and_consumables_conditionally_accessible': return "premium";
    case 'accessibility_issues_inaccessible':                         return "accessibility";
    case 'accessibility_issues_conditionally_accessible':             return "accessibility";
    case 'accessibility_issues_geographical':                         return "accessibility";

    case 'full_game':    return "secondary";
    case 'part_game':    return "secondary";
    case 'body_of_work': return "secondary";
    }
  }

  static function CSPModifierCategoryLabel($identifier) {
    $identifier = self::CSPModifierCategory($identifier);

    switch ($identifier) {
    case 'gameplay':      return "Gameplay differences";
    case 'premium':       return "Premium upgrades & consumables";
    case 'accessibility': return "Accessibility issues";
    case 'secondary':     return "Secondary Modifier";
    }
  }

  static function CSPModifierSubcategory($identifier) {
    switch ($identifier) {
    case 'gameplay_differences_device':                               return "Device";
    case 'gameplay_differences_game_version':                         return "Game version";
    case 'premium_upgrades_and_consumables_earnable':                 return "Earnable";
    case 'premium_upgrades_and_consumables_premium_only':             return "Premium-only";
    case 'premium_upgrades_and_consumables_inaccessible':             return "Inaccessible";
    case 'premium_upgrades_and_consumables_conditionally_accessible': return "Conditionally accessible";
    case 'accessibility_issues_inaccessible':                         return "Inaccessible";
    case 'accessibility_issues_conditionally_accessible':             return "Conditionally accessible";
    case 'accessibility_issues_geographical':                         return "Geographical";

    case 'full_game': return "Full-game";
    case 'part_game': return "Part-game";
    case 'body_of_work': return "Body of work";
    }
  }

  static function CSPModifierCategories() {
    $categories = [];
    foreach (self::$csp_modifiers as [$modifier,]) {
      $categories []= self::CSPModifierCategoryLabel($modifier);
    }

    return array_unique($categories);
  }

  static function CSPModifiersForCategory($category) {
    $modifiers = [];
    foreach (self::$csp_modifiers as [$modifier,]) {
      if (self::CSPModifierCategoryLabel($modifier) == $category) {
        $modifiers []= $modifier;
      }
    }

    return $modifiers;
  }


  //Provide a nice consistent CSP modifier value for front-end
  static function format_csp_modifier($value) {
    if ($value == -INF) {
      return "x0%";
    } else {
      return ($value < 0 ? "-" : "+") . abs(round($value * 100)) . "%";
    }
  }

  static function FetchChartFlagColouration($chart_id) {
    $chart_flags = database_find_by("chart_modifiers2", ['level_id' => $chart_id]);

    return self::ChartFlagColouration($chart_flags);
  }

  static $priorities = [
    'challenge',
    'speedrun',
    'solution',
    'incremental',
    'collectible',
    'arcade',
    'unranked',
    'standard',
  ];

  // Returns the chart colouration requirements based on a hierarchy
  static function ChartFlagColouration($chart_flags) {
    foreach (self::$priorities as $priority) {
      if ($chart_flags[$priority]) return $priority;
    }
  }

  // Returns the group colouration
  public static function GroupColouration($ranked) {
    switch ($ranked) {
      case 0: return "standard";
      case 7: return "arcade";
      case 1: return "speedrun";
      case 4: return "challenge";
      case 2: return "solution";
      case 3: return "unranked";
      case 5: return "collectible";
      case 6: return "incremental";
      default: return "default";
    }
  }

  //---------------------------------------------------------------------------
  static function SpeedrunBonus($position, $platinum_check) {
    if ($platinum_check == TRUE) { $first_bonus = 0.50; }
    else { $first_bonus = 0.30; }

    switch($position) {
    case 1: return $first_bonus;
    case 2: return +0.20;
    case 3: return +0.10;
    default: return 0;
    }
  }

  //---------------------------------------------------------------------------
  static function UpdateChartFlooder($game_id) {
    $charts = database_fetch_all("SELECT chart_modifiers2.* FROM chart_modifiers2 JOIN levels USING(level_id) WHERE game_id = ? AND not unranked", [$game_id]);

    $counts = [
      'standard' => array_count($charts, fn($c) => $c['standard']),
      'speedrun' => array_count($charts, fn($c) => $c['speedrun']),
      'solution' => array_count($charts, fn($c) => $c['solution']),
      'challenge' => array_count($charts, fn($c) => $c['challenge']),
      'collectible' => array_count($charts, fn($c) => $c['collectible']),
      'incremental' => array_count($charts, fn($c) => $c['incremental']),
      'arcade' => array_count($charts, fn($c) => $c['arcade']),
      'unranked' => array_count($charts, fn($c) => $c['unranked']),
    ];

    $flood_modifier = [];

    foreach (self::$chart_flags as [$chart_flag,]) {
      $flood_modifier[$chart_flag] = self::FloodModifier($counts[$chart_flag]);
    }

    $keys = [];
    $fields = [];
    foreach ($charts as $chart) {
      $flooder = min(array_map(fn($chart_flag) => $chart[$chart_flag[0]] ? $flood_modifier[$chart_flag[0]] : 0.0, self::$chart_flags));

      $keys []= $chart['level_id'];
      $fields []= ['chart_flooder' => $flooder];
    }

    database_update_all('chart_modifiers2', 'level_id', $keys, $fields);
  }

  static function FloodModifier($num_charts) {
    if ($num_charts < 250) {
      return 0;
    } else {
      $chart_flood_modifier = round(1.64367 * pow($num_charts, -0.09), 4);

      return -(1 - $chart_flood_modifier);
    }
  }
}
