<?php

namespace Aggregates;

class Maximum {
  public $max = 0;

  public function update($value) {
    $this->max = max($this->max, $value);
    return $this->max;
  }

  // What percentage of maximum is the given value?
  public function percentage($value) {
    return $this->max == 0 ? 0.0 : $value / $this->max;
  }
}
