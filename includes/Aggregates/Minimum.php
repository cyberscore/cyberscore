<?php

namespace Aggregates;

class Minimum {
  public $min = null;

  public function update($value) {
    if ($this->min === null) {
      $this->min = $value;
    } else {
      $this->min = min($this->min, $value);
    }
    return $this->min;
  }
}
