<?php

namespace Aggregates;

class Rank {
  public $rank = 0;
  public $position = 0;
  public $previous_value = NULL;

  public function update($value) {
    if ($this->position > 0 && $this->previous_value == $value) {
      $this->position++;
    } else {
      $this->position++;
      $this->rank = $this->position;
      $this->previous_value = $value;
    }

    return $this->rank;
  }
}
