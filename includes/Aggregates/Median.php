<?php

namespace Aggregates;

class Median {
  // This assumes $data is already sorted
  static function Calculate($data) {
    // If it's an empty array, return FALSE.
    if (empty($data)) {
      return false;
    }

    // Count how many elements are in the array.
    $num = count($data);

    // Determine the middle value of the array.
    $middleVal = floor(($num - 1) / 2);

    if ($num % 2) {
      // If the size of the array is an odd number, then the middle value is the
      // median.
      return $data[$middleVal];
    } else {
      // If the size of the array is an even number, then we have to get the two
      // middle values and get their average

      // The $middleVal var will be the low end of the middle
      $lowMid = $data[$middleVal];
      $highMid = $data[$middleVal + 1];

      // Return the average of the low and high.
      return (($lowMid + $highMid) / 2);
    }
  }
}
