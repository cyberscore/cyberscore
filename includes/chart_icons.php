<?php

// camel_to_kebab("ProofGemstones") => "proof-gemstones'
// camel_to_kebab("PLArceus")       => "p-larceus" (bug, should be "p-l-arceus")
// camel_to_kebab("Test")           => "test"
function camel_to_kebab($text) {
  return strtolower(preg_replace("/(.)([A-Z])/", "$1-$2", $text));
}

// kebab_to_camel("proof-gemstones") => "ProofGemstones"
// kebab_to_camel("pokemon")         => "Pokemon"
// kebab_to_camel("p-l-arceus")      => "PLArceus"
function kebab_to_camel($text) {
  return preg_replace_callback("/(^|-)([a-z])/", fn($m) => strtoupper($m[2]), $text);
}

class ChartIcon {
  public static function selection($user) {
    $flairs = [];

    $value = static::value($user);
    foreach (static::$levels as $i => $level) {
      $flairs []= [
        'available' => static::available($value, $level),
        'image' => "/images/chart-icons/" . camel_to_kebab(static::class) . "/$level.png",
        'title' => static::title($user['username'], $level, $level),
        'level' => $level,
        'id' => camel_to_kebab(static::class) . "@$level",
      ];
    }

    return $flairs;
  }

  public static function available($value, $level) {
    return $value >= $level;
  }

  public static function get($user) {
    if (!$user['preferred_chart_icon']) { return NULL; }

    [$set, $level] = explode("@", $user['preferred_chart_icon']);
    return [
      'available' => true,
      'image' => "/images/chart-icons/$set/$level.png",
      'title' => kebab_to_camel($set)::title($user['username'], $level, $level),
      'level' => $level,
      'id' => $set,
    ];
  }

  public static function all_selections($user) {
    return [
      ['Number of approved records (Proof gemstones)', ProofGemstones::selection($user)],      
      ['Number of standard proofs', StandardGemstones::selection($user)],      
      ['Number of arcade proofs', ArcadeGemstones::selection($user)],      
      ['Number of speedrun proofs', SpeedrunGemstones::selection($user)],    
      ['Number of user challenge proofs', ChallengeGemstones::selection($user)],      
      ['Number of solution proofs', SolutionGemstones::selection($user)],        
      ['Number of collectible proofs', CollectibleGemstones::selection($user)],      
      ['Number of incremental proofs', IncrementalGemstones::selection($user)],
      ['Number of video approved records (Video proof gemstones)', VproofGemstones::selection($user)],
      ['Number of standard video proofs', StandardVGemstones::selection($user)],      
      ['Number of arcade video proofs', ArcadeVGemstones::selection($user)],      
      ['Number of speedrun video proofs', SpeedrunVGemstones::selection($user)],    
      ['Number of user challenge video proofs', ChallengeVGemstones::selection($user)],      
      ['Number of solution video proofs', SolutionVGemstones::selection($user)],        
      ['Number of collectible video proofs', CollectibleVGemstones::selection($user)],    
      ['Starboard leadership (Rosettes)', StarboardRosettes::selection($user)],
      ['Trophy scoreboard leadership', TrophyRosettes::selection($user)],
      ['Rainbow scoreboard leadership', RainbowRosettes::selection($user)],
      ['Medal table leadership', StandardRosettes::selection($user)],
      ['Arcade board leadership', ArcadeRosettes::selection($user)],
      ['Speedrun scoreboard leadership', SpeedrunRosettes::selection($user)],
      ['User challenge board leadership', ChallengeRosettes::selection($user)],
      ['Solution hub leadership', SolutionRosettes::selection($user)],
      ["Collector's Cache leadership", CollectibleRosettes::selection($user)],
      ['Experience Table leadership', IncrementalRosettes::selection($user)],      
      ['Proof scoreboard leadership', ProofRosettes::selection($user)],
      ['Video Proof scoreboard leadership', VproofRosettes::selection($user)],
      ['Submission scoreboard leadership', SubmissionsRosettes::selection($user)],
      ['Number of submissions (Submitter sashes)', TotalSubmissionSashes::selection($user)],
      ['Number of standard submissions', StandardSubmissionSashes::selection($user)],
      ['Number of arcade submissions', ArcadeSubmissionSashes::selection($user)],
      ['Number of speedrun submissions', SpeedrunSubmissionSashes::selection($user)],
      ['Number of user challenge submissions', ChallengeSubmissionSashes::selection($user)],
      ['Number of solution submissions', SolutionSubmissionSashes::selection($user)],
      ['Number of collectible submissions', CollectibleSubmissionSashes::selection($user)],
      ['Number of incremental submissions', IncrementalSubmissionSashes::selection($user)],
      ['Percentage of approved records', ProofPercentGemstones::selection($user)],
      ['Number of referrals (Ambassador trophies)', AmbassadorTrophies::selection($user)],
    ];
  }
}

class ProofGemstones extends ChartIcon {
  public static $levels = [25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000];

  public static function title($username, $tier, $value) {
    $title = t('charticons_proof_gemstones_title', ['username' => $username, 'value' => $tier]);

    if ($tier >= 2500) {
      $title .= " " . t('charticons_exclamation_three');
    }

    return $title;
  }

  public static function value($user) {
    return database_single_value("SELECT total FROM sb_cache_proof WHERE user_id = ?", 's', [$user['user_id']]) ?? 0;
  }
}

class VproofGemstones extends ChartIcon {
  public static $levels = [25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000];

  public static function title($username, $tier, $value) {
    $title = t('charticons_video_proof_gemstones_title', ['username' => $username, 'value' => $tier]);

    if ($tier >= 1000) {
      $title .= " " . t('charticons_exclamation_three');
    }

    return $title;
  }

  public static function value($user) {
    return database_single_value("SELECT total FROM sb_cache_video_proof WHERE user_id = ?", 's', [$user['user_id']]) ?? 0;
  }
}

class ProofPercentGemstones extends ChartIcon {
  public static $levels = [25, 50, 75, 100];

  public static function title($username, $tier, $value) {
    $title = t('charticons_proof_percent_gemstones_title', ['username' => $username, 'value' => $tier]);

    if ($tier >= 100) {
      $title .= " " . t('charticons_exclamation_four');
    }

    return $title;
  }

  public static function value($user) {
    $proof = database_value("SELECT total FROM sb_cache_proof WHERE user_id = ?", [$user['user_id']]) ?? 0;
    $subs = database_value("SELECT total FROM sb_cache_total_subs WHERE user_id = ?", [$user['user_id']]) ?? 0;

    if ($subs == 0) {
      return 0;
    } else {
      return 100 * $proof / $subs;
    }
  }
}

class AmbassadorTrophies extends ChartIcon {
  static $levels = [2, 5, 10, 25];

  public static function title($username, $tier, $value) {
    return t('charticons_ambassador_trophies_title', ['username' => $username, 'value' => $tier]);
  }

  public static function value($user) {
    return database_single_value("SELECT COUNT(*) FROM referrals WHERE referrer_id = ? AND confirmed = 1", 's', [$user['user_id']]);
  }
}

class TotalSubmissionSashes extends ChartIcon {
  static $levels = [25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000];

  public static function title($username, $tier, $value) {
    $title = t('charticons_total_submissions_title', ['username' => $username, 'value' => $tier]);

    if ($tier >= 10000) $title .= " " . t('charticons_exclamation_one');
    if ($tier == 1000) $title .= " " . t('charticons_exclamation_two');

    return $title;
  }

  public static function value($user) {
    return database_single_value("SELECT total FROM sb_cache_total_subs WHERE user_id = ?", 's', [$user['user_id']]) ?? 0;
  }
}

class ChartTypeProofGemstones extends ChartIcon {
  static $levels = [25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000];

  public static function title($username, $tier, $value) {
    $title = t('charticons_chart_type_proof_title', [
      'username' => $username,
      'value' => $tier,
      'type' => static::type_name(),
    ]);

    if ($tier >= 5000) $title .= " " . t('charticons_exclamation_one');
    if ($tier == 1000) $title .= " " . t('charticons_exclamation_two');

    return $title;
  }

  public static function value($user) {
    return database_single_value("SELECT num_approved FROM " . static::$table . " WHERE user_id = ?", 's', [$user['user_id']]) ?? 0;
  }
}

class ChartTypeVideoProofGemstones extends ChartIcon {
  static $levels = [25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000];

  public static function title($username, $tier, $value) {
    $title = t('charticons_chart_type_vproof_title', [
      'username' => $username,
      'value' => $tier,
      'type' => static::type_name(),
    ]);

    if ($tier >= 2500) $title .= " " . t('charticons_exclamation_one');
    if ($tier == 1000) $title .= " " . t('charticons_exclamation_two');

    return $title;
  }

  public static function value($user) {
    return database_single_value("SELECT num_approved_v FROM " . static::$table . " WHERE user_id = ?", 's', [$user['user_id']]) ?? 0;
  }
}

class ChartTypeSubmissionSashes extends ChartIcon {
  static $levels = [25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000];

  public static function title($username, $tier, $value) {
    $title = t('charticons_chart_type_submissions_title', [
      'username' => $username,
      'value' => $tier,
      'type' => static::type_name(),
    ]);

    if ($tier >= 5000) $title .= " " . t('charticons_exclamation_one');
    if ($tier == 1000) $title .= " " . t('charticons_exclamation_two');

    return $title;
  }

  public static function value($user) {
    return database_single_value("SELECT num_subs FROM " . static::$table . " WHERE user_id = ?", 's', [$user['user_id']]) ?? 0;
  }
}

class GlobalScoreboardRosettes extends ChartIcon {
  static $levels = [100, 10, 3, 2, 1];

  public static function available($value, $level) {
    return $value <= $level;
  }

  public static function title($username, $tier, $value) {
    return t('charticons_chart_type_rosettes_title', [
      'username' => $username,
      'place' => nth($tier), // TODO: nth i18n
      'board' => static::board_name(),
    ]);
  }

  public static function value($user) {
    return database_single_value("SELECT scoreboard_pos FROM " . static::$table . " WHERE user_id = ?", 's', [$user['user_id']]) ?? 1000;
  }
}

class StandardGemstones extends ChartTypeProofGemstones {
  static $table = 'sb_cache_standard';
  static function type_name() { return "Standard"; }
}

class ArcadeGemstones extends ChartTypeProofGemstones {
  static $table = 'sb_cache_arcade';
  static function type_name() { return "Arcade"; }
}

class SpeedrunGemstones extends ChartTypeProofGemstones {
  static $table = 'sb_cache_speedrun';
  static function type_name() { return "Speedrun"; }
}

class SolutionGemstones extends ChartTypeProofGemstones {
  static $table = 'sb_cache_solution';
  static function type_name() { return "Solution"; }
}

class ChallengeGemstones extends ChartTypeProofGemstones {
  static $table = 'sb_cache_challenge';
  static function type_name() { return "User Challenge"; }
}

class CollectibleGemstones extends ChartTypeProofGemstones {
  static $table = 'sb_cache_collectible';
  static function type_name() { return "Collectible"; }
}

class IncrementalGemstones extends ChartTypeProofGemstones {
  static $table = 'sb_cache_incremental';
  static function type_name() { return "Incremental"; }
}

class StandardVGemstones extends ChartTypeVideoProofGemstones {
  static $table = 'sb_cache_standard';
  static function type_name() { return "Standard"; }
}

class ArcadeVGemstones extends ChartTypeVideoProofGemstones {
  static $table = 'sb_cache_arcade';
  static function type_name() { return "Arcade"; }
}

class SpeedrunVGemstones extends ChartTypeVideoProofGemstones {
  static $table = 'sb_cache_speedrun';
  static function type_name() { return "Speedrun"; }
}

class SolutionVGemstones extends ChartTypeVideoProofGemstones {
  static $table = 'sb_cache_solution';
  static function type_name() { return "Solution"; }
}

class ChallengeVGemstones extends ChartTypeVideoProofGemstones {
  static $table = 'sb_cache_challenge';
  static function type_name() { return "User Challenge"; }
}

class CollectibleVGemstones extends ChartTypeVideoProofGemstones {
  static $table = 'sb_cache_collectible';
  static function type_name() { return "Collectible"; }
}

class StandardSubmissionSashes extends ChartTypeSubmissionSashes {
  static $table = 'sb_cache_standard';
  static function type_name() { return "Standard"; }
}

class ArcadeSubmissionSashes extends ChartTypeSubmissionSashes {
  static $table = 'sb_cache_arcade';
  static function type_name() { return "Arcade"; }
}

class SpeedrunSubmissionSashes extends ChartTypeSubmissionSashes {
  static $table = 'sb_cache_speedrun';
  static function type_name() { return "Speedrun"; }
}

class SolutionSubmissionSashes extends ChartTypeSubmissionSashes {
  static $table = 'sb_cache_solution';
  static function type_name() { return "Solution"; }
}

class ChallengeSubmissionSashes extends ChartTypeSubmissionSashes {
  static $table = 'sb_cache_challenge';
  static function type_name() { return "User Challenge"; }
}

class CollectibleSubmissionSashes extends ChartTypeSubmissionSashes {
  static $table = 'sb_cache_collectible';
  static function type_name() { return "Collectible"; }
}

class IncrementalSubmissionSashes extends ChartTypeSubmissionSashes {
  static $table = 'sb_cache_incremental';
  static function type_name() { return "Incremental"; }
}

class StarboardRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache";
  static function board_name() { return "Starboard"; }
}

class StandardRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_standard";
  static function board_name() { return "Medal table"; }
}

class ArcadeRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_arcade";
  static function board_name() { return "Arcade board"; }
}

class SpeedrunRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_speedrun";
  static function board_name() { return "Speedrun board"; }
}

class ChallengeRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_challenge";
  static function board_name() { return "User Challenge board"; }
}

class SolutionRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_solution";
  static function board_name() { return "Solution hub"; }
}

class CollectibleRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_collectible";
  static function board_name() { return "Collector's Cache"; }
}

class IncrementalRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_incremental";
  static function board_name() { return "Experience Table"; }
}

class TrophyRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_trophy";
  static function board_name() { return "Trophy board"; }
}

class RainbowRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_rainbow";
  static function board_name() { return "Rainbow scoreboard"; }
}

class ProofRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_proof";
  static function board_name() { return "Proof board"; }
}

class VproofRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_video_proof";
  static function board_name() { return "Video proof board"; }
}

class SubmissionsRosettes extends GlobalScoreboardRosettes {
  static $table = "sb_cache_total_subs";
  static function board_name() { return "Total submissions board"; }
}
